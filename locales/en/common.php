<?php
$locales['CMbFieldSpec.adeli'] = 'Adeli number';
$locales['CMbFieldSpec.alphaAndNum'] = 'Alphanumeric';
$locales['CMbFieldSpec.autocomplete'] = 'Autocompleted';
$locales['CMbFieldSpec.byteUnit'] = 'Byte unit';
$locales['CMbFieldSpec.canonical'] = 'Canonical';
$locales['CMbFieldSpec.cascade'] = 'Cascade deletion';
$locales['CMbFieldSpec.ccam'] = 'CCAM code';
$locales['CMbFieldSpec.cim'] = 'CIM code';
$locales['CMbFieldSpec.class'] = 'Class';
$locales['CMbFieldSpec.confidential'] = 'Confidential';
$locales['CMbFieldSpec.control'] = 'Check key';
$locales['CMbFieldSpec.decimals'] = 'Decimals';
$locales['CMbFieldSpec.default'] = 'Valeur par défaut';
$locales['CMbFieldSpec.delimiter'] = 'Délimiteur';
$locales['CMbFieldSpec.dependsOn'] = 'Depends on';
$locales['CMbFieldSpec.format'] = 'Format';
$locales['CMbFieldSpec.helped'] = 'Helped';
$locales['CMbFieldSpec.insee'] = 'Secu number';
$locales['CMbFieldSpec.length'] = 'Fixed length';
$locales['CMbFieldSpec.list'] = 'List';
$locales['CMbFieldSpec.mask'] = 'Mask';
$locales['CMbFieldSpec.max'] = 'Max value';
$locales['CMbFieldSpec.maxLength'] = 'Max length';
$locales['CMbFieldSpec.meta'] = 'Class field';
$locales['CMbFieldSpec.min'] = 'Min value';
$locales['CMbFieldSpec.minLength'] = 'Longueur min';
$locales['CMbFieldSpec.moreEquals'] = 'Plus grand ou égal à';
$locales['CMbFieldSpec.moreThan'] = 'Plus grand que';
$locales['CMbFieldSpec.notContaining'] = 'Ne contient pas';
$locales['CMbFieldSpec.notNear'] = 'Pas proche de';
$locales['CMbFieldSpec.notNull'] = 'Obligatoire';
$locales['CMbFieldSpec.order_number'] = 'N° de commande';
$locales['CMbFieldSpec.pattern'] = 'Motif';
$locales['CMbFieldSpec.perm'] = 'Perm. nécessaire';
$locales['CMbFieldSpec.pos'] = 'Positif';
$locales['CMbFieldSpec.progressive'] = 'Progressif';
$locales['CMbFieldSpec.protected'] = 'Protégé';
$locales['CMbFieldSpec.purgeable'] = 'Purgeable';
$locales['CMbFieldSpec.reported'] = 'Reporté';
$locales['CMbFieldSpec.revealable'] = 'Révélable';
$locales['CMbFieldSpec.rib'] = 'RIB';
$locales['CMbFieldSpec.sameAs'] = 'Egal à';
$locales['CMbFieldSpec.seekable'] = 'Recherchable';
$locales['CMbFieldSpec.show'] = 'Afficher';
$locales['CMbFieldSpec.siret'] = 'Siret';
$locales['CMbFieldSpec.type.birthDate'] = 'Date de naissance';
$locales['CMbFieldSpec.type.bool'] = 'Booléen (oui/non)';
$locales['CMbFieldSpec.type.code'] = 'Code';
$locales['CMbFieldSpec.type.currency'] = 'Valeur monétaire';
$locales['CMbFieldSpec.type.date'] = 'Date';
$locales['CMbFieldSpec.type.dateTime'] = 'Date/heure';
$locales['CMbFieldSpec.type.email'] = 'Adresse e-mail';
$locales['CMbFieldSpec.type.enum'] = 'Choix unique';
$locales['CMbFieldSpec.type.er7'] = 'Source ER7';
$locales['CMbFieldSpec.type.float'] = 'Nombre à virgule';
$locales['CMbFieldSpec.type.html'] = 'Source HTML';
$locales['CMbFieldSpec.type.ipAddress'] = 'Adresse IP';
$locales['CMbFieldSpec.type.num'] = 'Nombre entier';
$locales['CMbFieldSpec.type.numchar'] = 'Caractères num.';
$locales['CMbFieldSpec.type.password'] = 'Mot de passe';
$locales['CMbFieldSpec.type.pct'] = 'Pourcentage';
$locales['CMbFieldSpec.type.php'] = 'Source PHP';
$locales['CMbFieldSpec.type.ref'] = 'Objet';
$locales['CMbFieldSpec.type.set'] = 'Choix multiple';
$locales['CMbFieldSpec.type.str'] = 'Texte court';
$locales['CMbFieldSpec.type.text'] = 'Texte long';
$locales['CMbFieldSpec.type.time'] = 'Heure';
$locales['CMbFieldSpec.type.xml'] = 'Source XML';
$locales['CMbFieldSpec.typeEnum'] = 'Affichage';
$locales['CMbFieldSpec.typeEnum.'] = 'Par défaut';
$locales['CMbFieldSpec.typeEnum.checkbox'] = 'Cases à cocher';
$locales['CMbFieldSpec.typeEnum.radio'] = 'Boutons radio';
$locales['CMbFieldSpec.typeEnum.select'] = 'Liste de choix';
$locales['CMbFieldSpec.unlink'] = 'Liaison faible';
$locales['CMbFieldSpec.vertical'] = 'Vertical';
$locales['CMbObject-back-files'] = 'Fichiers';
$locales['CMbObject-back-notes'] = 'Notes utilisateurs';
$locales['CMbObject-msg-cascade-issues'] = 'problèmes en cascade pour';
$locales['CMbObject-msg-check-failed'] = ': Données incorrectes, ';
$locales['CMbObject-msg-nodelete-backrefs'] = 'Suppression impossible en raison des liaisons suivantes';
$locales['CMbObject-msg-store-failed'] = ': Enregistrement impossible,';
$locales['CMbObject.none'] = 'No object';
$locales['CModule'] = 'Module';
$locales['Day-after'] = 'Le jour suivant';
$locales['Day-before'] = 'Le jour précédent';
$locales['Day view'] = 'Vue de la journée';
$locales['Delete-all'] = 'Tout supprimer';
$locales['FormObserver-msg-confirm'] = 'Vous avez modifié certaines informations sur cette page sans les sauvegarder. Si vous cliquez sur OK, ces données seront perdues.';
$locales['History.desc'] = 'Show history';
$locales['Import-CSV'] = 'Importer un fichier CSV';
$locales['Mode-readonly-description'] = 'Bien que toutes les informations soient pleinement accessibles, aucune donnée ne peut êre enregistré car Mediboard est en mode "lecture seule" pour des raisons de mainteance.';
$locales['Mode-readonly-disclaimer'] = 'Si le problème persiste, alerter l\'administrateur du système. Merci pour votre compréhension.';
$locales['Mode-readonly-msg'] = 'Mode consultation !';
$locales['Mode-readonly-title'] = 'Mediboard est en mode consultation !';
$locales['Mode-readonly-title_mode_auto'] = 'Passage automatique en mode lecture seule';
$locales['More-info'] = 'More info';
$locales['Obsolete'] = 'Deprecated';
$locales['Period.day'] = 'Quotidien';
$locales['Period.month'] = 'Mensuel';
$locales['Period.week'] = 'Hebdomadaire';
$locales['Popup blocker alert'] = 'Il semblerait que votre navigateur ait bloqué la popup "%s", veuillez la débloquer.';
$locales['Rendez-vous'] = 'Apointment';
$locales['Send-email'] = 'Send e-mail';
$locales['Uptodate'] = 'Up to date';
$locales['WaitingForAjaxRequestReturn'] = 'Un ou plusieurs formulaires n\'ont pas été soumis entièrement.nCliquez sur OK pour terminer ce traitement et continuer.';
$locales['Week-after'] = 'Next week';
$locales['bool.'] = 'undefined';
$locales['bool.0'] = 'No';
$locales['bool.1'] = 'Yes';
$locales['browser.chrome'] = 'Google Chrome';
$locales['browser.firefox'] = 'Mozilla Firefox';
$locales['browser.mozilla'] = 'Mozilla';
$locales['browser.msie'] = 'Microsoft Internet Explorer';
$locales['browser.opera'] = 'Opera';
$locales['browser.safari'] = 'Apple safari';
$locales['common-Bot'] = 'Bot';
$locales['common-suggest-no-mediuser'] = 'Il est fortement conseillé d\'installer le module Mediusers pour créer et utiliser un utilisateur relié à l\'organigramme.';
$locales['common-warning-no-mediuser'] = 'Vous n\'êtes pas relié à l\'organigramme, ce qui nuit au bon fonctionnement et réduit les fonctionnalités (vous êtes peut-être authentifié avec l\'utilisateur "admin")';
$locales['config-browser_compat'] = 'Compatibilité avec les navigateurs';
$locales['configure'] = 'Administrer';
$locales['confirm-ask-continue'] = 'nnVoulez-vous continuer ?';
$locales['date-deb'] = 'Date de début';
$locales['date-fin'] = 'Date de fin';
$locales['dateTime-deb'] = 'Date et heure de début';
$locales['dateTime-fin'] = 'Date et heure de fin';
$locales['extension-failed-needed'] = 'Extension \'%s\' nécessaire pour cette fonctionnalité.';
$locales['extension-msg-ok'] = 'Extension \'%s\' correctement installée.';
$locales['fifty-first-results'] = '50 premiers résultats';
$locales['filter-criteria'] = 'Critères de filtres';
$locales['install'] = 'Install';
$locales['language.en'] = 'English';
$locales['language.fr'] = 'French';
$locales['menu-changePassword'] = 'Changer de mot de passe';
$locales['menu-lockSession'] = 'Verrouiller la session';
$locales['menu-logout'] = 'Déconnexion';
$locales['menu-myInfo'] = 'Mon compte';
$locales['menu-switchUser'] = 'Changer d\'utilisateur';
$locales['mod--tab-'] = 'Connexion';
$locales['modele-choice'] = 'Choisir un modèle';
$locales['modele-none'] = 'Aucun modèle disponible';
$locales['module--court'] = 'Connexion';
$locales['module-all-court'] = 'All';
$locales['module-common-court'] = 'All modules';
$locales['modules'] = 'Modules';
$locales['msg-common-loading-soon'] = 'Loading soon';
$locales['msg-purge'] = 'Purge done';
$locales['no_tag_defined'] = 'Aucun tag (patient/séjour) de défini pour la synchronisation';
$locales['portal-help'] = 'Aide';
$locales['portal-tracker'] = 'Suggérer une amélioration';
$locales['pref-AUTOADDSIGN'] = 'Ajout automatique des éléments significatifs';
$locales['pref-AUTOADDSIGN-desc'] = 'Ajout automatique des éléments significatifs';
$locales['pref-DEFMODULE'] = 'Module/onglet par défaut';
$locales['pref-DEFMODULE-desc'] = 'Veuillez choisir le module ou l\'onglet par défaut à afficher';
$locales['pref-DEPARTEMENT'] = 'Veuillez choisir le numéro du département par défaut à utiliser';
$locales['pref-DEPARTEMENT-desc'] = 'N° du département par défaut';
$locales['pref-INFOSYSTEM'] = 'Afficher les informations système';
$locales['pref-INFOSYSTEM-desc'] = 'Afficher les informations système';
$locales['pref-LOCALE'] = 'Language';
$locales['pref-MenuPosition'] = 'Menu position';
$locales['pref-MenuPosition-left'] = 'Left';
$locales['pref-MenuPosition-top'] = 'Top';
$locales['pref-UISTYLE'] = 'Thème de l\'interface';
$locales['pref-UISTYLE-desc'] = 'Veuillez choisir l\'apparence que vous souhaitez utiliser';
$locales['pref-autocompleteDelay'] = 'Temps avant d\'afficher l\'autocomplete';
$locales['pref-autocompleteDelay-desc'] = 'Temps avant d\'afficher l\'autocomplete';
$locales['pref-showLastUpdate'] = 'Afficher la date de dernière mise à jour';
$locales['pref-showLastUpdate-desc'] = 'Afficher la date de dernière mise à jour';
$locales['pref-textareaToolbarPosition'] = 'Position de la barre d\'outils des zones de texte assistées';
$locales['pref-textareaToolbarPosition-desc'] = 'Position de la barre d\'outils des zones de texte assistées';
$locales['pref-textareaToolbarPosition-left'] = 'Left';
$locales['pref-textareaToolbarPosition-right'] = 'Right';
$locales['pref-tooltipAppearenceTimeout'] = 'Temps avant d\'afficher les infobulles';
$locales['pref-tooltipAppearenceTimeout-desc'] = 'Temps avant d\'afficher les infobulles';
$locales['pref-touchscreen'] = 'Touch screen';
$locales['pref-touchscreen-desc'] = 'Choisissez Oui si vous utilisez l\'application sur un écran tactile';
$locales['query-pace-0'] = 'Requête SQL ultra rapide sur DSN \'%s\' : %sms';
$locales['query-pace-1'] = 'Requête SQL très rapide sur DSN \'%s\' : %sms';
$locales['query-pace-2'] = 'Requête SQL rapide sur DSN \'%s\' : %sms';
$locales['query-pace-3'] = 'Requête SQL moyenne sur DSN \'%s\' : %sms';
$locales['query-pace-4'] = 'Requête SQL lente sur DSN \'%s\' : %sms';
$locales['query-pace-5'] = 'Requête SQL très lente sur DSN \'%s\' : %sms';
$locales['query-pace-6'] = 'Requête SQL ultra lente sur DSN \'%s\' : %sms';
$locales['style.AS400'] = 'Console IBM AS400 (noir)';
$locales['style.aero'] = 'Aéro Menu (DEPRECTAED)';
$locales['style.e-cap'] = 'Modern (white, blue)';
$locales['style.mediboard'] = 'Standard (grey, green)';
$locales['style.medilab'] = 'Medilab (red, beige)';
$locales['style.phenx'] = 'Phenix (white, grey, tabbed module)';
$locales['style.silver'] = 'Silver (grey, green)';
$locales['thirty-first-results'] = '30 first results';
