<?php
$locales['Action'] = 'Aktion';
$locales['Actions'] = 'Aktionen';
$locales['Add'] = 'Hinzufügen';
$locales['Address'] = 'Adresse';
$locales['All'] = 'Alle';
$locales['Apply'] = 'Anwenden';
$locales['Archive'] = 'Archivieren';
$locales['Arreter'] = 'Beenden';
$locales['Autre'] = 'Andere';
$locales['Bill'] = 'Rechnung';
$locales['Bill-court'] = 'Rechn';
$locales['Browse'] = 'Durchsuchen';
$locales['CANCEL'] = 'Annullieren';
$locales['CFlotrGraph-spreadsheet-Data'] = 'Daten';
$locales['CFlotrGraph-spreadsheet-Download CSV'] = 'CSV-Datei laden';
$locales['CFlotrGraph-spreadsheet-Graph'] = 'Grafik';
$locales['CFlotrGraph-spreadsheet-Select table'] = 'Tabelle auswählen';
$locales['CFunction'] = 'Praxis';
$locales['CMbFieldSpec.type.password'] = 'Passwort';
$locales['CMbFieldSpec.type.phone'] = 'Telefonnummer';
$locales['CMbFieldSpec.typeEnum'] = 'Anzeige';
$locales['CMbObject-back-files'] = 'Dateien';
$locales['CMbObject.none'] = 'Keine betreff';
$locales['CSyslogSource-password'] = 'Passwort';
$locales['Cancel'] = 'Abbrechen';
$locales['Category'] = 'Kategorie';
$locales['Choose'] = 'Auswählen';
$locales['Classname'] = 'Art des Objekts';
$locales['Clear'] = 'Leeren';
$locales['Close'] = 'Schliessen';
$locales['Code'] = 'Code';
$locales['Color'] = 'Farbe';
$locales['Comment'] = 'Kommentar';
$locales['Complete'] = 'Beenden';
$locales['Compute'] = 'Berechnen';
$locales['Content'] = 'Inhalts';
$locales['Coucher'] = 'Schlafengehen';
$locales['Create'] = 'Erstellen';
$locales['Date'] = 'Datum';
$locales['Day-court'] = 'T';
$locales['Default'] = 'Standard';
$locales['Delete'] = 'Löschen';
$locales['Delete-all'] = 'Alles löschen';
$locales['Deleted'] = 'Löschen';
$locales['Ditto'] = 'Dito';
$locales['Download'] = 'Download';
$locales['DryRun'] = 'Blindversuch';
$locales['Duplicate'] = 'Duplizieren';
$locales['Duration'] = 'Dauer';
$locales['Edit'] = 'Ändern';
$locales['Empty'] = 'Leeren';
$locales['Export'] = 'Ausführen';
$locales['Extend'] = 'Verlängern';
$locales['File'] = 'Datei';
$locales['Filter'] = 'Filtern';
$locales['Filter-by_fct'] = 'Filter nach Funktion';
$locales['Filter-other|pl'] = 'Andere Filter';
$locales['Friday'] = 'Freitag';
$locales['Function'] = 'Funktion';
$locales['General'] = 'Allgemein';
$locales['Help'] = 'Hilfe';
$locales['Hide_finished|f|pl'] = 'Abgeschlossene ausblenden';
$locales['Home'] = 'Startseite';
$locales['Hour'] = 'Stunde';
$locales['Identity'] = 'Identität';
$locales['Import'] = 'Importieren';
$locales['Import-CSV'] = 'CSV Import';
$locales['Import-XML'] = 'XML Import';
$locales['Indications'] = 'Angaben';
$locales['Keywords'] = 'Schlüsselwörter';
$locales['Last connection'] = 'Letzte Verbindung';
$locales['Latest update'] = 'Letzte Aktualisierung vor';
$locales['Legend'] = 'Legende';
$locales['Line'] = 'Linie ';
$locales['Lines'] = 'Linien';
$locales['Lists'] = 'Listes ';
$locales['Loading in progress'] = 'Lädt...';
$locales['Locked'] = 'Gesperrt ';
$locales['Login'] = 'Einloggen';
$locales['Logout'] = 'Ausloggen';
$locales['Lookup'] = 'Suchen';
$locales['Matin'] = 'Morgen';
$locales['Mediboard'] = 'Mediboard';
$locales['Medical'] = 'Medizinischer';
$locales['Merge'] = 'Vereinigen';
$locales['Midi'] = 'Mittag';
$locales['Modify'] = 'Ändern';
$locales['Module'] = 'Modul';
$locales['Monday'] = 'Montag';
$locales['Month'] = 'Monaten';
$locales['Move'] = 'Umzug';
$locales['Name'] = 'Name';
$locales['New'] = 'Neu';
$locales['New|f'] = 'Neue';
$locales['No'] = 'Nein';
$locales['No filter'] = 'Kein filter';
$locales['None'] = 'Keine';
$locales['None|f'] = 'Keine';
$locales['No result'] = 'Keine Ergebnisse';
$locales['Now'] = 'Jetzt';
$locales['Number-court'] = 'Nr';
$locales['OK'] = 'OK';
$locales['Object'] = 'Objekt';
$locales['Open'] = 'Öffnen';
$locales['Options'] = 'Optionen';
$locales['Order_by'] = 'Verschrieben von';
$locales['Other'] = 'Andere';
$locales['Password'] = 'Passwort';
$locales['Pause'] = 'Pause';
$locales['Period'] = 'Periode';
$locales['Period.day'] = 'Täglich';
$locales['Period.month'] = 'Monatlich';
$locales['Period.week'] = 'Wöchentlich';
$locales['Phone.none'] = 'Keine telefon';
$locales['Planning'] = 'Planning';
$locales['Planning-of'] = 'Plan vom';
$locales['Planning-perso'] = 'Persönlichen planning';
$locales['Preference'] = 'Präferenz';
$locales['Preferences'] = 'Präferenzen';
$locales['Preview'] = 'Übersicht';
$locales['Print'] = 'Drucken';
$locales['Profile.chir'] = 'Praktiker';
$locales['Rapid_acces'] = 'Schneller Zugriff';
$locales['Recompute'] = 'Neu berechnen';
$locales['Remove'] = 'Löschen';
$locales['Rendez-vous'] = 'Termin';
$locales['Repeat'] = 'Wiederholen';
$locales['Repeat New Password'] = 'Neues Passwort bestätigen';
$locales['Repeat new password'] = 'Neues Passwort bestätigen';
$locales['Report'] = 'Bericht';
$locales['Results'] = 'Ergebnisse';
$locales['Saturday'] = 'Samstag';
$locales['Save'] = 'Speichern';
$locales['Search'] = 'Suchen';
$locales['Select'] = 'Auswählen';
$locales['Send'] = 'Senden';
$locales['Show'] = 'Anzeigen';
$locales['Since-long'] = 'Seite dem';
$locales['Size'] = 'Grösse';
$locales['Soir'] = 'Abend';
$locales['Stats'] = 'Statistiken';
$locales['Summary'] = 'Zusammenfassung';
$locales['Sunday'] = 'Sonntag';
$locales['Support'] = 'Support';
$locales['Suspendre'] = 'Aussetzen';
$locales['Switch'] = 'Wechseln';
$locales['Tarifs mis a jour'] = 'Tarife aktualisiert';
$locales['The'] = 'am';
$locales['Thursday'] = 'Donnerstag';
$locales['To'] = 'um';
$locales['Tools'] = 'Tools';
$locales['Total'] = 'Gesamt';
$locales['Tuesday'] = 'Dienstag';
$locales['Type'] = 'Typ';
$locales['Unlock'] = 'Entsperren';
$locales['User'] = 'Benutzer';
$locales['User switch'] = 'Benutzer wechseln';
$locales['User template'] = 'Profil';
$locales['Validate'] = 'Validieren';
$locales['Validate-all'] = 'Alles validieren';
$locales['Value'] = 'Wert';
$locales['Version'] = 'Version';
$locales['View'] = 'Ansehen';
$locales['Wednesday'] = 'Mittwoch';
$locales['Week'] = 'Woche';
$locales['With_chir'] = 'mit Dr.';
$locales['Year'] = 'Jahr';
$locales['Yes'] = 'Ja';
$locales['access-forbidden'] = 'Sie sind nicht berechtigt, auf diese Information zuzugreifen';
$locales['and'] = 'und';
$locales['bool.0'] = 'Nein';
$locales['bool.1'] = 'Ja';
$locales['cancel'] = 'annullieren';
$locales['common-%d day'] = '%d tag';
$locales['common-%d day|pl'] = '%d tage';
$locales['common-%d hour'] = '%d Stunde';
$locales['common-%d hour|pl'] = '%d Stunden';
$locales['common-%d line|pl'] = '%d linien';
$locales['common-%d minute|pl'] = '%d minuten';
$locales['common-Accounting'] = 'Buchhaltung';
$locales['common-Action'] = 'Aktion';
$locales['common-Action|pl'] = 'Aktionen';
$locales['common-Address'] = 'Adresse';
$locales['common-Agenda'] = 'Agenda';
$locales['common-Alphabetical order'] = 'Alphabetische Reihenfolge';
$locales['common-Available user|pl'] = 'Verfügbare Benutzer';
$locales['common-By type'] = 'Von typ';
$locales['common-CGroup'] = 'Einrichtung';
$locales['common-CSV'] = 'CSV';
$locales['common-Canceled-court'] = 'Ann.';
$locales['common-Changing'] = 'Änderung';
$locales['common-Choice a practitioner'] = 'Auswahl der Praktiker';
$locales['common-Choice of a cabinet'] = 'Auswahl der Praxis';
$locales['common-Choice of establishment'] = 'Auswahl der Einrichtung';
$locales['common-Choice of the cabinet'] = 'Auswahl der Praxis';
$locales['common-Choose a graphic'] = 'Auswahl der Grafik';
$locales['common-Choose a number'] = 'Auswahl der Nummer';
$locales['common-Christmas'] = 'Weihnachten';
$locales['common-Closed'] = 'Geschlossen';
$locales['common-Comment'] = 'Kommentar';
$locales['common-Completed-court'] = '(Term.)';
$locales['common-Content'] = 'Inhalt';
$locales['common-Context'] = 'Kontext';
$locales['common-Contexts'] = 'Kontexte';
$locales['common-Context|pl'] = 'Kontexte';
$locales['common-Cost'] = 'Kosten';
$locales['common-Creation'] = 'Erstellen';
$locales['common-Dashboard'] = 'Dashboard';
$locales['common-Date'] = 'Datum';
$locales['common-Date|pl'] = 'Datum';
$locales['common-Day'] = 'Tages';
$locales['common-Denomination'] = 'Gebräuchliche Bezeichnung';
$locales['common-Display option|pl'] = 'Anzeigeoptionen';
$locales['common-Dry run'] = 'Blindversuch';
$locales['common-Duration'] = 'Dauer';
$locales['common-End date'] = 'Enddatum';
$locales['common-Establishment'] = 'Einrichtung';
$locales['common-Establishment-court'] = 'Einrichtung';
$locales['common-Exclusion|pl'] = 'Ausschlüsse';
$locales['common-Field'] = 'Feld';
$locales['common-File name'] = 'Name des Datei';
$locales['common-Filter'] = 'Filter';
$locales['common-From %s to %s'] = 'Ab %s bis %s';
$locales['common-Future'] = 'Zukünftige';
$locales['common-Global'] = 'Gesamt';
$locales['common-History'] = 'Überblick';
$locales['common-Hour'] = 'Stunde';
$locales['common-Hour|pl'] = 'Studen';
$locales['common-Iconography'] = 'Ikonografie';
$locales['common-Inclusion|pl'] = 'Inklusionen';
$locales['common-Indication'] = 'Indikation';
$locales['common-Indirect exclusion|pl'] = 'indirekte Ausschlüsse';
$locales['common-Information|pl'] = 'Informationen';
$locales['common-In progress'] = 'laufenden';
$locales['common-Interval'] = 'Abstand';
$locales['common-Keywords'] = 'Schlüsselwörter';
$locales['common-Label'] = 'Bezeichnung';
$locales['common-Line'] = 'Linie';
$locales['common-Logout'] = 'Ausloggen';
$locales['common-Message'] = 'Nachricht';
$locales['common-Month'] = 'Monat';
$locales['common-My account'] = 'Mein Konto';
$locales['common-Name'] = 'Name';
$locales['common-National holiday'] = 'Nationalfeiertag';
$locales['common-No'] = 'Nein';
$locales['common-No context'] = 'Keine kontext';
$locales['common-No data'] = 'Jeine Daten';
$locales['common-No label'] = 'Keine Bezeichnung';
$locales['common-None'] = 'Keine';
$locales['common-No password'] = 'keine Passwörter';
$locales['common-No result'] = 'Keine Ergebnisse';
$locales['common-Note|pl'] = 'Notiz';
$locales['common-Object'] = 'Betreff';
$locales['common-Object type'] = 'Art des Betreffs';
$locales['common-One event'] = 'Ein Ereignis';
$locales['common-Operation'] = 'Intervention';
$locales['common-Option|pl'] = 'Optionen';
$locales['common-Other date'] = 'Andere Datum';
$locales['common-Other information|pl'] = 'Andere Informationen';
$locales['common-Other|pl'] = 'Andere';
$locales['common-Overview'] = 'globale Übersicht';
$locales['common-Partial code'] = 'Teilcode';
$locales['common-Partner'] = 'Partner';
$locales['common-Partnership'] = 'Partnerschaft';
$locales['common-Password'] = 'Passwort';
$locales['common-Password|pl'] = 'Passwort';
$locales['common-Pending'] = 'Laufende ';
$locales['common-Per day'] = 'Pro Tag';
$locales['common-Performed|pl'] = 'Durchgeführte';
$locales['common-Per patient'] = 'Von Patienten';
$locales['common-Per product'] = 'Von Produkt';
$locales['common-Planned|pl'] = 'Vorgesehene';
$locales['common-Practitioner'] = 'Praktiker';
$locales['common-Practitioner-court'] = 'Prakt.';
$locales['common-Practitioner.all'] = 'Alle Praktiker';
$locales['common-Practitioner.select'] = 'Praktiker auswählen';
$locales['common-Practitioner|all'] = 'Alle Praktiker';
$locales['common-Price'] = 'Preis';
$locales['common-Print for'] = 'Drucken für';
$locales['common-Printing|pl'] = 'Eindrücke';
$locales['common-Product'] = 'Produkt';
$locales['common-Product|pl'] = 'Produkte';
$locales['common-Protocole.none'] = 'Keine Protokoll';
$locales['common-Receiver'] = 'Empfänger';
$locales['common-Receiver|pl'] = 'Empfänger ';
$locales['common-Recipient'] = 'Empfänger';
$locales['common-Removed object'] = 'Element löschen';
$locales['common-Rendez-vous'] = 'Termin';
$locales['common-Result'] = 'Ergebnisse';
$locales['common-Results'] = 'Ergebnisse';
$locales['common-Search by chapter|pl'] = 'Suche nach Kapiteln';
$locales['common-Search by subchapter|pl'] = 'Suche nach Unterkapiteln';
$locales['common-Search criteria'] = 'Suchkriterien';
$locales['common-Selection'] = 'Auswahl';
$locales['common-Selection criteria'] = 'Auswahlkriterien';
$locales['common-Signature'] = 'Unterschrift';
$locales['common-Since'] = 'Seite';
$locales['common-Size'] = 'Grösse';
$locales['common-Sorting'] = 'Sortierung';
$locales['common-Speciality'] = 'Spezialität';
$locales['common-Start'] = 'Anfang';
$locales['common-Statistic|pl'] = 'Statistik';
$locales['common-Status'] = 'Status';
$locales['common-Subchapter|pl'] = 'Unterkapitel';
$locales['common-Summary'] = 'Zusammenfassung';
$locales['common-The %s by %s'] = 'den %s vom %s';
$locales['common-Tools'] = 'Tools';
$locales['common-Total'] = 'Gesamtbetrag';
$locales['common-Total|pl'] = 'Gesamtbetrag';
$locales['common-Type'] = 'Typ';
$locales['common-User'] = 'Nutzer';
$locales['common-Username'] = 'Name des Nutzer';
$locales['common-Validity'] = 'Gültig ';
$locales['common-Validity date'] = 'Gültigkeitsdatum';
$locales['common-Value'] = 'Wert';
$locales['common-Week'] = 'Woche';
$locales['common-Week from %s to %s|pl'] = 'Woche von %s bis %s';
$locales['common-Year'] = 'Jahr';
$locales['common-Yes'] = 'Ja';
$locales['common-Zip'] = 'Postleitzahl';
$locales['common-Zip code'] = 'Postleitzahl';
$locales['common-action-Account management'] = 'Kontoverwaltung';
$locales['common-action-Add'] = 'hinzufügen';
$locales['common-action-Add %d minute|pl'] = '%s minuten hinzufügen';
$locales['common-action-Add comment'] = 'Kommentar hinzufügen';
$locales['common-action-Add element'] = 'Element hinzufügen';
$locales['common-action-Add file'] = 'Datei hinzufügen';
$locales['common-action-Add one hour'] = 'Eine Stunde hinzufügen';
$locales['common-action-Add this comment'] = 'Diesen Kommentar Hinzufügen';
$locales['common-action-All stop'] = 'Alles beenden';
$locales['common-action-Browse or drag and drop file here'] = 'Laufwerk durchsuchen oder Ordner hier ablegen';
$locales['common-action-Cancel signature'] = 'Unterschriften annullieren';
$locales['common-action-Cancel signature|pl'] = 'Unterschriften annullieren';
$locales['common-action-Choice of schedule'] = 'Auswahl des Stundenplans';
$locales['common-action-Choose'] = 'Wählen';
$locales['common-action-Close'] = 'Schliessen';
$locales['common-action-Control Criterion|pl'] = 'Kontrollkriterien';
$locales['common-action-Create'] = 'Erstellen';
$locales['common-action-Create a new card'] = 'Erstellung einer neuen Datei';
$locales['common-action-Delete'] = 'Löschen';
$locales['common-action-Delete-court'] = 'Löschen';
$locales['common-action-Display'] = 'Anzeigen';
$locales['common-action-Display all'] = 'Alles anzeigen';
$locales['common-action-Duplicate'] = 'Duplizieren';
$locales['common-action-Edit'] = 'Ändern';
$locales['common-action-Empty'] = 'leeren';
$locales['common-action-Fill new form'] = 'Neues Formular ausfüllen';
$locales['common-action-Filter'] = 'Filtern';
$locales['common-action-Get random password'] = 'Ein zufälliges Passwort erhalten';
$locales['common-action-Hide completed'] = 'Abgeschlossene ausblenden';
$locales['common-action-Import all'] = 'Alles importieren';
$locales['common-action-Notify'] = 'Mitteilen';
$locales['common-action-Paste image here'] = 'hierhin ein Bild kopieren';
$locales['common-action-Perform a dry run'] = 'Blindversuch lassen';
$locales['common-action-Plan'] = 'Planen';
$locales['common-action-Plan-court'] = 'Konsultation Plan';
$locales['common-action-Prescribe'] = 'Verschreiben ';
$locales['common-action-Print'] = 'Ausdrucken';
$locales['common-action-Report'] = 'Melden';
$locales['common-action-Save'] = 'Registrieren';
$locales['common-action-Search'] = 'Suchen';
$locales['common-action-Select'] = 'Auswählen';
$locales['common-action-Send'] = 'Schicken ';
$locales['common-action-Send a message-desc'] = 'Ein nachricht schicken ';
$locales['common-action-Show / Hide'] = 'Anzeigen/Ausblenden';
$locales['common-action-Show object|pl'] = 'Die Objekts einsehen';
$locales['common-action-Sign'] = 'Unterschreiben.';
$locales['common-action-Sign all'] = 'Alles unterschreiben';
$locales['common-action-Sort by date'] = 'Per Datum sortieren';
$locales['common-action-Sort by type'] = 'Nach Typ ordnen';
$locales['common-action-Validate'] = 'Validieren';
$locales['common-all|f|pl'] = 'Alle';
$locales['common-all|pl'] = 'Alle';
$locales['common-and sign'] = 'Und unterschreiben';
$locales['common-by'] = 'vom';
$locales['common-contact-support'] = 'Den Support kontaktieren';
$locales['common-datetime_spec-desc'] = 'Datum und stunde';
$locales['common-day'] = 'Tag';
$locales['common-first'] = 'Ersten';
$locales['common-for'] = 'für';
$locales['common-form(|pl)'] = 'Formulare';
$locales['common-from_long'] = 'Ab';
$locales['common-holiday'] = 'Feiertag';
$locales['common-home'] = 'Startseite';
$locales['common-hour-court'] = 'Uhr';
$locales['common-hour|pl'] = 'Stunden';
$locales['common-in progress'] = 'Laufende';
$locales['common-label-Group event|pl'] = 'Die Ereignisse gruppieren';
$locales['common-label-Hide patient identity|pl'] = 'Die Identität der Patienten verbergen';
$locales['common-left'] = 'Links';
$locales['common-minute|pl'] = 'Minuten';
$locales['common-msg-%d file(s) selected'] = '%d datei(en) ausgewählt';
$locales['common-msg-Created'] = 'Erstellt';
$locales['common-msg-Image paste'] = 'Bild einfügen';
$locales['common-msg-No password'] = 'Keine Passwort';
$locales['common-msg-No result'] = 'Kein Ergebnisse ';
$locales['common-msg-Passphrase saved'] = 'Passwort registrieren';
$locales['common-msg-Paste your image here'] = 'Fügen Sie hier Ihr Bild ein';
$locales['common-msg-You are not allowed to access this information (%s)'] = 'Der Zugang zu dieser Information ist Ihnen nicht gestattet.';
$locales['common-my-account'] = 'Mein Konto';
$locales['common-new Year s Day'] = 'Neujahr';
$locales['common-noun-minutes-court'] = 'min';
$locales['common-period'] = 'Periode';
$locales['common-search'] = 'Suche';
$locales['common-second|pl'] = 'Sekunden';
$locales['common-settings'] = 'Einstellungen';
$locales['common-since'] = 'seit';
$locales['common-the %s'] = 'am %s';
$locales['common-with %s'] = 'mit %s';
$locales['date.From'] = 'vom';
$locales['date.from'] = 'von';
$locales['date.to'] = 'am';
$locales['day'] = 'tag';
$locales['days'] = 'tagen';
$locales['delete'] = 'Löschen';
$locales['early'] = 'Beginn';
$locales['end'] = 'Ende';
$locales['fast-search'] = 'Schnelle Suche';
$locales['filter'] = 'Filter';
$locales['filters'] = 'Filtern';
$locales['from'] = 'von';
$locales['hatching'] = 'annulliert';
$locales['hour'] = 'Stunde';
$locales['hourly'] = 'Stundenplans';
$locales['hours'] = 'Stunden';
$locales['language.de'] = 'Deutsch';
$locales['language.en'] = 'Englisch';
$locales['language.fr'] = 'Französisch';
$locales['latest-month'] = 'letzten monaten';
$locales['long'] = 'Lang';
$locales['medium'] = 'Mittel';
$locales['menu-changePassword'] = 'Passwort ändern';
$locales['menu-lockSession'] = 'Sitzung sperren';
$locales['menu-logout'] = 'Ausloggen';
$locales['menu-myInfo'] = 'Mein Konto';
$locales['menu-switchUser'] = 'Benutzer wechseln';
$locales['minute'] = 'Minute';
$locales['minutes'] = 'Minuten';
$locales['modele-choice'] = 'Ein Modell wählen';
$locales['modele-none'] = 'Kein verfügbare modell';
$locales['module-all-court'] = 'Alle';
$locales['module-common-court'] = 'Alle Module';
$locales['modules'] = 'Modulen';
$locales['month'] = 'Monate';
$locales['months'] = 'Monate';
$locales['most_frequent'] = 'die häufigsten';
$locales['night'] = 'Nacht';
$locales['night|pl'] = 'Nachten';
$locales['others'] = 'Andere';
$locales['portal-help'] = 'Hilfe';
$locales['pref-DEFMODULE'] = 'Standardmässiges Modul/Registerkarte';
$locales['pref-DEFMODULE-desc'] = 'Bitte wählen Sie das/die standardmässig anzuzeigende Modul/Registerkarte';
$locales['pref-LOCALE'] = 'Sprache';
$locales['pref-LOCALE-desc'] = 'Bitte wählen Sie die Sprache, die Sie benutzen möchten';
$locales['pref-MenuPosition'] = 'Position des Menüs';
$locales['pref-MenuPosition-desc'] = 'Die Positionierung des Hauptmenüs im Anwendungsfenster';
$locales['pref-MenuPosition-left'] = 'Links';
$locales['pref-MenuPosition-top'] = 'Oben';
$locales['pref-UISTYLE'] = 'Interface-Thema';
$locales['pref-UISTYLE-desc'] = 'Bitte wählen Sie die Erscheinung, die Sie benutzen möchten';
$locales['pref-accessibility_dyslexic'] = 'Aktivierung des Fonts für Personen mit Legasthenie';
$locales['pref-accessibility_dyslexic-desc'] = 'Verwendung des angepassten Fonts für Personen mit Legasthenie';
$locales['pref-autocompleteDelay'] = 'Dauer, bevor Autocomplete angezeigt wird';
$locales['pref-autocompleteDelay-desc'] = 'Dauer, bevor Autocomplete angezeigt wird';
$locales['pref-display_search-alpha'] = 'Alphabetische Reihenfolge';
$locales['pref-navigationHistoryLength'] = 'Grösse des Browserverlaufs';
$locales['pref-navigationHistoryLength-desc'] = 'Anzahl der memorisierten Seiten, um schnell zurückzugelangen (0 = Verlauf deaktiviert; maximal 20)';
$locales['pref-notes_anonymous'] = 'Notizen ohne standardmässigen Besitzer';
$locales['pref-notes_anonymous-desc'] = 'Notizen ohne standardmässigen Besitzer';
$locales['pref-planning_dragndrop'] = 'Drag and Drop im Planning benutzen';
$locales['pref-planning_dragndrop-desc'] = 'Die Felder des Plannings dürfen mit der Maus per Drag and Drop verschoben werden, wenn dies möglich ist';
$locales['pref-planning_hour_division'] = 'Einteilung der Stunden des Plannings';
$locales['pref-planning_hour_division-2'] = '30 Minuten';
$locales['pref-planning_hour_division-3'] = '20 Minuten';
$locales['pref-planning_hour_division-4'] = '15 Minuten';
$locales['pref-planning_hour_division-6'] = '10 Minuten';
$locales['pref-planning_hour_division-desc'] = 'Jede Stunde des Plannings wird in folgende Zeitspanne eingeteilt:';
$locales['pref-planning_resize'] = 'Neudimensionierung der Felder des Plannings';
$locales['pref-planning_resize-desc'] = 'Ein Feld des Plannings auf visueller Weise neu dimensionieren dürfen';
$locales['pref-showCounterTip'] = 'Anzeige eines Ergebniszählers';
$locales['pref-showCounterTip-desc'] = 'Anzeige eines Ergebniszählers';
$locales['pref-textareaToolbarPosition'] = 'Positionierung der Toolbar der unterstützten Textbereiche';
$locales['pref-textareaToolbarPosition-desc'] = 'Positionierung der Toolbar der unterstützten Textfelder';
$locales['pref-textareaToolbarPosition-left'] = 'Links';
$locales['pref-textareaToolbarPosition-right'] = 'Rechts';
$locales['pref-tooltipAppearenceTimeout'] = 'Dauer, bevor Tooltips angezeigt werden';
$locales['pref-tooltipAppearenceTimeout-desc'] = 'Dauer, bevor Tooltips angezeigt werden';
$locales['pref-touchscreen'] = 'Touchscreen';
$locales['pref-touchscreen-desc'] = 'Wählen Sie Ja, wenn Sie die Anwendung auf einem Touchscreen verwenden';
$locales['pref-useEditAutocompleteUsers'] = 'Die Benutzerliste der Dropdownlisten filtern';
$locales['pref-useEditAutocompleteUsers-desc'] = 'Die Liste der Benutzer der Dropdownlisten filtern, selbst im schreibgeschützt Modus';
$locales['reglee'] = 'bezahlt';
$locales['result'] = 'Ergebnisse';
$locales['results'] = 'Ergebnisse';
$locales['second'] = 'Sekunde';
$locales['seconds'] = 'Sekunden';
$locales['short'] = 'Kurz';
$locales['thirty-first-results'] = '30 ERSTEN ERGEBNISSE';
$locales['to'] = 'um';
$locales['total'] = 'Gesamt';
$locales['undated'] = 'undatiert';
$locales['week'] = 'Woche';
$locales['weeks'] = 'Wochen';
$locales['year'] = 'Jahre';
$locales['years'] = 'Jahren';
$locales['years_old'] = 'Jahren';
