<?php
/**
 * @package Mediboard\Ssr
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Ssr;

use Ox\Core\Module\CAbstractModuleCache;

/**
 * Description
 */
class CModuleCacheSsr extends CAbstractModuleCache {
  public $module = 'ssr';

  protected $shm_patterns = array(
    'activite_csarr_',
  );
}
