<?php
$locales['CCIM10'] = 'CIM10';
$locales['CCIM10-Prohibited diagnosis'] = 'Diagnostic interdit';
$locales['CCIM10-code'] = 'Code CIM10';
$locales['CCIM10-code-court'] = 'Code';
$locales['CCIM10-code-desc'] = 'Code CIM10 Î° usage du pmsi';
$locales['CCIM10-complete_name'] = 'Libellé complet';
$locales['CCIM10-complete_name-court'] = 'Libellé complet';
$locales['CCIM10-complete_name-desc'] = 'Libellé complet';
$locales['CCIM10-libelle-court'] = 'Description';
$locales['CCIM10-libelle-desc'] = 'Description';
$locales['CCIM10-libelle_court-court'] = 'Libellé court';
$locales['CCIM10-libelle_court-desc'] = 'Libellé court';
$locales['CCIM10-msg-create'] = 'Code CIM10 créé';
$locales['CCIM10-msg-delete'] = 'Code CIM10 supprimé';
$locales['CCIM10-msg-modify'] = 'Code CIM10 modifié';
$locales['CCIM10-short_name'] = 'Libellé court';
$locales['CCIM10-short_name-court'] = 'Libellé court';
$locales['CCIM10-short_name-desc'] = 'Libellé court';
$locales['CCIM10-title-create'] = 'Création du code CIM10';
$locales['CCIM10-title-modify'] = 'Modification du code CIM10';
$locales['CCIM10-type'] = 'Type';
$locales['CCIM10-type-court'] = 'Type';
$locales['CCIM10-type-desc'] = 'Type permettant l\'aide au codage des diagnostics';
$locales['CCIM10-type_mco-court'] = 'Type MCO';
$locales['CCIM10-type_mco-desc'] = 'Type MCO';
$locales['CCIM10.DAS'] = 'DA';
$locales['CCIM10.DP'] = 'DP';
$locales['CCIM10.DR'] = 'DR';
$locales['CCIM10.ailleurs'] = 'Ailleurs';
$locales['CCIM10.all'] = 'Tous les codes CIM10';
$locales['CCIM10.none'] = 'Aucun code CIM10';
$locales['CCIM10.one'] = 'Code CIM10';
$locales['CCIM10.type.'] = 'Défaut';
$locales['CCIM10.type.0'] = 'Pas de restriction particuliÎ¸re ';
$locales['CCIM10.type.1'] = 'Diagnostic interdit en DP et DR - Autorisé ailleurs';
$locales['CCIM10.type.2'] = 'Diagnostic interdit en DP et DR - Cause externe de morbidité';
$locales['CCIM10.type.3'] = 'Diagnostic interdit en DP, DR et DA - Catégories et sous-catégories non vides ou code pÎ¸re interdit';
$locales['CCIM10.type.4'] = 'Diagnostic interdit en DP Â– Autoriée? ailleurs';
$locales['CCIM10.type_mco.0'] = 'Pas de restriction particuliÎ¸re';
$locales['CCIM10.type_mco.1'] = 'Diagnostic interdit en DP et DR - Autorisé ailleurs';
$locales['CCIM10.type_mco.2'] = 'Diagnostic interdit en DP et DR - Cause externe de morbidité';
$locales['CCIM10.type_mco.3'] = 'Diagnostic interdit en DP, DR et DA - Catégories et sous-catégories non vides ou code pÎ¸re interdit';
$locales['CCIM10.type_mco.4'] = 'Diagnostic interdit en DP et Autoriée ailleurs';
$locales['CCMA'] = 'Complications et morbidité associée';
$locales['CCMA-code_cim'] = 'Diagnostic';
$locales['CCMA-code_cim-desc'] = 'Diagnostic associé Î° la CMA';
$locales['CCMA-libelle_cim'] = 'Libellé';
$locales['CCMA-libelle_cim-desc'] = 'Libellé du diagnostic associé Î° la CMA';
$locales['CCMA-niveau_severite'] = 'Niveau de sévérité';
$locales['CCMA-niveau_severite-desc'] = 'Niveau de sévérité du diagnostic en CMA';
$locales['CCMA.niveau_severite.2'] = 'Niveau de sévérité 2';
$locales['CCMA.niveau_severite.3'] = 'Niveau de sévérité 3';
$locales['CCMA.niveau_severite.4'] = 'Niveau de sévérité 4';
$locales['CGHM'] = 'Groupement HomogÎ¸ne de Malade';
$locales['CGHM-msg-create'] = 'GHM créée';
$locales['CGHM-msg-delete'] = 'GHM supprimée';
$locales['CGHM-msg-modify'] = 'GHM modifiée';
$locales['CMA.none'] = 'Pas de CMA';
$locales['CMediusers-back-dim'] = 'Directeur de l\'information médicale';
$locales['CMediusers-back-dim.empty'] = 'Pas de directeur de l\'information médicale';
$locales['CMediusers-back-relances'] = 'Relances';
$locales['CMediusers-back-relances.empty'] = 'Aucune relance';
$locales['COperation-action-codage_access-add_user'] = 'Ajouter un praticien';
$locales['COperation-action-manage_codage_access'] = 'Gestion des accÎ¸s';
$locales['COperation-msg-access_authorized'] = 'AccÎ¸s autorisé du';
$locales['COperation-msg-codage_access-empty'] = 'Aucun accÎ¸s défini';
$locales['COperation-msg-unlocked'] = 'L\'intervention n\'est pas encore verrouillée';
$locales['COperation-title-manage_codage_access'] = 'Gestion des autorisations de codage';
$locales['CPatient-back-relances'] = 'Relances';
$locales['CPatient-back-relances.empty'] = 'Aucune relance';
$locales['CRSS-back-traitement_dossier'] = 'RSS traité dans le dossier';
$locales['CRSS-back-traitement_dossier.empty'] = 'Pas de résumé standardisé de séjour traité dans le dossier';
$locales['CRSS-das'] = 'Diagnostics Associés Significatifs';
$locales['CRSS-das-court'] = 'Diag. Associés Significatifs';
$locales['CRSS-das-desc'] = 'Diagnostics associés avec CIM-10 Î° visée du pmsi (atih)';
$locales['CRSS-dp'] = 'Diagnostic principal';
$locales['CRSS-dp-court'] = 'Diag. Principal';
$locales['CRSS-dp-desc'] = 'Diagnostic principal du RSS avec CIM-10 Î° visée du pmsi (atih)';
$locales['CRSS-dr'] = 'Diagnostic Relié (CIM10 - PMSI)';
$locales['CRSS-dr-court'] = 'Diag. Relié';
$locales['CRSS-dr-desc'] = 'Diagnostic relié avec CIM-10 Î° visée du pmsi (atih)';
$locales['CRSS.statut.'] = 'Statut du RSS';
$locales['CRUM'] = 'RUM';
$locales['CRUM-age_gestationnel-desc'] = 'Î’ge gestationnel ';
$locales['CRUM-annee_ivg_precedent'] = 'Année de lÂ’IVG précédente';
$locales['CRUM-annee_ivg_precedent-desc'] = 'Année de lÂ’IVG précédente';
$locales['CRUM-anonyme-desc'] = 'anonyme';
$locales['CRUM-code_postal-desc'] = 'code postal';
$locales['CRUM-confirm_codage'] = 'Confirmation du codage';
$locales['CRUM-date_entree_um-desc'] = 'date d\'entrée dans l\'unité medicale';
$locales['CRUM-date_naissance-desc'] = 'Date de naissance';
$locales['CRUM-date_sortie_um-desc'] = 'date de sortie dans l\'unité médicale';
$locales['CRUM-destination-desc'] = 'Destination';
$locales['CRUM-igsII-desc'] = 'Igs II';
$locales['CRUM-mode_hospi'] = 'Mode d\'hospitalisation';
$locales['CRUM-nbr_ivg_anterieures'] = 'Nombre dÂ’IVG antérieures';
$locales['CRUM-nbr_ivg_anterieures-desc'] = 'Nombre dÂ’IVG antérieures';
$locales['CRUM-nbr_naissance_vivante_anterieures'] = 'Nombres de naissances vivantes antérieures';
$locales['CRUM-nbr_naissance_vivante_anterieures-desc'] = 'Nombres de naissances vivantes antérieures';
$locales['CRUM-permission-desc'] = 'permission';
$locales['CRUM-poids_nouveau_ne-desc'] = 'Poids du nouveau-né en kilogrammes';
$locales['CRUM-provenance-desc'] = 'Provenance';
$locales['CRUM-rum_id'] = 'Id.';
$locales['CRUM-sexe-desc'] = 'Sexe';
$locales['CRUM-um_id'] = 'Type d\'autorisation d\'UM dédié';
$locales['CRUM-um_id-court'] = 'Type d\'autorisation d\'UM';
$locales['CRUM-um_id-desc'] = 'Identifiant de l\'unité médicale';
$locales['CRUM.provenance.R'] = 'Réanimation';
$locales['CRelancePMSI'] = 'Relance PMSI';
$locales['CRelancePMSI-Closed'] = 'ClÏ„turées';
$locales['CRelancePMSI-Doc type'] = 'Type doc';
$locales['CRelancePMSI-Empty'] = 'Vide';
$locales['CRelancePMSI-Filter reminders'] = 'Filtrer les relances';
$locales['CRelancePMSI-First raise'] = 'PremiÎ¸re relance';
$locales['CRelancePMSI-Indifferent'] = 'Indifférent';
$locales['CRelancePMSI-Informed'] = 'Renseigné';
$locales['CRelancePMSI-Level'] = 'Niveau';
$locales['CRelancePMSI-Medical Comment-court'] = 'Commentaire Méd.';
$locales['CRelancePMSI-Not closed'] = 'Non clÏ„turées';
$locales['CRelancePMSI-Responsible Physician-court'] = 'Méd. Resp.';
$locales['CRelancePMSI-Restate Status'] = 'Statut Relance';
$locales['CRelancePMSI-Second raise'] = 'DeuxiÎ¸me relance';
$locales['CRelancePMSI-Statistics-court'] = 'Stat.';
$locales['CRelancePMSI-autre'] = 'Autre';
$locales['CRelancePMSI-autre-court'] = 'Autre';
$locales['CRelancePMSI-autre-desc'] = 'Autre';
$locales['CRelancePMSI-chir_id'] = 'Médecin responsable';
$locales['CRelancePMSI-chir_id-court'] = 'Méd. Resp.';
$locales['CRelancePMSI-chir_id-desc'] = 'Médecin responsable de la relance';
$locales['CRelancePMSI-commentaire_dim'] = 'Commentaire DIM';
$locales['CRelancePMSI-commentaire_dim-court'] = 'Commentaire DIM';
$locales['CRelancePMSI-commentaire_dim-desc'] = 'Commentaire DIM';
$locales['CRelancePMSI-commentaire_med'] = 'Commentaire médecin';
$locales['CRelancePMSI-commentaire_med-court'] = 'Commentaire médecin';
$locales['CRelancePMSI-commentaire_med-desc'] = 'Commentaire médecin';
$locales['CRelancePMSI-cotation'] = 'Codage';
$locales['CRelancePMSI-cotation-court'] = 'Codage';
$locales['CRelancePMSI-cotation-desc'] = 'Codage';
$locales['CRelancePMSI-cra'] = 'Compte-rendu d\'accouchement';
$locales['CRelancePMSI-cra-court'] = 'CRAcc';
$locales['CRelancePMSI-cra-desc'] = 'CRAcc (Compte-rendu d\'accouchement)';
$locales['CRelancePMSI-crana'] = 'Compte-rendu d\'anapath';
$locales['CRelancePMSI-crana-court'] = 'CRAna';
$locales['CRelancePMSI-crana-desc'] = 'CRAna (Compte-rendu d\'anapath)';
$locales['CRelancePMSI-cro'] = 'Compte-rendu opératoire';
$locales['CRelancePMSI-cro-court'] = 'CRO';
$locales['CRelancePMSI-cro-desc'] = 'CRO (Compte-rendu opératoire)';
$locales['CRelancePMSI-datetime_cloture'] = 'Date de clÏ„ture';
$locales['CRelancePMSI-datetime_cloture-court'] = 'Date de clÏ„ture';
$locales['CRelancePMSI-datetime_cloture-desc'] = 'Date de clÏ„ture de la relance';
$locales['CRelancePMSI-datetime_creation'] = 'Date de création';
$locales['CRelancePMSI-datetime_creation-court'] = 'Date de création';
$locales['CRelancePMSI-datetime_creation-desc'] = 'Date de création de la relance';
$locales['CRelancePMSI-datetime_med'] = 'Date de réponse du médecin';
$locales['CRelancePMSI-datetime_med-court'] = 'Date de réponse du médecin';
$locales['CRelancePMSI-datetime_med-desc'] = 'Date de réponse du médecin';
$locales['CRelancePMSI-datetime_relance'] = 'Date de deuxiÎ¸me relance';
$locales['CRelancePMSI-datetime_relance-court'] = 'Date de deuxiÎ¸me relance';
$locales['CRelancePMSI-datetime_relance-desc'] = 'Date de deuxiÎ¸me relance';
$locales['CRelancePMSI-description'] = 'Description';
$locales['CRelancePMSI-description-desc'] = 'Description des documents attendus';
$locales['CRelancePMSI-ls'] = 'Lettre de sortie';
$locales['CRelancePMSI-ls-court'] = 'LS';
$locales['CRelancePMSI-ls-desc'] = 'LS (Lettre de sortie)';
$locales['CRelancePMSI-msg-create'] = 'Relance créée';
$locales['CRelancePMSI-msg-delete'] = 'Relance supprimée';
$locales['CRelancePMSI-msg-modify'] = 'Relance modifiée';
$locales['CRelancePMSI-patient_id'] = 'Patient';
$locales['CRelancePMSI-patient_id-court'] = 'Patient';
$locales['CRelancePMSI-patient_id-desc'] = 'Patient associé';
$locales['CRelancePMSI-relance_pmsi_id'] = 'Identifiant';
$locales['CRelancePMSI-relance_pmsi_id-court'] = 'Id';
$locales['CRelancePMSI-relance_pmsi_id-desc'] = 'Identifiant interne';
$locales['CRelancePMSI-sejour_id'] = 'Séjour';
$locales['CRelancePMSI-sejour_id-court'] = 'Séjour';
$locales['CRelancePMSI-sejour_id-desc'] = 'Séjour associé';
$locales['CRelancePMSI-title-create'] = 'Création d\'une relance';
$locales['CRelancePMSI-title-modify'] = 'Modification d\'une relance';
$locales['CRelancePMSI-urgence'] = 'Urgence';
$locales['CRelancePMSI-urgence-court'] = 'Urgence';
$locales['CRelancePMSI-urgence-desc'] = 'Urgence de la relance';
$locales['CRelancePMSI.all'] = 'Toutes les relances';
$locales['CRelancePMSI.none'] = 'Aucune relance';
$locales['CRelancePMSI.one'] = 'Une relance';
$locales['CRelancePMSI.urgence.'] = '-';
$locales['CRelancePMSI.urgence.normal'] = 'Normal';
$locales['CRelancePMSI.urgence.urgent'] = 'Urgent';
$locales['CSearchItem'] = 'Item de recherche';
$locales['CSearchItem-rmq'] = 'Remarques';
$locales['CSearchItem-rmq-desc'] = 'Remarques sur le document';
$locales['CSearchItem-search_class'] = 'Type';
$locales['CSearchItem-search_class-desc'] = 'Type d\'un item de recherche';
$locales['CSearchItem-search_id'] = 'identifiant';
$locales['CSearchItem-search_id-desc'] = 'identifiant d\'un item de recherche';
$locales['CSejour-back-relance'] = 'Relances';
$locales['CSejour-back-relance.empty'] = 'Aucune relance';
$locales['CSejour-back-traitement_dossier'] = 'Séjour traité dans le dossier';
$locales['CSejour-back-traitement_dossier.empty'] = 'Pas de séjour traité dans le dossier';
$locales['CTraitementDossier'] = 'Traitement du dossier';
$locales['CTraitementDossier-GHS'] = 'GHS';
$locales['CTraitementDossier-GHS-court'] = 'Groupement homogÎ¸ne de séjours';
$locales['CTraitementDossier-GHS-desc'] = 'Groupement homogÎ¸ne de séjours';
$locales['CTraitementDossier-Sortie'] = 'Sorties';
$locales['CTraitementDossier-Sortie-short'] = 'Sort.';
$locales['CTraitementDossier-Traitement'] = 'Dossiers traités';
$locales['CTraitementDossier-Traitement-short'] = 'trait.';
$locales['CTraitementDossier-Validate'] = 'Dossiers validés';
$locales['CTraitementDossier-Validate-short'] = 'valid.';
$locales['CTraitementDossier-dim_id'] = 'Id DIM';
$locales['CTraitementDossier-dim_id-court'] = 'Identifiant du DIM';
$locales['CTraitementDossier-dim_id-desc'] = 'Identifiant du directeur de l\'information médicale';
$locales['CTraitementDossier-failed-dossier'] = 'Echec du traitement du dossier';
$locales['CTraitementDossier-msg-create'] = 'Traitement du dossier effectué';
$locales['CTraitementDossier-msg-delete'] = 'Suppression du traitement du dossier';
$locales['CTraitementDossier-msg-modify'] = 'Modification du traitement du dossier';
$locales['CTraitementDossier-rss_id'] = 'Id rss';
$locales['CTraitementDossier-rss_id-court'] = 'Identifiant du rss';
$locales['CTraitementDossier-rss_id-desc'] = 'Identifiant du résumé standardisé de séjour';
$locales['CTraitementDossier-sejour_id'] = 'Id séjour';
$locales['CTraitementDossier-sejour_id-court'] = 'Identifiant';
$locales['CTraitementDossier-sejour_id-desc'] = 'Identifiant du séjour';
$locales['CTraitementDossier-title-create'] = 'Création du traitement du dossier';
$locales['CTraitementDossier-title-modify'] = 'Modification du traitement du dossier';
$locales['CTraitementDossier-traitement'] = 'Traitement';
$locales['CTraitementDossier-traitement-court'] = 'Traité';
$locales['CTraitementDossier-traitement-desc'] = 'Traitement du dossier PMSI';
$locales['CTraitementDossier-traitement_dossier_id'] = 'Id';
$locales['CTraitementDossier-traitement_dossier_id-court'] = 'Identifiant du dossier';
$locales['CTraitementDossier-traitement_dossier_id-desc'] = 'Identifiant du traitement du dossier';
$locales['CTraitementDossier-validate'] = 'Validation';
$locales['CTraitementDossier-validate-court'] = 'Validé';
$locales['CTraitementDossier-validate-desc'] = 'Dossier validé par le PMSI';
$locales['CTraitementDossier.all'] = 'Tous les dossiers traités';
$locales['CTraitementDossier.none'] = 'Dossier non traité';
$locales['CTraitementDossier.one'] = 'Dossier Traité';
$locales['CUniteMedicale-libelle'] = 'Libellé';
$locales['CUniteMedicale-libelle-desc'] = 'Libellé de l\'unité médicale';
$locales['CUniteMedicaleInfos'] = 'Informations relatives aux UM';
$locales['CUniteMedicaleInfos-date_effet'] = 'Date d\'effet';
$locales['CUniteMedicaleInfos-date_effet-desc'] = 'Date d\'effet de l\'autorisation medicale';
$locales['CUniteMedicaleInfos-mode_hospi'] = 'Mode hospitalisation';
$locales['CUniteMedicaleInfos-mode_hospi-desc'] = 'Mode hospitalisation de l\'unité médicale';
$locales['CUniteMedicaleInfos-nb_lits'] = 'Nombre de lits';
$locales['CUniteMedicaleInfos-nb_lits-desc'] = 'Nombre de lits autorisés pour l\'unité médicale';
$locales['CUniteMedicaleInfos-um_code'] = 'Code UM';
$locales['CUniteMedicaleInfos-um_code-desc'] = 'Code de l\'unité médicale';
$locales['GHS'] = 'GHS';
$locales['Import.CIM'] = 'Import CIM';
$locales['NDA'] = 'NDA';
$locales['PMSI'] = 'PMSI';
$locales['PMSI-Medical comment-court'] = 'Commentaire med.';
$locales['PMSI-Medical type-sejour'] = 'Type de séjour';
$locales['PMSI-PMSI Diagnostics'] = 'Diagnostics PMSI';
$locales['PMSI-Responsible doctor-court'] = 'Médecin resp.';
$locales['PMSI-action-Choose a file number directly'] = 'Choisissez directement un numero de dossier';
$locales['PMSI-cim10-atih'] = '(CIM10 - ATIH)';
$locales['PMSI-cim10-oms'] = '(CIM10 - OMS)';
$locales['PMSI-dp-desc'] = 'Diagnostic principal du séjour (OMS)';
$locales['PMSI-dr-desc'] = 'Diagnostic relié du séjour';
$locales['PMSI-export operations list title'] = 'Exporter les interventions planifiées';
$locales['PMSI-msg-Available only for UF %s'] = 'Disponible seulement pour UF %s';
$locales['PMSI-Operations list from %s to %s'] = 'Liste des interventions du %s au %s';
$locales['PMSI.CSejour disponibles'] = 'Séjours disponibles';
$locales['PMSI.Complete Dossier'] = 'Dossier complet';
$locales['PMSI.Diagnostic Associe'] = 'Diagnostics Associés Significatifs';
$locales['PMSI.Diagnostic Principal'] = 'Diagnostic Principal';
$locales['PMSI.Diagnostic Relie'] = 'Diagnostic Relié';
$locales['PMSI.Diagnostics'] = 'Diagnostics';
$locales['PMSI.Documents'] = 'Documents';
$locales['PMSI.DossierPatient'] = 'Dossier Patient';
$locales['PMSI.DossierSejour'] = 'Dossier du séjour';
$locales['PMSI.Groupage'] = 'Groupage';
$locales['PMSI.Prescription'] = 'Prescription';
$locales['PMSI.RSS'] = 'RSS';
$locales['PMSI.search dossier pmsi'] = 'Recherche de dossier';
$locales['PMSI.search fields'] = 'Champs de recherche';
$locales['Praticien'] = 'Praticien';
$locales['RSS-desc'] = 'Résumé de Sortie Standardisé';
$locales['Result'] = 'Résultat';
$locales['Services'] = 'Services';
$locales['apply_facture_hprim_intervention'] = 'Passer les factures en H\'XML';
$locales['common-Operating entree-court'] = 'Entrée';
$locales['config-dPpmsi-display'] = 'Affichage';
$locales['config-dPpmsi-display-see_recept_dossier'] = 'Afficher les onglets de réception des dossiers';
$locales['config-dPpmsi-display-see_recept_dossier-desc'] = 'Afficher les onglets de réception des dossiers';
$locales['config-dPpmsi-passage_facture'] = 'Passage du séjour en facturé';
$locales['config-dPpmsi-passage_facture-desc'] = 'Déclenchement du passage du séjour en facturé';
$locales['config-dPpmsi-passage_facture-envoi'] = 'Envoi au systÎ¸me de facturation';
$locales['config-dPpmsi-passage_facture-reception'] = 'Reception par le systÎ¸me de facturation';
$locales['config-dPpmsi-relances'] = 'Relance';
$locales['config-dPpmsi-relances-autre'] = 'Afficher le document Autre';
$locales['config-dPpmsi-relances-autre-desc'] = 'Afficher le document Autre';
$locales['config-dPpmsi-relances-cotation'] = 'Afficher le codage';
$locales['config-dPpmsi-relances-cotation-desc'] = 'Afficher le codage';
$locales['config-dPpmsi-relances-cra'] = 'Afficher le compte-rendu d\'accouchement';
$locales['config-dPpmsi-relances-cra-desc'] = 'Afficher le compte-rendu d\'accouchement';
$locales['config-dPpmsi-relances-crana'] = 'Afficher le compte-rendu d\'anapath';
$locales['config-dPpmsi-relances-crana-desc'] = 'Afficher le compte-rendu d\'anapath';
$locales['config-dPpmsi-relances-cro'] = 'Afficher le compte-rendu opératoire';
$locales['config-dPpmsi-relances-cro-desc'] = 'Afficher le compte-rendu opératoire';
$locales['config-dPpmsi-relances-ls'] = 'Afficher la lettre de sortie';
$locales['config-dPpmsi-relances-ls-desc'] = 'Afficher la lettre de sortie';
$locales['config-dPpmsi-server'] = 'Mode Serveur d\'Actes';
$locales['config-dPpmsi-server-desc'] = 'Mode Serveur d\'Actes';
$locales['config-dPpmsi-systeme_facturation'] = 'SystÎ¸me de facturation';
$locales['config-dPpmsi-systeme_facturation-'] = 'Générique';
$locales['config-dPpmsi-systeme_facturation-desc'] = 'SystÎ¸me de facturation';
$locales['config-dPpmsi-systeme_facturation-siemens'] = 'SHS';
$locales['config-dPpmsi-transmission_actes'] = 'Transmission des actes';
$locales['config-dPpmsi-transmission_actes-desc'] = 'Déclenchement transmission des actes';
$locales['config-dPpmsi-transmission_actes-pmsi'] = 'Par le PMSI';
$locales['config-dPpmsi-transmission_actes-signature'] = 'Lors de la signature';
$locales['config-dPpmsi-type_document'] = 'Type de document PMSI';
$locales['config-dPpmsi-type_document-autre'] = 'Autre';
$locales['config-dPpmsi-type_document-autre-desc'] = 'Autre (Plusieurs catégories de fichier sont sélectionnable)';
$locales['config-dPpmsi-type_document-cotation'] = 'Codage';
$locales['config-dPpmsi-type_document-cotation-desc'] = 'Codage (Plusieurs catégories de fichier sont sélectionnable)';
$locales['config-dPpmsi-type_document-cra'] = 'Compte rendu d\'accouchement';
$locales['config-dPpmsi-type_document-cra-desc'] = 'Compte rendu d\'accouchement (Plusieurs catégories de fichier sont sélectionnable)';
$locales['config-dPpmsi-type_document-crana'] = 'Compte rendu d\'anapath';
$locales['config-dPpmsi-type_document-crana-desc'] = 'Compte rendu d\'anapath (Plusieurs catégories de fichier sont sélectionnable)';
$locales['config-dPpmsi-type_document-cro'] = 'Compte rendu opératoire';
$locales['config-dPpmsi-type_document-cro-desc'] = 'Compte rendu opératoire (Plusieurs catégories de fichier sont sélectionnable)';
$locales['config-dPpmsi-type_document-ls'] = 'Lettre de liaison';
$locales['config-dPpmsi-type_document-ls-desc'] = 'Lettre de liaison (Plusieurs catégories de fichier sont sélectionnable)';
$locales['config-dPpmsi-uf'] = 'Unité Fonctionnelle';
$locales['config-dPpmsi-uf-uf_pmsi'] = 'Choix du type d\'unité fonctionnelle';
$locales['config-dPpmsi-uf-uf_pmsi-desc'] = 'Choix du type d\'unité fonctionnelle pour connaitre l\'unité médicale PMSI';
$locales['config-dPpmsi-use_cim_pmsi'] = 'Utilisation de la nomenclature CIM10';
$locales['config-dPpmsi-use_cim_pmsi-desc'] = 'Utilisation de la nomenclature CIM10 Î° usage du pmsi';
$locales['config_atih_um'] = 'Configuration des Unités Médicales';
$locales['config_facture_hprim'] = 'Facture H\'XML';
$locales['mod-dPpmsi-tab-ajax_actes_ccam_pmsi'] = 'Actes CCAM';
$locales['mod-dPpmsi-tab-ajax_cotations_ngap'] = 'Codages NGAP';
$locales['mod-dPpmsi-tab-ajax_current_dossiers'] = 'Dossiers en cours';
$locales['mod-dPpmsi-tab-ajax_current_operations'] = 'Interventions en cours';
$locales['mod-dPpmsi-tab-ajax_current_sejours'] = 'Séjours en cours';
$locales['mod-dPpmsi-tab-ajax_current_urgences'] = 'Hors plages en cours';
$locales['mod-dPpmsi-tab-ajax_diags_dossier'] = 'Diagnostics du dossier';
$locales['mod-dPpmsi-tab-ajax_diags_pmsi'] = 'Diagnostics pmsi';
$locales['mod-dPpmsi-tab-ajax_edit_relance'] = 'Création / Modification de relance';
$locales['mod-dPpmsi-tab-ajax_graph_delai_facturation'] = 'Graphique du délai de facturation';
$locales['mod-dPpmsi-tab-ajax_graph_nbDossiersSeries'] = 'Graphique du nombre de dossiers';
$locales['mod-dPpmsi-tab-ajax_import_cim_pmsi'] = 'Import de la CIM10 Î° usage PMSI';
$locales['mod-dPpmsi-tab-ajax_print_planning'] = 'Impression planning';
$locales['mod-dPpmsi-tab-ajax_recept_dossier_line'] = 'Dossier receptionné';
$locales['mod-dPpmsi-tab-ajax_recept_dossiers_legende'] = 'Légende';
$locales['mod-dPpmsi-tab-ajax_recept_dossiers_lines'] = 'Dossiers réceptionnés';
$locales['mod-dPpmsi-tab-ajax_recept_dossiers_month'] = 'Dossiers réceptionnés par mois';
$locales['mod-dPpmsi-tab-ajax_search_nomenclature_cim10'] = 'Recherche CIM10';
$locales['mod-dPpmsi-tab-ajax_search_relances'] = 'Relances';
$locales['mod-dPpmsi-tab-ajax_seek_cim10_pmsi'] = 'Recherche CIM10 Î° usage PMSI';
$locales['mod-dPpmsi-tab-ajax_select_services'] = 'Sélection de services';
$locales['mod-dPpmsi-tab-ajax_sortie_line'] = 'Sortie';
$locales['mod-dPpmsi-tab-ajax_traitement_dossier_line'] = 'Dossier traité';
$locales['mod-dPpmsi-tab-ajax_traitement_dossiers_lines'] = 'Dossiers traités';
$locales['mod-dPpmsi-tab-ajax_traitement_dossiers_month'] = 'Dossiers traités par mois';
$locales['mod-dPpmsi-tab-ajax_view_actes_pmsi'] = 'Vue des actes pmsi';
$locales['mod-dPpmsi-tab-ajax_view_diagnostics_pmsi'] = 'Vue des diagnostics pmsi';
$locales['mod-dPpmsi-tab-ajax_view_export_actes'] = 'Vue d\'export des actes';
$locales['mod-dPpmsi-tab-ajax_view_patient_pmsi'] = 'Vue du patient';
$locales['mod-dPpmsi-tab-viewDossierSejour'] = 'Vue du dossier du séjour';
$locales['mod-dPpmsi-tab-ajax_vw_sejour'] = 'Vue du séjour';
$locales['mod-dPpmsi-tab-configure'] = 'Administrer';
$locales['mod-dPpmsi-tab-export_actes_pmsi'] = 'Export des événements du serveur d\'activité PMSI';
$locales['mod-dPpmsi-tab-exportCsvPlannedOperations'] = 'Export des interventions planifiées';
$locales['PMSI-export-operation-error-invalid_period'] = 'La période selectionnée est incorrecte';
$locales['mod-dPpmsi-tab-vw_cim10_explorer'] = 'Nomenclature CIM10 (PMSI)';
$locales['mod-dPpmsi-tab-vw_cotations'] = 'Etat des codages';
$locales['mod-dPpmsi-tab-vw_cotations_ngap'] = 'Codages NGAP';
$locales['mod-dPpmsi-tab-vw_current_dossiers'] = 'Dossiers en cours';
$locales['mod-dPpmsi-tab-vw_dossier_pmsi'] = 'Dossier PMSI';
$locales['mod-dPpmsi-tab-vw_ghs_explorer'] = 'Nomenclature GHS';
$locales['mod-dPpmsi-tab-vw_idx_sortie'] = 'Sorties du jour';
$locales['mod-dPpmsi-tab-vw_list_hospi'] = 'Liste des hospitalisations';
$locales['mod-dPpmsi-tab-vw_list_interv'] = 'Liste des interventions';
$locales['mod-dPpmsi-tab-vw_print_planning'] = 'Impression planning';
$locales['mod-dPpmsi-tab-vw_recept_dossiers'] = 'Réception des dossiers';
$locales['mod-dPpmsi-tab-vw_reception_multiple'] = 'Réception multiple';
$locales['mod-dPpmsi-tab-vw_relances'] = 'Relances';
$locales['mod-dPpmsi-tab-vw_traitement_dossiers'] = 'Traitement des dossiers';
$locales['mod-dPsearch-tab-vw_search_item'] = 'Ajout/Edition d\'un item de recherche';
$locales['mod-pmsi-tab-ajax_codage_credentials'] = 'Gestion des autorisations de codage';
$locales['mod-pmsi-tab-ajax_cotation_operations_details'] = 'Détails des interventions';
$locales['mod-pmsi-tab-ajax_recept_dossiers_legende'] = 'Légende';
$locales['mod-pmsi-tab-ajax_select_services'] = 'Sélection de services';
$locales['mod-search-tab-vw_search_item'] = 'Ajout/Edition d\'un item de recherche';
$locales['module-dPpmsi-court'] = 'PMSI';
$locales['module-dPpmsi-long'] = 'Recueil des actes PMSI';
$locales['module-dPpmsi-trigramme'] = 'PMSI';
$locales['pmsi-2_relance'] = '2Î¸me relance';
$locales['pmsi-action-Cancel without folder'] = 'Annuler sans dossier';
$locales['pmsi-action-Without folder'] = 'Sans dossier';
$locales['pmsi-action-receipt_multiple_sejour-error'] = 'ProblÎ¸me lors de la réception des séjours';
$locales['pmsi-action-receipt_multiple_sejour-success'] = 'Réception des séjours validée';
$locales['pmsi-create_relance'] = 'Créer une relance';
$locales['pmsi-edit_relance'] = 'Modifier la relance';
$locales['pmsi-title-details_cotation'] = 'Détails des codages de ';
$locales['pmsi-title-details_cotation-created_by_anesth'] = 'Actes codés par l\'anesth';
$locales['pmsi-title-details_cotation-created_by_chir'] = 'Actes codés par le chir';
$locales['pmsi-title-details_cotation-unexported_acts'] = 'Actes non exportés';
$locales['pmsi-title-stats_cotation.1_2'] = 'De 1 Î° 2 jours';
$locales['pmsi-title-stats_cotation.3_7'] = 'De 3 Î° 7 jours';
$locales['pmsi-title-stats_cotation.8_30'] = 'De 8 Î° 30 jours';
$locales['pmsi-title-stats_cotation.31_60'] = 'De 31 Î° 60 jours';
$locales['pmsi-title-stats_cotation.61_120'] = 'De 61 Î° 120 jours';
$locales['pmsi-title-stats_cotation.121_202'] = 'De 121 Î° 202 jours';
$locales['pmsi-title-stats_cotation.203'] = 'Plus de 203 jours';
$locales['pmsi-title-stats_cotation.no_codes'] = 'SC';
$locales['pmsi-title-stats_cotation.no_codes-desc'] = 'Nombre d\'interventions sans code CCAM pour la période';
$locales['pmsi-title-stats_cotation.number_uncreated_acts'] = 'Nb<br/>NC';
$locales['pmsi-title-stats_cotation.number_uncreated_acts-desc'] = 'Nombre d\'actes non cotés pour la période';
$locales['pmsi-title-stats_cotation.number_unexported_acts'] = 'Nb<br/>NE';
$locales['pmsi-title-stats_cotation.number_unexported_acts-desc'] = 'Nombre d\'actes non exportés pour la période';
$locales['pmsi-title-stats_cotation.total'] = 'Total';
$locales['pmsi-title-stats_cotation.total_uncreated_acts'] = 'Prix<br/>NC';
$locales['pmsi-title-stats_cotation.total_uncreated_acts-desc'] = 'Prix total des actes non cotés pour la période';
$locales['pmsi-title-stats_cotation.total_unexported_acts'] = 'Prix<br/>NE';
$locales['pmsi-title-stats_cotation.total_unexported_acts-desc'] = 'Prix total des actes non exportés pour la période';
