<?php

/**
 * @package Mediboard\dPcim10
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Ccam;

use Ox\Core\Module\CAbstractModuleCache;

/**
 * Description
 */
class CModuleCacheCCAM extends CAbstractModuleCache
{
    public $module = 'dPccam';

    protected $shm_patterns = [
        'CCodeCCAM',
        'COldCodeCCAM',
        'CDatedCodeCCAM',
        'CDentCCAM',
        'CCodeNGAP',
        'CCCAM',
        'CActiviteModificateurCCAM',
        'CActiviteCCAM',
        'CActiviteClassifCCAM',
        'CInfoTarifCCAM',
    ];
}
