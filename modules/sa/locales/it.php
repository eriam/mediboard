<?php
$locales['CSaEventObjectHandler'] = 'Gestionnaire évÎ¸nement SA';
$locales['CSaObjectHandler'] = 'Gestionnaire SA';
$locales['CSaObjectHandler-send_only_with_ipp_nda'] = 'L\'IPP et/ou le numéro de dossier (NDA) sont manquants pour l\'envoi d\'actes en facturation';
$locales['CSaObjectHandler-send_only_with_type'] = 'Le type du séjour \'%s\' n\'est pas pris en charge pour un envoi d\'actes';
$locales['SA'] = 'Serveur d\'activité PMSI';
$locales['config-object_handlers-CSaEventObjectHandler'] = 'Activer le gestionnaire SA évÎ¸nement';
$locales['config-object_handlers-CSaEventObjectHandler-desc'] = 'Activer le gestionnaire d\'événement  (intervention, consultation, ...)';
$locales['config-object_handlers-CSaObjectHandler'] = 'Activer le gestionnaire SA';
$locales['config-object_handlers-CSaObjectHandler-desc'] = 'Activer le gestionnaire du serveur d\'actes des séjours, consultations, interventions, etc...';
$locales['config-object_server-CSaObjectHandler'] = 'Serveur d\'objets SA';
$locales['config-sa'] = 'Configuration mode SA';
$locales['config-sa-CSa'] = 'Traitement du mode SA';
$locales['config-sa-CSa-facture_codable_with_sejour'] = 'Passage des évÎ¸nements Î° facturé lors de l\'envoi des actes';
$locales['config-sa-CSa-facture_codable_with_sejour-desc'] = 'Passage des séjours, interventions et consultations Î° facturé lors de l\'envoi des actes depuis un séjour';
$locales['config-sa-CSa-send_acte_immediately'] = 'Envoyer les actes au fil de l\'eau';
$locales['config-sa-CSa-send_diag_immediately'] = 'Envoyer les diags au fil de l\'eau';
$locales['config-sa-CSa-send_diag_immediately-desc'] = 'Envoyer les diags au fil de l\'eau';
$locales['config-sa-CSa-send_acte_immediately-desc'] = 'Envoyer les actes au fil de l\'eau';
$locales['config-sa-CSa-send_actes_consult'] = 'Envoyer les actes des consults du séjour';
$locales['config-sa-CSa-send_actes_consult-desc'] = 'Envoyer les actes des consultations du séjour';
$locales['config-sa-CSa-send_actes_consult_anesth'] = 'Envoyer les actes des consults. anesth. du séjour';
$locales['config-sa-CSa-send_actes_consult_anesth-desc'] = 'Envoyer les actes des consultations d\'anesthétésie du séjour';
$locales['config-sa-CSa-send_actes_interv'] = 'Envoyer les actes des intervention du séjour';
$locales['config-sa-CSa-send_actes_interv-desc'] = 'Envoyer les actes des intervention du séjour';
$locales['config-sa-CSa-send_diags_with_actes'] = 'Envoyer les diags avec les actes';
$locales['config-sa-CSa-send_diags_with_actes-desc'] = 'Envoyer les diags avec les actes';
$locales['config-sa-CSa-send_igs_immediately'] = 'Envoyer le score IGS au fil de l\'eau';
$locales['config-sa-CSa-send_igs_immediately-desc'] = 'Envoyer le score IGS au fil de l\'eau';
$locales['config-sa-CSa-send_only_with_ipp_nda'] = 'Envoyer les actes ssi IPP et NDA';
$locales['config-sa-CSa-send_only_with_ipp_nda-desc'] = 'Envoyer les actes si et seulement si IPP et NDA';
$locales['config-sa-CSa-send_only_with_type'] = 'Envoyer les actes d\'un type de séjour';
$locales['config-sa-CSa-send_only_with_type-'] = 'Tous';
$locales['config-sa-CSa-send_only_with_type-ambu'] = 'Ambulatoire';
$locales['config-sa-CSa-send_only_with_type-comp'] = 'Hospi. complÎ¸te';
$locales['config-sa-CSa-send_only_with_type-consult'] = 'Consultation';
$locales['config-sa-CSa-send_only_with_type-desc'] = 'Envoyer les actes d\'un type de séjour';
$locales['config-sa-CSa-send_only_with_type-exte'] = 'Externe';
$locales['config-sa-CSa-send_only_with_type-psy'] = 'Psychiatrie';
$locales['config-sa-CSa-send_only_with_type-seances'] = 'Séance(s)';
$locales['config-sa-CSa-send_only_with_type-ssr'] = 'SSR';
$locales['config-sa-CSa-send_only_with_type-urg'] = 'Passage aux urgences';
$locales['config-sa-CSa-send_rhs'] = 'Envoyer les RHS';
$locales['config-sa-CSa-send_rhs-desc'] = 'Envoyer les RHS (dépendances, actes de réeducation, ...)';
$locales['config-sa-CSa-trigger_consultation'] = 'Trigger sur la consultation';
$locales['config-sa-CSa-trigger_consultation-desc'] = 'Trigger sur la consultation';
$locales['config-sa-CSa-trigger_consultation-facture'] = 'Facturée';
$locales['config-sa-CSa-trigger_consultation-sortie_reelle'] = 'Sortie réelle du séjour';
$locales['config-sa-CSa-trigger_consultation-valide'] = 'Validée';
$locales['config-sa-CSa-trigger_consultation.cloture'] = 'ClÏ„ture';
$locales['config-sa-CSa-trigger_consultation.facture'] = 'Facturée';
$locales['config-sa-CSa-trigger_consultation.sortie_reelle'] = 'Sortie réelle';
$locales['config-sa-CSa-trigger_consultation.valide'] = 'Validée';
$locales['config-sa-CSa-trigger_consultationsortie_reelle'] = 'Sortie réelle du séjour';
$locales['config-sa-CSa-trigger_operation'] = 'Trigger sur l\'intervention';
$locales['config-sa-CSa-trigger_operation-desc'] = 'Trigger sur l\'intervention';
$locales['config-sa-CSa-trigger_operation-facture'] = 'Facturée';
$locales['config-sa-CSa-trigger_operation-sortie_reelle'] = 'Sortie réelle du séjour';
$locales['config-sa-CSa-trigger_operation-testCloture'] = 'Actes clÏ„turés';
$locales['config-sa-CSa-trigger_operation.facture'] = 'Facturée';
$locales['config-sa-CSa-trigger_operation.sortie_reelle'] = 'Sortie réelle du séjour';
$locales['config-sa-CSa-trigger_operation.testCloture'] = 'Actes clÏ„turés';
$locales['config-sa-CSa-trigger_sejour'] = 'Trigger sur le séjour';
$locales['config-sa-CSa-trigger_sejour-desc'] = 'Trigger sur le séjour';
$locales['config-sa-CSa-trigger_sejour-facture'] = 'Facturé';
$locales['config-sa-CSa-trigger_sejour-sortie_reelle'] = 'Sortie réelle';
$locales['config-sa-CSa-trigger_sejour-testCloture'] = 'Actes clÏ„turés';
$locales['config-sa-CSa-trigger_sejour.facture'] = 'Facturé';
$locales['config-sa-CSa-trigger_sejour.sortie_reelle'] = 'Sortie réelle';
$locales['config-sa-CSa-trigger_sejour.testCloture'] = 'Actes clÏ„turés';
$locales['config-sa-server'] = 'Mode Serveur';
$locales['config-sa-server-desc'] = 'Mode Serveur';
$locales['config-traitement-sa'] = 'Traitement du mode SA';
$locales['mod-sa-tab-configure'] = 'Configuration';
$locales['module-sa-court'] = 'SA';
$locales['module-sa-long'] = 'Serveur d\'actes';
