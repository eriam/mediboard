<?php
$locales['config-maidis-plage_consult_end_hour'] = 'Heure de fin des plages de consultation (H:i:s)';
$locales['config-maidis-plage_consult_end_hour-desc'] = 'Heure de fin des plages de consultation (H:i:s)';
$locales['config-maidis-plage_consult_start_hour'] = 'Heure de début des plages de consultation (H:i:s)';
$locales['config-maidis-plage_consult_start_hour-desc'] = 'Heure de début des plages de consultation (H:i:s)';
$locales['mod-import-type-solde_patient'] = 'Solde patient';
$locales['mod-maidis-tab-vw_import_fw'] = 'Importation (FW)';
$locales['mod-maidis-tab-vw_users_fw'] = 'Utilisateurs (FW)';
$locales['module-maidis-court'] = 'Maidis';
$locales['module-maidis-long'] = 'Maidis';
