<?php
$locales['CContext-rpps-unavailable'] = 'RPPS %s inconnu de ce contexte';
$locales['CContext-user_undefined'] = 'Utilisateur \'%s\' non reconnu';
$locales['CContextIntegration-title-create'] = 'Nouvelle intégration';
$locales['CContextualIntegration'] = 'Intégration contextuelle';
$locales['CContextualIntegration-active'] = 'Active';
$locales['CContextualIntegration-active-court'] = 'Active';
$locales['CContextualIntegration-active-desc'] = 'Active';
$locales['CContextualIntegration-back-integration_locations'] = 'Emplacements';
$locales['CContextualIntegration-back-integration_locations.empty'] = 'Aucun emplacement';
$locales['CContextualIntegration-contextual_integration_id'] = 'Id';
$locales['CContextualIntegration-contextual_integration_id-court'] = 'Id';
$locales['CContextualIntegration-contextual_integration_id-desc'] = 'Identifiant';
$locales['CContextualIntegration-description'] = 'Description';
$locales['CContextualIntegration-description-court'] = 'Description';
$locales['CContextualIntegration-description-desc'] = 'Description';
$locales['CContextualIntegration-display_mode'] = 'Mode d\'affichage';
$locales['CContextualIntegration-display_mode-court'] = 'Mode d\'affichage';
$locales['CContextualIntegration-display_mode-desc'] = 'Mode d\'affichage';
$locales['CContextualIntegration-group_id'] = 'Etablissement';
$locales['CContextualIntegration-group_id-court'] = 'Etab';
$locales['CContextualIntegration-group_id-desc'] = 'Etablissement';
$locales['CContextualIntegration-icon_url'] = 'IcÏ„ne';
$locales['CContextualIntegration-icon_url-court'] = 'IcÏ„ne';
$locales['CContextualIntegration-icon_url-desc'] = 'IcÏ„ne';
$locales['CContextualIntegration-msg-create'] = 'Intégration créée';
$locales['CContextualIntegration-msg-delete'] = 'Intégration supprimée';
$locales['CContextualIntegration-msg-modify'] = 'Intégration modifiée';
$locales['CContextualIntegration-title'] = 'Titre';
$locales['CContextualIntegration-title-court'] = 'Titre';
$locales['CContextualIntegration-title-create'] = 'Nouvelle intégration';
$locales['CContextualIntegration-title-desc'] = 'Titre';
$locales['CContextualIntegration-title-modify'] = 'Modifier l\'intégration';
$locales['CContextualIntegration-url'] = 'URL';
$locales['CContextualIntegration-url-court'] = 'URL';
$locales['CContextualIntegration-url-desc'] = 'URL';
$locales['CContextualIntegration.all'] = 'Toutes les intégrations';
$locales['CContextualIntegration.display_mode.'] = '-';
$locales['CContextualIntegration.display_mode.current_tab'] = 'Onglet actuel';
$locales['CContextualIntegration.display_mode.modal'] = 'FenÎºtre modale';
$locales['CContextualIntegration.display_mode.new_tab'] = 'Nouvel onglet';
$locales['CContextualIntegration.display_mode.none'] = 'Ne pas afficher';
$locales['CContextualIntegration.display_mode.popup'] = 'FenÎºtre popup';
$locales['CContextualIntegration.display_mode.tooltip'] = 'Infobulle';
$locales['CContextualIntegration.none'] = 'Aucune intégration';
$locales['CContextualIntegration.one'] = 'Une intégration';
$locales['CContextualIntegration.pattern.ip'] = 'Adresse IP utilisateur';
$locales['CContextualIntegration.pattern.ipp'] = 'IPP';
$locales['CContextualIntegration.pattern.nda'] = 'NDA';
$locales['CContextualIntegration.pattern.user'] = 'Login utilisateur';
$locales['CContextualIntegrationLocation'] = 'Emplacement';
$locales['CContextualIntegrationLocation-button_type'] = 'Type de bouton';
$locales['CContextualIntegrationLocation-button_type-court'] = 'Type de bouton';
$locales['CContextualIntegrationLocation-button_type-desc'] = 'Type de bouton';
$locales['CContextualIntegrationLocation-contextual_integration_location_id'] = 'Id';
$locales['CContextualIntegrationLocation-contextual_integration_location_id-court'] = 'Id';
$locales['CContextualIntegrationLocation-contextual_integration_location_id-desc'] = 'Identifiant';
$locales['CContextualIntegrationLocation-integration_id'] = 'Intégration contextuelle';
$locales['CContextualIntegrationLocation-integration_id-court'] = 'Intégration';
$locales['CContextualIntegrationLocation-integration_id-desc'] = 'Intégration contextuelle';
$locales['CContextualIntegrationLocation-location'] = 'Emplacement';
$locales['CContextualIntegrationLocation-location-court'] = 'Emplacement';
$locales['CContextualIntegrationLocation-location-desc'] = 'Emplacement';
$locales['CContextualIntegrationLocation-msg-create'] = 'Emplacement créé';
$locales['CContextualIntegrationLocation-msg-delete'] = 'Emplacement supprimé';
$locales['CContextualIntegrationLocation-msg-modify'] = 'Emplacement modifié';
$locales['CContextualIntegrationLocation-title-create'] = 'Nouvel emplacement';
$locales['CContextualIntegrationLocation-title-modify'] = 'Modifier l\'emplacement';
$locales['CContextualIntegrationLocation.all'] = 'Tous les emplacements';
$locales['CContextualIntegrationLocation.button_type.button'] = 'Bouton';
$locales['CContextualIntegrationLocation.button_type.button_text'] = 'Bouton avec texte';
$locales['CContextualIntegrationLocation.button_type.icon'] = 'IcÏ„ne';
$locales['CContextualIntegrationLocation.location.help'] = 'Aide';
$locales['CContextualIntegrationLocation.location.patient_header'] = 'Bandeau patient';
$locales['CContextualIntegrationLocation.none'] = 'Aucun emplacement';
$locales['CContextualIntegrationLocation.one'] = 'Un emplacement';
$locales['CGroups-back-contextual_integrations'] = 'Intégrations contextuelles';
$locales['CGroups-back-contextual_integrations.empty'] = 'Aucune intégration contextuelle';
$locales['config-context-General'] = 'Général';
$locales['config-context-General-token_lifetime'] = 'Durée de vie';
$locales['config-context-General-token_lifetime-desc'] = 'Durée de vie des jetons de connexion transmis Î° l\'application tierce';
$locales['config-context-General-token_lifetime.'] = 'Param. serveur';
$locales['config-context-General-token_lifetime.10'] = '10 minutes';
$locales['config-context-General-token_lifetime.15'] = '15 minutes';
$locales['config-context-General-token_lifetime.20'] = '20 minutes';
$locales['config-context-General-token_lifetime.25'] = '25 minutes';
$locales['config-context-General-token_lifetime.30'] = '30 minutes';
$locales['config-context-General-token_lifetime.45'] = '45 minutes';
$locales['config-context-General-token_lifetime.60'] = '1 heure';
$locales['config-context-General-token_lifetime.120'] = '2 heures';
$locales['config-context-General-token_lifetime.180'] = '3 heures';
$locales['config-context-General-token_lifetime.240'] = '4 heures';
$locales['config-context-General-token_lifetime.300'] = '5 heures';
$locales['context-module%s-not-activated'] = 'Erreur, Le module %s doit Îºtre installé et actif.';
$locales['context-multiple-patient'] = '%d patients vérifient vos critÎ¸res. Affinez votre recherche (prenom, nom, date de naissance)';
$locales['context-multiple-sejour'] = 'Plusieurs séjours ont été trouvés en suivant vos critÎ¸res, essayer avec un NDA';
$locales['context-nda-or-PatientPlusDate-required'] = 'Pour trouver un sejour, il faut soit un numéro NDA soit un patient ET une date de séjour';
$locales['context-nda-required'] = 'La vue "%s" requiÎ¸re un numéro de dossier administratif ($nda)';
$locales['context-non-existing-sejour-nda'] = 'Aucun séjour trouvé Î° partir du numéro NDA [%s]';
$locales['context-none-patient'] = 'Aucun patient n\'a été trouvé suivant les critÎ¸res';
$locales['context-none-sejour'] = 'Aucun séjour répondant Î° vos critÎ¸res';
$locales['context-nonexisting-labo-id'] = 'Pas de numéro de séjour spécifié. Vous devez définir un numéro de séjour ($nda)';
$locales['context-nonexisting-patient'] = 'Impossible de trouver le patient avec les éléments donnés';
$locales['context-nonexisting-patient-ipp'] = 'Aucun patient n\'a l\'IPP [%s], patient introuvable';
$locales['context-nonexisting-patient-ipp%s'] = 'Erreur : l\'IPP [%d] n\'est lié Î° aucun patient.';
$locales['context-sejour-patientOK-date-required'] = 'Pour trouver un séjour, vous devez définir le paramÎ¸tre "admit_date" en plus du patient';
$locales['context-view_not-registered'] = 'Erreur, la vue "%s" n\'existe pas.';
$locales['context-view_required'] = 'Erreur : Vue non définie. Une vue est requise dans les paramÎ¸tres ($view)';
$locales['mod-context-tab-ajax_edit_integration'] = 'Modification intégration';
$locales['mod-context-tab-ajax_edit_integration_location'] = 'Emplacement d\'intégration';
$locales['mod-context-tab-ajax_list_integrations'] = 'Liste des intégrations';
$locales['mod-context-tab-ajax_widget_integration'] = 'Intégration';
$locales['mod-context-tab-call'] = 'requÎºte contextuelle';
$locales['mod-context-tab-configure'] = 'Configurer';
$locales['mod-context-tab-tokenize'] = 'Tokenizer messages contextuels';
$locales['mod-context-tab-vw_doc'] = 'Documentation';
$locales['mod-context-tab-vw_expose'] = 'Exposition';
$locales['mod-context-tab-vw_helps'] = 'Aides';
$locales['mod-context-tab-vw_integrations'] = 'Intégration';
$locales['mod-context-tab-vw_tests'] = 'RequÎºtes de test';
$locales['mod-dPcontext-tab-vw_helps'] = 'Aides';
$locales['module-context-court'] = 'Affichage contextuel';
$locales['module-context-long'] = 'Affichage contextuel';
$locales['CContextualIntegrationLocation.action'] = 'Action';
$locales['CContextualIntegrationLocation.value_url'] = 'Insertion de valeurs dans l\'URL';
