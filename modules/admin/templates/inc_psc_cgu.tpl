{{*
 * @package Mediboard\Admin
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<style>
  {{mb_include module=admin template='/../css/psc.css'}}
</style>

<div class="cgu_main_container">
  <h1 class="cgu">Conditions Générales d'Utilisation de PRO Santé Connect</h1>
  <p class="cgu_version"><strong>Version du 08/04/2020</strong></p>

  <h2>Identification électronique par Pro Santé Connect</h2>
  <p>
    Pro Santé Connect est un téléservice mis en oeuvre par l'Agence du Numérique en Santé (ANS) contribuant à simplifier
    l'identification électronique des professionnels intervenant en santé. 
    L'utilisateur peut se connecter grâce à son application mobile e-CPS ou sa carte CPS, 
    avec un lecteur de cartes et les composants nécessaires. 
  </p>
  
  <p>
    Consulter les conditions générales d'utilisation de Pro Santé Connect sur le site
    <a href="https://integrateurscps.asipsante.fr/pages/prosanteconnect/cgu" target="_blank">
      https://integrateurscps.asipsante.fr/pages/prosanteconnect/cgu</a>.
  </p>

  <h2>Mentions légales</h2>
  <p>
    Le service PRO Santé Connect est édité par l'Agence du Numérique en Santé (ANS), Groupement d'intérêt public prévu à
    l'article L.1111-24 du code de santé publique, dont le siège social est situé au 9, rue Georges Pitard - 75015 PARIS
    et dont le numéro de SIREN est : 187 512 751 et le numéro de téléphone est : 01 58 45 32 50.
  </p>

  <p>
    La directrice de la publication est Madame Annie PREVOT, Directrice de l'ANS.
  </p>

  <p>
    Le service PRO Santé connect est hébergé par IN Groupe, dont le siège social est situé 104 avenue du Président Kennedy, 75016, Paris,
    enregistrée au RCS de Paris sous le numéro 352 973 622.
  </p>

  <p>
    Numéro de téléphone : 01 40 58 30 00 (standard).
  </p>

  <h2>Article 1. Objet</h2>
  <p>
    Le service PRO Santé connect, fournisseur d'identité, permet aux fournisseurs de services numériques à destination des
    professionnels de santé de déléguer l'authentification des utilisateurs finaux de leurs services.
  </p>
  <p>
    Le présent document a pour objet de définir les conditions d'utilisation du service PRO santé connect par les fournisseurs
    de services et les relations entre l'ANS et les fournisseurs de services.
  </p>

  <h2>Article 2. Accès et fonctionnement du service</h2>
  <p>
    La demande d'utilisation de PRO Santé connect se fait auprès de l'ANS par le biais d'un formulaire en ligne.
  </p>
  <p>
    L'ANS vérifie que l'objet du service proposé par le fournisseur de service est conforme aux présentes conditions
    générales d'utilisation.
  </p>
  <p>
    Lorsque la demande est validée, l'ANS envoie au fournisseur de service un moyen d'authentification lui permettant de
    déléguer l'authentification de ses utilisateurs.
  </p>
  <p>
    Lorsque sa demande est acceptée par l'ANS, le fournisseur de service peut accéder :
  <ul>
    <li>
      au bac à sable afin de réaliser les tests nécessaires à l'intégration de PRO Santé connect à son service ;
    </li>
    <li>
      à l'espace production afin de réaliser l'intégration de PRO Santé connect à son service.
    </li>
  </ul>
  </p>
  <p>
    Le fournisseur de service peut également demander à bénéficier d'une assistance technique de la part de l'ANS par le
    biais d'un formulaire en ligne.
  </p>

  <h2>Article 3. Engagements du fournisseur de services</h2>
  <p>
    L'utilisation de PRO Santé connect implique le respect de la législation applicable et des présentes conditions
    d'utilisation.
  </p>
  <p>
    Le fournisseur de service s'engage à ne pas porter atteinte ou tenter de porter atteinte à l'intégrité,
    au fonctionnement ou à la sécurité du système d'information à la base du fonctionnement de PRO Santé connect.
  </p>
  <p>
    Le fournisseur de service s'engage à utiliser le jeton transmis par l'ANS uniquement pour l'authentification de
    l'utilisateur final au service qu'il fournit. Le fournisseur de service s'engage à traiter les données à caractère
    personnel contenues dans le jeton conformément à la législation applicable et notamment, le RGPD, la loi n°78-17 du
    6 janvier 1978 et l'arrêté RPPS du 6 février 2009.
  </p>
  <p>
    En cas de manquement du fournisseur de service aux présentes conditions générales d'utilisation ou à la législation applicable,
    l'ANS se réserve le droit, sans préavis ni mise en demeure, de suspendre unilatéralement, temporairement ou définitivement
    le compte du fournisseur de services à l'origine du manquement.
  </p>
  <h2>Article 4. Engagements de l'ANS</h2>
  <p>
    L'ANS met tout en oeuvre pour assurer un fonctionnement régulier et une disponibilité permanente de PRO Santé connect.
    Celui-ci est néanmoins mis à disposition du fournisseur de service « en l'état », sans garantie de l'absence d'erreurs,
    de périodes d'indisponibilité, de failles ou de défauts.
  </p>
  <p>
    L'ANS ne pourra en aucun cas être tenue pour responsable :
  <ul>
    <li>
      de toute interruption ou restriction d'accès à PRO Santé connect, du fait d'opérations de maintenance,
      de mises à niveau ou de modification de PRO Santé connect ;
    </li>
    <li>
      des éventuels préjudices indirects qui seraient subis par le fournisseur de service ou les professionnels utilisant ses services ;
    </li>
    <li>
      de tout préjudice subi par un fournisseur de services ou un tiers qui résulterait d'une utilisation de PRO Santé
      connect non conforme aux présentes conditions générales d'utilisation ;
    </li>
    <li>
      de tout manquement à ses obligations prévues aux présentes conditions générales d'utilisation qui serait dû ou
      trouverait son origine dans un cas fortuit ou un cas de force majeure tel que défini par la jurisprudence.
      Sont également considérés comme des cas de force majeure les grèves totales ou partielles, internes ou externes à l'ANS,
      ainsi que le blocage et/ou les dysfonctionnements des réseaux de télécommunications, d'électricité ou informatiques
      dès lors que ces dysfonctionnements n'ont pas pour origine les moyens techniques mis en oeuvre par l'ANS et ne relèvent
      pas de sa responsabilité.
    </li>
  </ul>
  </p>

  <h2>Article 5. Protection des données à caractère personnel</h2>
  <p>
    Le fournisseur de service a la qualité de responsable de traitement au sens de la loi n°78-17 du 6 janvier 1978, relative
    à l'informatique, aux fichiers et aux libertés et est tenu à ce titre de réaliser les formalités préalables à la mise en
    oeuvre d'un traitement de données à caractère personnel (ex. tenue du registre des traitements, réalisation d'un analyse
    d'impact sur la protection des données, etc.).
  </p>
  <p>
    Le fournisseur de service s'engage également à assurer la sécurité des données des utilisateurs finaux et à les utiliser
    uniquement pour l'authentification des utilisateurs finaux au service qu'il fournit.
  </p>
  <p>
    Il appartient au fournisseur de service de délivrer aux utilisateurs finaux les informations prévues à l'article 13 du RGPD.
    Ces informations pourront être délivrées par exemple dans les conditions générales d'utilisation du service du fournisseur de service.
  </p>
  <p>
    Le respect des obligations relatives aux données traitées et aux droits des utilisateurs finaux relève de la seule responsabilité
    du fournisseur de service.
  </p>

  <h2>Article 6. Propriété intellectuelle</h2>
  <p>
    Le fournisseur de service s'engage à respecter les droits de propriété intellectuelle de l'ANS et des tiers,
    et notamment à ne pas porter atteinte à tout signe distinctif (ex. marque, logo, etc.) reproduit sur PRO Santé connect.
  </p>

  <h2>Article 7. Dispositions générales</h2>
  <h3>7.1 Création de liens</h3>
  <p>
    La mise en place de liens vers PRO Santé connect n'est conditionnée à aucun accord préalable sous réserve de ne pas utiliser
    la technique du lien profond, c'est-à-dire que les pages du site ne doivent pas être imbriquées à l'intérieur des pages
    d'un autre site, mais visibles par l'ouverture d'une fenêtre indépendante.
  </p>
  <p>
    Cette autorisation ne s'applique en aucun cas aux sites internet diffusant des informations à caractère raciste, pornographique,
    sexiste, xénophobe, polémique ou pouvant, d'une façon générale, porter atteinte à la sensibilité du plus grand nombre.
  </p>
  <h3>7.2 Liens externes</h3>
  <p>
    Des liens vers d'autres sites, privés ou officiels, français ou étrangers, peuvent être proposés.
    Leur présence ne saurait engager l'ANS quant à leur contenu et ne vise qu'à permettre au visiteur de trouver plus facilement
    d'autres ressources documentaires sur le sujet consulté. Le contenu des pages, diffusé à titre purement informatif,
    ne saurait donc engager la responsabilité de l'ANS.
  </p>
  <h3>7.3 Loi applicable et tribunaux compétents</h3>
  <p>
    Les présentes conditions générales d'utilisation sont régies par la loi française.
    Tout litige résultant de leur application relèvera de la compétence des tribunaux français.
  </p>
  <h3>7.4 Modification des conditions générales d'utilisation</h3>
  <p>
    L'ANS s'engage à avertir par tout moyen le fournisseur de service de toutes modifications des présentes
    conditions générales d'utilisation.
  </p>
</div>
