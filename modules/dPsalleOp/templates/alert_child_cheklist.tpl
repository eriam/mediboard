{{*
 * @package Mediboard\SalleOp
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<tr>
  <td colspan="2">
    <div class="small-warning text">
      <h3>ATTENTION SI ENFANT !</h3>
      <ul>
        <li>Associer les parents à la vérification de l'identité, de l'intervention et du site opératoire. </li>
        <li>Autorisation d'opérer signée.</li>
        <li>Installation, matériel et prescription adaptés au poids, à l'âge et à la taille.</li>
        <li>Prévention de l'hypothermie.</li>
        <li>Seuils d'alerte en post-op définis.</li>
      </ul>
    </div>
  </td>
</tr>