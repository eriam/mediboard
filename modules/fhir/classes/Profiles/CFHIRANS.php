<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Profiles;

class CFHIRANS extends CFHIR
{
    /** @var string */
    public const BASE_PROFILE = 'http://esante.gouv.fr/ci-sis/fhir/StructureDefinition/';

    /**
     * CFHIRANS constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->name = "FHIR";
        $this->type = "ANS";
    }
}
