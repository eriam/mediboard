<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Profiles;

class CFHIRAnnuaireSante extends CFHIR
{
    /** @var string */
    public const BASE_PROFILE = 'https://apifhir.annuaire.sante.fr/ws-sync/exposed/structuredefinition/';

    public const RESOURCE_META_SOURCE = "https://annuaire.sante.fr";

    /**
     * CFHIRANS constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->name = "FHIR";
        $this->type = "AnnuaireSante";
    }
}
