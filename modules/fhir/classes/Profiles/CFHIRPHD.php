<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Profiles;

class CFHIRPHD extends CFHIR
{
    /** @var string */
    public const BASE_PROFILE = 'http://hl7.org/fhir/uv/phd/StructureDefinition/';

    /**
     * CFHIRPHD constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->name = "FHIR";
        $this->type = "PHD";
    }
}
