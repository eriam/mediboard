<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Utilities;

use Ox\Interop\Fhir\Datatypes\CFHIRDataType;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeChoice;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeComplex;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeExtension;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\R4\CFHIRDefinition;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Description
 */
trait CFHIRTools
{
    /**
     * @param CFHIRResource|CFHIRDataType $resource
     *
     * @return array
     * @throws InvalidArgumentException
     */
    public static function getNonEmptyFields($resource): array
    {
        $is_object   = is_object($resource);
        $is_resource = $is_object && is_subclass_of($resource, CFHIRResource::class);
        $is_datatype = $is_object && !$is_resource && is_subclass_of($resource, CFHIRDataType::class);
        if (!$is_resource && !$is_datatype) {
            return [];
        }

        $fields = $resource->isSummary()
            ? CFHIRDefinition::getSummariesFields($resource)
            : CFHIRDefinition::getFields($resource);
        $data   = [];
        foreach ($fields as $field) {
            // si property existe pas
            if (!property_exists($resource, $field)) {
                continue;
            }

            /** @var CFHIRDataType $v */
            $v = $resource->{$field};

            // si pas de data
            if (is_array($v) && count($v) === 0) {
                continue;
            }

            // si pas un de data || pas un type CFHIRDatatype
            if (!$resource->{$field} || (!$resource->{$field} instanceof CFHIRDataType && !is_array($v))) {
                continue;
            }

            if (!is_array($v) && $v->isNull()) {
                continue;
            } elseif (is_array($v)) {
                $v = array_filter($v, function (CFHIRDataType $datatype) {
                    return !$datatype->isNull();
                });
                if (empty($v)) {
                    continue;
                }
            }

            $data[$field] = $v;
        }

        // transform datatype choice
        $ordered_data = [];
        foreach ($data as $key => $item) {
            if ($item instanceof CFHIRDataTypeChoice) {
                unset($data[$key]);
                $item = $item->getValue();
                $key  = $key . $item::NAME;
            }

            $ordered_data[$key] = $item;
        }

        return $ordered_data;
    }

    /**
     * @param array  $datatypes
     * @param string $field
     *
     * @return array
     */
    public static function manageDatatypeJSONArray(array $datatypes, string $field): array
    {
        $values = [];
        /** @var CFHIRDataType $datatype */
        foreach ($datatypes as $datatype) {
            // item is complex type
            $items = $datatype->toJSON($field);
            if ($datatype instanceof CFHIRDataTypeComplex && !$datatype instanceof CFHIRDataTypeExtension) {
                $values[$field][] = $items[$field] ?? null;
            } else {
                $values[$field][]    = $items[$field] ?? null;
                $values["_$field"][] = $items["_$field"] ?? null;
            }
        }

        if (count(array_filter($values["_$field"] ?? [])) === 0) {
            unset($values["_$field"]);
        }

        if (count(array_filter($values[$field] ?? [])) === 0) {
            unset($values[$field]);
        }

        return $values;
    }
}
