<?php

/**
 * @package Mediboard\Fhir\Objects
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Utilities\Helper;

use Exception;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeCode;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeDateTime;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeHumanName;
use Ox\Interop\Fhir\Exception\CFHIRException;
use Ox\Interop\Fhir\Resources\R4\Patient\CFHIRResourcePatient;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\CPatientINSNIR;

class PatientHelper
{
    /** @var string  */
    public const INS_NIR = CPatientINSNIR::OID_INS_NIR;
    /** @var string  */
    public const INS_NIA = CPatientINSNIR::OID_INS_NIA;
    /** @var string  */
    public const INS_TEST = CPatientINSNIR::OID_INS_NIR_TEST;

    /**
     * Search INS (type) of patient in resource fhir
     *
     * @param CFHIRResourcePatient $resource_patient
     * @param string               $ins_type
     *
     * @return string|null
     * @throw CFHIRException|Exception
     */
    public static function getINS(CFHIRResourcePatient $resource_patient, string $ins_type = self::INS_NIR): ?string
    {
        if (!in_array($ins_type, [self::INS_NIR, self::INS_NIA, self::INS_TEST])) {
            throw CFHIRException::tr('PatientHelper-msg-type INS invalid', $ins_type);
        }

        foreach ($resource_patient->identifier as $identifier) {
            if (!$identifier->isSystemMatch($ins_type)) {
                continue;
            }

            return $identifier->value->getValue();
        }

        return null;
    }

    /**
     * Search IPP of patient in resource fhir
     *
     * @param CFHIRResourcePatient $resource_patient
     * @param string               $ipp_oid
     *
     * @return string|null
     */
    public static function getIPP(CFHIRResourcePatient $resource_patient, string $ipp_oid): ?string
    {
        foreach ($resource_patient->identifier as $identifier) {
            if (!$identifier->isSystemMatch($ipp_oid)) {
                continue;
            }

            return $identifier->value->getValue();
        }

        return null;
    }

    /**
     * Search patient infos in resource and map on object CPatient
     *
     * @param CFHIRResourcePatient $resource_patient
     *
     * @return CPatient|null
     * @throws Exception
     */
    public static function primaryMapping(CFHIRResourcePatient $resource_patient): ?CPatient
    {
        $patient = new CPatient();

        // birthdate
        $patient->naissance = $resource_patient->birthDate ? $resource_patient->birthDate->getDate() : null;

        if ($resource_name = self::getOfficialName($resource_patient)) {
            // First name
            if ($resource_name->family && !$resource_name->family->isNull()) {
                $patient->nom = $resource_name->family->getValue();
            }

            // Given name
            if ($resource_name->given) {
                foreach ($resource_name->given as $key => $given) {
                    if ($key > 3) {
                        break;
                    }

                    $key = $key === 0 ? 'prenom' : ('_prenom_' . ($key + 1));

                    $patient->{$key} = $given->getValue();
                }
            }
        }

        $patient->deces = $resource_patient->deceased instanceof CFHIRDataTypeDateTime
            ? $resource_patient->deceased->getValue() : null;

        $patient->sexe = self::mapGender($resource_patient->gender);

        return $patient;
    }

    /**
     * Search official name in the resource
     *
     * @param CFHIRResourcePatient $resource_patient
     *
     * @return CFHIRDataTypeHumanName|null
     */
    public static function getOfficialName(CFHIRResourcePatient $resource_patient): ?CFHIRDataTypeHumanName
    {
        foreach ($resource_patient->name ?? [] as $name) {
            if ($name->use && $name->use->getValue() === 'official') {
                return $name;
            }
        }

        return null;
    }

    /**
     * Map gender of patient
     *
     * @param CFHIRDataTypeCode|null $code
     *
     * @return string|null
     */
    public static function mapGender(?CFHIRDataTypeCode $code): ?string
    {
        if (!$code || !$code->getValue()) {
            return null;
        }

        switch ($code->getValue()) {
            case 'male':
                return 'm';
            case 'female':
                return 'f';
            case 'unknown':
            default:
                return 'i';
        }
    }
}
