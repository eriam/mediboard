<?php

/**
 * @package Mediboard\Fhir\Objects
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Utilities\Helper;

use Ox\Core\CMbException;
use Ox\Core\CStoredObject;
use Ox\Interop\Eai\CMbOID;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeUri;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeIdentifier;
use Ox\Interop\Fhir\Exception\CFHIRException;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Mediboard\Loinc\CLoinc;
use Ox\Mediboard\Sante400\CIdSante400;

class ResourceHelper
{
    /**
     * Search identifier of object Ox
     *
     * @param CFHIRDomainResource $resource
     * @param string              $class
     *
     * @return string|null
     * @throw CFHIRException
     */
    public static function getOxObjectIdentifier(CFHIRDomainResource $resource, string $class): ?string
    {
        if (!is_subclass_of($class, CStoredObject::class)) {
            throw CFHIRException::tr('ResourceHelper-msg-invalid class given', $class);
        }

        $system = CMbOID::OX_ROOT_OID;

        foreach ($resource->identifier as $identifier) {
            if ($identifier->isSystemMatch($system)) {
                return $identifier->value->getValue();
            }
        }

        return null;
    }

    /**
     * @param string $system
     *
     * @return bool
     */
    public static function isLoincSystem(CFHIRDataTypeUri $datatype_uri): bool
    {
        if (!$system = $datatype_uri->getValue()) {
            return false;
        }

        return $system === CLoinc::$system_loinc || $datatype_uri->isSystemMatch(CLoinc::$oid_loinc);
    }

    /**
     * Get idex mapped from Datatype identifier
     *
     * @param CFHIRDataTypeIdentifier $identifier
     *
     * @return CIdSante400|null
     * @throws CMbException
     */
    public static function getIdexFromIdentifier(CFHIRDataTypeIdentifier $identifier): ?CIdSante400
    {
        $value = $identifier->value ? $identifier->value->getValue() : null;
        if ($value === null || !$identifier->system->isSystemMatch(CMbOID::getClassOID(CIdSante400::class))) {
            return null;
        }

        if (($delimiter_pos = strrpos($value, '|')) === false) {
            return null;
        }

        $idex = new CIdSante400();
        $idex->tag   = substr($value, 0, $delimiter_pos);
        $idex->id400 = substr($value, $delimiter_pos + 1, strlen($value));

        return $idex;
    }
}
