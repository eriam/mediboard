<?php

/**
 * @package Mediboard\Fhir\Objects
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Utilities\Helper;

use Ox\Interop\Fhir\Resources\R4\Encounter\CFHIRResourceEncounter;

class SejourHelper
{
    /**
     * Search NDA identifier in resource encounter
     *
     * @param CFHIRResourceEncounter $encounter
     * @param string                 $nda_oid
     *
     * @return string|null
     */
    public static function getNDA(CFHIRResourceEncounter $encounter, string $nda_oid): ?string
    {
        foreach ($encounter->identifier as $identifier) {
            if (!$identifier->isSystemMatch($nda_oid)) {
                continue;
            }

            return $identifier->value->getValue();
        }

        return null;
    }
}
