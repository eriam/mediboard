<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir;

use Ox\Core\Module\CAbstractModuleCache;
use Ox\Interop\Fhir\ClassMap\FHIRClassMap;
use Ox\Interop\Fhir\Datatypes\CFHIRDataType;
use Ox\Interop\Fhir\Resources\R4\CFHIRDefinition;
use Psr\SimpleCache\InvalidArgumentException;

class CModuleCacheFhir extends CAbstractModuleCache
{
    /** @var string */
    public $module = 'fhir';

    /**
     * @inheritdoc
     * @throws InvalidArgumentException
     */
    public function clearSpecialActions(): void
    {
        // Delete cache Definition Fhir
        CFHIRDefinition::clearCache();

        // Delete cache datatype
        CFHIRDataType::resetCacheMappingComplexType();

        // Delete cache resource map
        (new FHIRClassMap())->clearCache();
    }
}
