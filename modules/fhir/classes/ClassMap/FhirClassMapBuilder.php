<?php

/**
 * @package Mediboard\Fhir\Resources
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\ClassMap;

use Exception;
use Ox\Core\CClassMap;
use Ox\Core\CMbArray;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectHandleInterface;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectInterface;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectSearcherInterface;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;

class FhirClassMapBuilder
{
    /** @var array  */
    private $delegated_objects;

    /**
     * @return array
     * @throws Exception
     */
    public function build(array $only_resources = []): array
    {
        /** @var CFHIRResource[] $all_resources */
        if (!$only_resources) {
            $all_resources = CClassMap::getInstance()->getClassChildren(CFHIRResource::class, true, true);
        } else {
            $all_resources = [];
            foreach ($only_resources as $resource) {
                $all_resources[] = new $resource();
            }
        }

        $this->delegated_objects = $this->findDelegated();

        return $this->buildResources($all_resources);
    }

    /**
     * @param CFHIRResource[] $resources
     *
     * @return array
     */
    private function buildResources(array $resources): array
    {
        $map = [];
        foreach ($resources as $resource) {
            $versions = $resource->getAvailableFHIRVersions();
            foreach ($versions as $version) {
                $this->buildResource($map, $resource, $version);

                $this->buildDelegated($map, $resource, $version);
            }
        }

        return $map;
    }

    /**
     * @param array         $map
     * @param CFHIRResource $resource
     * @param string        $fhir_version
     *
     * @return array
     */
    private function buildResource(array &$map, CFHIRResource $resource, string $fhir_version): array
    {
        /** @var CFHIR|string $profile_class */
        $profile_class  = $resource::PROFILE_CLASS;
        $resource_class = get_class($resource);
        $canonical      = $resource->getProfile();

        // versions fhir_version
        if (!in_array($fhir_version, CMbArray::getRecursive($map, 'versions fhir_version', []))) {
            $map['versions']['fhir_version'][] = $fhir_version;
        }

        // versions canonical <canonical> fhir_version
        if (!in_array($fhir_version, CMbArray::getRecursive($map, "versions canonical $canonical fhir_version", []))) {
            $map['versions']['canonical'][$canonical]['fhir_version'][] = $fhir_version;
        }

        // profiles <fhir_version> canonical <canonical>
        $map['profiles'][$fhir_version]['canonical'][$canonical] = $profile_class;

        // profiles <fhir_version> profile_class <profile_class>
        if (!in_array($canonical, CMbArray::getRecursive($map, "profiles $fhir_version profile $profile_class", []))) {
            $map['profiles'][$fhir_version]['profile_class'][$profile_class][] = $canonical;
        }

        // profiles <fhir_version> fhir_resource <fhir_resource>
        $map['profiles'][$fhir_version]['fhir_resource'][$resource::RESOURCE_TYPE][] = $profile_class;

        // profiles <fhir_version> profile <base_url> => profile_class
        $map['profiles'][$fhir_version]['profile'][$profile_class::BASE_PROFILE] = $profile_class;

        // resource <fhir_version> canonical <canonical>
        $map['resource'][$fhir_version]['canonical'][$canonical] = $resource_class;

        // resource <fhir_version> fhir_resource resource_type
        if ($resource->isFHIRResource()) {
            $map['resource'][$fhir_version]['fhir_resource'][$resource::RESOURCE_TYPE] = $resource_class;
        }

        // resource <fhir_version> type <resource_type>
        $map['resource'][$fhir_version]['type'][$resource::RESOURCE_TYPE][] = $resource_class;

        return $map;
    }

    /**
     * @param array         $map
     * @param CFHIRResource $resource
     * @param string        $fhir_version
     *
     * @return array
     */
    private function buildDelegated(array &$map, CFHIRResource $resource, string $fhir_version): array
    {
        // delegated mapper
        $map['delegated'][$fhir_version]['mapper'] =
            $this->buildSpecificDelegated('mapper', $map, $resource, $fhir_version);

        // delegated searcher
        $map['delegated'][$fhir_version]['searcher'] =
            $this->buildSpecificDelegated('searcher', $map, $resource, $fhir_version);

        // delegated handle
        $map['delegated'][$fhir_version]['handle'] =
            $this->buildSpecificDelegated('handle', $map, $resource, $fhir_version);

        return $map;
    }

    /**
     * @param string        $key
     * @param array         $map
     * @param CFHIRResource $resource
     * @param string        $fhir_version
     *
     * @return array
     */
    private function buildSpecificDelegated(string $key, array $map, CFHIRResource $resource, string $fhir_version): array
    {
        $profile_class  = $resource::PROFILE_CLASS;
        $canonical      = $resource->getProfile();
        $resource_type  = $resource::RESOURCE_TYPE;

        $delegated_objects = CMbArray::getRecursive($map, "delegated $fhir_version $key", []);
        // resource <fhir_version> delegated mapper type <resource_type>
        if ($types_delegated = CMbArray::getRecursive($this->delegated_objects, "$key type $resource_type", [])) {
            $delegated_objects['type'][$resource_type] = $types_delegated;
        }

        // resource <fhir_version> delegated mapper canonical
        if ($profile_delegated = CMbArray::getRecursive($this->delegated_objects, "$key profiled $resource_type $profile_class")) {
            $delegated_objects['canonical'][$canonical] = $profile_delegated;
        }

        // resource <fhir_version delegated mapper all
        if (!CMbArray::get($delegated_objects, "all", [])) {
            if ($all_delegated = CMbArray::getRecursive($this->delegated_objects, "$key all")) {
                $delegated_objects['all'] = $all_delegated;
            }
        }

        return $delegated_objects;
    }

    /**
     * @return array
     * @throws Exception
     */
    private function findDelegated(): array
    {
        $delegated_objects = [];
        $classmap = CClassMap::getInstance();

        // mapper
        $delegated_objects['mapper'] = $this->findSpecificDelegated(DelegatedObjectMapperInterface::class, $classmap);

        // searcher
        $delegated_objects['searcher'] = $this->findSpecificDelegated(
            DelegatedObjectSearcherInterface::class,
            $classmap
        );

        // handle
        $delegated_objects['handle'] = $this->findSpecificDelegated(DelegatedObjectHandleInterface::class, $classmap);

        return $delegated_objects;
    }

    /**
     * @param string    $interface
     * @param CClassMap $classMap
     *
     * @return array
     * @throws Exception
     */
    private function findSpecificDelegated(string $interface, CClassMap $classMap): array
    {
        $result = [];
        foreach ($classMap->getClassChildren($interface) as $class) {
            if ($classMap->getClassMap($class)) {
                /** @var DelegatedObjectInterface $object_handle */
                $object_handle = new $class();

                if (is_subclass_of($class, DelegatedObjectHandleInterface::class)) {
                    $resource_type = (new $class())->getTargetResource()::RESOURCE_TYPE;
                } else {
                    $resource_type = $class::RESOURCE_TYPE;
                }

                foreach ($object_handle->getProfiles() ?: ['all'] as $profile_class) {
                    $result['profiled'][$resource_type][$profile_class][] = $class;
                }

                // all
                if (!in_array($class, $result['all'] ?? [])) {
                    $result['all'][] = $class;
                }

                // resource type
                if (!in_array($class, $result['type'] ?? [])) {
                    $result['type'][$resource_type][] = $class;
                }
            }
        }

        return $result;
    }
}
