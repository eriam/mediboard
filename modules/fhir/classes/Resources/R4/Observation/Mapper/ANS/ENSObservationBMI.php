<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Observation\Mapper\ANS;

use Exception;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeCode;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeString;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeUri;
use Ox\Interop\Fhir\Datatypes\Complex\Backbone\CFHIRDataTypeBackboneElement;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeChoice;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeCodeableConcept;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeCoding;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeExtension;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeQuantity;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationBMIENS;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationBodyHeightENS;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationHeartrateENS;
use Ox\Interop\Fhir\Resources\ResourceTrait;
use Ox\Mediboard\Patients\Constants\CAbstractConstant;
use Ox\Mediboard\Patients\Constants\CConstantException;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Description
 */
class ENSObservationBMI extends ENSObservation
{
    use ResourceTrait;

    /** @var CAbstractConstant */
    protected $object;

    /** @var CFHIRResourceObservationBMIENS */
    protected $resource;

    public function mapCode(): ?CFHIRDataTypeCodeableConcept
    {
        $system  = "http://loinc.org";
        $code    = "39156-5";
        $display = "BMICode";

        $coding = CFHIRDataTypeCoding::addCoding($system, $code, $display);
        $text   = 'Indice de Masse Corporelle';

        return CFHIRDataTypeCodeableConcept::addCodeable($coding, $text);
    }

    /**
     * @throws CConstantException
     */
    public function mapValue(): ?CFHIRDataTypeChoice
    {
        // L'unité de l'IMC est généralement en kg/m2
        // Ici on a l'unité g/mmÂ²
        $valueQuantity = [
            'value'  => $this->object->getValue(),
            'unit'   => "kg/m2",
            'system' => new CFHIRDataTypeUri("http://unitsofmeasure.org"),
            'code'   => new CFHIRDataTypeCode("kg/m2"),
        ];

        return new CFHIRDataTypeChoice(CFHIRDataTypeQuantity::class, $valueQuantity);
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function mapReferenceRange(): array
    {
        $system = "https://mos.esante.gouv.fr/NOS/TRE_R303-HL7v3AdministrativeGender/FHIR/TRE-R303-HL7v3AdministrativeGender";

        $patient = $this->object->loadRefPatient();
        $sexe    = $patient->sexe;

        $grossesse = $patient->loadLastGrossesse();

        if (!$grossesse) {
            return [];
        }

        $isPregnant = $grossesse->active;

        $code    = $sexe;
        $display = $sexe === 'M' ? 'Masculin' : 'Féminin';

        if ($isPregnant) {
            $system  = "https://mos.esante.gouv.fr/NOS/TRE_A04-Loinc/FHIR/TRE-A04-Loinc";
            $code    = "LA15173-0";
            $display = "Femme enceinte";
        }

        $coding = CFHIRDataTypeCoding::addCoding($system, $code, $display);
        $text   = $display;

        $appliesTo = CFHIRDataTypeCodeableConcept::addCodeable($coding, $text);

        return [
            CFHIRDataTypeBackboneElement::build([
                                                    'appliesTo' => $appliesTo,
                                                ]),
        ];
    }
}
