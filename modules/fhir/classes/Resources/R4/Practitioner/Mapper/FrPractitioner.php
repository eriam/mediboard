<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Practitioner\Mapper;

use Exception;
use Ox\Core\CMbArray;
use Ox\Interop\Eai\CDomain;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeCode;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeDate;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeString;
use Ox\Interop\Fhir\Datatypes\Complex\Backbone\Patient\CFHIRDataTypePatientContact;
use Ox\Interop\Fhir\Datatypes\Complex\Backbone\Practitioner\CFHIRDataTypePractitionerQualification;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeAddress;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeCodeableConcept;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeCoding;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeContactPoint;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeExtension;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeHumanName;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeIdentifier;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeReference;
use Ox\Interop\Fhir\Profiles\CFHIRInteropSante;
use Ox\Interop\Fhir\Resources\R4\Organization\Profiles\InteropSante\CFHIRResourceOrganizationFR;
use Ox\Interop\Fhir\Resources\R4\Patient\Profiles\InteropSante\CFHIRResourcePatientFR;
use Ox\Interop\Fhir\Resources\R4\Practitioner\Mapper\Practitioner;
use Ox\Interop\Fhir\Resources\R4\Practitioner\Profiles\InteropSante\CFHIRResourcePractitionerFR;
use Ox\Interop\Fhir\Resources\R4\RelatedPerson\Profiles\InteropSante\CFHIRResourceRelatedPersonFR;
use Ox\Interop\Fhir\Resources\ResourceTrait;
use Ox\Mediboard\Patients\CCorrespondantPatient;
use Ox\Mediboard\Patients\CExercicePlace;
use Ox\Mediboard\Patients\CIdentityProofType;
use Ox\Mediboard\Patients\CINSPatient;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\CMedecinExercicePlace;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Patients\CSourceIdentite;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

/**
 * Description
 */
class FrPractitioner extends Practitioner
{
    use ResourceTrait;

    /** @var CMedecin */
    protected $object;

    /** @var CFHIRResourcePractitionerFR */
    protected $resource;

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function mapIdentifier(): array
    {
        $identifier = $this->resource->identifier;

        /** @var CMedecinExercicePlace[] $medecin_exercice_places */
        $medecin_exercice_places = $this->object->getMedecinExercicePlaces();
        $adeli                   = array_unique(CMbArray::pluck($medecin_exercice_places, "adeli"))[0];

        // ADELI
        if ($adeli || $this->object->adeli) {
            $system  = 'http://interopsante.org/fhir/CodeSystem/fr-v2-0203';
            $code    = 'ADELI';
            $display = 'NÂ° ADELI';
            $text    = 'NÂ° ADELI';
            $coding  = CFHIRDataTypeCoding::addCoding($system, $code, $display);

            $codeableConcept = CFHIRDataTypeCodeableConcept::addCodeable($coding, $text);

            $identifier = CFHIRDataTypeIdentifier::addIdentifier(
                $this->resource->identifier,
                $adeli ?? $this->object->adeli,
                $codeableConcept,
                'official',
                'urn:oid:1.2.250.1.71.4.2.1'
            );
        }

        // RPPS
        if ($this->object->rpps) {
            $system  = 'http://interopsante.org/fhir/CodeSystem/fr-v2-0203';
            $code    = 'RPPS';
            $display = 'NÂ° RPPS';
            $text    = 'NÂ° RPPS';

            $coding = CFHIRDataTypeCoding::addCoding($system, $code, $display);

            $codeableConcept = CFHIRDataTypeCodeableConcept::addCodeable($coding, $text);

            $identifier = CFHIRDataTypeIdentifier::addIdentifier(
                $this->resource->identifier,
                $this->object->rpps,
                $codeableConcept,
                'official',
                'urn:oid:1.2.250.1.71.4.2.1'
            );
        }

        return $identifier;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function mapName(): array
    {
        // TODO PASSER EN FrHumanName
        return CFHIRDataTypeHumanName::addName(
            $this->object->nom,
            $this->object->prenom,
            'usual',
            $this->object->_view
        );
    }

    /**
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    public function mapTelecom(): array
    {
        // TODO PASSER EN FrContactPoint
        $telecoms = [];
        if ($this->object->tel) {
            $telecoms[] = CFHIRDataTypeContactPoint::build(
                [
                    "system" => "phone",
                    "value"  => $this->object->tel,
                ]
            );
        }

        if ($this->object->tel_autre) {
            $telecoms[] = CFHIRDataTypeContactPoint::build(
                [
                    "system" => "phone",
                    "value"  => $this->object->tel_autre,
                ]
            );
        }

        if ($this->object->fax) {
            $telecoms[] = CFHIRDataTypeContactPoint::build(
                [
                    "system" => "fax",
                    "value"  => $this->object->fax,
                ]
            );
        }

        if ($this->object->portable) {
            $telecoms[] = CFHIRDataTypeContactPoint::build(
                [
                    "system" => "portable",
                    "value"  => $this->object->portable,
                ]
            );
        }

        if ($this->object->email) {
            $telecoms[] = CFHIRDataTypeContactPoint::build(
                [
                    "system" => "email",
                    "value"  => $this->object->email,
                ]
            );
        }

        return $telecoms;
    }

    /**
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function mapAddress(): array
    {
        // TODO PASSER EN FrAddress
        if ($this->object->adresse || $this->object->ville || $this->object->cp) {
            return [
                CFHIRDataTypeAddress::build(
                    [
                        'use'        => 'work',
                        'type'       => 'postal',
                        'line'       => preg_split('/[\r\n]+/', $this->object->adresse) ?? null,
                        'city'       => $this->object->ville ?? null,
                        'postalCode' => $this->object->cp ?? null,
                    ]
                ),
            ];
        }

        return [];
    }

    /**
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function mapQualification(): array
    {
        // TODO Mapper avec les spécialités franÎ·aise
        return parent::mapQualification();
    }

    public function getProfiles(): array
    {
        return [CFHIRInteropSante::class];
    }
}
