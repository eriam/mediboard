<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\PractitionerRole\Profiles\AnnuaireSante;

use Exception;
use Ox\Core\CStoredObject;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Exception\CFHIRExceptionNotSupported;
use Ox\Interop\Fhir\Profiles\CFHIRAnnuaireSante;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\CFHIRResourcePractitionerRole;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\Mapper\AnnuaireSante\PractitionerRoleMappingOrganizationalRassExercicePlace;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\Mapper\AnnuaireSante\PractitionerRoleMappingOrganizationalRassMediusers;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\PractitionerRoleMappingInterface;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CExercicePlace;

class CFHIRResourcePractitionerRoleOrganizationalRass extends CFHIRResourcePractitionerRole
{
    // constants
    /** @var string */
    public const PROFILE_TYPE = 'practitionerRole-organizationalRole-rass';

    /** @var string */
    public const PROFILE_CLASS = CFHIRAnnuaireSante::class;

    /** @var CExercicePlace */
    public $object;

    /**
     * return CExercicePlace
     */
    public function getClass(): ?string
    {
        return CExercicePlace::class;
    }

    /**
     * @param CStoredObject $object
     *
     * @return PractitionerRoleMappingInterface
     */
    protected function setMapperOld(CStoredObject $object): ?DelegatedObjectMapperInterface
    {
        switch (get_class($object)) {
            case CExercicePlace::class:
                $object_mapper = PractitionerRoleMappingOrganizationalRassExercicePlace::class;
                break;
            case CMediusers::class:
                $object_mapper = PractitionerRoleMappingOrganizationalRassMediusers::class;
                break;
            default:
                throw new CFHIRExceptionNotSupported('Mapping object not supported');
        }

        return new $object_mapper();
    }

    /**
     * Map property extension
     * @throws Exception
     */
    protected function mapPractitioner(): void
    {
        // forbidden in this profile
    }

    protected function mapSpecialty(): void
    {
        // forbidden in this profile
    }

    protected function mapHealthCareService(): void
    {
        // forbidden in this profile
    }

    protected function mapAvailabilityExceptions(): void
    {
        // forbidden in this profile
    }

    protected function mapAvailableTime(): void
    {
        // forbidden in this profile
    }

    protected function mapNotAvailable(): void
    {
        // forbidden in this profile
    }

    protected function mapEndpoint(): void
    {
        // forbidden in this profile
    }
}
