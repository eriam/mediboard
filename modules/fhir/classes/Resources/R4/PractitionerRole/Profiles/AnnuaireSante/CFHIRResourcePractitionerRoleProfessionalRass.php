<?php

/**
 * @package Mediboard\Fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\PractitionerRole\Profiles\AnnuaireSante;

use Ox\Core\CStoredObject;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Exception\CFHIRExceptionNotSupported;
use Ox\Interop\Fhir\Profiles\CFHIRAnnuaireSante;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\CFHIRResourcePractitionerRole;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\Mapper\AnnuaireSante\PractitionerRoleProfessionalRassMedecin;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\Mapper\AnnuaireSante\PractitionerRoleProfessionalRassMediusers;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\PractitionerRoleMappingInterface;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CMedecin;

class CFHIRResourcePractitionerRoleProfessionalRass extends CFHIRResourcePractitionerRole
{
    // constants
    /** @var string */
    public const PROFILE_TYPE = 'practitionerRole-professionalRole-rass';

    /** @var string */
    public const PROFILE_CLASS = CFHIRAnnuaireSante::class;



    /** @var CMedecin|CMediusers */
    public $object;

    /**
     * return CMedecin
     */
    public function getClass(): ?string
    {
        return CMedecin::class;
    }

    /**
     * @param CStoredObject $object
     *
     * @return PractitionerRoleMappingInterface
     */
    protected function setMapperOld(CStoredObject $object): ?DelegatedObjectMapperInterface
    {
        switch (get_class($object)) {
            case CMediusers::class:
                $mapping_object = PractitionerRoleProfessionalRassMediusers::class;
                break;
            case CMedecin::class:
                $mapping_object = PractitionerRoleProfessionalRassMedecin::class;
                break;
            default:
                throw new CFHIRExceptionNotSupported('Mapping object not supported');
        }

        return new $mapping_object();
    }

    protected function mapOrganization(): void
    {
        // forbidden in this profile
    }

    protected function mapLocation(): void
    {
        // forbidden in this profile
    }

    protected function mapHealthCareService(): void
    {
        // forbidden in this profile
    }

    protected function mapAvailableTime(): void
    {
        // forbidden in this profile
    }

    protected function mapNotAvailable(): void
    {
        // forbidden in this profile
    }

    protected function mapAvailabilityExceptions(): void
    {
        // forbidden in this profile
    }

    protected function mapEndpoint(): void
    {
        // forbidden in this profile
    }
}
