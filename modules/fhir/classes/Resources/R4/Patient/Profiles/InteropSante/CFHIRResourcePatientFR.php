<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Patient\Profiles\InteropSante;

use Exception;
use Ox\Core\CStoredObject;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\PatientMappingInterface;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeCode;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeDate;
use Ox\Interop\Fhir\Datatypes\Complex\Backbone\Patient\CFHIRDataTypePatientContact;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeAddress;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeContactPoint;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeExtension;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeHumanName;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeIdentifier;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeReference;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionDelete;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionUpdate;
use Ox\Interop\Fhir\Profiles\CFHIRInteropSante;
use Ox\Interop\Fhir\Resources\R4\Patient\CFHIRResourcePatient;
use Ox\Interop\Fhir\Resources\R4\Patient\Mapper\FrPatient;
use Ox\Interop\Fhir\Resources\R4\Patient\PatientInterface;
use Ox\Interop\Fhir\Resources\R4\Practitioner\Profiles\InteropSante\CFHIRResourcePractitionerFR;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;
use Ox\Mediboard\Patients\CPatient;

/**
 * FHIR patient resource
 */
class CFHIRResourcePatientFR extends CFHIRResourcePatient
{
    // constants
    /** @var string */
    public const PROFILE_TYPE = 'FrPatient';

    /** @var string */
    public const PROFILE_CLASS = CFHIRInteropSante::class;

    // attributes
    /** @var CFHIRDataTypeExtension[] */
    public $extension;

    /** @var CFHIRDataTypeIdentifier[] */
    public $identifier;

    /** @var CFHIRDataTypeHumanName[] */
    public $name;

    /** @var CFHIRDataTypeContactPoint[] */
    public $telecom;

    /** @var CFHIRDataTypeCode */
    public $gender;

    /** @var CFHIRDataTypeDate */
    public $birthDate;

    /** @var CFHIRDataTypeAddress[] */
    public $address;

    /** @var CFHIRDataTypePatientContact[] */
    public $contact;

    /** @var CFHIRDataTypeReference[] */
    public $generalPractitioner;

    /** @var CFHIRDataTypeReference */
    public $managingOrganization;

    /** @var CPatient */
    protected $object;

    /** @var PatientMappingInterface */
    protected $object_mapping;

    /**
     * return CPatient
     */
    public function getClass(): ?string
    {
        return CPatient::class;
    }

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->addInteractions(
                [
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionUpdate::NAME,
                    CFHIRInteractionDelete::NAME,
                ]
            );
    }

    /**
     * @param CStoredObject $object
     *
     * @return DelegatedObjectMapperInterface
     */
    protected function setMapperOld(CStoredObject $object): DelegatedObjectMapperInterface
    {
        $mapping_object = FrPatient::class;

        return new $mapping_object();
    }

    /**
     * Map property extension
     */
    protected function mapExtension(): void
    {
        $this->object_mapping->mapExtension();
    }

    /**
     * Map property identifier
     * @throws Exception
     */
    protected function mapIdentifier(): void
    {
        $this->object_mapping->mapIdentifier();
    }

    /**
     * Map property name
     */
    protected function mapName(): void
    {
        $this->object_mapping->mapName();
    }

    /**
     * Map property telecom
     */
    protected function mapTelecom(): void
    {
        $this->object_mapping->mapTelecom();
    }

    /**
     * Map property address
     */
    protected function mapAddress(): void
    {
        $this->object_mapping->mapAddress();
    }

    /**
     * Map property generalPractitioner
     *
     */
    protected function mapGeneralPractitioner(): void
    {
        $this->object_mapping->mapGeneralPractitioner(CFHIRResourcePractitionerFR::class);
    }

    /**
     * Map property contact
     */
    protected function mapContact(): void
    {
        $this->object_mapping->mapContact();
    }
}
