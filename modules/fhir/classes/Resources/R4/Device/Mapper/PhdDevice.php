<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Device\Mapper;

use Ox\Interop\Fhir\Profiles\CFHIRPHD;
use Ox\Interop\Fhir\Resources\ResourceTrait;

/**
 * Description
 */
class PhdDevice extends Device
{
    use ResourceTrait;

    public function getProfiles(): array
    {
        return [CFHIRPHD::class];
    }
}
