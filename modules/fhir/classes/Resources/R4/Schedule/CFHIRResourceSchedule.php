<?php

/**
 * @package Mediboard\fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Schedule;

use Exception;
use Ox\Core\CStoredObject;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\ScheduleMappingInterface;
use Ox\Interop\Fhir\Contracts\Resources\ResourceScheduleInterface;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeBoolean;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeString;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeCodeableConcept;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeIdentifier;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypePeriod;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeReference;
use Ox\Interop\Fhir\Event\CFHIREvent;
use Ox\Interop\Fhir\Exception\CFHIRException;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionCreate;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionHistory;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionRead;
use Ox\Interop\Fhir\Interactions\CFHIRInteractionSearch;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRDomainResource;
use Ox\Interop\Fhir\Resources\R4\Practitioner\CFHIRResourcePractitioner;
use Ox\Interop\Fhir\Resources\R4\Schedule\Mapper\Schedule;
use Ox\Interop\Fhir\Utilities\CCapabilitiesResource;
use Ox\Interop\Fhir\Utilities\SearchParameters\SearchParameterDate;
use Ox\Mediboard\Cabinet\CPlageconsult;
use Ox\Mediboard\Patients\CMedecin;

/**
 * @package Mediboard\fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * Class CFHIRResourceSchedule
 */
class CFHIRResourceSchedule extends CFHIRDomainResource implements ResourceScheduleInterface
{
    // constants
    /** @var string */
    public const RESOURCE_TYPE = 'Schedule';

    // attributes
    /** @var CFHIRDataTypeIdentifier[] */
    public $identifier;

    /** @var CFHIRDataTypeBoolean */
    public $active;

    /** @var CFHIRDataTypeCodeableConcept[] */
    public $serviceCategory;

    /** @var CFHIRDataTypeCodeableConcept[] */
    public $serviceType;

    /** @var CFHIRDataTypeCodeableConcept[] */
    public $specialty;

    /** @var CFHIRDataTypeReference[] */
    public $actor;

    /** @var CFHIRDataTypePeriod */
    public $planningHorizon;

    /** @var CFHIRDataTypeString */
    public $comment;

    /** @var CPlageconsult */
    public $object;

    /** @var ScheduleMappingInterface */
    protected $object_mapping;

    /**
     * return CPlageconsult
     */
    public function getClass(): ?string
    {
        return CPlageconsult::class;
    }

    /**
     * @return CCapabilitiesResource
     */
    public function generateCapabilities(): CCapabilitiesResource
    {
        return (parent::generateCapabilities())
            ->setInteractions(
                [
                    CFHIRInteractionRead::NAME,
                    CFHIRInteractionCreate::NAME,
                    CFHIRInteractionSearch::NAME,
                    CFHIRInteractionHistory::NAME,
                ]
            )
            ->addSearchAttributes(
                [
                    new SearchParameterDate('start'),
                ]
            );
    }

    /**
     * @param CStoredObject $object
     *
     * @throws Exception
     */
    final protected function map(CStoredObject $object): void
    {
        if (!$object->_id) {
            return;
        }

        // Pour passer dans l'extension l'identifier, il faut mapper l'objet en amont
        $this->object = $object;

        parent::map($object);

        $this->mapActive();

        $this->mapServiceCategory();

        $this->mapServiceType();

        $this->mapSpecialty();

        $this->mapActor();

        $this->mapPlanningHorizon();

        $this->mapComment();
    }

    /**
     * @inheritdoc
     * @throws Exception
     * @throws CFHIRException
     */
    public function build(CStoredObject $object, CFHIREvent $event): void
    {
        parent::build($object, $event);

        if (!$object instanceof CPlageconsult) {
            throw new CFHIRException("Object is not a schedule");
        }

        $this->map($object);
    }

    /**
     * @param CStoredObject $object
     *
     * @return DelegatedObjectMapperInterface
     */
    protected function setMapperOld(CStoredObject $object): DelegatedObjectMapperInterface
    {
        $mapping_object = Schedule::class;

        return new $mapping_object();
    }

    /**
     * Map property active
     */
    protected function mapActive(): void
    {
        // not implemented
        $this->active = $this->object_mapping->mapActive();
    }

    /**
     * Map property serviceCategory
     */
    protected function mapServiceCategory(): void
    {
        $this->serviceCategory = $this->object_mapping->mapServiceCategory();
    }

    /**
     * Map property serviceType
     * @throws Exception
     */
    protected function mapServiceType(): void
    {
        $this->serviceType = $this->object_mapping->mapServiceType();
    }

    /**
     * Map property specialty
     */
    protected function mapSpecialty(): void
    {
        $this->specialty = $this->object_mapping->mapSpecialty();
    }

    /**
     * Map property actor
     * @throws Exception
     */
    protected function mapActor(): void
    {
        $this->actor = $this->object_mapping->mapActor();
    }

    /**
     * Map property planningHorizon
     * @throws Exception
     */
    protected function mapPlanningHorizon(): void
    {
        $this->planningHorizon = $this->object_mapping->mapPlanningHorizon();
    }

    /**
     * Map property comment
     */
    protected function mapComment(): void
    {
        $this->comment = $this->object_mapping->mapComment();
    }
}
