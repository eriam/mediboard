<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Encounter\Mapper;

use Ox\Core\CStoredObject;
use Ox\Interop\Eai\CDomain;
use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\EncounterMappingInterface;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeCode;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeInstant;
use Ox\Interop\Fhir\Datatypes\Complex\Backbone\Encounter\CFHIRDataTypeEncounterParticipant;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeCodeableConcept;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeIdentifier;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypePeriod;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeReference;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\R4\Encounter\CFHIRResourceEncounter;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\ResourceTrait;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\CSejour;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

/**
 * Description
 */
class Encounter implements DelegatedObjectMapperInterface, EncounterMappingInterface
{
    use ResourceTrait;

    /** @var CSejour */
    protected $object;

    /** @var CFHIRResourceEncounter */
    protected $resource;

    public function setResource(CFHIRResource $resource, CStoredObject $object): void
    {
        $this->object   = $object;
        $this->resource = $resource;
    }

    public function getProfiles(): array
    {
        return [CFHIR::class];
    }

    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
    }

    public function mapExtension(): array
    {
        return [];
    }

    /**
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function mapIdentifier(): array
    {
        $domains     = CDomain::loadDomainIdentifiers($this->object);
        $identifiers = [];

        foreach ($domains as $_domain) {
            if (empty($sejour->_returned_oids) || in_array($_domain->OID, $sejour->_returned_oids)) {
                $identifiers[] = CFHIRDataTypeIdentifier::build(
                    [
                        "system" => "urn:oid:$_domain->OID",
                        "value"  => $_domain->_identifier->id400,
                    ]
                );
            }
        }

        return $identifiers;
    }

    public function mapStatus(): ?CFHIRDataTypeCode
    {
        if ($this->object->annule) {
            return new CFHIRDataTypeCode('cancelled');
        }

        switch ($this->object->_etat) {
            case "preadmission":
                return new CFHIRDataTypeCode('planned');
            case "encours":
                return new CFHIRDataTypeCode('in-progress');
            case "cloture":
                return new CFHIRDataTypeCode('finished');
            default:
                return null;
        }
    }

    public function mapType(): ?CFHIRDataTypeCodeableConcept
    {
        return null;
    }

    public function mapSubject(): ?CFHIRDataTypeReference
    {
        return $this->resource->addReference(get_class(new CPatient()), $this->object->loadRefPatient());
    }

    /**
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function mapParticipant(): array
    {
        return [
            CFHIRDataTypeEncounterParticipant::build(
                [
                    "type"       => "ADM",
                    "individual" => $this->resource->addReference(
                        get_class(new CMedecin()),
                        $this->object->loadRefMedecinTraitant()
                    ),
                ]
            ),
        ];
    }

    /**
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function mapPeriod(): ?CFHIRDataTypePeriod
    {
        return CFHIRDataTypePeriod::build(
            CFHIRDataTypeInstant::formatPeriod($this->object->entree, $this->object->sortie)
        );
    }
}
