<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Resources\R4\Organization\Mapper;

use Ox\Interop\Fhir\Contracts\Delegated\DelegatedObjectMapperInterface;
use Ox\Interop\Fhir\Contracts\Mapping\R4\OrganizationMappingInterface;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeBoolean;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeCode;
use Ox\Interop\Fhir\Datatypes\CFHIRDataTypeString;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeAddress;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeCodeableConcept;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeCoding;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeContactPoint;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeExtension;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeIdentifier;
use Ox\Interop\Fhir\Datatypes\Complex\CFHIRDataTypeReference;
use Ox\Interop\Fhir\Profiles\CFHIRInteropSante;
use Ox\Interop\Fhir\Resources\R4\Organization\CFHIRResourceOrganization;
use Ox\Interop\Fhir\Resources\R4\Organization\Profiles\InteropSante\CFHIRResourceOrganizationFR;
use Ox\Interop\Fhir\Resources\ResourceTrait;
use Ox\Mediboard\Patients\CExercicePlace;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionException;

/**
 * Description
 */
class FrOrganization extends Organization
{
    use ResourceTrait;

    /** @var CExercicePlace */
    protected $object;

    /** @var CFHIRResourceOrganizationFR */
    protected $resource;

    public function mapExtension(): array
    {
        return [
            CFHIRDataTypeExtension::addExtension(
                'http://interopsante.org/fhir/StructureDefinition/FrOrganizationShortName',
                [
                    'valueString' => $this->object->raison_sociale,
                ]
            ),
        ];
    }

    /**
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function mapIdentifier(): array
    {
        $identifiers = [];
        // SYSTEM
        $system = 'http://interopsante.org/CodeSystem/fr-v2-0203';

        // USE
        $use = 'usual';

        if ($this->object->finess_juridique) {
            // FINEJ
            $code       = 'FINEJ';
            $display    = 'FINESS d\'entité juridique';
            $coding     = CFHIRDataTypeCoding::addCoding($system, $code, $display);
            $finej      = CFHIRDataTypeCodeableConcept::addCodeable($coding);
            $finejValue = $this->object->finess_juridique;

            // add FINEJ as identifier
            $identifiers[] = CFHIRDataTypeIdentifier::addIdentifier(
                $this->resource->identifier,
                $finejValue,
                $finej,
                $use,
                $system
            );
        }

        if ($this->object->finess) {
            // FINEG
            $code       = 'FINEG';
            $display    = 'FINESS d\'entité géographique';
            $coding     = CFHIRDataTypeCoding::addCoding($system, $code, $display);
            $fineg      = CFHIRDataTypeCodeableConcept::addCodeable($coding);
            $finegValue = $this->object->finess;

            // add FINEG as identifier
            $identifiers[] = CFHIRDataTypeIdentifier::addIdentifier(
                $this->resource->identifier,
                $finegValue,
                $fineg,
                $use,
                $system
            );
        }

        if ($this->object->siren) {
            // SIREN
            $code       = 'SIREN';
            $display    = 'Identification de l\'organisation au SIREN';
            $coding     = CFHIRDataTypeCoding::addCoding($system, $code, $display);
            $siren      = CFHIRDataTypeCodeableConcept::addCodeable($coding);
            $sirenValue = $this->object->siren;

            // add SIREN as identifier
            $identifiers[] = CFHIRDataTypeIdentifier::addIdentifier(
                $this->resource->identifier,
                $sirenValue,
                $siren,
                $use,
                $system
            );
        }


        if ($this->object->siret) {
            // SIRET
            $code       = 'SIRET';
            $display    = 'Identification de l\'organisation au SIRET';
            $coding     = CFHIRDataTypeCoding::addCoding($system, $code, $display);
            $siret      = CFHIRDataTypeCodeableConcept::addCodeable($coding);
            $siretValue = $this->object->siret;

            // add SIRET as identifier
            $identifiers[] = CFHIRDataTypeIdentifier::addIdentifier(
                $this->resource->identifier,
                $siretValue,
                $siret,
                $use,
                $system
            );
        }

        return $identifiers;
    }

    public function mapType(): array
    {
        $types = [];

        // organizationType
        $system           = "https://simplifier.net/frenchprofiledfhirar/v2-3307";
        $code             = "GROUP";
        $display          = "Groupe privé/hospitalier";
        $coding           = CFHIRDataTypeCoding::addCoding($system, $code, $display);
        $organizationType = $coding;

        // secteurActiviteRASS
        $system  = "https://mos.esante.gouv.fr/NOS/TRE_R02-SecteurActivite/FHIR/TRE-R02-SecteurActivite";
        $code    = "SA04";
        $display = "Etablissement privé non PSPH";
        //$version             = 20201030120000;
        //$user_selected             = true;
        $coding              = CFHIRDataTypeCoding::addCoding($system, $code, $display);
        $secteurActiviteRASS = $coding;

        // categorieEtablissementRASS
        $system  = "https://mos.esante.gouv.fr/NOS/TRE_R66-CategorieEtablissement/FHIR/TRE-R66-CategorieEtablissement";
        $code    = "106";
        $display = "Centre hospitalier, ex HÏ„pital local";
        //$version             = 20210528120000;
        //$user_selected             = true;
        $coding                     = CFHIRDataTypeCoding::addCoding($system, $code, $display);
        $categorieEtablissementRASS = $coding;

        $types[] = CFHIRDataTypeCodeableConcept::addCodeable($organizationType, '');
        $types[] = CFHIRDataTypeCodeableConcept::addCodeable($secteurActiviteRASS, '', $this->resource->type);
        $types[] = CFHIRDataTypeCodeableConcept::addCodeable($categorieEtablissementRASS, '', $this->resource->type);

        return $types;
    }

    public function getProfiles(): array
    {
        return [CFHIRInteropSante::class];
    }
}
