<?php
/**
 * @package Mediboard\fhir
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

/**
 * Description
 */

namespace Ox\Interop\Fhir\Tests\Unit;

use Ox\Interop\Fhir\ClassMap\FHIRClassMap;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Profiles\CFHIRAnnuaireSante;
use Ox\Interop\Fhir\Profiles\CFHIRANS;
use Ox\Interop\Fhir\Profiles\CFHIRInteropSante;
use Ox\Interop\Fhir\Profiles\CFHIRPHD;
use Ox\Interop\Fhir\Resources\CFHIRResource;
use Ox\Interop\Fhir\Resources\R4\AllergyIntolerance\CFHIRResourceAllergyIntolerance;
use Ox\Interop\Fhir\Resources\R4\Appointment\CFHIRResourceAppointment;
use Ox\Interop\Fhir\Resources\R4\Appointment\Profiles\InteropSante\CFHIRResourceAppointmentFR;
use Ox\Interop\Fhir\Resources\R4\Binary\CFHIRResourceBinary;
use Ox\Interop\Fhir\Resources\R4\Bundle\CFHIRResourceBundle;
use Ox\Interop\Fhir\Resources\R4\Bundle\Profiles\ANS\CDL\CFHIRResourceBundleCreationNoteCdL;
use Ox\Interop\Fhir\Resources\R4\CapabilityStatement\CFHIRResourceCapabilityStatement;
use Ox\Interop\Fhir\Resources\R4\ConceptMap\CFHIRResourceConceptMap;
use Ox\Interop\Fhir\Resources\R4\Device\CFHIRResourceDevice;
use Ox\Interop\Fhir\Resources\R4\Device\Profiles\PHD\CFHIRResourceDevicePHD;
use Ox\Interop\Fhir\Resources\R4\DocumentManifest\CFHIRResourceDocumentManifest;
use Ox\Interop\Fhir\Resources\R4\DocumentReference\CFHIRResourceDocumentReference;
use Ox\Interop\Fhir\Resources\R4\DocumentReference\Profiles\ANS\CFHIRResourceDocumentReferenceCdL;
use Ox\Interop\Fhir\Resources\R4\Encounter\CFHIRResourceEncounter;
use Ox\Interop\Fhir\Resources\R4\Observation\CFHIRResourceObservation;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationBMIENS;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationBodyHeightENS;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationBodyTemperatureENS;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationBodyWeightENS;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationBPENS;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationGlucoseENS;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationHeartrateENS;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationPainSeverityENS;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationStepsByDayENS;
use Ox\Interop\Fhir\Resources\R4\Observation\Profiles\ANS\CFHIRResourceObservationWaistCircumferenceENS;
use Ox\Interop\Fhir\Resources\R4\OperationOutcome\CFHIRResourceOperationOutcome;
use Ox\Interop\Fhir\Resources\R4\Organization\CFHIRResourceOrganization;
use Ox\Interop\Fhir\Resources\R4\Organization\Profiles\InteropSante\CFHIRResourceOrganizationFR;
use Ox\Interop\Fhir\Resources\R4\Parameters\CFHIRResourceParameters;
use Ox\Interop\Fhir\Resources\R4\Patient\CFHIRResourcePatient;
use Ox\Interop\Fhir\Resources\R4\Patient\Profiles\InteropSante\CFHIRResourcePatientFR;
use Ox\Interop\Fhir\Resources\R4\Practitioner\CFHIRResourcePractitioner;
use Ox\Interop\Fhir\Resources\R4\Practitioner\Profiles\ANS\CFHIRResourcePractitionerRASS;
use Ox\Interop\Fhir\Resources\R4\Practitioner\Profiles\InteropSante\CFHIRResourcePractitionerFR;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\CFHIRResourcePractitionerRole;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\Profiles\AnnuaireSante\CFHIRResourcePractitionerRoleOrganizationalRass;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\Profiles\AnnuaireSante\CFHIRResourcePractitionerRoleProfessionalRass;
use Ox\Interop\Fhir\Resources\R4\RelatedPerson\CFHIRResourceRelatedPerson;
use Ox\Interop\Fhir\Resources\R4\RelatedPerson\Profiles\InteropSante\CFHIRResourceRelatedPersonFR;
use Ox\Interop\Fhir\Resources\R4\Schedule\CFHIRResourceSchedule;
use Ox\Interop\Fhir\Resources\R4\Schedule\Profiles\InteropSante\CFHIRResourceScheduleFR;
use Ox\Interop\Fhir\Resources\R4\Slot\CFHIRResourceSlot;
use Ox\Interop\Fhir\Resources\R4\Slot\Profiles\InteropSante\CFHIRResourceSlotFR;
use Ox\Interop\Fhir\Resources\R4\StructureDefinition\CFHIRResourceStructureDefinition;
use Ox\Interop\Fhir\Utilities\SearchParameters\SearchParameter;
use Ox\Interop\Fhir\Utilities\SearchParameters\SearchParameterString;
use Ox\Tests\UnitTestMediboard;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Description
 */
class FHIRConformanceTest extends UnitTestMediboard
{
    /** @var string[] */
    private const RESOURCES_HL7_DEFINED = [
        CFHIRResourceAllergyIntolerance::class  => 'AllergyIntolerance',
        CFHIRResourceAppointment::class         => 'Appointment',
        CFHIRResourceBinary::class              => 'Binary',
        CFHIRResourceBundle::class              => 'Bundle',
        CFHIRResourceCapabilityStatement::class => 'CapabilityStatement',
        CFHIRResourceConceptMap::class          => 'ConceptMap',
        CFHIRResourceDevice::class              => 'Device',
        CFHIRResourceDocumentManifest::class    => 'DocumentManifest',
        CFHIRResourceDocumentReference::class   => 'DocumentReference',
        CFHIRResourceEncounter::class           => 'Encounter',
        CFHIRResourceObservation::class         => 'Observation',
        CFHIRResourceOperationOutcome::class    => 'OperationOutcome',
        CFHIRResourceOrganization::class        => 'Organization',
        CFHIRResourceParameters::class          => 'Parameters',
        CFHIRResourcePatient::class             => 'Patient',
        CFHIRResourcePractitioner::class        => 'Practitioner',
        CFHIRResourcePractitionerRole::class    => 'PractitionerRole',
        CFHIRResourceSchedule::class            => 'Schedule',
        CFHIRResourceSlot::class                => 'Slot',
        CFHIRResourceStructureDefinition::class => 'StructureDefinition',
        CFHIRResourceRelatedPerson::class       => 'RelatedPerson',
    ];

    private const RESOURCES_PROFILED_DEFINED = [
        CFHIRResourcePatientFR::class                          => 'FrPatient',
        CFHIRResourceAppointmentFR::class                      => 'FrAppointment',
        CFHIRResourceOrganizationFR::class                     => 'FrOrganization',
        CFHIRResourceSlotFR::class                             => 'FrSlot',
        CFHIRResourceScheduleFR::class                         => 'FrSchedule',
        CFHIRResourcePractitionerFR::class                     => 'FrPractitioner',
        CFHIRResourcePractitionerRASS::class                   => 'practitioner-rass',
        CFHIRResourcePractitionerRoleProfessionalRass::class   => 'practitionerRole-professionalRole-rass',
        CFHIRResourcePractitionerRoleOrganizationalRass::class => 'practitionerRole-organizationalRole-rass',
        CFHIRResourceBundleCreationNoteCdL::class              => 'CdL_BundleCreationNoteCdL',
        CFHIRResourceDocumentReferenceCdL::class               => 'CdL_DocumentReferenceCdL',
        CFHIRResourceRelatedPersonFR::class                    => 'FrRelatedPerson',
        CFHIRResourceObservationBodyTemperatureENS::class      => 'ENS_FrObservationBodyTemperature',
        CFHIRResourceObservationBMIENS::class                  => 'ENS_FrObservationBmi',
        CFHIRResourceObservationHeartrateENS::class            => 'ENS_FrObservationHeartrate',
        CFHIRResourceObservationBodyHeightENS::class           => 'ENS_FrObservationBodyHeight',
        CFHIRResourceObservationBodyWeightENS::class           => 'ENS_FrObservationBodyWeight',
        CFHIRResourceObservationStepsByDayENS::class           => 'ENS_ObservationStepsByDay',
        CFHIRResourceObservationBPENS::class                   => 'ENS_FrObservationBp',
        CFHIRResourceObservationWaistCircumferenceENS::class   => 'ENS_FrObservationWaistCircumference',
        CFHIRResourceObservationPainSeverityENS::class         => 'ENS_FrObservationPainSeverity',
        CFHIRResourceObservationGlucoseENS::class              => 'ENS_FrObservationGlucose',
        CFHIRResourceDevicePHD::class                          => 'PhdDevice',
    ];

    /** @var string[] */
    private const PROFILE_DEFINED = [
        CFHIR::class              => 'http://hl7.org/fhir/StructureDefinition/',
        CFHIRANS::class           => 'http://esante.gouv.fr/ci-sis/fhir/StructureDefinition/',
        CFHIRAnnuaireSante::class => 'https://apifhir.annuaire.sante.fr/ws-sync/exposed/structuredefinition/',
        CFHIRInteropSante::class  => 'http://interopsante.org/fhir/StructureDefinition/',
        CFHIRPHD::class           => 'http://hl7.org/fhir/uv/phd/StructureDefinition/',
    ];

    /**
     * @return array
     */
    public function providerBaseUrlProfile(): array
    {
        $data = [];
        foreach (self::PROFILE_DEFINED as $class => $value) {
            $data[$class] = ['class' => $class, 'expected' => $value];
        }

        return $data;
    }

    /**
     * @param string $class
     * @param string $expected
     *
     * @dataProvider providerBaseUrlProfile
     */
    public function testBaseURLProfile(string $class, string $expected): void
    {
        /** @var $class CFHIR */
        $this->assertEquals($expected, $class::BASE_PROFILE);
    }

    /**
     * @return array
     */
    public function providerResourceTypeFHIR(): array
    {
        $data = [];
        foreach (self::RESOURCES_HL7_DEFINED as $class => $value) {
            $data[$class] = ['resource' => $class, 'expected' => $value];
        }

        return $data;
    }

    /**
     * @param string $resource
     * @param string $expected
     *
     * @dataProvider providerResourceTypeFHIR
     */
    public function testResourceTypeFHIR(string $resource, string $expected): void
    {
        /** @var $resource CFHIRResource */
        $this->assertEquals($expected, $resource::RESOURCE_TYPE);
    }

    /**
     * @return array
     */
    public function providerResourceTypeProfile(): array
    {
        $data = [];
        foreach (self::RESOURCES_PROFILED_DEFINED as $class => $value) {
            $data[$class] = ['resource' => $class, 'expected' => $value];
        }

        return $data;
    }

    /**
     * @param string $resource
     * @param string $expected
     *
     * @dataProvider providerResourceTypeProfile
     */
    public function testResourceTypeProfile(string $resource, string $expected): void
    {
        /** @var $resource CFHIRResource */
        $this->assertEquals($expected, $resource::PROFILE_TYPE);
    }

    /**
     * @return array
     */
    public function providerResourceTypeDefined(): array
    {
        $map  = new FHIRClassMap();
        $data = [];

        foreach ($map->resource->listResources(null, CFHIR::class) as $resource) {
            $class        = get_class($resource);
            $data[$class] = ['resource_class' => $class];
        }

        return $data;
    }

    /**
     * @param string $resource_class
     *
     * @dataProvider providerResourceTypeDefined
     */
    public function testResourceTypeDefined(string $resource_class): void
    {
        $resource_classes = array_keys(self::RESOURCES_HL7_DEFINED);
        $this->assertTrue(
            in_array($resource_class, $resource_classes),
            "The resource '$resource_class' is not defined in constant Ox\Mediboard\\fhir\Tests\Unit\CFHIRTest::RESOURCES_HL7_DEFINED"
        );
    }

    /**
     * @return array
     */
    public function providerResourceProfileDefined(): array
    {
        $map  = new FHIRClassMap();
        $data = [];

        foreach ($map->resource->setReturnClass(true)->listProfiled() as $class) {
            $data[$class] = ['resource_class' => $class];
        }

        return $data;
    }

    /**
     * @param string $resource_class
     *
     * @dataProvider providerResourceProfileDefined
     */
    public function testResourceProfileDefined(string $resource_class): void
    {
        $resource_classes = array_keys(self::RESOURCES_PROFILED_DEFINED);

        $this->assertTrue(
            in_array($resource_class, $resource_classes),
            "The resource '$resource_class' is not defined in constant Ox\Mediboard\\fhir\Tests\Unit\CFHIRTest::RESOURCES_PROFILED_DEFINED"
        );
    }

    /**
     * @return array
     */
    public function providerProfileDefined(): array
    {
        $map  = new FHIRClassMap();
        $data = [];

        foreach ($map->profile->setReturnClass(true)->listProfiles() as $class) {
            $data[$class] = ['profile_class' => $class];
        }

        return $data;
    }

    /**
     * @param string $profile_class
     *
     * @dataProvider providerProfileDefined
     */
    public function testProfileDefined(string $profile_class): void
    {
        $profiles_classes = array_keys(self::PROFILE_DEFINED);

        $this->assertTrue(
            in_array($profile_class, $profiles_classes),
            "The profile '$profile_class' is not defined in constant Ox\Mediboard\\fhir\Tests\Unit\CFHIRTest::PROFILE_DEFINED"
        );
    }

    /**
     * @param mixed $expectedResult
     * @param mixed $input
     *
     * @dataProvider providerParameters
     */
    public function testParameters($expectedResult, $input): void
    {
        $this->assertSame($expectedResult, $input);
    }

    /**
     * @return array[]
     */
    public function providerParameters(): array
    {
        $format = new SearchParameter(new SearchParameterString('format'), 'application/fhir+xml');
        $family = new SearchParameter(new SearchParameterString('family'), 'DUPONT');
        $given  = new SearchParameter(new SearchParameterString('given'), 'Jean');
        $sex    = new SearchParameter(new SearchParameterString('sex'), 'm');

        $parameterBag = new ParameterBag();
        $parameterBag->set($format->getParameterName(), $format);
        $parameterBag->set($family->getParameterName(), $family);
        $parameterBag->set($given->getParameterName(), $given);
        $parameterBag->set($sex->getParameterName(), $sex);
        $parameterBag->set('url', 'www.openxtrem.com');

        $resource = new CFHIRResourcePatient();
        $resource->setParameter($parameterBag);

        return [
            'FHIR searchParameters' => [count($resource->getParameters()), 4],
            'FHIR brutParameters'   => [count($resource->getParametersBrut()), 1],
            'FHIR getParameter'     => [$resource->getParameter('family')->getValue(), 'DUPONT'],
            'FHIR getBrutParameter' => [$resource->getParameterBrut('url'), 'www.openxtrem.com'],
            'FHIR getNullParameter' => [$resource->getParameter('birthdate'), null],
        ];
    }


    public function providerUnicityDelegatedShortnameClass(): array
    {
        $fhir_map = new FHIRClassMap();
        $fhir_map->delegated->setReturnClass(true);
        $data            = [];
        $types_delegated = ['mapper', 'searcher', 'handle'];

        foreach ($types_delegated as $type_delegated) {
            $delegated_classes = $fhir_map->delegated->listDelegated($type_delegated);

            foreach ($delegated_classes as $delegated_class) {
                $short_name = substr($delegated_class, strrpos($delegated_class, '\\') + 1, strlen($delegated_class));

                $data["Unicity of $short_name for $type_delegated"] = [
                    $short_name,
                    $delegated_classes,
                    $type_delegated,
                ];
            }
        }

        return $data;
    }

    /**
     * Assert that each delegated object has a unique shortname per type of delegated object
     *
     * @dataProvider providerUnicityDelegatedShortnameClass
     *
     * @param string $shortname
     * @param array  $all_delegated_per_type
     * @param string $delegated_type
     *
     * @return void
     */
    public function testUnicityDelegatedShortnameClass(
        string $shortname,
        array $all_delegated_per_type,
        string $delegated_type
    ): void {
        $classes = array_filter($all_delegated_per_type, function ($class) use ($shortname) {
            return str_ends_with($class, "\\$shortname");
        });

        $nb_classes = count($classes);

        $this->assertCount(
            1,
            $classes,
            "The shortname class '$shortname' is not unique, $nb_classes as the same shortname for $delegated_type delegated object"
        );
    }
}
