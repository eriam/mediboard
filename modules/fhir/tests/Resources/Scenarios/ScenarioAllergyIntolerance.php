<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Tests\Resources\Scenarios;

use Ox\Core\CMbDT;
use Ox\Core\CMbException;
use Ox\Core\CModelObjectException;
use Ox\Interop\Fhir\Resources\R4\AllergyIntolerance\CFHIRResourceAllergyIntolerance;
use Ox\Interop\Fhir\Tests\Resources\FhirScenarios;
use Ox\Mediboard\Loinc\CLoinc;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Patients\CAntecedent;
use Ox\Mediboard\Patients\CDossierMedical;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\Sante400\CIdSante400;
use Ox\Tests\Fixtures\FixturesUsersGenerator;
use Ox\Tests\TestsException;

class ScenarioAllergyIntolerance extends FhirScenarios
{
    /**
     * @return CFHIRResourceAllergyIntolerance
     * @throws TestsException
     */
    protected function scenarioAllergyLoinc(): CFHIRResourceAllergyIntolerance
    {
        $antecedent = $this->getBaseAntecedent();
        $antecedent->date = $this->getRandomDate(true);
        $antecedent->rques = "allergy loinc";
        $antecedent->store();

        $idex = new CIdSante400();
        $idex->setObject($antecedent);
        $idex->tag = CLoinc::getLoincTag();
        $idex->id400 = '52120';

        $resource = new CFHIRResourceAllergyIntolerance();
        $resource->mapFrom($antecedent);

        return $resource;
    }

    /**
     * @return CFHIRResourceAllergyIntolerance
     * @throws TestsException
     */
    protected function scenarioAllergyCIM10(): CFHIRResourceAllergyIntolerance
    {
        $antecedent = $this->getBaseAntecedent();
        $antecedent->date = $this->getRandomDate(true);
        $antecedent->date_fin = CMbDT::date('+1 YEARS', $antecedent->date);
        $antecedent->rques = "L509";
        $antecedent->store();

        $resource = new CFHIRResourceAllergyIntolerance();
        $resource->mapFrom($antecedent);

        return $resource;
    }

    /**
     * @return CFHIRResourceAllergyIntolerance[]
     */
    protected function createFhirResources(): array
    {
        return [
            'Allergy with code loinc' => $this->scenarioAllergyLoinc(),
            'Allergy with code CM10' => $this->scenarioAllergyCIM10()
        ];
    }

    /**
     * @return CAntecedent
     * @throws TestsException
     */
    protected function getBaseAntecedent(): CAntecedent
    {
        $is_major = random_int(0, 1) ? true : false;
        $object = new CAntecedent();
        $object->type = 'alle';
        $object->majeur = $is_major;
        $object->important = $is_major;
        $object->owner_id = $this->getDoctor()->_id;
        $object->dossier_medical_id = $this->getDossierMedical()->_id;

        return $object;
    }

    /**
     * @return CMediusers
     * @throws TestsException
     */
    protected function getDoctor(): CMediusers
    {
        return FixturesUsersGenerator::generate(1)[0];
    }

    /**
     * @return CDossierMedical
     */
    protected function getDossierMedical(): CDossierMedical
    {
        $patient = $this->getPatient();
        CDossierMedical::dossierMedicalId($patient->_id, $patient->_class);

        return $patient->loadRefDossierMedical();
    }

    /**
     * @return CPatient
     * @throws CMbException
     * @throws CModelObjectException
     * @throws TestsException
     */
    protected function getPatient(): CPatient
    {
        /** @var CPatient $patient
         */
        $patient = CPatient::getSampleObject();
        if ($msg = $patient->store()) {
            throw new CMbException($msg);
        }

        return $patient;
    }

    /**
     * @param false $random_progressive
     *
     * @return string
     * @throws \Exception
     */
    protected function getRandomDate($random_progressive = false): string
    {
        $random_day = random_int(1, 10000);
        $date = CMbDT::date("-$random_day DAYS");

        // make progressive date
        if ($random_progressive && random_int(0, 100) > 80) {
            $date = preg_replace('#(\d{4})\-(\d{2})\-(\d{2})#', '${1}-${2}-00', $date);

            if (random_int(0, 100) > 50) {
                $date = preg_replace('#(\d{4})\-(\d{2})\-(\d{2})#', '${1}-00-00', $date);
            }
        }

        return $date;
    }
}
