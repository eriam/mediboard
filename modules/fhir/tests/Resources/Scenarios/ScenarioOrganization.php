<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Tests\Resources\Scenarios;

use Exception;
use Ox\Interop\Fhir\Resources\R4\Organization\CFHIRResourceOrganization;
use Ox\Interop\Fhir\Resources\R4\Organization\Profiles\InteropSante\CFHIRResourceOrganizationFR;
use Ox\Interop\Fhir\Tests\Resources\FhirScenarios;
use Ox\Mediboard\Patients\CExercicePlace;
use Ox\Tests\TestsException;

class ScenarioOrganization extends FhirScenarios
{
    /**
     * @return CFHIRResourceOrganization
     * @throws TestsException
     * @throws Exception
     */
    protected function scenarioOrganization(): CFHIRResourceOrganization
    {
        $organization = $this->getRandomObjects(CExercicePlace::class);

        $resource = new CFHIRResourceOrganization();
        $resource->mapFrom($organization);

        return $resource;
    }

    /**
     * @return CFHIRResourceOrganizationFR
     * @throws TestsException
     * @throws Exception
     */
    protected function scenarioOrganizationFR(): CFHIRResourceOrganizationFR
    {
        $organization = $this->getRandomObjects(CExercicePlace::class);

        $resource = new CFHIRResourceOrganizationFR();
        $resource->mapFrom($organization);

        return $resource;
    }

    /**
     * @return CFHIRResourceOrganization
     * @throws TestsException
     */
    protected function createFhirResources(): array
    {
        return [
            'Organization' => $this->scenarioOrganization(),
            'FrOrganization' => $this->scenarioOrganizationFR()
        ];
    }
}
