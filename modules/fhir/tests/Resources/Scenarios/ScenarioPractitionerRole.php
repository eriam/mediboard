<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Tests\Resources\Scenarios;

use Exception;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\CFHIRResourcePractitionerRole;
use Ox\Interop\Fhir\Resources\R4\PractitionerRole\Profiles\AnnuaireSante\CFHIRResourcePractitionerRoleProfessionalRass;
use Ox\Interop\Fhir\Tests\Resources\FhirScenarios;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Tests\TestsException;

class ScenarioPractitionerRole extends FhirScenarios
{
    /**
     * @return CFHIRResourcePractitionerRole
     * @throws TestsException
     * @throws Exception
     */
    protected function scenarioPractitionerRole(): CFHIRResourcePractitionerRole
    {
        $practitionerRole = $this->getRandomObjects(CMediusers::class);

        $resource = new CFHIRResourcePractitionerRole();
        $resource->mapFrom($practitionerRole);

        return $resource;
    }

    /**
     * @return CFHIRResourcePractitionerRoleProfessionalRass
     * @throws TestsException
     * @throws Exception
     */
    protected function scenarioPractitionerRoleFR(): CFHIRResourcePractitionerRoleProfessionalRass
    {
        $practitionerRole = $this->getRandomObjects(CMediusers::class);

        $resource = new CFHIRResourcePractitionerRoleProfessionalRass();
        $resource->mapFrom($practitionerRole);

        return $resource;
    }

    /**
     * @return CFHIRResourcePractitionerRole
     * @throws TestsException
     */
    protected function createFhirResources(): array
    {
        return [
            'PractitionerRole' => $this->scenarioPractitionerRole(),
            'FrPractitionerRole' => $this->scenarioPractitionerRoleFR()
        ];
    }
}
