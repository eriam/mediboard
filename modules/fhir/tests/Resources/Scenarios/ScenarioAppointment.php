<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Tests\Resources\Scenarios;

use Exception;
use Ox\Core\CMbDT;
use Ox\Interop\Fhir\Resources\R4\Appointment\CFHIRResourceAppointment;
use Ox\Interop\Fhir\Resources\R4\Appointment\Profiles\InteropSante\CFHIRResourceAppointmentFR;
use Ox\Interop\Fhir\Tests\Resources\FhirScenarios;
use Ox\Mediboard\Cabinet\CConsultation;

class ScenarioAppointment extends FhirScenarios
{
    /**
     * @return CFHIRResourceAppointment
     * @throws Exception
     */
    protected function scenarioAppointment(): CFHIRResourceAppointment
    {
        /** @var CConsultation $appointment */
        $appointment = $this->getRandomObjects(CConsultation::class);

        $resource = new CFHIRResourceAppointment();
        $resource->mapFrom($appointment);

        return $resource;
    }

    /**
     * @return CFHIRResourceAppointmentFR
     * @throws Exception
     */
    protected function scenarioAppointmentFR(): CFHIRResourceAppointmentFR
    {
        /** @var CConsultation $appointment */
        $appointment = $this->getRandomObjects(CConsultation::class);

        $resource = new CFHIRResourceAppointmentFR();
        $resource->mapFrom($appointment);

        return $resource;
    }

    /**
     * @throws Exception
     */
    protected function createFhirResources(): array
    {
        return [
            'Appointment' => $this->scenarioAppointment(),
            'FrAppointment' => $this->scenarioAppointmentFR()
        ];
    }
}
