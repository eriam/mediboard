<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Tests\Resources\Scenarios;

use Exception;
use Ox\Interop\Fhir\Resources\R4\Practitioner\CFHIRResourcePractitioner;
use Ox\Interop\Fhir\Resources\R4\Practitioner\Profiles\InteropSante\CFHIRResourcePractitionerFR;
use Ox\Interop\Fhir\Tests\Resources\FhirScenarios;
use Ox\Mediboard\Patients\CMedecin;
use Ox\Tests\TestsException;

class ScenarioPractitioner extends FhirScenarios
{
    /**
     * @return CFHIRResourcePractitioner
     * @throws TestsException
     * @throws Exception
     */
    protected function scenarioPractitioner(): CFHIRResourcePractitioner
    {
        $practitioner = $this->getRandomObjects(CMedecin::class);

        $resource = new CFHIRResourcePractitioner();
        $resource->mapFrom($practitioner);

        return $resource;
    }

    /**
     * @return CFHIRResourcePractitionerFR
     * @throws TestsException
     * @throws Exception
     */
    protected function scenarioPractitionerFR(): CFHIRResourcePractitionerFR
    {
        $practitioner = $this->getRandomObjects(CMedecin::class);

        $resource = new CFHIRResourcePractitionerFR();
        $resource->mapFrom($practitioner);

        return $resource;
    }

    /**
     * @return CFHIRResourcePractitioner
     * @throws TestsException
     */
    protected function createFhirResources(): array
    {
        return [
            'Practitioner' => $this->scenarioPractitioner(),
            'FrPractitioner' => $this->scenarioPractitionerFR()
        ];
    }
}
