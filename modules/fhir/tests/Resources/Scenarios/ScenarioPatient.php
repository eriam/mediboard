<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Tests\Resources\Scenarios;

use Exception;
use Ox\Interop\Fhir\Resources\R4\Patient\CFHIRResourcePatient;
use Ox\Interop\Fhir\Resources\R4\Patient\Profiles\InteropSante\CFHIRResourcePatientFR;
use Ox\Interop\Fhir\Tests\Resources\FhirScenarios;
use Ox\Mediboard\Patients\CPatient;
use Ox\Tests\TestsException;

class ScenarioPatient extends FhirScenarios
{
    /**
     * @return CFHIRResourcePatient
     * @throws TestsException
     * @throws Exception
     */
    protected function scenarioPatient(): CFHIRResourcePatient
    {
        $patient = $this->getRandomObjects(CPatient::class);

        $resource = new CFHIRResourcePatient();
        $resource->mapFrom($patient);

        return $resource;
    }

    /**
     * @return CFHIRResourcePatientFR
     * @throws TestsException
     * @throws Exception
     */
    protected function scenarioPatientFR(): CFHIRResourcePatientFR
    {
        $patient = $this->getRandomObjects(CPatient::class);

        $resource = new CFHIRResourcePatientFR();
        $resource->mapFrom($patient);

        return $resource;
    }

    /**
     * @return CFHIRResourcePatient
     * @throws TestsException
     */
    protected function createFhirResources(): array
    {
        return [
            'Patient' => $this->scenarioPatient(),
            'FrPatient' => $this->scenarioPatientFR()
        ];
    }
}
