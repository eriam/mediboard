<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Tests\Resources\Scenarios;

use Exception;
use Ox\Interop\Fhir\Resources\R4\Schedule\CFHIRResourceSchedule;
use Ox\Interop\Fhir\Resources\R4\Schedule\Profiles\InteropSante\CFHIRResourceScheduleFR;
use Ox\Interop\Fhir\Tests\Resources\FhirScenarios;
use Ox\Mediboard\Cabinet\CPlageconsult;

class ScenarioSchedule extends FhirScenarios
{
    /**
     * @return CFHIRResourceSchedule
     * @throws Exception
     */
    protected function scenarioSchedule(): CFHIRResourceSchedule
    {
        /** @var CPlageconsult $schedule */
        $schedule = $this->getRandomObjects(CPlageconsult::class);

        $resource = new CFHIRResourceSchedule();
        $resource->mapFrom($schedule);

        return $resource;
    }

    /**
     * @return CFHIRResourceScheduleFR
     * @throws Exception
     */
    protected function scenarioScheduleFR(): CFHIRResourceScheduleFR
    {
        /** @var CPlageconsult $schedule */
        $schedule = $this->getRandomObjects(CPlageconsult::class);

        $resource = new CFHIRResourceScheduleFR();
        $resource->mapFrom($schedule);

        return $resource;
    }

    /**
     * @return CFHIRResourceSchedule[]
     * @throws Exception
     */
    protected function createFhirResources(): array
    {
        return [
            'Schedule' => $this->scenarioSchedule(),
            'FrSchedule' => $this->scenarioScheduleFR()
        ];
    }
}
