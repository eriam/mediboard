<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Fhir\Tests\Resources\Scenarios;

use Exception;
use Ox\Interop\Fhir\Resources\R4\Slot\CFHIRResourceSlot;
use Ox\Interop\Fhir\Resources\R4\Slot\Profiles\InteropSante\CFHIRResourceSlotFR;
use Ox\Interop\Fhir\Tests\Resources\FhirScenarios;
use Ox\Mediboard\Cabinet\CPlageconsult;

class ScenarioSlot extends FhirScenarios
{
    /**
     * @return CFHIRResourceSlot
     * @throws Exception
     */
    protected function scenarioSlot(): CFHIRResourceSlot
    {
        /** @var CPlageconsult $scenario */
        $scenario = $this->getRandomObjects(CPlageconsult::class);

        $resource = new CFHIRResourceSlot();
        $resource->mapFrom($scenario);

        return $resource;
    }

    /**
     * @return CFHIRResourceSlot
     * @throws Exception
     */
    protected function scenarioSlotFR(): CFHIRResourceSlotFR
    {
        /** @var CPlageconsult $scenario */
        $scenario = $this->getRandomObjects(CPlageconsult::class);

        $resource = new CFHIRResourceSlotFR();
        $resource->mapFrom($scenario);

        return $resource;
    }

    /**
     * @return CFHIRResourceSlot[]
     * @throws Exception
     */
    protected function createFhirResources(): array
    {
        return [
            'Slot' => $this->scenarioSlot(),
            'FrSlot' => $this->scenarioSlotFR()
        ];
    }
}
