<?php
/**
 * @package Mediboard\Cabinet
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use Ox\Core\CView;

CView::checkin();

?>

<div class="big-info">
  <p>
    La télétransmission des <strong>feuilles de soins électroniques (FSE)</strong>
    se fait Î° présent dans le <strong>module FSE</strong>
    <img src="modules/fse/images/icon.png" title="IcÏ„ne du module FSE" />
  </p>
</div>
