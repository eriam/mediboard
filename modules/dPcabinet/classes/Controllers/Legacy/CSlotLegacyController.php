<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Cabinet\Controllers\Legacy;

use Ox\Core\CApp;
use Ox\Core\CLegacyController;
use Ox\Core\CMbDT;
use Ox\Core\CSQLDataSource;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Mediboard\Cabinet\CPlageconsult;

class CSlotLegacyController extends CLegacyController
{
    public function modalReplaySlot(): void
    {
        $this->checkPermAdmin();

        CView::checkin();

        $this->renderSmarty('vw_replay_slot');
    }

    public function replaySlot(): void
    {
        $this->checkPermAdmin();

        $start = CView::get("start", "num default|0");

        CView::checkin();

        CApp::setTimeLimit(300);
        CApp::setMemoryLimit("2048M");

        $plage_consult  = new CPlageconsult();
        $step           = 1000;
        $limit          = "$start,$step";
        $where          = ["date" => ">= '2021-01-01'"];
        $plages_consult = $plage_consult->loadList($where, null, $limit);
        $insert         = [];
        $ds             = CSQLDataSource::get("std");

        CStoredObject::massLoadBackRefs($plages_consult, "slots");
        foreach ($plages_consult as $_plage_consult) {
            $slots_by_datetime = [];
            $_plage_consult->loadRefsSlots();
            $date           = CMbDT::date($_plage_consult->date);
            $datetime_start = $date . " " . CMbDT::time($_plage_consult->debut);
            $datetime_end   = $date . " " . CMbDT::time($_plage_consult->fin);
            foreach ($_plage_consult->_ref_slots as $_slot) {
                if (!isset($slots_by_datetime[$_slot->start])) {
                    $slots_by_datetime[$_slot->start] = [];
                }
                $slots_by_datetime[$_slot->start][$_slot->_id] = $_slot;
            }
            while ($datetime_start < $datetime_end) {
                $datetime_end_slot = CMbDT::addDateTime($_plage_consult->freq, $datetime_start);

                //Si aucun slot pour ce créneau => on en crée un dispo
                if (!isset($slots_by_datetime[$datetime_start])) {
                    $insert[] = [
                        "plageconsult_id" => $_plage_consult->_id,
                        "start"           => $datetime_start,
                        "end"             => $datetime_end_slot,
                    ];
                } else {
                    //Si un slot busy existe pour ce créneau on vérifie les surréservation
                    ksort($slots_by_datetime[$datetime_start]);
                    $first_slot = reset($slots_by_datetime[$datetime_start]);
                    if ($first_slot->status == "busy") {
                        $first_slot->overbooked = 0;
                        $first_slot->rawStore();
                    }
                }

                $datetime_start = $datetime_end_slot;
            }
        }

        if (!empty($insert)) {
            $ds->insertMulti("slot", $insert, 1000);
        }

        $this->renderJson(["countPlage" => count($plages_consult)]);
    }
}
