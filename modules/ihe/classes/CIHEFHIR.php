<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Ihe;


use Exception;
use Ox\Core\CClassMap;
use Ox\Interop\Eai\CMessageSupported;
use Ox\Interop\Fhir\ClassMap\FHIRClassMap;
use Ox\Interop\Fhir\Exception\CFHIRExceptionInformational;
use Ox\Interop\Fhir\Exception\CFHIRExceptionNotSupported;
use Ox\Interop\Fhir\Profiles\CFHIR;
use Ox\Interop\Fhir\Resources\CFHIRResource;

/**
 * Class CIHE
 * IHE classes
 */
class CIHEFHIR extends CIHE
{
    /** @var string */
    public const TYPE = 'FHIR';

    /** @var string */
    protected const PREFIX_TRANSLATE_VERSION = 'FHIR';

    /**
     * @return array
     * @throws Exception
     */
    public function getCategoryVersions(): array
    {
        if ($this->_versions_category) {
            return $this->_versions_category;
        }

        $map = (new FHIRClassMap());
        $versions = [];
        foreach (array_keys($this->_categories) as $canonical) {
            $versions[$canonical] = $map->version->getResourceVersions($canonical);
        }

        return $this->_versions_category = $versions;
    }

    /**
     * @param CMessageSupported $message_supported
     * @param string            $category
     *
     * @return bool
     */
    protected function isTransactionSupported(CMessageSupported $message_supported, string $category): bool
    {
        if (!$message_supported->transaction) {
            return false;
        }

        $map               = new FHIRClassMap();
        $expected_resource = $map->resource->getResource($category);
        $actual_resource   = $map->resource->getResource($message_supported->transaction);

        return $actual_resource instanceof $expected_resource;
    }

    /**
     * @param string $canonical
     *
     * @return string
     * @throws Exception
     */
    public function getResourceProfile(string $canonical): string
    {
        $map      = new FHIRClassMap();
        if (!$resource = $map->resource->getResource($canonical)) {
            return '';
        }

        return CClassMap::getInstance()->getShortName($resource::PROFILE_CLASS);
    }

    /**
     * @param string $ref_canonical
     * @param string $profile_resource
     *
     * @return string
     * @throws Exception
     */
    public function getCanonical(string $ref_canonical, string $profile_resource): string
    {
        $map = new FHIRClassMap();
        if (!$resource = $map->resource->getResource($ref_canonical)) {
            throw new CFHIRExceptionNotSupported("Resource '$ref_canonical' not supported");
        }

        /** @var CFHIR $profile_resource */
        $profile_resource = new $profile_resource();

        /** @var CFHIRResource[] $resources */
        $resources = $map->resource->listResources($resource::RESOURCE_TYPE);
        $resources = array_filter($resources, function(CFHIRResource $resource) use ($profile_resource) {
            return $resource::PROFILE_CLASS === get_class($profile_resource);
        });

        // only one resource profiled for the same actor profile resource
        if (count($resources) === 1) {
            return reset($resources)->getProfile();
        }

        // find resource from context of profiling
        if ($resource::RESOURCE_CONTEXT_PROFILING) {
            $resources = array_filter($resources, function(CFHIRResource $element) use ($resource) {
                return $resource::RESOURCE_CONTEXT_PROFILING === $element::RESOURCE_CONTEXT_PROFILING;
            });

            if (count($resources) === 1) {
                return reset($resources)->getProfile();
            }
        }

        throw new CFHIRExceptionInformational(
            "The resource is possibly not supported or RESOURCE_CONTEXT_PROFILING is not defined and they are multiples profiling for the same resource and the same actor profile"
        );
    }
}
