{{*
 * @package Mediboard\Jfse
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}

<table class="tbl me-no-box-shadow">
    <thead>
        {{mb_include module=jfse template=medical_act/tarif_selection}}
        <tr>
            <th>
                <button type="button" class="edit notext" onclick="MedicalActs.editActs('{{$consultation->_id}}', '{{$invoice->id}}');">Gérer les actes</button>
            </th>
            <th>
                Code
            </th>
            <th>
                Suppléments
            </th>
            <th>
                {{tr}}Date{{/tr}}
            </th>
            <th style="text-align: right;">
                {{tr}}CActeNGAP-montant_base{{/tr}}
            </th>
            <th style="text-align: right;">
                {{tr}}CActeNGAP-montant_depassement{{/tr}}
            </th>
            <th style="text-align: right;" title="{{tr}}CJfseActPricing-rate-desc{{/tr}}">
                {{tr}}CJfseActPricing-rate{{/tr}}
            </th>
            <th style="text-align: right;" title="{{tr}}CJfseActPricing-total_amo-desc{{/tr}}">
                {{tr}}CJfseActPricing-total_amo{{/tr}}
            </th>
            <th style="text-align: right;" title="{{tr}}CJfseActPricing-total_amc-desc{{/tr}}">
                {{tr}}CJfseActPricing-total_amc{{/tr}}
            </th>
            <th style="text-align: right;">
                Montant Total
            </th>
            <th class="narrow"></th>
        </tr>
    </thead>
    <tbody>
        {{if count($consultation->_ref_actes) != 0}}
            {{if count($invoice->linked_acts)}}
                {{foreach from=$invoice->linked_acts item=linked_act}}
                    <tr>
                        {{mb_include module=jfse template=medical_act/act_unlink act=$linked_act->_act}}
                        {{mb_include module=jfse template=medical_act/act_line act=$linked_act->_act act_view=$linked_act->_medical_act linked=true}}
                    </tr>
                {{/foreach}}
            {{/if}}
            {{if count($invoice->unlinked_acts)}}
                {{foreach from=$invoice->unlinked_acts item=unlinked_act}}
                    <tr>
                        {{mb_include module=jfse template=medical_act/act_link act=$unlinked_act}}
                        {{mb_include module=jfse template=medical_act/act_line act=$unlinked_act}}
                    </tr>
                {{/foreach}}
            {{/if}}
            {{if count($invoice->other_invoices_acts)}}
                {{foreach from=$invoice->other_invoices_acts item=act}}
                    <tr>
                        <td></td>
                        {{mb_include module=jfse template=medical_act/act_line act=$act}}
                    </tr>
                {{/foreach}}
            {{/if}}
        {{else}}
            <tr>
                <td colspan="11" class="empty">
                    {{tr}}CActe.none{{/tr}}
                </td>
            </tr>
        {{/if}}
        {{if $invoices|@count > 1 || count($consultation->_ref_actes) != count($invoice->linked_acts)}}
            <tr>
                <th>Total FSE</th>
                <th colspan="3"></th>
                <th style="text-align: right;">
                    {{mb_value object=$invoice field=_total_base}}
                </th>
                <th style="text-align: right;">{{mb_value object=$invoice field=_total_exceeding_fees}}</th>
                <th></th>
                <th style="text-align: right;">
                    {{mb_value object=$invoice field=total_amo}}
                </th>
                <th style="text-align: right;">
                    {{mb_value object=$invoice field=total_amc}}
                </th>
                <th style="text-align: right;">{{mb_value object=$invoice field=_total}}</th>
                <th></th>
            </tr>
        {{/if}}
        <tr>
            <th>Total{{if $invoices|@count > 1}} consultation{{/if}}</th>
            <th colspan="3"></th>
            <th style="text-align: right;">
                {{mb_value object=$consultation field=secteur1}}
            </th>
            <th style="text-align: right;">{{mb_value object=$consultation field=secteur2}}</th>
            <th></th>
            <th style="text-align: right;">
                {{if $invoices|@count == 1}}{{mb_value object=$invoice field=total_amo}}{{/if}}
            </th>
            <th style="text-align: right;">
                {{if $invoices|@count == 1}}{{mb_value object=$invoice field=total_amc}}{{/if}}
            </th>
            <th style="text-align: right;">{{mb_value object=$consultation field=_somme}}</th>
            <th></th>
        </tr>
    </tbody>
</table>

{{if $invoice->user_interface->amendment_27_consultation_help}}
    {{mb_include module=jfse template=invoicing/children_consultation_assistant}}
{{/if}}
