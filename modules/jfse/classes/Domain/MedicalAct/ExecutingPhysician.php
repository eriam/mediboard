<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Jfse\Domain\MedicalAct;

use Ox\Mediboard\Jfse\Domain\PrescribingPhysician\Physician;

class ExecutingPhysician extends Physician
{
    /** @var string */
    protected $id;

    /** @var int */
    protected $convention;

    /** @var int */
    protected $pricing_zone;

    /** @var string */
    protected $practice_condition;

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getConvention(): ?int
    {
        return $this->convention;
    }

    /**
     * @return int
     */
    public function getPricingZone(): ?int
    {
        return $this->pricing_zone;
    }

    /**
     * @return string
     */
    public function getPracticeCondition(): ?string
    {
        return $this->practice_condition;
    }
}
