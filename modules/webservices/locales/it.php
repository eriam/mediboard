<?php
$locales['CEchangeSOAP'] = 'Echange SOAP';
$locales['CEchangeSOAP-_date_max'] = 'Date maximum';
$locales['CEchangeSOAP-_date_max-court'] = 'Date maximum';
$locales['CEchangeSOAP-_date_max-desc'] = 'Date maximum de recherche';
$locales['CEchangeSOAP-_date_min'] = 'Date minimum';
$locales['CEchangeSOAP-_date_min-court'] = 'Date minimum';
$locales['CEchangeSOAP-_date_min-desc'] = 'Date minimum de recherche';
$locales['CEchangeSOAP-_self_receiver'] = 'Destinataire';
$locales['CEchangeSOAP-_self_receiver-court'] = 'Destinataire';
$locales['CEchangeSOAP-_self_receiver-desc'] = 'Destinataire';
$locales['CEchangeSOAP-_self_sender'] = 'Emetteur';
$locales['CEchangeSOAP-_self_sender-court'] = 'Emetteur';
$locales['CEchangeSOAP-_self_sender-desc'] = 'Emetteur';
$locales['CEchangeSOAP-date_echange'] = 'Date d\'échange';
$locales['CEchangeSOAP-date_echange-court'] = 'Envoyé';
$locales['CEchangeSOAP-date_echange-desc'] = 'Date d\'échange de l\'envoi / réception';
$locales['CEchangeSOAP-date_production'] = 'Date production';
$locales['CEchangeSOAP-date_production-court'] = 'Généré';
$locales['CEchangeSOAP-date_production-desc'] = 'Date production de l\'échange SOAP';
$locales['CEchangeSOAP-destinataire'] = 'Destinataire';
$locales['CEchangeSOAP-destinataire-court'] = 'Destinataire';
$locales['CEchangeSOAP-destinataire-desc'] = 'Destinataire de l\'échange destinataire SOAP';
$locales['CEchangeSOAP-echange_soap_id'] = 'Identifiant';
$locales['CEchangeSOAP-echange_soap_id-court'] = 'Identifiant';
$locales['CEchangeSOAP-echange_soap_id-desc'] = 'Identifiant de l\'échange SOAP ';
$locales['CEchangeSOAP-emetteur'] = 'Emetteur';
$locales['CEchangeSOAP-emetteur-court'] = 'Emetteur';
$locales['CEchangeSOAP-emetteur-desc'] = 'Emetteur de l\'échange SOAP';
$locales['CEchangeSOAP-function_name'] = 'Fonction';
$locales['CEchangeSOAP-function_name-court'] = 'Fonction';
$locales['CEchangeSOAP-function_name-desc'] = 'Fonction SOAP appellée';
$locales['CEchangeSOAP-input'] = 'ParamÎ¸tres';
$locales['CEchangeSOAP-input-court'] = 'ParamÎ¸tres';
$locales['CEchangeSOAP-input-desc'] = 'ParamÎ¸tres de la fonction SOAP';
$locales['CEchangeSOAP-last_request'] = 'RequÎºte';
$locales['CEchangeSOAP-last_request-court'] = 'RequÎºte';
$locales['CEchangeSOAP-last_request-desc'] = 'RequÎºte';
$locales['CEchangeSOAP-last_request_headers'] = 'EntÎºte requÎºte';
$locales['CEchangeSOAP-last_request_headers-court'] = 'EntÎºte requÎºte';
$locales['CEchangeSOAP-last_request_headers-desc'] = 'DerniÎ¸re entÎºte requÎºte';
$locales['CEchangeSOAP-last_response'] = 'Réponse';
$locales['CEchangeSOAP-last_response-court'] = 'Réponse';
$locales['CEchangeSOAP-last_response-desc'] = 'Réponse';
$locales['CEchangeSOAP-last_response_headers'] = 'EntÎºte réponse';
$locales['CEchangeSOAP-last_response_headers-court'] = 'EntÎºte réponse';
$locales['CEchangeSOAP-last_response_headers-desc'] = 'DerniÎ¸re entÎºte réponse';
$locales['CEchangeSOAP-msg-create'] = 'Echange SOAP crée';
$locales['CEchangeSOAP-msg-delete'] = 'Echange SOAP supprimé';
$locales['CEchangeSOAP-msg-delete_count'] = '%d échanges SOAP Î° supprimer';
$locales['CEchangeSOAP-msg-deleted_count'] = '%d échanges supprimés';
$locales['CEchangeSOAP-msg-modify'] = 'Echange SOAP modifié';
$locales['CEchangeSOAP-msg-purge_count'] = '%d échanges SOAP Î° purger';
$locales['CEchangeSOAP-msg-purged_count'] = '%d échanges purgés';
$locales['CEchangeSOAP-output'] = 'Résultat';
$locales['CEchangeSOAP-output-court'] = 'Résultat';
$locales['CEchangeSOAP-output-desc'] = 'Résultat de la fonction SOAP';
$locales['CEchangeSOAP-purge'] = 'Purge';
$locales['CEchangeSOAP-purge-court'] = 'Purge';
$locales['CEchangeSOAP-purge-desc'] = 'Purge';
$locales['CEchangeSOAP-purge-search'] = 'Chercher les échanges SOAP Î° purger';
$locales['CEchangeSOAP-response_datetime-court'] = 'ReÎ·u';
$locales['CEchangeSOAP-response_datetime-desc'] = 'ReÎ·u';
$locales['CEchangeSOAP-response_time'] = 'Temps de réponse';
$locales['CEchangeSOAP-response_time-court'] = 'Tps rép.';
$locales['CEchangeSOAP-response_time-desc'] = 'Temps de réponse';
$locales['CEchangeSOAP-services'] = 'Services';
$locales['CEchangeSOAP-soapfault'] = 'Erreur SOAP';
$locales['CEchangeSOAP-soapfault-court'] = 'Erreur SOAP';
$locales['CEchangeSOAP-soapfault-desc'] = 'Erreur SOAP';
$locales['CEchangeSOAP-source_class'] = 'Source';
$locales['CEchangeSOAP-source_class-court'] = 'Source';
$locales['CEchangeSOAP-source_class-desc'] = 'Source';
$locales['CEchangeSOAP-source_id'] = 'Source';
$locales['CEchangeSOAP-source_id-court'] = 'Source';
$locales['CEchangeSOAP-source_id-desc'] = 'Source';
$locales['CEchangeSOAP-title-create'] = 'Création d\'un échange SOAP';
$locales['CEchangeSOAP-title-modify'] = 'Modifier l\'échange SOAP';
$locales['CEchangeSOAP-trace'] = 'Trace';
$locales['CEchangeSOAP-trace-court'] = 'Trace';
$locales['CEchangeSOAP-trace-desc'] = 'Trace du suivi des requÎºtes';
$locales['CEchangeSOAP-type'] = 'Type ';
$locales['CEchangeSOAP-type-court'] = 'Type ';
$locales['CEchangeSOAP-type-desc'] = 'Type de l\'échange destinataire SOAP';
$locales['CEchangeSOAP-web_service_name'] = 'Nom web service';
$locales['CEchangeSOAP-web_service_name-court'] = 'Web Service';
$locales['CEchangeSOAP-web_service_name-desc'] = 'Nom du web service';
$locales['CEchangeSOAP.all'] = 'Tous les échanges SOAP';
$locales['CEchangeSOAP.none'] = 'Aucun échange SOAP';
$locales['CEchangeSOAP.one'] = 'echange SOAP';
$locales['CEchangeSOAP.source_class.'] = 'Source';
$locales['CEchangeSOAP.source_class.CSourceFTP'] = 'Source FTP';
$locales['CEchangeSOAP.source_class.CSourceFileSystem'] = 'Source FS';
$locales['CEchangeSOAP.source_class.CSourceHTTP'] = 'Source HTTP';
$locales['CEchangeSOAP.source_class.CSourceSOAP'] = 'Source SOAP';
$locales['CExchangeSOAP.add'] = 'Ajout d\'un échange SOAP';
$locales['CExchangeSource-back-.empty'] = 'Aucune source';
$locales['CGroups-back-senders_soap'] = 'Etablissements';
$locales['CGroups-back-senders_soap.empty'] = 'Aucun établissement';
$locales['CMediusers-back-expediteur_soap'] = 'Mediusers';
$locales['CMediusers-back-expediteur_soap.empty'] = 'Aucun mediuser';
$locales['CSenderSOAP'] = 'Expéditeur SOAP';
$locales['CSenderSOAP-OID'] = 'OID';
$locales['CSenderSOAP-OID-desc'] = 'OID de l\'expéditeur SOAP';
$locales['CSenderSOAP-_parent_class'] = 'Classe parente';
$locales['CSenderSOAP-_parent_class-court'] = 'Classe parente';
$locales['CSenderSOAP-_parent_class-desc'] = 'Classe parente';
$locales['CSenderSOAP-_reachable'] = 'Accessible';
$locales['CSenderSOAP-_reachable-court'] = 'Accessible';
$locales['CSenderSOAP-_reachable-desc'] = 'Accessible';
$locales['CSenderSOAP-_ref_last_message'] = 'Dernier message';
$locales['CSenderSOAP-_ref_last_message-court'] = 'Dernier message';
$locales['CSenderSOAP-_ref_last_message-desc'] = 'Dernier message';
$locales['CSenderSOAP-_self_tag'] = 'Tag';
$locales['CSenderSOAP-_self_tag-court'] = 'Tag';
$locales['CSenderSOAP-_self_tag-desc'] = 'Tag de l\'expéditeur';
$locales['CSenderSOAP-_tag_chambre'] = 'Tag chambre';
$locales['CSenderSOAP-_tag_chambre-court'] = 'Tag chambre';
$locales['CSenderSOAP-_tag_chambre-desc'] = 'Tag chambre';
$locales['CSenderSOAP-_tag_consultation'] = 'Tag consultation';
$locales['CSenderSOAP-_tag_consultation-court'] = 'Tag consultation';
$locales['CSenderSOAP-_tag_consultation-desc'] = 'Tag consultation';
$locales['CSenderSOAP-_tag_lit'] = 'Tag lit';
$locales['CSenderSOAP-_tag_lit-court'] = 'Tag lit';
$locales['CSenderSOAP-_tag_lit-desc'] = 'Tag lit';
$locales['CSenderSOAP-_tag_mediuser'] = 'Tag médiuser';
$locales['CSenderSOAP-_tag_mediuser-court'] = 'Tag médiuser';
$locales['CSenderSOAP-_tag_mediuser-desc'] = 'Tag médiuser';
$locales['CSenderSOAP-_tag_movement'] = 'Tag mouvement';
$locales['CSenderSOAP-_tag_movement-court'] = 'Tag mouvement';
$locales['CSenderSOAP-_tag_movement-desc'] = 'Tag mouvement';
$locales['CSenderSOAP-_tag_patient'] = 'Tag patient';
$locales['CSenderSOAP-_tag_patient-court'] = 'Tag patient';
$locales['CSenderSOAP-_tag_patient-desc'] = 'Tag patient';
$locales['CSenderSOAP-_tag_sejour'] = 'Tag séjour';
$locales['CSenderSOAP-_tag_sejour-court'] = 'Tag séjour';
$locales['CSenderSOAP-_tag_sejour-desc'] = 'Tag séjour';
$locales['CSenderSOAP-_tag_service'] = 'Tag service';
$locales['CSenderSOAP-_tag_service-court'] = 'Tag service';
$locales['CSenderSOAP-_tag_service-desc'] = 'Tag service';
$locales['CSenderSOAP-_tag_visit_number'] = 'Tag numéro de visite';
$locales['CSenderSOAP-_tag_visit_number-court'] = 'Tag numéro de visite';
$locales['CSenderSOAP-_tag_visit_number-desc'] = 'Tag numéro de visite';
$locales['CSenderSOAP-actif'] = 'Actif';
$locales['CSenderSOAP-actif-court'] = 'Actif';
$locales['CSenderSOAP-actif-desc'] = 'Actif';
$locales['CSenderSOAP-failed-user'] = 'L\'utilisateur doit Îºtre unique';
$locales['CSenderSOAP-group_id'] = 'Etablissement';
$locales['CSenderSOAP-group_id-court'] = 'Etab.';
$locales['CSenderSOAP-group_id-desc'] = 'Etablissement';
$locales['CSenderSOAP-libelle'] = 'Libellé';
$locales['CSenderSOAP-libelle-court'] = 'Libellé';
$locales['CSenderSOAP-libelle-desc'] = 'Libellé';
$locales['CSenderSOAP-msg-create'] = 'Expéditeur SOAP créé';
$locales['CSenderSOAP-msg-delete'] = 'Expéditeur SOAP supprimé';
$locales['CSenderSOAP-msg-modify'] = 'Expéditeur SOAP modifié';
$locales['CSenderSOAP-no_utilities'] = 'Aucun utilitaire SOAP';
$locales['CSenderSOAP-nom'] = 'Nom';
$locales['CSenderSOAP-nom-court'] = 'Nom';
$locales['CSenderSOAP-nom-desc'] = 'Nom';
$locales['CSenderSOAP-role'] = 'RÏ„le';
$locales['CSenderSOAP-role-desc'] = 'RÏ„le du destinataire';
$locales['CSenderSOAP-save_unsupported_message'] = 'Enregistrer le message non pris en charge';
$locales['CSenderSOAP-save_unsupported_message-court'] = 'Enregistrer le message non pris en charge';
$locales['CSenderSOAP-save_unsupported_message-desc'] = 'Enregistrer le message non pris en charge par l\'acteur';
$locales['CSenderSOAP-sender_soap_id'] = 'Identifiant';
$locales['CSenderSOAP-sender_soap_id-court'] = 'Identifiant';
$locales['CSenderSOAP-sender_soap_id-desc'] = 'Identifiant expéditeur SOAP';
$locales['CSenderSOAP-title-create'] = 'Nouveau expéditeur SOAP';
$locales['CSenderSOAP-title-modify'] = 'Modification expéditeur SOAP ';
$locales['CSenderSOAP-user_id'] = 'Utilisateur associé';
$locales['CSenderSOAP-user_id-court'] = 'Utilisateur associé';
$locales['CSenderSOAP-user_id-desc'] = 'Utilisateur associé Î° l\'expéditeur';
$locales['CSenderSOAP-utilities'] = 'Utilitaires SOAP';
$locales['CSenderSOAP-utilities_dispatch'] = 'Envoyer le message au dispatcher';
$locales['CSenderSOAP.all'] = 'Tous les expéditeurs SOAP';
$locales['CSenderSOAP.none'] = 'Aucun expéditeur SOAP';
$locales['CSenderSOAP.one'] = 'Expéditeur SOAP';
$locales['CSenderSOAP.role.prod'] = 'Production';
$locales['CSenderSOAP.role.qualif'] = 'Qualification';
$locales['CSenderSOAP_actions'] = 'Actions';
$locales['CSenderSOAP_configs-formats'] = 'Configs formats';
$locales['CSenderSOAP_formats-available'] = 'Formats disponibles';
$locales['CSenderSOAP_routes'] = 'Routes';
$locales['CSourceSOAP'] = 'Source SOAP';
$locales['CSourceSOAP-_incompatible'] = 'RÏ„le de la source incompatible avec le rÏ„le de l\'instance';
$locales['CSourceSOAP-_incompatible-court'] = 'RÏ„le de la source incompatible avec le rÏ„le de l\'instance';
$locales['CSourceSOAP-_incompatible-desc'] = 'RÏ„le de la source incompatible avec le rÏ„le de l\'instance';
$locales['CSourceSOAP-_reachable'] = 'Accessible';
$locales['CSourceSOAP-_reachable-court'] = 'Accessible';
$locales['CSourceSOAP-_reachable-desc'] = 'La source est-elle accessible';
$locales['CSourceSOAP-_response_time'] = 'Temps de réponse';
$locales['CSourceSOAP-_response_time-court'] = 'Temps de réponse';
$locales['CSourceSOAP-_response_time-desc'] = 'Temps de réponse';
$locales['CSourceSOAP-active'] = 'Active';
$locales['CSourceSOAP-active-court'] = 'Active';
$locales['CSourceSOAP-active-desc'] = 'Active';
$locales['CSourceSOAP-cafile'] = 'Fichier de l\'autorité du certificat';
$locales['CSourceSOAP-cafile-court'] = 'Fichier de l\'autorité du certificat';
$locales['CSourceSOAP-cafile-desc'] = 'Endroit oÏ‰ se trouve le fichier de l\'autorité du certificat sur le systÎ¸me de fichiers local';
$locales['CSourceSOAP-connection_timeout'] = 'Délai de connexion';
$locales['CSourceSOAP-connection_timeout-desc'] = 'Définis le délai de connexion en secondes pour la connexion au service SOAP';
$locales['CSourceSOAP-encoding'] = 'Encodage';
$locales['CSourceSOAP-encoding-court'] = 'Encodage';
$locales['CSourceSOAP-encoding-desc'] = 'Jeu d\'encodage des caractÎ¸res internes';
$locales['CSourceSOAP-evenement_name'] = 'Nom méthode';
$locales['CSourceSOAP-evenement_name-court'] = 'Nom méthode';
$locales['CSourceSOAP-evenement_name-desc'] = 'Nom méthode de l\'événement';
$locales['CSourceSOAP-feature'] = 'Fonctionnement';
$locales['CSourceSOAP-feature-court'] = 'Fonctionnement';
$locales['CSourceSOAP-feature-desc'] = 'Définit le fonctionnement de SOAP';
$locales['CSourceSOAP-host'] = 'HÏ„te';
$locales['CSourceSOAP-host-court'] = 'HÏ„te';
$locales['CSourceSOAP-host-desc'] = 'HÏ„te source SOAP';
$locales['CSourceSOAP-iv'] = 'Vecteur d\'initialisation';
$locales['CSourceSOAP-iv-court'] = 'IV mot de passe';
$locales['CSourceSOAP-iv-desc'] = 'Vecteur d\'initialisation du mot de passe';
$locales['CSourceSOAP-iv_passphrase'] = 'IV phrase de passe';
$locales['CSourceSOAP-iv_passphrase-court'] = 'Vecteur d\'initialisation de la phrase de passe';
$locales['CSourceSOAP-iv_passphrase-desc'] = 'Vecteur d\'initialisation de la phrase de passe';
$locales['CSourceSOAP-libelle'] = 'Libellé';
$locales['CSourceSOAP-libelle-court'] = 'Libellé';
$locales['CSourceSOAP-libelle-desc'] = 'Libellé de la source SOAP';
$locales['CSourceSOAP-local_cert'] = 'Chemin du certificat';
$locales['CSourceSOAP-local_cert-court'] = 'Chemin du certificat';
$locales['CSourceSOAP-local_cert-desc'] = 'Chemin du certificat';
$locales['CSourceSOAP-loggable'] = 'Loggable';
$locales['CSourceSOAP-loggable-court'] = 'Loggable';
$locales['CSourceSOAP-loggable-desc'] = 'Aucun log Î° produire';
$locales['CSourceSOAP-msg-create'] = 'Source SOAP créée';
$locales['CSourceSOAP-msg-delete'] = 'Source SOAP supprimée';
$locales['CSourceSOAP-msg-modify'] = 'Source SOAP modifiée';
$locales['CSourceSOAP-name'] = 'Nom';
$locales['CSourceSOAP-name-court'] = 'Nom';
$locales['CSourceSOAP-name-desc'] = 'Nom source SOAP';
$locales['CSourceSOAP-no-acknowledgment'] = 'Aucun acquittement reÎ·u lors de l\'appel Î° la méthode \'%s\' pour la source : \'%s\'';
$locales['CSourceSOAP-no-evenement'] = 'Aucune méthode définie pour l\'appel SOAP de la source : \'%s\'';
$locales['CSourceSOAP-no-source'] = 'Aucune source d\'échange : \'%s\' d\'enregistrée';
$locales['CSourceSOAP-passphrase'] = 'Phrase de passe';
$locales['CSourceSOAP-passphrase-court'] = 'Phrase de passe';
$locales['CSourceSOAP-passphrase-desc'] = 'Phrase de passe';
$locales['CSourceSOAP-password'] = 'Mot de passe';
$locales['CSourceSOAP-password-court'] = 'Mot de passe';
$locales['CSourceSOAP-password-desc'] = 'Mot de passe source SOAP';
$locales['CSourceSOAP-port_name'] = 'Nom du port';
$locales['CSourceSOAP-port_name-desc'] = 'Nom du port spécifique Î° cibler dans le WSDL';
$locales['CSourceSOAP-reachable-source'] = 'Connexion effectuée Î° la source d\'échange : \'%s\'';
$locales['CSourceSOAP-return_mode'] = 'Mode de retour';
$locales['CSourceSOAP-return_mode-court'] = 'Mode de retour';
$locales['CSourceSOAP-return_mode-desc'] = 'Mode de retour des données';
$locales['CSourceSOAP-role'] = 'RÏ„le';
$locales['CSourceSOAP-role-court'] = 'RÏ„le';
$locales['CSourceSOAP-role-desc'] = 'RÏ„le de la source SOAP';
$locales['CSourceSOAP-safe_mode'] = 'Mode \'safe\'';
$locales['CSourceSOAP-safe_mode-court'] = 'Mode \'safe\'';
$locales['CSourceSOAP-safe_mode-desc'] = 'Le mode permet de ne pas tester si l\'URL existe, ainsi que le WSDL soit bien en XML';
$locales['CSourceSOAP-single_parameter'] = 'ParamÎ¸tre unique';
$locales['CSourceSOAP-single_parameter-court'] = 'ParamÎ¸tre unique';
$locales['CSourceSOAP-single_parameter-desc'] = 'ParamÎ¸tre unique';
$locales['CSourceSOAP-soap_version'] = 'Version client';
$locales['CSourceSOAP-soap_version-court'] = 'Version client';
$locales['CSourceSOAP-soap_version-desc'] = 'Version du client SOAP';
$locales['CSourceSOAP-soapclient-impossible'] = 'Instanciation du SoapClient impossible';
$locales['CSourceSOAP-socket_timeout'] = 'Timeout de la socket';
$locales['CSourceSOAP-socket_timeout-court'] = 'Timeout socket';
$locales['CSourceSOAP-socket_timeout-desc'] = 'Limiter la durée d\'attente de fin des appels (en secondes)';
$locales['CSourceSOAP-source_soap_id'] = 'Identifiant';
$locales['CSourceSOAP-source_soap_id-court'] = 'Identifiant';
$locales['CSourceSOAP-source_soap_id-desc'] = 'Identifiant source SOAP';
$locales['CSourceSOAP-stream_context'] = 'Contexte par défaut des flux';
$locales['CSourceSOAP-stream_context-court'] = 'Contexte';
$locales['CSourceSOAP-stream_context-desc'] = 'Configuration du contexte par défaut des flux';
$locales['CSourceSOAP-title-create'] = 'Nouvelle source SOAP';
$locales['CSourceSOAP-title-modify'] = 'Modifier la source SOAP';
$locales['CSourceSOAP-type_echange'] = 'Type échange';
$locales['CSourceSOAP-type_echange-court'] = 'Type échange';
$locales['CSourceSOAP-type_echange-desc'] = 'Type échange (hprimxml, hprim21, ...)';
$locales['CSourceSOAP-type_soap'] = 'Type SOAP';
$locales['CSourceSOAP-type_soap-court'] = 'Type SOAP';
$locales['CSourceSOAP-type_soap-desc'] = 'Le type de la librairie SOAP';
$locales['CSourceSOAP-unable-to-parse-url'] = 'Impossible d\'analyser l\'url : \'%s\'';
$locales['CSourceSOAP-unreachable-source'] = 'Impossible de joindre la source de donnée : \'%s\'';
$locales['CSourceSOAP-use_tunnel'] = 'Mode tunnel';
$locales['CSourceSOAP-use_tunnel-court'] = 'Mode tunnel';
$locales['CSourceSOAP-use_tunnel-desc'] = 'Utilise le tunnel vers le DMP';
$locales['CSourceSOAP-user'] = 'Utilisateur';
$locales['CSourceSOAP-user-court'] = 'Utilisateur';
$locales['CSourceSOAP-user-desc'] = 'Utilisateur source SOAP';
$locales['CSourceSOAP-verify_peer'] = 'Vérification du certificat SSL';
$locales['CSourceSOAP-verify_peer-court'] = 'Vérification du certificat SSL';
$locales['CSourceSOAP-verify_peer-desc'] = 'Nécessite une vérification du certificat SSL utilisé';
$locales['CSourceSOAP-web_service_name'] = 'Nom WebService';
$locales['CSourceSOAP-web_service_name-court'] = 'Nom WebService';
$locales['CSourceSOAP-web_service_name-desc'] = 'Nom WebService';
$locales['CSourceSOAP-wsdl-invalid'] = 'Erreur de connexion sur le service web. WSDL non accessible ou au mauvais format';
$locales['CSourceSOAP-wsdl_external'] = 'WSDL externe';
$locales['CSourceSOAP-wsdl_external-court'] = 'WSDL externe';
$locales['CSourceSOAP-wsdl_external-desc'] = 'WSDL externe (stocké sur la machine)';
$locales['CSourceSOAP-wsdl_mode'] = 'Controle du WSDL';
$locales['CSourceSOAP-wsdl_mode-court'] = 'Controle du WSDL';
$locales['CSourceSOAP-wsdl_mode-desc'] = 'Controle du WSDL';
$locales['CSourceSOAP-xop_mode'] = 'Mode XOP';
$locales['CSourceSOAP-xop_mode-court'] = 'Mode XOP';
$locales['CSourceSOAP-xop_mode-desc'] = 'Activer l\'utilisation du MTOM/XOP';
$locales['CSourceSOAP._reachable.'] = 'Accessible';
$locales['CSourceSOAP._reachable.0'] = 'Non';
$locales['CSourceSOAP._reachable.1'] = 'Partiellement';
$locales['CSourceSOAP._reachable.2'] = 'Oui';
$locales['CSourceSOAP.all'] = 'Toutes les sources SOAP';
$locales['CSourceSOAP.encoding.'] = 'Encodage';
$locales['CSourceSOAP.encoding.utf-8'] = 'utf-8';
$locales['CSourceSOAP.encoding.utf-85'] = 'utf-85';
$locales['CSourceSOAP.encoding.UTF-8'] = 'UTF-8 ';
$locales['CSourceSOAP.feature.'] = 'Choix du fonctionnement';
$locales['CSourceSOAP.feature.SOAP_SINGLE_ELEMENT_ARRAYS'] = 'SOAP_SINGLE_ELEMENT_ARRAYS';
$locales['CSourceSOAP.feature.SOAP_USE_XSI_ARRAY_TYPE'] = 'SOAP_USE_XSI_ARRAY_TYPE';
$locales['CSourceSOAP.feature.SOAP_WAIT_ONE_WAY_CALLS'] = 'SOAP_WAIT_ONE_WAY_CALLS';
$locales['CSourceSOAP.none'] = 'Aucune source SOAP';
$locales['CSourceSOAP.one'] = 'Une source SOAP';
$locales['CSourceSOAP.return_mode.'] = 'Choix mode de retour';
$locales['CSourceSOAP.return_mode.file'] = 'Vers un fichier temporaire';
$locales['CSourceSOAP.return_mode.normal'] = 'Normal';
$locales['CSourceSOAP.return_mode.raw'] = 'Données brutes';
$locales['CSourceSOAP.role.prod'] = 'Production';
$locales['CSourceSOAP.role.qualif'] = 'Qualification';
$locales['CSourceSOAP.soap_version.SOAP_1_1'] = '1.1';
$locales['CSourceSOAP.soap_version.SOAP_1_2'] = '1.2';
$locales['CSourceSOAP.type_soap.CMbSOAPClient'] = 'SoapClient';
$locales['config-soap-server'] = 'Configuration du serveur SOAP';
$locales['config-list-wsdl-server'] = 'WSDL du serveur';
$locales['config-list-wsdl-client'] = 'WSDL du client';
$locales['CSourceSoap-msg-List wsdl server'] = 'WSDL du serveur';
$locales['CSourceSoap-msg-List wsdl client'] = 'WSDL du client';
$locales['config-webservices'] = 'Configuration';
$locales['config-webservices-CSoapHandler'] = 'Serveur SOAP';
$locales['config-webservices-CSoapHandler-loggable'] = 'Loggable';
$locales['config-webservices-CSoapHandler-loggable-desc'] = 'Enregistrer les traces du serveur SOAP';
$locales['config-webservices-connection_timeout'] = 'Délai de connexion';
$locales['config-webservices-connection_timeout-desc'] = 'Définit le délai de connexion en secondes pour la connexion au service SOAP';
$locales['config-webservices-purge-echange'] = 'Purge échanges';
$locales['config-webservices-response_timeout'] = 'Délai de réponse';
$locales['config-webservices-response_timeout-desc'] = 'Limite la durée d\'attente de fin des appels';
$locales['config-webservices-soap-server'] = 'SOAP Serveur';
$locales['config-webservices-soap_server_encoding'] = 'Encodage';
$locales['config-webservices-soap_server_encoding-utf-8'] = 'utf-8';
$locales['config-webservices-soap_server_encoding-UTF-8'] = 'UTF-8';
$locales['config-webservices-soap_server_encoding-desc'] = 'Encodage du serveur SOAP (UTF-8, 8859-1, ...)';
$locales['config-webservices-trace'] = 'Activation du suivi des requÎºtes';
$locales['config-webservices-trace-desc'] = 'Active le suivi des requÎºtes pour permettre le retraÎ·age des défauts';
$locales['config-webservices-webservice'] = 'Nom du webservice';
$locales['config-webservices-webservice-CWebservice'] = 'Aucun';
$locales['config-webservices-webservice-desc'] = 'Nom du webservice';
$locales['config-webservices-wsdl_root_url'] = 'URL du WSDL';
$locales['config-webservices-wsdl_root_url-desc'] = 'URL externe du WSDL (Web Services Description Language)';
$locales['mod-webservices-tab-ajax_connexion_soap'] = 'Test de la connexion SOAP';
$locales['mod-webservices-tab-ajax_dispatch_event'] = 'Dispatcher les requÎºtes';
$locales['mod-webservices-tab-ajax_filter_web_func'] = 'Filtre sur les fonctions';
$locales['mod-webservices-tab-ajax_getFunctions_soap'] = 'Récupération des fonctions du serveur';
$locales['mod-webservices-tab-ajax_purge_echange'] = 'Purge d\'échanges de services web';
$locales['mod-webservices-tab-ajax_test_send_event'] = 'Test d\'envoi SOAP';
$locales['mod-webservices-tab-ajax_view_echange_soap'] = 'Détails de l\'échange SOAP';
$locales['mod-webservices-tab-ajax_get_wsdl_server'] = 'Liste des WSDL';
$locales['mod-webservices-tab-ajax_show_wsdl'] = 'Visualisation du WSDL';
$locales['mod-webservices-tab-configure'] = 'Configuration';
$locales['mod-webservices-tab-download_echange'] = 'Téléchargement de l\'échange';
$locales['mod-webservices-tab-soap_server'] = 'Serveur SOAP';
$locales['mod-webservices-tab-vw_idx_echange_soap'] = 'Echange SOAP';
$locales['mod-webservices-tab-vw_stats'] = 'Statistiques';
$locales['module-webservices-court'] = 'Service web';
$locales['module-webservices-long'] = 'Service Web';
$locales['stats-echanges-soap'] = 'Statistiques des échanges SOAP';
$locales['utilities-source-soap'] = 'Utilitaires SOAP';
$locales['utilities-source-soap-connexion'] = 'Test de la connexion';
$locales['utilities-source-soap-getFunctions'] = 'Liste des fonctions';
$locales['webservices'] = 'Services Web';
