<?php
$locales['CCommune-commune-retrieved'] = 'Commune retrouvée';
$locales['CCommune-msg-create'] = 'Commune créée';
$locales['CCommuneCp'] = 'Code postal';
$locales['CCommuneCp-code_postal'] = 'Code postal';
$locales['CCommuneCp-code_postal-court'] = 'CP';
$locales['CCommuneCp-code_postal-desc'] = 'Code postal';
$locales['CCommuneCp-commune_cp_id'] = 'Identifiant';
$locales['CCommuneCp-commune_cp_id-court'] = 'ID';
$locales['CCommuneCp-commune_cp_id-desc'] = 'Identifiant du code postal';
$locales['CCommuneCp-commune_id'] = 'Commune';
$locales['CCommuneCp-commune_id-court'] = 'Commune';
$locales['CCommuneCp-commune_id-desc'] = 'Commune correspondant au code postal';
$locales['CCommuneCp-cp-retrieved'] = 'Code postal retrouvé';
$locales['CCommuneCp-msg-create'] = 'Code postal créé';
$locales['CCommuneCp-msg-delete'] = 'Code postal supprimé';
$locales['CCommuneCp-msg-modify'] = 'Code postal modifié';
$locales['CCommuneCp-title-create'] = 'Création d\'un code postal';
$locales['CCommuneCp-title-modify'] = 'Modification d\'un code postal';
$locales['CCommuneCp.all'] = 'Tous les codes postaux';
$locales['CCommuneCp.none'] = 'Aucun code postal';
$locales['CCommuneCp.one'] = 'Un code postal';
$locales['CCommuneDemographie'] = 'Démographie des communes';
$locales['CCommuneDemographie-age_max'] = 'Age maximum';
$locales['CCommuneDemographie-age_max-court'] = 'Age max';
$locales['CCommuneDemographie-age_max-desc'] = 'Age maximum de la tranche d\'Î²ge';
$locales['CCommuneDemographie-age_min'] = 'Age minimum';
$locales['CCommuneDemographie-age_min-court'] = 'Age min';
$locales['CCommuneDemographie-age_min-desc'] = 'Age minimum de la tranche d\'Î²ge';
$locales['CCommuneDemographie-annee'] = 'Année';
$locales['CCommuneDemographie-annee-court'] = 'Année';
$locales['CCommuneDemographie-annee-desc'] = 'Année';
$locales['CCommuneDemographie-commune_id'] = 'Commune';
$locales['CCommuneDemographie-commune_id-court'] = 'Commune';
$locales['CCommuneDemographie-commune_id-desc'] = 'Commune référencée par la démographie';
$locales['CCommuneDemographie-communes_demographie_id'] = 'ID';
$locales['CCommuneDemographie-communes_demographie_id-court'] = 'ID';
$locales['CCommuneDemographie-communes_demographie_id-desc'] = 'Identifiant';
$locales['CCommuneDemographie-msg-create'] = 'Démographie créé';
$locales['CCommuneDemographie-msg-delete'] = 'Démographie supprimée';
$locales['CCommuneDemographie-msg-found'] = 'Démographie de commune retrouvée';
$locales['CCommuneDemographie-msg-modify'] = 'Démographie modifiée';
$locales['CCommuneDemographie-nationalite'] = 'Nationalité';
$locales['CCommuneDemographie-nationalite-court'] = 'Nationalité';
$locales['CCommuneDemographie-nationalite-desc'] = 'Nationalité';
$locales['CCommuneDemographie-population'] = 'Population';
$locales['CCommuneDemographie-population-court'] = 'Population';
$locales['CCommuneDemographie-population-desc'] = 'Population pour la tranche d\'Î²ge, sexe et nationalité de la commune';
$locales['CCommuneDemographie-sexe'] = 'Sexe';
$locales['CCommuneDemographie-sexe-court'] = 'Sexe';
$locales['CCommuneDemographie-sexe-desc'] = 'Sexe';
$locales['CCommuneDemographie-title-create'] = 'Création de démographies de communes';
$locales['CCommuneDemographie-title-modify'] = 'Modification de démographies de communes';
$locales['CCommuneDemographie.all'] = 'Toutes les démographies';
$locales['CCommuneDemographie.nationalite.'] = 'Nationalité';
$locales['CCommuneDemographie.nationalite.etranger'] = 'Etrangé';
$locales['CCommuneDemographie.nationalite.francais'] = 'FranÎ·ais';
$locales['CCommuneDemographie.none'] = 'Aucune démographie';
$locales['CCommuneDemographie.one'] = 'Démographie';
$locales['CCommuneDemographie.sexe.'] = 'Sexe';
$locales['CCommuneDemographie.sexe.f'] = 'Féminin';
$locales['CCommuneDemographie.sexe.m'] = 'Masculin';
$locales['CCommuneFrance'] = 'Commune franÎ·aise';
$locales['CCommuneFrance-INSEE'] = 'INSEE';
$locales['CCommuneFrance-INSEE-court'] = 'INSEE';
$locales['CCommuneFrance-INSEE-desc'] = 'Numéro INSEE de la commune';
$locales['CCommuneFrance-back-cp'] = 'Code postal de la commune';
$locales['CCommuneFrance-back-cp.empty'] = 'Aucun code postal';
$locales['CCommuneFrance-back-demographies'] = 'Démographies de la commune';
$locales['CCommuneFrance-back-demographies.empty'] = 'Aucune démographie pour la commune';
$locales['CCommuneFrance-commune'] = 'Nom';
$locales['CCommuneFrance-commune-court'] = 'Nom';
$locales['CCommuneFrance-commune-desc'] = 'Nom de la commune';
$locales['CCommuneFrance-commune-retrieved'] = 'Commune retrouvée';
$locales['CCommuneFrance-commune_france_id'] = 'Identifiant';
$locales['CCommuneFrance-commune_france_id-court'] = 'ID';
$locales['CCommuneFrance-commune_france_id-desc'] = 'Identifiant de la commune';
$locales['CCommuneFrance-departement'] = 'Département';
$locales['CCommuneFrance-departement-court'] = 'Département';
$locales['CCommuneFrance-departement-desc'] = 'Département de la commune';
$locales['CCommuneFrance-failed-INSEE'] = 'INSEE inexistant';
$locales['CCommuneFrance-forme_geographique'] = 'Forme géographique';
$locales['CCommuneFrance-forme_geographique-court'] = 'Forme géographique';
$locales['CCommuneFrance-forme_geographique-desc'] = 'Forme géographique de la commune';
$locales['CCommuneFrance-msg-create'] = 'Commune créée';
$locales['CCommuneFrance-msg-delete'] = 'Commune supprimée';
$locales['CCommuneFrance-msg-found'] = 'Commune franÎ·aise retrouvée';
$locales['CCommuneFrance-msg-modify'] = 'Commune modifiée';
$locales['CCommuneFrance-point_geographique'] = 'Point géographique';
$locales['CCommuneFrance-point_geographique-court'] = 'Point géographique';
$locales['CCommuneFrance-point_geographique-desc'] = 'Coordonnées du point géographique central de la commune';
$locales['CCommuneFrance-population'] = 'Population';
$locales['CCommuneFrance-population-court'] = 'Population';
$locales['CCommuneFrance-population-desc'] = 'Population de la commune';
$locales['CCommuneFrance-region'] = 'Région';
$locales['CCommuneFrance-region-court'] = 'Région';
$locales['CCommuneFrance-region-desc'] = 'Région de la commune';
$locales['CCommuneFrance-statut'] = 'Type';
$locales['CCommuneFrance-statut-court'] = 'Type';
$locales['CCommuneFrance-statut-desc'] = 'Type de commune';
$locales['CCommuneFrance-superficie'] = 'Superficie';
$locales['CCommuneFrance-superficie-court'] = 'Superficie';
$locales['CCommuneFrance-superficie-desc'] = 'Superficie de la commune';
$locales['CCommuneFrance-title-create'] = 'Créer une commune';
$locales['CCommuneFrance-title-modify'] = 'Modifier une commune';
$locales['CCommuneFrance.all'] = 'Toutes les communes';
$locales['CCommuneFrance.none'] = 'Aucune commune';
$locales['CCommuneFrance.one'] = 'Commune';
$locales['CCommuneFrance.statut.'] = 'Aucun statut';
$locales['CCommuneFrance.statut.capital'] = 'Capitale';
$locales['CCommuneFrance.statut.cheflieu'] = 'Chef lieu';
$locales['CCommuneFrance.statut.comm'] = 'Commune';
$locales['CCommuneFrance.statut.pref'] = 'Préfecture';
$locales['CCommuneFrance.statut.prefregion'] = 'Préfecture de la région';
$locales['CCommuneFrance.statut.souspref'] = 'Sous-préfecture';
$locales['CCommuneFranceImport-file-downloaded'] = 'Fichier des communes téléchargé';
$locales['CCommuneImport-commune-retrieved'] = 'Commune retrouvée';
$locales['CCommuneImport-file-deleted'] = 'Import terminé, suppression du fichier';
$locales['CCommuneImport-file-downloaded'] = 'Fichier des communes téléchargé';
$locales['CCommuneImport-lines-imported'] = '%d lignes importées';
$locales['CHDActivite'] = 'Activité HospiDiag';
$locales['CHDActivite-annee'] = 'Année';
$locales['CHDActivite-annee-court'] = 'Année';
$locales['CHDActivite-annee-desc'] = 'Année';
$locales['CHDActivite-hd_activite_id'] = 'ID';
$locales['CHDActivite-hd_activite_id-court'] = 'ID';
$locales['CHDActivite-hd_activite_id-desc'] = 'Identifiant de l\'activité';
$locales['CHDActivite-hd_etablissement_id'] = 'Etablissement';
$locales['CHDActivite-hd_etablissement_id-court'] = 'Etablissement';
$locales['CHDActivite-hd_etablissement_id-desc'] = 'Etablissement HospiDiag de l\'activité';
$locales['CHDActivite-indice_enseignement'] = 'Enseignement';
$locales['CHDActivite-indice_enseignement-court'] = 'Enseignement';
$locales['CHDActivite-indice_enseignement-desc'] = 'Cet indicateur permet d\'estimer la performance de l\'activité d\'enseignement de l\'établissement via l\'encadrement des étudiants formés (rapporté aux ETP dédiées Î° l\'enseignement)';
$locales['CHDActivite-indice_recherche'] = 'Recherche et publications';
$locales['CHDActivite-indice_recherche-court'] = 'Recherche et publications';
$locales['CHDActivite-indice_recherche-desc'] = 'Cet indicateur permet d\'estimer la performance de l\'activité de recherche et de publications de l\'établissement, au travers : 
- des points SIGAPS (SystÎ¸me d \'Interrogation, de Gestion et d \'Analyse des Publications Scientifiques) pour les publications, 
- des points SIGREC (SystÎ¸me dÂ‘Information et de Gestion de la Recherche et des Essais Cliniques) pour la recherche clinique 
C\'est un indicateur composite';
$locales['CHDActivite-line-%d-hd_etablissement_id-mandatory'] = 'Ligne %d : l\'identifiant de l\'établissement est obligatoire';
$locales['CHDActivite-msg-create'] = 'Activité HospiDIag créée';
$locales['CHDActivite-msg-delete'] = 'Activité HospiDiag supprimée';
$locales['CHDActivite-msg-found'] = 'Activité HospiDiag retrouvée';
$locales['CHDActivite-msg-modify'] = 'Activité HospiDiag modifiée';
$locales['CHDActivite-pct_entree_prov_urgence'] = 'Pourcentage d\'entrées en HC en provenance des urgences';
$locales['CHDActivite-pct_entree_prov_urgence-court'] = 'Pct entrée HC provenant des urgences';
$locales['CHDActivite-pct_entree_prov_urgence-desc'] = 'Cet indicateur mesure la contribution des urgences Î° l\'activ ité traditionnelle et permet d\'appréhender les impacts organisationnels des urgences sur les activités programmées des établissements. 
Il permet a contrario de mettre en évidence l\'attractivité intrinsÎ¸que de l\'établissement';
$locales['CHDActivite-pct_ghm'] = 'Pourcentage des GHM Â« recours / référenceÂ» dans l\'activité';
$locales['CHDActivite-pct_ghm-court'] = 'Pct GHM activité';
$locales['CHDActivite-pct_ghm-desc'] = 'Cet indicateur mesure le pourcentage de l\'activité réalisée sur des GHM pris en charge principalement mais non exclusivement dans les établissements hospitalo-universitaires. Il mesure une Â« technicité Â»de la prise en charge.
Ces activités ont été identifiées Î° partir du rapport ATIHÂ« l\'activité des CHU dans le PMSI : peut-on isoler l\'activité spécifique des CHU ? Â» (Version 2, Mai 2009) croisées avec une DMS&gt; 3 j';
$locales['CHDActivite-pct_hospi_cancer'] = 'Pourcentage de l\'activité représentée par l\'hospitalisation en cancérologie (hors séances)';
$locales['CHDActivite-pct_hospi_cancer-court'] = 'Pct activité hospi cancérologie';
$locales['CHDActivite-pct_hospi_cancer-desc'] = 'Cet indicateur permet de mettre en évidence le poids représenté par les hospitalisations en cancérologie (séjours, hors séances) dans les hospitalisations de l\'établissement';
$locales['CHDActivite-pct_sejours_severite_3_4'] = 'Pourcentage des séjours de niveau de sévérité 3 et 4';
$locales['CHDActivite-pct_sejours_severite_3_4-court'] = 'Pct séjours sévérité 3/4';
$locales['CHDActivite-pct_sejours_severite_3_4-desc'] = 'Cet indicateur met en évidence la lourdeur du terrain des patients pris en charge par l\'établissement (comorbidités, complications). 
Pour chaque GHM, 4 niveaux de sévérité existent, définis par la V11, le niveau 1 étant le niveau de base, sans sévérité. Le niveau de sévérité est déterminé Î° partir des co-morbidités associées (ex : diabÎ¸te, anorexie...), des durées minimum de séjours et de l\'Î²ge';
$locales['CHDActivite-pm_chir_ambu_reg'] = 'Part de marché en chirurgie ambulatoire sur la région';
$locales['CHDActivite-pm_chir_ambu_reg-court'] = 'PM chirurgie ambu région';
$locales['CHDActivite-pm_chir_ambu_reg-desc'] = 'Cet indicateur mesure la part de marché de l\'établissement en chirurgie ambulatoire sur les patients de sa région et traduit ainsi la capacité de l\'établissement Î° avoir un rayonnement régional (positionnement sur des activités recours / référence)';
$locales['CHDActivite-pm_chir_reg'] = 'Part de marché en chirurgie (HC) sur la région';
$locales['CHDActivite-pm_chir_reg-court'] = 'PM chirurgie région';
$locales['CHDActivite-pm_chir_reg-desc'] = 'Cet indicateur mesure la part de marché de l\'établissement en chirurgie complÎ¸te sur les patients de sa région et traduit ainsi la capacité de l\'établissement Î° avoir un rayonnement régional';
$locales['CHDActivite-pm_hospi_cancer_reg'] = 'Part de marché en hospitalisation en cancérologie sur la région (hors séances)';
$locales['CHDActivite-pm_hospi_cancer_reg-court'] = 'PM hospi cancer région';
$locales['CHDActivite-pm_hospi_cancer_reg-desc'] = 'Cet indicateur mesure la part de marché de l\'établissement en hospitalisation cancérologique hors séances sur les patients de sa région et traduit ainsi la capacité de l\'établissement Î° avoir un rayonnement régional (positionnement sur des activités recours / référence)';
$locales['CHDActivite-pm_med_reg'] = 'Part de marché globale en médecine sur la région';
$locales['CHDActivite-pm_med_reg-court'] = 'PM médecine région';
$locales['CHDActivite-pm_med_reg-desc'] = 'Cet indicateur mesure la part de marché de l\'établissement en médecine sur les patients de sa région et traduit ainsi la capacité de l\'établissement Î° avoir un rayonnement régional';
$locales['CHDActivite-pm_obs_reg'] = 'Part de marché en obstétrique sur la région';
$locales['CHDActivite-pm_obs_reg-court'] = 'Pm obstétrique région';
$locales['CHDActivite-pm_obs_reg-desc'] = 'Cet indicateur mesure la part de marché de l\'établissement en obstétrique sur les patients de sa région et traduit ainsi la capacité de l\'établissement Î° avoir un rayonnement régional (positionnement sur des activités recours / référence)';
$locales['CHDActivite-taux_util_lits_chir'] = 'Taux d\'occupation/utilisation des lits en chirurgie (hors ambulatoire)';
$locales['CHDActivite-taux_util_lits_chir-court'] = 'Utilisation lits chirurgie';
$locales['CHDActivite-taux_util_lits_chir-desc'] = 'Cet indicateur mesure l\'occupation, par des patients de chirurgie, des lits exploitables en chirurgie, en hospitalisation complÎ¸te ou de semaine';
$locales['CHDActivite-taux_util_lits_med'] = 'Taux d\'utilisation/occupation des lits en médecine (hors ambulatoire)';
$locales['CHDActivite-taux_util_lits_med-court'] = 'Utilisation lits médecine';
$locales['CHDActivite-taux_util_lits_med-desc'] = 'Cet indicateur mesure l\'occupation, par des patients de médecine, des lits exploitables en médecine, en hospitalisation complÎ¸te ou de semaine';
$locales['CHDActivite-taux_util_lits_obs'] = 'Taux d\'occupation/utilisation des lits en obstétrique (hors ambulatoire)';
$locales['CHDActivite-taux_util_lits_obs-court'] = 'Utilisation lits obstétrique';
$locales['CHDActivite-taux_util_lits_obs-desc'] = 'Cet indicateur mesure l\'occupation, par des patientes d\'obstétrique, des lits exploitables en obstétrique, en hospitalisation complÎ¸te ou de semaine';
$locales['CHDActivite-title-create'] = 'Création d\'une activité HospiDiag';
$locales['CHDActivite-title-modify'] = 'Modification d\'une activité HospiDiag';
$locales['CHDActivite-zone_reg'] = 'Région';
$locales['CHDActivite-zone_reg-court'] = 'Région';
$locales['CHDActivite-zone_reg-desc'] = 'Région';
$locales['CHDActivite.all'] = 'Toutes les activités HospiDiag';
$locales['CHDActivite.none'] = 'Aucune activité HospiDiag';
$locales['CHDActivite.one'] = 'Une activité HospiDiag';
$locales['CHDActiviteZone'] = 'Activité de la zone d\'attractivité HospiDiag';
$locales['CHDActiviteZone-annee'] = 'Année';
$locales['CHDActiviteZone-annee-court'] = 'Année';
$locales['CHDActiviteZone-annee-desc'] = 'Année';
$locales['CHDActiviteZone-hd_activite_zone_id'] = 'ID';
$locales['CHDActiviteZone-hd_activite_zone_id-court'] = 'ID';
$locales['CHDActiviteZone-hd_activite_zone_id-desc'] = 'Identifiant de la zone d\'attractivité HospiDiag';
$locales['CHDActiviteZone-hd_etablissement_id'] = 'Etablissement';
$locales['CHDActiviteZone-hd_etablissement_id-court'] = 'Etablissement';
$locales['CHDActiviteZone-hd_etablissement_id-desc'] = 'Etablissement de la zone d\'attractivité';
$locales['CHDActiviteZone-msg-create'] = 'Activité de la zone d\'attractivité HospiDiag créée';
$locales['CHDActiviteZone-msg-delete'] = 'Activité de la zone d\'attractivité HospiDiag supprimée';
$locales['CHDActiviteZone-msg-found'] = 'Activité de la zone d\'attractivité HospiDiag retrouvée';
$locales['CHDActiviteZone-msg-modify'] = 'Activité de la zone d\'attractivité HospiDiag modifiée';
$locales['CHDActiviteZone-pm_chimio'] = 'Part de marché en séances de chimiothérapie sur la zone d\'attractivité';
$locales['CHDActiviteZone-pm_chimio-court'] = 'PM en séances de chimiothérapie sur la ZA';
$locales['CHDActiviteZone-pm_chimio-desc'] = 'Cet indicateur permet d\'appréhender la capacité de l\'établissement Î° s\'imposer sur sa zone d\'attractivité en séances de chimiothérapie et de mettre en évidence ses concurrents. La définition de la zone d\'attractivité retenue est la suivante : la zone d\'attractivité d\'un établissement est le territoire défini par la liste des codes postaux dans lesquels l\'établissement réalise les taux d\'hospitalisation les plus élevés. Ces codes postaux sont classés de maniÎ¸re décroissante. Sont retenues dans la zone d\'attractivité, les localités dont le cumul des séances représente 80% de l\'activité de l\'établissement';
$locales['CHDActiviteZone-pm_chir'] = 'Part de marché en chirurgie (HC) sur la zone d\'attractivité';
$locales['CHDActiviteZone-pm_chir-court'] = 'PM en chirurgie (HC) sur la ZA';
$locales['CHDActiviteZone-pm_chir-desc'] = 'Cet indicateur permet d\'appréhender la capacité de l\'établissement Î° s\'imposer sur sa zone d\'attractivité en chirurgie complÎ¸te et de mettre en évidence ses concurrents. La définition de la zone d\'attractivité retenue est la suivante : la zone d\'attractivité d\'un établissement est le territoire défini par la liste des codes postaux dans lesquels l\'établissement réalise les taux d\'hospitalisation (Nb séjours de chirurgie complÎ¸te/ Nb habitants) les plus élevés. Ces codes postaux sont classés de maniÎ¸re décroissante. Sont retenues dans la zone d\'attractivité de chirurgie les localités dont le cumul des séjours représente 80% de l\'activité de l\'établissement';
$locales['CHDActiviteZone-pm_chir_ambu'] = 'Part de marché en chirurgie ambulatoire sur la zone d\'attractivité';
$locales['CHDActiviteZone-pm_chir_ambu-court'] = 'PM en chirurgie ambulatoire sur la ZA';
$locales['CHDActiviteZone-pm_chir_ambu-desc'] = 'Cet indicateur permet d\'appréhender la capacité de l\'établissement Î° s\'imposer sur sa zone d\'attractivité en chirurgie ambulatoire et de mettre en évidence ses concurrents. La définition de la zone d\'attractivité retenue est la suivante : la zone d\'attractivité d\'un établissement est le territoire défini par la liste des codes postaux dans lesquels l\'établissement réalise les taux d\'hospitalisation (Nb séjours de chirurgie ambulatoire/ Nb habitants) les plus élevés. Ces codes postaux sont classés de maniÎ¸re décroissante. Sont retenues dans la zone d\'attractivité de chirurgie les localités dont le cumul des séjours représente 80% de l\'activité de l\'établissement';
$locales['CHDActiviteZone-pm_hospi_cancer'] = 'Part de marché en hospitalisation en cancérologie sur la zone d\'attractivité (hors séances)';
$locales['CHDActiviteZone-pm_hospi_cancer-court'] = 'PM en hospitalisation en cancérologie sur la ZA';
$locales['CHDActiviteZone-pm_hospi_cancer-desc'] = 'Cet indicateur permet d\'appréhender la capacité de l\'établissement Î° s\'imposer sur sa zone d\'attractivité en hospitalisation hors séances en cancérologie et de mettre en évidence ses concurrents. La définition de la zone d\'attractivité retenue est la suivante : la zone d\'attractivité d\'un établissement est le territoire défini par la liste des codes postaux dans lesquels l\'établissement réalise les taux d\'hospitalisation (Nb séjours hors séances / Nb habitants) les plus élevés. Ces codes postaux sont classés de maniÎ¸re décroissante. Sont retenues dans la zone d\'attractivité de chirurgie les localités dont le cumul des séjours représente 80% de l\'activité de l\'établissement';
$locales['CHDActiviteZone-pm_med'] = 'Part de marché globale en médecine sur la zone d\'attractivité';
$locales['CHDActiviteZone-pm_med-court'] = 'PM en médecine sur la ZA';
$locales['CHDActiviteZone-pm_med-desc'] = 'Cet indicateur permet d\'appréhender la capacité de l\'établissement Î° s\'imposer sur sa zone d\'attractivité en médecine et de mettre en évidence ses concurrents. La définition de la zone d\'attractivité retenue est la suivante : la zone d\'attractivité d\'un établissement est le territoire défini par la liste des codes postaux dans lesquels l\'établissement réalise les taux d\'hospitalisation (Nb séjours médicaux/ Nb habitants) les plus élevés. Ces codes postaux  ont classés de maniÎ¸re décroissante. Sont retenues dans la zone d\'attractivité de médecine, les localités dont le cumul des séjours représente 80% de l\'activité de l\'établissement';
$locales['CHDActiviteZone-pm_obs'] = 'Part de marché en obstétrique sur la zone d\'attractivité';
$locales['CHDActiviteZone-pm_obs-court'] = 'PM en obstétrique sur la ZA';
$locales['CHDActiviteZone-pm_obs-desc'] = 'Cet indicateur permet d\'appréhender la capacité de l\'établissement Î° s\'imposer sur sa zone d\'attractivité en obstétrique et de mettre en évidence ses concurrents. La définition de la zone d\'attractivité retenue est la suivante : la zone d\'attractivité d\'un établissement est le territoire défini par la liste des codes postaux dans lesquels l\'établissement réalise les taux d\'hospitalisation (Nb séjours obstétrique/ Nb habitants) les plus élevés. Ces codes postaux sont classés de maniÎ¸re décroissante. Sont retenues dans la zone d\'attractivité de chirurgie les localités dont le cumul des séjours représente 80% de l\'activité de l\'établissement';
$locales['CHDActiviteZone-title-create'] = 'Création d\'une zone d\'attractivité';
$locales['CHDActiviteZone-title-modify'] = 'Modification d\'une zone d\'attractivité';
$locales['CHDActiviteZone-zone'] = 'Etablissements de la zone d\'attractivité';
$locales['CHDActiviteZone-zone-court'] = 'Etablissements de la zone d\'attractivité';
$locales['CHDActiviteZone-zone-desc'] = 'Etablissements de la zone d\'attractivité';
$locales['CHDActiviteZone.all'] = 'Toutes les activités de la zone d\'attractivité HospiDiag';
$locales['CHDActiviteZone.none'] = 'Aucune activité de la zone d\'attractivité HospiDiag';
$locales['CHDActiviteZone.one'] = 'Une activité de la zone d\'attractivité HospiDiag';
$locales['CHDEtablissement'] = 'Etablissement HospiDiag';
$locales['CHDEtablissement|pl'] = 'Etablissements HospiDiag';
$locales['CHDEtablissement-back-activites'] = 'Activités de l\'établissement';
$locales['CHDEtablissement-back-activites.empty'] = 'Aucune activité pour l\'établissement';
$locales['CHDEtablissement-back-activites_zones'] = 'Zone d\'attractivité de l\'établissement';
$locales['CHDEtablissement-back-activites_zones.empty'] = 'Aucune zone d\'attractivité pour l\'établissement';
$locales['CHDEtablissement-back-finances'] = 'Finances de l\'établissement';
$locales['CHDEtablissement-back-finances.empty'] = 'Aucune finance pour l\'établissement';
$locales['CHDEtablissement-back-identites'] = 'Identités de l\'établissement';
$locales['CHDEtablissement-back-identites.empty'] = 'Aucune identité pour l\'établissement';
$locales['CHDEtablissement-back-process'] = 'Process des établissements';
$locales['CHDEtablissement-back-process.empty'] = 'Aucun process pour l\'établissement';
$locales['CHDEtablissement-back-qualites'] = 'Qualités de l\'établissement';
$locales['CHDEtablissement-back-qualites.empty'] = 'Aucune qualité pour l\'établissement';
$locales['CHDEtablissement-back-resshums'] = 'Ressources humaines de l\'établissement';
$locales['CHDEtablissement-back-resshums.empty'] = 'Aucune ressource humaine pour l\'établissement';
$locales['CHDEtablissement-cat'] = 'Catégorie';
$locales['CHDEtablissement-cat-court'] = 'Catégorie';
$locales['CHDEtablissement-cat-desc'] = 'Catégorie de l\'établissement';
$locales['CHDEtablissement-champ_pmsi'] = 'Champ PMSI';
$locales['CHDEtablissement-champ_pmsi-court'] = 'Champ PMSI';
$locales['CHDEtablissement-champ_pmsi-desc'] = 'Champ PMSI de l\'établissement';
$locales['CHDEtablissement-finess'] = 'Finess';
$locales['CHDEtablissement-finess-court'] = 'Finess';
$locales['CHDEtablissement-finess-desc'] = 'Numéro Finess de l\'établissement';
$locales['CHDEtablissement-hd_etablissement_id'] = 'ID';
$locales['CHDEtablissement-hd_etablissement_id-court'] = 'ID';
$locales['CHDEtablissement-hd_etablissement_id-desc'] = 'Identifiant de l\'établissement';
$locales['CHDEtablissement-msg-create'] = 'Etablissement HospiDiag créé';
$locales['CHDEtablissement-msg-delete'] = 'Etablissement HospiDiag supprimé';
$locales['CHDEtablissement-msg-found'] = 'Etablissement HospiDiag retrouvé';
$locales['CHDEtablissement-msg-modify'] = 'Etablissement HospiDiag modifié';
$locales['CHDEtablissement-raison_sociale'] = 'Raison sociale';
$locales['CHDEtablissement-raison_sociale-court'] = 'Raison sociale';
$locales['CHDEtablissement-raison_sociale-desc'] = 'Raison sociale de l\'établissement';
$locales['CHDEtablissement-taa'] = 'TAA';
$locales['CHDEtablissement-taa-court'] = 'TAA';
$locales['CHDEtablissement-taa-desc'] = 'TAA';
$locales['CHDEtablissement-taille_c'] = 'Taille chirurgie';
$locales['CHDEtablissement-taille_c-court'] = 'C';
$locales['CHDEtablissement-taille_c-desc'] = 'Taille de la chirurgie';
$locales['CHDEtablissement-taille_m'] = 'Taille médecine';
$locales['CHDEtablissement-taille_m-court'] = 'M';
$locales['CHDEtablissement-taille_m-desc'] = 'Taille de la médecine';
$locales['CHDEtablissement-taille_mco'] = 'Taille MCO';
$locales['CHDEtablissement-taille_mco-court'] = 'MCO';
$locales['CHDEtablissement-taille_mco-desc'] = 'Taille de la médecine, chirurgie et obstétrique de l\'établissement';
$locales['CHDEtablissement-taille_o'] = 'Taille obstétrique';
$locales['CHDEtablissement-taille_o-court'] = 'O';
$locales['CHDEtablissement-taille_o-desc'] = 'Taille de l\'obstétrique';
$locales['CHDEtablissement-title-create'] = 'Création d\'un établissement HospiDiag';
$locales['CHDEtablissement-title-modify'] = 'Modification d\'un établissement HospiDiag';
$locales['CHDEtablissement.all'] = 'Tous les établissements HospiDiag';
$locales['CHDEtablissement.none'] = 'Aucun établissement HospiDiag';
$locales['CHDEtablissement.one'] = 'Un établissement HospiDiag';
$locales['CHDFinance'] = 'Finance HospiDiag';
$locales['CHDFinance-annee'] = 'Année';
$locales['CHDFinance-annee-court'] = 'Année';
$locales['CHDFinance-annee-desc'] = 'Année';
$locales['CHDFinance-besoin_fonds_roulement'] = 'Besoin en Fonds de Roulement en jours de charges courantes';
$locales['CHDFinance-besoin_fonds_roulement-court'] = 'Besoin fonds roulement';
$locales['CHDFinance-besoin_fonds_roulement-desc'] = 'Cet indicateur mesure le besoin de financement engendré par l\'exploitation courante de l\'établissement';
$locales['CHDFinance-caf'] = 'Taux de CAF';
$locales['CHDFinance-caf-court'] = 'CAF';
$locales['CHDFinance-caf-desc'] = 'Cet indicateur permet d\'apprécier la capacité de l\'activité de l\'établissement Î° investir. Il mesure la Â« marge Â» que l\'établissement dégage sur son exploitation Â« courante Â», laquelle permet , au plan comptable de couvrir les charges d\'amortissements et de provisions et, au plan financier, de dégager de la trésorerie utilisée pour le remboursement des dettes existantes et pour financer tout ou partie des nouveaux investissements';
$locales['CHDFinance-caf_nette'] = 'Taux de CAF nette';
$locales['CHDFinance-caf_nette-court'] = 'CAF nette';
$locales['CHDFinance-caf_nette-desc'] = 'Cet indicateur mesure la capacité structurelle (hors effet éventuel du résultat de l\'exercice ainsi que des provisions par essence aléatoires et non pérennes) de l\'établissement Î° autofinancer le renouvellement de ses immobilisations. 
Si l\'établissement est un établissement en Insuffisance d\'auto-financement ie CAF, alors cet indicateur n\'est pas calculé. Par contre, si ce ratio est négatif, il est affiché';
$locales['CHDFinance-creances_non_recouvrees'] = 'Créances patients et mutuelles non recouvrées en nombre de jours d\'exploitation';
$locales['CHDFinance-creances_non_recouvrees-court'] = 'Créances patients/mutuelles non recouvrées';
$locales['CHDFinance-creances_non_recouvrees-desc'] = 'Cet indicateur met en évidence le risque de non recouvrement des créances sur les patients hospitalisés et les consultants. 
A noter : la définition et le mode de calcul de cet indicateur varie entre les établissements publics et espics';
$locales['CHDFinance-dette_fournisseur'] = 'Dettes fournisseurs en nombre de jours d\'exploitation';
$locales['CHDFinance-dette_fournisseur-court'] = 'Dette fournisseur';
$locales['CHDFinance-dette_fournisseur-desc'] = 'Mesure la rotation de rÎ¸glement aux fournisseurs en fin d\'année';
$locales['CHDFinance-duree_dette'] = 'Durée apparente de la dette';
$locales['CHDFinance-duree_dette-court'] = 'Durée dette';
$locales['CHDFinance-duree_dette-desc'] = 'Cet indicateur permet d\'apprécier la capacité d\'un établissement Î° rembourser sa dette. 
C\'est un indicateur central dans l\'appréhension du niveau d\'endettement relatif de l\'établissement. Quand la CAF est négative, ce ratio n\'est
pas calculé';
$locales['CHDFinance-fond_roulement_net'] = 'Fonds de Roulement Net Global (FRNG) en jours de charges courantes';
$locales['CHDFinance-fond_roulement_net-court'] = 'Besoin FRNG';
$locales['CHDFinance-fond_roulement_net-desc'] = 'Cet indicateur mesure la capacité de l\'établissement Î° financer son cycle d\'exploitation par des ressources stables';
$locales['CHDFinance-hd_etablissement_id'] = 'Etablissement';
$locales['CHDFinance-hd_etablissement_id-court'] = 'Etablissement';
$locales['CHDFinance-hd_etablissement_id-desc'] = 'Etablissement de la finance';
$locales['CHDFinance-hd_finance_id'] = 'ID';
$locales['CHDFinance-hd_finance_id-court'] = 'ID';
$locales['CHDFinance-hd_finance_id-desc'] = 'Identifiant de la finance';
$locales['CHDFinance-inde_finance'] = 'Ratio d\'indépendance financiÎ¸re';
$locales['CHDFinance-inde_finance-court'] = 'Indépendance financiÎ¸re';
$locales['CHDFinance-inde_finance-desc'] = 'Cet indicateur mesure le taux de dépendance financiÎ¸re de l\'établissement et donc les marges de manÂœuvre permettant d\'envisager de nouveaux emprunts';
$locales['CHDFinance-intensite_invest'] = 'Intensité de l\'investissement';
$locales['CHDFinance-intensite_invest-court'] = 'Intensité investissement';
$locales['CHDFinance-intensite_invest-desc'] = 'Cet indicateur permet de mesurer la dynamique d\'investissement de l\'établissement.';
$locales['CHDFinance-marge_brute'] = 'Taux de marge brute';
$locales['CHDFinance-marge_brute-court'] = 'Marge brute';
$locales['CHDFinance-marge_brute-desc'] = 'Cet indicateur mesure la Â« marge Â» que l\'établissement dégage sur son exploitation Â« courante Â» pour financer ses charges financiÎ¸res, d\'amortissement et de provisions, c\'est-Î°-dire pour financer ses investissements';
$locales['CHDFinance-msg-create'] = 'Finance HospiDiag créée';
$locales['CHDFinance-msg-delete'] = 'Finance HospiDiag supprimée';
$locales['CHDFinance-msg-found'] = 'Finance HospiDiag retrouvée';
$locales['CHDFinance-msg-modify'] = 'Finance HospiDiag modifiée';
$locales['CHDFinance-title-create'] = 'Création d\'une finance';
$locales['CHDFinance-title-modify'] = 'Modification d\'une finance';
$locales['CHDFinance-vetuste_bat'] = 'Taux de vétusté des bÎ²timents';
$locales['CHDFinance-vetuste_bat-court'] = 'Vétusté des bÎ²timents';
$locales['CHDFinance-vetuste_bat-desc'] = 'Cet indicateur mesure le besoin de rénovation/reconstruction des bÎ²timents';
$locales['CHDFinance-vetuste_equip'] = 'Taux de vétusté des équipements';
$locales['CHDFinance-vetuste_equip-court'] = 'Vétusté des équipements';
$locales['CHDFinance-vetuste_equip-desc'] = 'Cet indicateur mesure le besoin de renouvellement des équipements';
$locales['CHDFinance.all'] = 'Toutes les finance HospiDiag';
$locales['CHDFinance.none'] = 'Aucune finance HospiDiag';
$locales['CHDFinance.one'] = 'Une finance HospiDiag';
$locales['CHDIdentite'] = 'Identité HospiDiag';
$locales['CHDIdentite-annee'] = 'Année';
$locales['CHDIdentite-annee-court'] = 'Année';
$locales['CHDIdentite-annee-desc'] = 'Année';
$locales['CHDIdentite-caf'] = 'Capacité d\'auto-financement';
$locales['CHDIdentite-caf-court'] = 'CAF';
$locales['CHDIdentite-caf-desc'] = 'La capacité d\'autofinancement (CAF) est le potentiel de l\'entreprise Î° dégager, de par son activité de la période, une ressource (un enrichissement de flux de fonds). 
Cette ressource interne pourra Îºtre utilisée notamment pour financer la croissance de l\'activité, financer de nouveaux investissements, rembourser des emprunts ou des dettes, de verser des dividendes aux propriétaires de l\'entreprise et augmenter le fonds de roulement.';
$locales['CHDIdentite-coeff_transition'] = 'Coefficient de transition';
$locales['CHDIdentite-coeff_transition-court'] = 'Coeff de transition';
$locales['CHDIdentite-coeff_transition-desc'] = 'Le passage Î° 100% devant comporter un Â« amortisseur Â», un coefficient de transition est appliqué de maniÎ¸re modulée Î° chaque établissement depuis 2008. 
ConcrÎ¸tement, l\'établissement devait disposer au 1er janvier 2008 du mÎºme montant de recettes que celui dont il aurait disposé dans le dispositif auparavant. Pour répondre Î° cette exigence, un mécanisme de coefficient de transition a été mis en place, permettant de prendre en compte l\'impact du passage Î° 100% T2A et assurant Î° l\'établissement le maintien de son niveau de recettes. 
L\'application de la T2A Î° 100% emportera son plein effet en 2012, les coefficients de convergence devant tendre vers la valeur 1.';
$locales['CHDIdentite-encours_dette'] = 'Encours de la dette';
$locales['CHDIdentite-encours_dette-court'] = 'Encoure de la dette';
$locales['CHDIdentite-encours_dette-desc'] = 'Compte 163 Â« Emprunts Obligataires Â» + Compte 164 Â« Emprunts auprÎ¸s des établissements de crédit Â» + Comptes 165, 167, 168 Â« Emprunts
et dettes financiÎ¸res divers Â»';
$locales['CHDIdentite-fond_roulement_besoin'] = 'Besoin en fonds de roulement (BFR)';
$locales['CHDIdentite-fond_roulement_besoin-court'] = 'BFR';
$locales['CHDIdentite-fond_roulement_besoin-desc'] = 'BFR : Besoin en Fonds de Roulement
Cette information mesure le besoin de financement engendré par l\'exploitation courante de l\'établissement. Le BFR étant généralement positif dans un établissement de santé, cette donnée met souvent en évidence l\'avance de trésorerie faite par l\'établissement. 
Le BFR est la différence entre l\'actif circulant (principalement stocks et créances) et le passif exigible (principalement dettes fournisseurs, dettes sociales et fiscales et avances reÎ·ues). 
= Â« Stocks et en cours Â» + Â« Créances d\'exploitation Â» + Â« Créances div erses Â» + Â« Charges constatées d\'avance Â» + Â« Comptes de régularisation (actif) sauf dotations attendues, prime de remboursement des obligations et charges Î° répartir sur plusieurs exercicesÂ» - Â« Avances reÎ·ues Â» - Â« Dettes fournisseurs et comptes rattachés Â» - Â« Dettes fiscales et sociales Â» - Â« Produits constatés d\'avance Â» - Â« Dettes sur immobilisations et comptes rattachés Â» - Â« dettes diverses autres Â» - Â« Comptes de régularisation (passif)Â». 
Les créances dites de l\'article 58 et de la sectorisation psychiatrique sont exclues.';
$locales['CHDIdentite-fond_roulement_net_global'] = 'Fonds de roulement net global (FRNG)';
$locales['CHDIdentite-fond_roulement_net_global-court'] = 'FRNG';
$locales['CHDIdentite-fond_roulement_net_global-desc'] = 'FRNG : Fond de Roulement Net Global
Cette information met en évidence la capacité de financement du cycle d\'exploitation par des ressources stables. 
Total ressources FRNG (hors Dépréciation des stocks, comptes de tiers et comptes financiers c/39, c/49, c/59) - Total emplois FRNG';
$locales['CHDIdentite-group_chir_1'] = '5 premiers Groupes d\'activité (GA) annuels en Chirurgie : Groupe 1';
$locales['CHDIdentite-group_chir_1-court'] = 'Groupe d\'activités chirurgie 1';
$locales['CHDIdentite-group_chir_1-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une Â« traditionnelle Â» d\'une chirurgie plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_chir_1_libelle'] = '5 premiers groupes d\'activité (GA) annuels en chirurgie : Libelle 1';
$locales['CHDIdentite-group_chir_1_libelle-court'] = 'Libelle GA chirurgie';
$locales['CHDIdentite-group_chir_1_libelle-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une Â« traditionnelle Â» d\'une chirurgie plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_chir_2'] = '5 premiers Groupes d\'activité (GA) annuels en Chirurgie : Groupe 2';
$locales['CHDIdentite-group_chir_2-court'] = 'Groupe d\'activités chirurgie 2';
$locales['CHDIdentite-group_chir_2-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une Â« traditionnelle Â» d\'une chirurgie plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_chir_2_libelle'] = '5 premiers groupes d\'activité (GA) annuels en chirurgie : Libelle 2';
$locales['CHDIdentite-group_chir_2_libelle-court'] = 'Libelle GA chirurgie';
$locales['CHDIdentite-group_chir_2_libelle-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une Â« traditionnelle Â» d\'une chirurgie plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_chir_3'] = '5 premiers Groupes d\'activité (GA) annuels en Chirurgie : Groupe 3';
$locales['CHDIdentite-group_chir_3-court'] = 'Groupe d\'activités chirurgie 3';
$locales['CHDIdentite-group_chir_3-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une Â« traditionnelle Â» d\'une chirurgie plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_chir_3_libelle'] = '5 premiers groupes d\'activité (GA) annuels en chirurgie : Libelle 3';
$locales['CHDIdentite-group_chir_3_libelle-court'] = 'Libelle GA chirurgie';
$locales['CHDIdentite-group_chir_3_libelle-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une Â« traditionnelle Â» d\'une chirurgie plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_chir_4'] = '5 premiers Groupes d\'activité (GA) annuels en Chirurgie : Groupe 4';
$locales['CHDIdentite-group_chir_4-court'] = 'Groupe d\'activités chirurgie 4';
$locales['CHDIdentite-group_chir_4-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une Â« traditionnelle Â» d\'une chirurgie plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_chir_4_libelle'] = '5 premiers groupes d\'activité (GA) annuels en chirurgie : Libelle 4';
$locales['CHDIdentite-group_chir_4_libelle-court'] = 'Libelle GA chirurgie';
$locales['CHDIdentite-group_chir_4_libelle-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une Â« traditionnelle Â» d\'une chirurgie plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_chir_5'] = '5 premiers Groupes d\'activité (GA) annuels en Chirurgie : Groupe 5';
$locales['CHDIdentite-group_chir_5-court'] = 'Groupe d\'activités chirurgie 5';
$locales['CHDIdentite-group_chir_5-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une Â« traditionnelle Â» d\'une chirurgie plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_chir_5_libelle'] = '5 premiers groupes d\'activité (GA) annuels en chirurgie : Libelle 1';
$locales['CHDIdentite-group_chir_5_libelle-court'] = 'Libelle GA chirurgie';
$locales['CHDIdentite-group_chir_5_libelle-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une Â« traditionnelle Â» d\'une chirurgie plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_med_1'] = '5 premiers Groupes d\'activité (GA) annuels en Médecine : Groupe 1';
$locales['CHDIdentite-group_med_1-court'] = 'Groupe d\'activité médecine 1';
$locales['CHDIdentite-group_med_1-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une médecine Â« traditionnelle Â» d\'une médecine plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_med_1_libelle'] = '5 premiers groupes d\'activité (GA) annuels en médecine : Libelle 1';
$locales['CHDIdentite-group_med_1_libelle-court'] = 'Libelle GA médecine';
$locales['CHDIdentite-group_med_1_libelle-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une médecine Â« traditionnelle Â» d\'une médecine plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_med_2'] = '5 premiers Groupes d\'activité (GA) annuels en Médecine : Groupe 2';
$locales['CHDIdentite-group_med_2-court'] = 'Groupe d\'activités médecine 2';
$locales['CHDIdentite-group_med_2-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une médecine Â« traditionnelle Â» d\'une médecine plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_med_2_libelle'] = '5 premiers groupes d\'activité (GA) annuels en médecine : Libelle 1';
$locales['CHDIdentite-group_med_2_libelle-court'] = 'Libelle GA médecine';
$locales['CHDIdentite-group_med_2_libelle-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une médecine Â« traditionnelle Â» d\'une médecine plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_med_3'] = '5 premiers Groupes d\'activité (GA) annuels en Médecine : Groupe 3';
$locales['CHDIdentite-group_med_3-court'] = 'Groupe d\'activités médecine 3';
$locales['CHDIdentite-group_med_3-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une médecine Â« traditionnelle Â» d\'une médecine plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_med_3_libelle'] = '5 premiers groupes d\'activité (GA) annuels en médecine : Libelle 1';
$locales['CHDIdentite-group_med_3_libelle-court'] = 'Libelle GA médecine';
$locales['CHDIdentite-group_med_3_libelle-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une médecine Â« traditionnelle Â» d\'une médecine plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_med_4'] = '5 premiers Groupes d\'activité (GA) annuels en Médecine : Groupe 4';
$locales['CHDIdentite-group_med_4-court'] = 'Groupe d\'activités médecine 4';
$locales['CHDIdentite-group_med_4-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une médecine Â« traditionnelle Â» d\'une médecine plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_med_4_libelle'] = '5 premiers groupes d\'activité (GA) annuels en médecine : Libelle 1';
$locales['CHDIdentite-group_med_4_libelle-court'] = 'Libelle GA médecine';
$locales['CHDIdentite-group_med_4_libelle-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une médecine Â« traditionnelle Â» d\'une médecine plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_med_5'] = '5 premiers Groupes d\'activité (GA) annuels en Médecine : Groupe 5';
$locales['CHDIdentite-group_med_5-court'] = 'Groupe activités médecine 5';
$locales['CHDIdentite-group_med_5-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une médecine Â« traditionnelle Â» d\'une médecine plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-group_med_5_libelle'] = '5 premiers groupes d\'activité (GA) annuels en médecine : Libelle 1';
$locales['CHDIdentite-group_med_5_libelle-court'] = 'Libelle GA médecine';
$locales['CHDIdentite-group_med_5_libelle-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement et met en évidence les spécialités dans lesquels l\'établissement a un volume d\'activité important. 
Cette information permet de repérer une médecine Â« traditionnelle Â» d\'une médecine plus technique/spécialisée. 
Le classement est établi Î° partir de la derniÎ¸re année disponible.
Les données sont repérées Î° partir de la classification V2016 des GHM';
$locales['CHDIdentite-hd_etablissement_id'] = 'Etablissement';
$locales['CHDIdentite-hd_etablissement_id-court'] = 'Etablissement';
$locales['CHDIdentite-hd_etablissement_id-desc'] = 'Etablissement HospiDiag';
$locales['CHDIdentite-hd_identite_id'] = 'ID';
$locales['CHDIdentite-hd_identite_id-court'] = 'ID';
$locales['CHDIdentite-hd_identite_id-desc'] = 'Identifiant';
$locales['CHDIdentite-msg-create'] = 'Identité HospiDiag créée';
$locales['CHDIdentite-msg-delete'] = 'Identité HospiDiag supprimée';
$locales['CHDIdentite-msg-found'] = 'Identité HospiDiag retrouvée';
$locales['CHDIdentite-msg-modify'] = 'Identité HospiDiag modifiée';
$locales['CHDIdentite-nb_accouchement'] = 'Nombre d\'accouchements';
$locales['CHDIdentite-nb_accouchement-court'] = 'Nb accouchements';
$locales['CHDIdentite-nb_accouchement-desc'] = 'Cette information permet d\'évaluer le volume d\'activité représenté par la maternité';
$locales['CHDIdentite-nb_actes_chir'] = 'Nombre d\'actes chirurgicaux';
$locales['CHDIdentite-nb_actes_chir-court'] = 'Nb actes chirurgicaux';
$locales['CHDIdentite-nb_actes_chir-desc'] = 'Cette information permet d\'évaluer le dynamisme de l\'activité chirurgicale au bloc opératoire (hors interventionnel).
A noter que cette donnée se distingue des RSS chirurgicaux. 
Le Â« nombre de séjours chirurgicaux avec acte classant Â», est l\'unité de mesure qui est celle des futurs décrets chirurgie';
$locales['CHDIdentite-nb_actes_endo'] = 'Nombre d\'actes d\'endoscopies';
$locales['CHDIdentite-nb_actes_endo-court'] = 'Nb actes endoscopies';
$locales['CHDIdentite-nb_actes_endo-desc'] = 'Cette information permet d\'évaluer le dynamisme de l\'activité interventionnelle';
$locales['CHDIdentite-nb_arthro_genou'] = 'Nombre d\'arthroscopie du genou';
$locales['CHDIdentite-nb_arthro_genou-court'] = 'Nb arthroscopie genou';
$locales['CHDIdentite-nb_arthro_genou-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement : elle illustre le dynamisme des spécialités majeures, Î° travers le volume d\'activité représenté dans l\'établissement par les interventions les plus fréquentes, au niveau national. 
Ces interventions sont au nombre de six. Cinq font partie de la liste des Â« 18 gestes marqueurs de chirurgie ambulatoire Â». L\'énumération des
codes CCAM conduisant Î° chacun de ces groupes est donnée en annexe 2. 
Pour la RTU prostate le code CCAM est Â« JGFA015 - résection d\'une hypertrophie de la prostate par urétrocystoscopie Â».';
$locales['CHDIdentite-nb_atu'] = 'Nombre d\'ATU';
$locales['CHDIdentite-nb_atu-court'] = 'Nb ATU';
$locales['CHDIdentite-nb_atu-desc'] = 'Cette information permet d\'évaluer le nombre total de passages aux urgences ne donnant pas lieu Î° une hospitalisation, facturés ou non Î° l\'assurance maladie. 
Pour cerner l\'intégralité de l\'activité d\'urgences, il faut compléter cette donnée par le nombre de passages aux urgences donnant lieu Î° une hospitalisation (en UHCD ou en hospitalisation complÎ¸te)';
$locales['CHDIdentite-nb_autre'] = 'Nombre de séances autres';
$locales['CHDIdentite-nb_autre-court'] = 'Nb séances autres';
$locales['CHDIdentite-nb_autre-desc'] = 'Ces informations permettent d\'évaluer le volume d\'activité représenté par les séances par exclusion des indicateurs précédents.
Les informations suivantes sont analysées : 
Nombre de séances des RSA des GHM 
- 28Z01Z - EntraÎ¾nements Î° la dialyse péritonéale automatisée, en séances 
- 28Z02Z - EntraÎ¾nements Î° la dialyse péritonéale continue ambulatoire, en séances 
- 28Z03Z - EntraÎ¾nements Î° l\'hémodialyse, en séances 
- 28Z14Z - Transfusions, en séances 
- 28Z15Z - Oxygénothérapie hyperbare, en séances 
- 28Z16Z - AphérÎ¸ses sanguines, en séances 
- 28Z17Z - Chimiothérapie pour affection non tumorale, en séances';
$locales['CHDIdentite-nb_b'] = 'Nombre de B (B et GHN Î° partir de 2009)';
$locales['CHDIdentite-nb_b-court'] = 'Nb B';
$locales['CHDIdentite-nb_b-desc'] = 'Cette information permet d\'apporter une indication sur la production du laboratoire. Sont pris en compte les B produits (pour les malades hospitalisés, pour les patients des consultations externes, pour l\'extérieur) pour les laboratoires de biochimie, de microbiologie, d\'immuno-hématologie, et pour les autres spécialités et laboratoires indifférenciés';
$locales['CHDIdentite-nb_cataracte'] = 'Nombre de cataractes';
$locales['CHDIdentite-nb_cataracte-court'] = 'Nb cataractes';
$locales['CHDIdentite-nb_cataracte-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement : elle illustre le dynamisme des spécialités majeures, Î° travers le volume d\'activité représenté dans l\'établissement par les interventions les plus fréquentes, au niveau national. 
Ces interventions sont au nombre de six. Cinq font partie de la liste des Â« 18 gestes marqueurs de chirurgie ambulatoire Â». L\'énumération des
codes CCAM conduisant Î° chacun de ces groupes est donnée en annexe 2. 
Pour la RTU prostate le code CCAM est Â« JGFA015 - résection d\'une hypertrophie de la prostate par urétrocystoscopie Â».';
$locales['CHDIdentite-nb_chimio'] = 'Nombre de séances de chimiothérapie';
$locales['CHDIdentite-nb_chimio-court'] = 'Nb séances chimio';
$locales['CHDIdentite-nb_chimio-desc'] = 'Ces informations permettent d\'évaluer le volume d\'activité représenté par les séances de chimiothérapie.
Les informations suivantes sont analysées : 
Nombre de séances des RSA des GHM : 
- 28Z07Z - Chimiothérapie pour tumeur, en séances';
$locales['CHDIdentite-nb_chir_sein'] = 'Nombre de chirurgies du sein';
$locales['CHDIdentite-nb_chir_sein-court'] = 'Nb chirurgies du sein';
$locales['CHDIdentite-nb_chir_sein-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement : elle illustre le dynamisme des spécialités majeures, Î° travers le volume d\'activité représenté dans l\'établissement par les interventions les plus fréquentes, au niveau national. 
Ces interventions sont au nombre de six. Cinq font partie de la liste des Â« 18 gestes marqueurs de chirurgie ambulatoire Â». L\'énumération des
codes CCAM conduisant Î° chacun de ces groupes est donnée en annexe 2. 
Pour la RTU prostate le code CCAM est Â« JGFA015 - résection d\'une hypertrophie de la prostate par urétrocystoscopie Â».';
$locales['CHDIdentite-nb_etp_medicaux'] = 'Nombre ETP médicaux ';
$locales['CHDIdentite-nb_etp_medicaux-court'] = 'Nb ETP médicaux';
$locales['CHDIdentite-nb_etp_medicaux-desc'] = 'Ces informations permettent d\'avoir une vision globale des ressources humaines médicales de l\'établissement, déclinées par discipline. 
Les informations suivantes sont analysées : 
- ETP médicaux
    - dont Médecins (hors anesthésistes)
    - dont Chirurgiens (hors gynécologues-obstétriciens)
    - dont Anesthésistes
    - dont Gynécologues-obstétriciens';
$locales['CHDIdentite-nb_etp_medicaux_anesth'] = 'Nombre ETP médicaux dont anesthésistes';
$locales['CHDIdentite-nb_etp_medicaux_anesth-court'] = 'Nb anesthésistes';
$locales['CHDIdentite-nb_etp_medicaux_anesth-desc'] = 'L\'information suivante est analysée : ETP médicaux dont Anesthésistes';
$locales['CHDIdentite-nb_etp_medicaux_chir'] = 'Nombre ETP médicaux dont chirurgiens (hors gynécologues-obstétriciens)';
$locales['CHDIdentite-nb_etp_medicaux_chir-court'] = 'Nb chirurgiens';
$locales['CHDIdentite-nb_etp_medicaux_chir-desc'] = 'L\'information suivante est analysée : ETP médicaux dont Chirurgiens (hors gynécologues-obstétriciens).';
$locales['CHDIdentite-nb_etp_medicaux_gyneco_obs'] = 'Nombre ETP médicaux dont gynécologues-obstétriciens';
$locales['CHDIdentite-nb_etp_medicaux_gyneco_obs-court'] = 'Nb gynécologues-obstétriciens';
$locales['CHDIdentite-nb_etp_medicaux_gyneco_obs-desc'] = 'L\'information suivante est analysée : ETP médicaux dont Gynécologues-obstétriciens.';
$locales['CHDIdentite-nb_etp_medicaux_med'] = 'Nombre ETP médicaux dont médecins (hors anesthésistes)';
$locales['CHDIdentite-nb_etp_medicaux_med-court'] = 'Nb médecins';
$locales['CHDIdentite-nb_etp_medicaux_med-desc'] = 'L\'information suivante est analysée : 
- ETP médicaux dont Médecins (hors anesthésistes)';
$locales['CHDIdentite-nb_etp_non_med'] = 'Nombre ETP non médicaux';
$locales['CHDIdentite-nb_etp_non_med-court'] = 'Nb ETP non médicaux';
$locales['CHDIdentite-nb_etp_non_med-desc'] = 'Ces informations permettent d\'avoir une vision globale des ressources humaines non médicales de l\'établissement, déclinées par métiers.
Les informations suivantes sont analysées : 
- ETP non médicaux
    - dont Personnels de Direction et Administratifs
    - dont Personnels des services de Soins
    - dont Personnels Educatifs et Sociaux
    - dont Personnels Médico-Techniques
    - dont Personnels Techniques et Ouvriers.';
$locales['CHDIdentite-nb_etp_non_med_direction_administratif'] = 'Nombre ETP non médicaux  dont personnels de direction et administratifs';
$locales['CHDIdentite-nb_etp_non_med_direction_administratif-court'] = 'Nb personnel direction et administratif';
$locales['CHDIdentite-nb_etp_non_med_direction_administratif-desc'] = 'L\'information suivante est analysée : ETP non médicaux dont Personnels des services de Soins';
$locales['CHDIdentite-nb_etp_non_med_educatifs_sociaux'] = 'Nombre ETP non médicaux dont personnels éducatifs et sociaux';
$locales['CHDIdentite-nb_etp_non_med_educatifs_sociaux-court'] = 'Nb personnel éducatif et sociaux';
$locales['CHDIdentite-nb_etp_non_med_educatifs_sociaux-desc'] = 'L\'information suivante est analysée : ETP non médicaux dont Personnels Educatifs et Sociaux';
$locales['CHDIdentite-nb_etp_non_med_medico_technique'] = 'Nombre ETP non médicaux dont personnel médico-techniques';
$locales['CHDIdentite-nb_etp_non_med_medico_technique-court'] = 'Nb personnel médico-techniques';
$locales['CHDIdentite-nb_etp_non_med_medico_technique-desc'] = 'L\'information suivante est analysée : ETP non médicaux dont Personnels Médico-Techniques';
$locales['CHDIdentite-nb_etp_non_med_services_soins'] = 'Nombre ETP non médicaux  dont personnels des services de soins';
$locales['CHDIdentite-nb_etp_non_med_services_soins-court'] = 'Nb personnel services de soins';
$locales['CHDIdentite-nb_etp_non_med_services_soins-desc'] = 'L\'information suivante est analysée : ETP non médicaux dont Personnels des services de Soins';
$locales['CHDIdentite-nb_etp_non_med_techniques_ouvriers'] = 'Nombre ETP non médicaux dont personnels techniques et ouvriers';
$locales['CHDIdentite-nb_etp_non_med_techniques_ouvriers-court'] = 'Nb personnel techniques et ouvriers';
$locales['CHDIdentite-nb_etp_non_med_techniques_ouvriers-desc'] = 'L\'information suivante est analysée : ETP non médicaux dont Personnels Techniques et Ouvriers';
$locales['CHDIdentite-nb_examens'] = 'Nombre d\'examens';
$locales['CHDIdentite-nb_examens-court'] = 'Nb examens';
$locales['CHDIdentite-nb_examens-desc'] = 'Cette information permet d\'apporter une indication sur la production du laboratoire.
Sont pris en compte les nombres d\'examens de biologie (pour les malades hospitalisés, pour les patients des consultations externes, pour l\'extérieur) pour les laboratoires de biochimie, de microbiologie, d\'immuno-hématologie, et pour les autres spécialités et laboratoires indifférenciés';
$locales['CHDIdentite-nb_hemo'] = 'Nombre de séances d\'hémodialyse';
$locales['CHDIdentite-nb_hemo-court'] = 'Nb séances hémodialyse';
$locales['CHDIdentite-nb_hemo-desc'] = 'Ces informations permettent d\'évaluer le volume d\'activité représenté par les séances de dialyse d\'autre part. 
Les informations suivantes sont analysées : 
Nombre de séances des RSA des GHM 
- 28Z04Z - Hémodialyse, en séances';
$locales['CHDIdentite-nb_hernie_adulte'] = 'Nombre d\'hernies (adulte)';
$locales['CHDIdentite-nb_hernie_adulte-court'] = 'Nb hernies (adulte)';
$locales['CHDIdentite-nb_hernie_adulte-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement : elle illustre le dynamisme des spécialités majeures, Î° travers le volume d\'activité représenté dans l\'établissement par les interventions les plus fréquentes, au niveau national. 
Ces interventions sont au nombre de six. Cinq font partie de la liste des Â« 18 gestes marqueurs de chirurgie ambulatoire Â». L\'énumération des
codes CCAM conduisant Î° chacun de ces groupes est donnée en annexe 2. 
Pour la RTU prostate le code CCAM est Â« JGFA015 - résection d\'une hypertrophie de la prostate par urétrocystoscopie Â».';
$locales['CHDIdentite-nb_hernie_enfant'] = 'Nombre d\'hernies (enfant)';
$locales['CHDIdentite-nb_hernie_enfant-court'] = 'Nb hernies (enfant)';
$locales['CHDIdentite-nb_hernie_enfant-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement : elle illustre le dynamisme des spécialités majeures, Î° travers le volume d\'activité représenté dans l\'établissement par les interventions les plus fréquentes, au niveau national. 
Ces interventions sont au nombre de six. Cinq font partie de la liste des Â« 18 gestes marqueurs de chirurgie ambulatoire Â». L\'énumération des
codes CCAM conduisant Î° chacun de ces groupes est donnée en annexe 2. 
Pour la RTU prostate le code CCAM est Â« JGFA015 - résection d\'une hypertrophie de la prostate par urétrocystoscopie Â».';
$locales['CHDIdentite-nb_irm'] = 'Nombre d\'IRM';
$locales['CHDIdentite-nb_irm-court'] = 'Nb IRM';
$locales['CHDIdentite-nb_irm-desc'] = 'Cette information permet d\'apporter une indication sur les équipements dont dispose l\'établissement 
Les informations analysées sont les suivantes : 
IRM';
$locales['CHDIdentite-nb_lits_chir'] = 'Nombre de lits installés en chirurgie';
$locales['CHDIdentite-nb_lits_chir-court'] = 'Nb lits chirurgie';
$locales['CHDIdentite-nb_lits_chir-desc'] = 'Ces informations donnent un aperÎ·u des capacités d\'accueil de l\'établissement en chirurgie 
Les données concernant ces tableaux sont issues se l\'enquÎºte SAE. Les données sont consultables sur le site SAE diffusion : 
http://www.sae-diffusion.sante.gouv .fr 
L\'information suivante est analysée : 
- Lits installés de Chirurgie';
$locales['CHDIdentite-nb_lits_med'] = 'Nombre de lits installés en médecine';
$locales['CHDIdentite-nb_lits_med-court'] = 'Nb lits médecine';
$locales['CHDIdentite-nb_lits_med-desc'] = 'Ces informations donnent un aperÎ·u des capacités d\'accueil de l\'établissement en médecine. 
Les données concernant ces tableaux sont issues se l\'enquÎºte SAE. Les données sont consultables sur le site SAE diffusion : 
http://www.sae-diffusion.sante.gouv .fr 
Les informations suivantes sont analysées : 
- Nb de lits installés en Médecine :
    - dont lits installés de soins intensifs
    - dont lits installés de réanimation
    - dont lits installés de surveillance continue';
$locales['CHDIdentite-nb_lits_obs'] = 'Nombre de lits installés en obstétrique';
$locales['CHDIdentite-nb_lits_obs-court'] = 'Nb lits obstétrique';
$locales['CHDIdentite-nb_lits_obs-desc'] = 'Ces informations donnent un aperÎ·u des capacités d\'accueil de l\'établissement en obstétrique 
Les données concernant ces tableaux sont issues se l\'enquÎºte SAE. Les données sont consultables sur le site SAE diffusion : 
http://www.sae-diffusion.sante.gouv .fr 
L\'information suivante est analysée :
- Lits installés d\'Obstétrique';
$locales['CHDIdentite-nb_lits_reanimation'] = 'Nombre de lits installés en médecine dont lits de réanimation';
$locales['CHDIdentite-nb_lits_reanimation-court'] = 'Nb lits réanimation';
$locales['CHDIdentite-nb_lits_reanimation-desc'] = 'Les données concernant ces tableaux sont issues de l\'enquÎºte SAE. Les données sont consultables sur le site SAE diffusion : 
http://www.sae-diffusion.sante.gouv .fr 
L\'information suivante est analysée : Nb de lits installés en Médecine dont lits de réanimation';
$locales['CHDIdentite-nb_lits_soins_intensifs'] = 'Nombre de lits installés en médecine dont lits de soins intensifs';
$locales['CHDIdentite-nb_lits_soins_intensifs-court'] = 'Nb lits soins intensifs';
$locales['CHDIdentite-nb_lits_soins_intensifs-desc'] = 'Les données concernant ces tableaux sont issues de l\'enquÎºte SAE. Les données sont consultables sur le site SAE diffusion : 
http://www.sae-diffusion.sante.gouv .fr 
L\'information suivante est analysée : Nb de lits installés de Médecine dont lits de soins intensifs';
$locales['CHDIdentite-nb_lits_surv_continue'] = 'Nombre de lits installés en médecine dont lits de surveillance continue';
$locales['CHDIdentite-nb_lits_surv_continue-court'] = 'Nb lits surveillance continue';
$locales['CHDIdentite-nb_lits_surv_continue-desc'] = 'Les données concernant ces tableaux sont issues de l\'enquÎºte SAE. Les données sont consultables sur le site SAE diffusion : 
http://www.sae-diffusion.sante.gouv .fr 
L\'information suivante est analysée : Nb de lits installés de Médecine dont lits de surveillance continue';
$locales['CHDIdentite-nb_places_chir'] = 'Nombre de places installées en chirurgie';
$locales['CHDIdentite-nb_places_chir-court'] = 'Nb places chirurgie';
$locales['CHDIdentite-nb_places_chir-desc'] = 'Ces informations donnent un aperÎ·u des capacités d\'accueil de l\'établissement en chirurgie 
Les données concernant ces tableaux sont issues se l\'enquÎºte SAE. Les données sont consultables sur le site SAE diffusion : 
http://www.sae-diffusion.sante.gouv .fr 
L\'information suivante est analysée :
- Places installées de Chirurgie';
$locales['CHDIdentite-nb_places_med'] = 'Nombre de places installées en médecine';
$locales['CHDIdentite-nb_places_med-court'] = 'Nb places médecine';
$locales['CHDIdentite-nb_places_med-desc'] = 'Ces informations donnent un aperÎ·u des capacités d\'accueil de l\'établissement en médecine. 
Les données concernant ces tableaux sont issues se l\'enquÎºte SAE. Les données sont consultables sur le site SAE diffusion : 
http://www.sae-diffusion.sante.gouv .fr 
L\'information suivante est analysée :
- Places installés de Médecine';
$locales['CHDIdentite-nb_places_obs'] = 'Nombre de places installées en obstétrique';
$locales['CHDIdentite-nb_places_obs-court'] = 'Nb places obstétrique';
$locales['CHDIdentite-nb_places_obs-desc'] = 'Ces informations donnent un aperÎ·u des capacités d\'accueil de l\'établissement en obstétrique 
Les données concernant ces tableaux sont issues se l\'enquÎºte SAE. Les données sont consultables sur le site SAE diffusion : 
http://www.sae-diffusion.sante.gouv .fr 
L\'information suivante est analysée :
- Places installées d\'Obstétrique';
$locales['CHDIdentite-nb_racine_ghm'] = 'Nombre minimum de racines de GHM pour 80% des séjours';
$locales['CHDIdentite-nb_racine_ghm-court'] = 'Nb minimum GHM pour 80% des séjours';
$locales['CHDIdentite-nb_racine_ghm-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement : établissement généraliste (couvrant un large spectre de racines de GHM) ou spécialiste (couvrant un nombre réduit de racines de GHM). 
Le nombre minimum de racines de GHM est obtenu aprÎ¸s tri du casemix par effectifs décroissants';
$locales['CHDIdentite-nb_radio'] = 'Nombre de séances de radiothérapie';
$locales['CHDIdentite-nb_radio-court'] = 'Nb séances radiothérapie';
$locales['CHDIdentite-nb_radio-desc'] = 'Ces informations permettent d\'évaluer le volume d\'activité représenté par les séances de radiothérapie. 
A noter : l\'activité de radiothérapie, faite par les centres spécialisés non MCO, ne remonte pas dans les bases.
Les informations suivantes sont analysées : 
Nombre de séances des RSA des GHM  : 
- 28Z10Z - Curiethérapie, en séances 
- 28Z11Z - Techniques spéciales d\'irradiation externe, en séances 
- 28Z18Z - séances de radiothérapie conformationnelle avec modulation d\'intensité 
- 28Z19Z - Préparations Î° une irradiation externe par RCMI ou techniques spéciales 
- 28Z20Z - Préparations Î° une irradiation externe av c dosimétrie tridimensionnelle avec HDV 
- 28Z21Z - Préparations Î° une irradiation externe avec dosimétrie tridimensionnelle sans HDV 
- 28Z22Z - Autres préparations Î° une irradiation externe 
- 28Z23Z - Techniques complexes d\'irradiation externe avec repositionnement, en séances 
- 28Z24Z - Techniques complexes d\'irradiation externe sans repositionnement, en séances 
- 28Z25Z - Autres techniques d\'irradiation externe, en séances';
$locales['CHDIdentite-nb_rsa_chir'] = 'Nombre de RSA de chirurgie (HC)';
$locales['CHDIdentite-nb_rsa_chir-court'] = 'Nb RSA chirurgie (HC)';
$locales['CHDIdentite-nb_rsa_chir-desc'] = 'Ces informations permettent d\'évaluer le volume d\'activité pris en charge par l\'établissement en hospitalisation complÎ¸te en chirurgie (ASO=C) 
Nombre de RSA de durée est égale ou supérieure Î° 1 jour (date de sortie différente de la date d\'entrée)';
$locales['CHDIdentite-nb_rsa_chir_ambu'] = 'Nombre de RSA de chirurgie (ambulatoire)';
$locales['CHDIdentite-nb_rsa_chir_ambu-court'] = 'Nb RSA chirurgie (ambu)';
$locales['CHDIdentite-nb_rsa_chir_ambu-desc'] = 'Ces informations permettent d\'évaluer le volume d\'activité pris en charge par l\'établissement en ambulatoire en chirurgie (ASO=C) 
Nombre de RSA de durée &lt;1 jour (date de sortie = date d\'entrée) 
Hors endoscopies';
$locales['CHDIdentite-nb_rsa_med'] = 'Nombre de RSA de médecine (HC)';
$locales['CHDIdentite-nb_rsa_med-court'] = 'Nb RSA médecine (HC)';
$locales['CHDIdentite-nb_rsa_med-desc'] = 'Ces informations permettent d\'évaluer le volume d\'activité pris en charge par l\'établissement en hospitalisation complÎ¸te en médecine (ASO=M) 
Nombre de RSA de durée est égale ou supérieure Î° 1 jour (date de sortie différente de la date d\'entrée)';
$locales['CHDIdentite-nb_rsa_med_ambu'] = 'Nombre de RSA de médecine (ambulatoire)';
$locales['CHDIdentite-nb_rsa_med_ambu-court'] = 'Nb RSA médecine (ambu)';
$locales['CHDIdentite-nb_rsa_med_ambu-desc'] = 'Ces informations permettent d\'évaluer le volume d\'activité pris en charge par l\'établissement en ambulatoire en médecine (ASO=M) 
Nombre de RSA de durée &lt;1 jour (date de sortie = date d\'entrée)';
$locales['CHDIdentite-nb_rsa_obs'] = 'Nombre de RSA d\'obstétrique (HC)';
$locales['CHDIdentite-nb_rsa_obs-court'] = 'Nb RSA obstétrique (HC)';
$locales['CHDIdentite-nb_rsa_obs-desc'] = 'Ces informations permettent d\'évaluer le volume d\'activité pris en charge par l\'établissement en hospitalisation complÎ¸te en obstétrique(ASO=O) 
Nombre de RSA de durée est égale ou supérieure Î° 1 jour (date de sortie différente de la date d\'entrée)';
$locales['CHDIdentite-nb_rsa_obs_ambu'] = 'Nombre de RSA d\'obstétrique (ambulatoire)';
$locales['CHDIdentite-nb_rsa_obs_ambu-court'] = 'Nb RSA obstétrique (ambu)';
$locales['CHDIdentite-nb_rsa_obs_ambu-desc'] = 'Ces informations permettent d\'évaluer le volume d\'activité pris en charge par l\'établissement en ambulatoire en obstétrique (ASO=O) 
Nombre de RSA de durée &lt;1 jour (date de sortie = date d\'entrée)';
$locales['CHDIdentite-nb_rtu_prostate'] = 'Nombre de RTU prostate';
$locales['CHDIdentite-nb_rtu_prostate-court'] = 'Nb RTU prostate';
$locales['CHDIdentite-nb_rtu_prostate-desc'] = 'Cette information permet d\'apporter une indication sur le profil de l\'établissement : elle illustre le dynamisme des spécialités majeures, Î° travers le volume d\'activité représenté dans l\'établissement par les interventions les plus fréquentes, au niveau national. 
Ces interventions sont au nombre de six. Cinq font partie de la liste des Â« 18 gestes marqueurs de chirurgie ambulatoire Â». L\'énumération des
codes CCAM conduisant Î° chacun de ces groupes est donnée en annexe 2. 
Pour la RTU prostate le code CCAM est Â« JGFA015 - résection d\'une hypertrophie de la prostate par urétrocystoscopie Â».';
$locales['CHDIdentite-nb_salles_interv_chir'] = 'Nombre de salles d\'intervention chirurgicale';
$locales['CHDIdentite-nb_salles_interv_chir-court'] = 'Nb salles d\'interv chirurgicale';
$locales['CHDIdentite-nb_salles_interv_chir-desc'] = 'Cette information permet d\'apporter une indication sur les équipements dont dispose l\'établissement.
Les informations analysées sont les suivantes : 
Salles d\'interventions chirurgicales avec anesthésie';
$locales['CHDIdentite-nb_salles_radio_vasc'] = 'Nombre de salles de radiologie vasculaire';
$locales['CHDIdentite-nb_salles_radio_vasc-court'] = 'Nb salles radiologie vasculaire';
$locales['CHDIdentite-nb_salles_radio_vasc-desc'] = 'Cette information permet d\'apporter une indication sur les équipements dont dispose l\'établissement';
$locales['CHDIdentite-nb_scanners'] = 'Nombre de scanners';
$locales['CHDIdentite-nb_scanners-court'] = 'Nb scanners';
$locales['CHDIdentite-nb_scanners-desc'] = 'Cette information permet d\'apporter une indication sur les équipements dont dispose l\'établissement 
Les informations analysées sont les suivantes : 
Scanner';
$locales['CHDIdentite-nb_table_corona'] = 'Nombre de tables de coronarographie';
$locales['CHDIdentite-nb_table_corona-court'] = 'Nb tables coronarographie';
$locales['CHDIdentite-nb_table_corona-desc'] = 'Cette information permet d\'apporter une indication sur les équipements dont dispose l\'établissement. 
Les informations analysées sont les suivantes : 
Coronarographie';
$locales['CHDIdentite-nb_tep_scan'] = 'Nombre de TEP-SCAN';
$locales['CHDIdentite-nb_tep_scan-court'] = 'Nb TEP-SCAN';
$locales['CHDIdentite-nb_tep_scan-desc'] = 'Cette information permet d\'apporter une indication sur les équipements dont dispose l\'établissement 
Les informations analysées sont les suivantes : 
TEP-SCAN';
$locales['CHDIdentite-niveau_mater'] = 'Niveau de la maternité';
$locales['CHDIdentite-niveau_mater-court'] = 'Niveau maternité';
$locales['CHDIdentite-niveau_mater-desc'] = 'Cette information permet d\'apporter une indication sur le profil de la maternité de l\'établissement : maternité Â« traditionnelle Â» ou de recours';
$locales['CHDIdentite-prod_ac'] = 'Total des produits courants de fonctionnement dont AC (Aide Î° la contractualisation)';
$locales['CHDIdentite-prod_ac-court'] = 'AC';
$locales['CHDIdentite-prod_ac-desc'] = 'Total des recettes d\'exploitation (Compte 70 Î° 75 hors 7087) dont AC (Aide Î° la Contractualisation)';
$locales['CHDIdentite-prod_merri'] = 'Total des produits courants de fonctionnement dont MERRI';
$locales['CHDIdentite-prod_merri-court'] = 'MERRI';
$locales['CHDIdentite-prod_merri-desc'] = 'Total des recettes d\'exploitation (Compte 70 Î° 75 hors 7087) dont MERRI (les missions Â« PHRC Â» [MIG_06] et Â« laboratoires d\'oncogénétique, de génétique moléculaire, de cytogénétique et de neurogénétique Â» [MIG_28] ne sont pas prises en compte)';
$locales['CHDIdentite-prod_migac'] = 'Total des produits courants de fonctionnement dont recettes MIGAC';
$locales['CHDIdentite-prod_migac-court'] = 'Recettes MIGAC';
$locales['CHDIdentite-prod_migac-desc'] = 'Total des recettes d\'exploitation (Compte 70 Î° 75 hors 7087) dont recettes MIGAC';
$locales['CHDIdentite-prod_recette_daf'] = 'Total des produits courants de fonctionnement dont recettes DAF';
$locales['CHDIdentite-prod_recette_daf-court'] = 'DAF';
$locales['CHDIdentite-prod_recette_daf-desc'] = 'Total des recettes d\'exploitation (Compte 70 Î° 75 hors 7087) dont Recettes DAF';
$locales['CHDIdentite-prod_recette_mco'] = 'Total des produits courants de fonctionnement dont recettes autres que MCO (PSY,SSR)';
$locales['CHDIdentite-prod_recette_mco-court'] = 'Non MCO';
$locales['CHDIdentite-prod_recette_mco-desc'] = 'Total des produits courants de fonctionnement dont recettes autres que MCO (PSY,SSR)';
$locales['CHDIdentite-prod_taa'] = 'Total des produits courants de fonctionnement dont recettes TAA';
$locales['CHDIdentite-prod_taa-court'] = 'Recettes TAA';
$locales['CHDIdentite-prod_taa-desc'] = 'Total des recettes d\'exploitation (Compte 70 Î° 75 hors 7087) dont recettes T2A';
$locales['CHDIdentite-resultat_consolide'] = 'Résultat consolidé';
$locales['CHDIdentite-resultat_consolide-court'] = 'Résultat consolidé';
$locales['CHDIdentite-resultat_consolide-desc'] = 'Cette information est le reflet de la performance financiÎ¸re globale de l\'établissement et de son équilibre budgétaire. Le résultat est analysé Î° la fois en consolidé (prise en compte des budgets annexes) et sur le budget principal (hÏ„pital) 
Les informations suivantes sont analysées : 
- Résultat consolidé
    - Dont résultat du budget principal';
$locales['CHDIdentite-resultat_consolide_budget_principal'] = 'Résultat net dont résultat du budget principal';
$locales['CHDIdentite-resultat_consolide_budget_principal-court'] = 'Résultat budget princiapl';
$locales['CHDIdentite-resultat_consolide_budget_principal-desc'] = 'Résultat consolidé dont résultat du budget principal';
$locales['CHDIdentite-resultat_net'] = 'Résultat net';
$locales['CHDIdentite-resultat_net-court'] = 'Résultat net';
$locales['CHDIdentite-resultat_net-desc'] = 'Cette information est le reflet de la performance financiÎ¸re globale de l\'établissement et de son équilibre budgétaire. 
Les informations suivantes sont analysées :
- Résultat consolidé ("Bénéfice ou Perte" (HN))';
$locales['CHDIdentite-taux_info_agenda_patient'] = 'Taux d\'informatisation de la programmation des ressources et agenda du patient';
$locales['CHDIdentite-taux_info_agenda_patient-court'] = 'Informatisation "agenda patient"';
$locales['CHDIdentite-taux_info_agenda_patient-desc'] = 'Cette donnée mesure le niveau d\'informatisation de l\'établissement de santé sur le domaine fonctionnel Â« Programmation des ressources et agenda du patient Â»';
$locales['CHDIdentite-taux_info_dpii'] = 'Taux d\'informatisation du DPII (Dossier Patient Informatisé et Interopérable)';
$locales['CHDIdentite-taux_info_dpii-court'] = 'Informatisation DPII';
$locales['CHDIdentite-taux_info_dpii-desc'] = 'Cette donnée mesure le niveau d\'informatisation de l\'établissement de santé sur le domaine fonctionnel Â« DPII (Dossier patient informatisé et interopérable) et communication extérieure Â»';
$locales['CHDIdentite-taux_info_ima_bio_ana'] = 'Taux d\'informatisation des résultats d\'imagerie, de biologie et d\'anatomo-patho';
$locales['CHDIdentite-taux_info_ima_bio_ana-court'] = 'Informatisation imagerie, biologie, anatomo-pathologie';
$locales['CHDIdentite-taux_info_ima_bio_ana-desc'] = 'Cette donnée mesure le niveau d\'informatisation de l\'établissement de santé sur le domaine fonctionnel Â«Résultats d\'imagerie, de biologie et d\'anatomo-pathologieÂ».';
$locales['CHDIdentite-taux_info_med'] = 'Taux d\'informatisation du pilotage médico-économique';
$locales['CHDIdentite-taux_info_med-court'] = 'Informatisation pilotage médico-économique';
$locales['CHDIdentite-taux_info_med-desc'] = 'Cette donnée mesure le niveau d\'informatisation de l\'établissement de santé sur le domaine fonctionnel Â« Pilotage médico-économique Â»';
$locales['CHDIdentite-taux_info_presc'] = 'Taux d\'informatisation de la prescription alimentant le plan de soins';
$locales['CHDIdentite-taux_info_presc-court'] = 'Informatisation de la prescription';
$locales['CHDIdentite-taux_info_presc-desc'] = 'Cette donnée mesure le niveau d\'informatisation de l\'établissement de santé sur le domaine fonctionnel Â« Prescription électronique alimentant le plan de soins Â»';
$locales['CHDIdentite-title-create'] = 'Créer une identité HospiDiag';
$locales['CHDIdentite-title-modify'] = 'Modifier une identité HospiDiag';
$locales['CHDIdentite-total_bilan'] = 'Total bilan';
$locales['CHDIdentite-total_bilan-court'] = 'Total bilan';
$locales['CHDIdentite-total_bilan-desc'] = 'Cette information renseigne sur le volume des actifs de l\'établissement.';
$locales['CHDIdentite-total_charges'] = 'Total des charges courantes de fonctionnement';
$locales['CHDIdentite-total_charges-court'] = 'Charges courantes de fonctionnement';
$locales['CHDIdentite-total_charges-desc'] = 'Cette information met en évidence la taille de l\'établissement (avec un focus sur les charges liées Î° l\'activité MCO). 
Les informations suivantes sont analysées : 
- Total des charges de fonctionnement (comptes 60 Î° 65 hors 7087)
    - Dont dépenses MCO';
$locales['CHDIdentite-total_charges_mco'] = 'Total des charges courantes de fonctionnement dont dépenses MCO';
$locales['CHDIdentite-total_charges_mco-court'] = 'Charges MCO';
$locales['CHDIdentite-total_charges_mco-desc'] = 'Total des charges de fonctionnement (comptes 60 Î° 65 hors 7087) dont dépenses MCO 
Correspond au total des charges nettes majorées pour les activités Court séjour MCO
(MCO_Hospitalisation,MCO_ConsultationsEtActesExternes), MCO_Urgences et ActivitésSpécifiquesMCO';
$locales['CHDIdentite-total_prod'] = 'Total des produits courants de fonctionnement';
$locales['CHDIdentite-total_prod-court'] = 'Tot produits courants de fonctionnement';
$locales['CHDIdentite-total_prod-desc'] = 'Cette information met en évidence la taille de l\'établissement et apporte un éclairage sur la décomposition des recettes d\'exploitation. On attend par recettes d\'exploitation la somme des recettes T2A (part Assurance Maladie et patients), MIGAC et DAF. 
Les informations suivantes sont analysées : 
Total des recettes d\'exploitation (Compte 70 Î° 75 + 7722 hors 7087) dont: 
- Recettes T2A 
- Recettes MIGAC 
    - dont MERRI (les missions Â« PHRC Â» [MIG_06] et Â« laboratoires d\'oncogénétique, de génétique moléculaire, de cytogénétique et de neurogénétique Â» [MIG_28] ne sont pas prises en compte) 
    - dont AC 
- Recettes DAF';
$locales['CHDIdentite-tresorerie'] = 'Trésorerie';
$locales['CHDIdentite-tresorerie-court'] = 'Trésorerie';
$locales['CHDIdentite-tresorerie-desc'] = 'FRNG-BFR= Trésorerie';
$locales['CHDIdentite.all'] = 'Toutes les identités HospiDiag';
$locales['CHDIdentite.none'] = 'Aucune identité HospiDiag';
$locales['CHDIdentite.one'] = 'Une identité HospiDiag';
$locales['CHDProcess'] = 'Process HospiDiag';
$locales['CHDProcess-annee'] = 'Année';
$locales['CHDProcess-annee-court'] = 'Année';
$locales['CHDProcess-annee-desc'] = 'Année';
$locales['CHDProcess-hd_etablissement_id'] = 'Etablissement';
$locales['CHDProcess-hd_etablissement_id-court'] = 'Etablissement';
$locales['CHDProcess-hd_etablissement_id-desc'] = 'Etablissement du process';
$locales['CHDProcess-hd_process_id'] = 'ID';
$locales['CHDProcess-hd_process_id-court'] = 'ID';
$locales['CHDProcess-hd_process_id-desc'] = 'Identifiant';
$locales['CHDProcess-indice_facturation'] = 'Indice de facturation';
$locales['CHDProcess-indice_facturation-court'] = 'Indice de facturation';
$locales['CHDProcess-indice_facturation-desc'] = 'L\'indicateur cherche Î° mesurer l\'efficacité de la chaine de facturation de l\'établissement, en mettant en évidence le manque Î° gagner lié Î° une facturation tardive des séjours Î° l\'Assurance Maladie';
$locales['CHDProcess-ip_dms_chir'] = 'IP - DMS Chirurgie (hors ambulatoire)';
$locales['CHDProcess-ip_dms_chir-court'] = 'DMS chirurgie';
$locales['CHDProcess-ip_dms_chir-desc'] = 'Cet indicateur compare la DMS de chirurgie de l\'établissement Î° celle standardisée de son casemix auquel on applique les DMS de référence
de chaque GHM de chirurgie. 
Il synthétise ainsi la sur ou sous performance de l\'organisation de l\'activité de chirurgie, mesurée par la DMS, dans l\'établissement';
$locales['CHDProcess-ip_dms_med'] = 'IP - DMS Médecine (hors ambulatoire)';
$locales['CHDProcess-ip_dms_med-court'] = 'DMS médecine';
$locales['CHDProcess-ip_dms_med-desc'] = 'Cet indicateur compare la DMS de médecine de l\'établissement Î° celle standardisée de son casemix auquel on applique les DMS de référence de chaque GHM de médecine. 
Il synthétise ainsi la sur ou sous performance de l\'organisation médicale de l\'établissement en médecine (hors ambulatoire)';
$locales['CHDProcess-ip_dms_obs'] = 'IP - DMS Obstétrique (hors ambulatoire)';
$locales['CHDProcess-ip_dms_obs-court'] = 'DMS Obstétrique';
$locales['CHDProcess-ip_dms_obs-desc'] = 'Cet indicateur compare la DMS de l\'obstétrique de l\'établissement Î° celle standardisée de son case mix auquel on applique les DMS de
référence de chaque GHM d\'obstétrique. 
Il synthétise ainsi la sur ou sous performance de l\'organisation de l\'activité de l\'obstétrique dans l\'établissement';
$locales['CHDProcess-msg-create'] = 'Process HospiDiag créé';
$locales['CHDProcess-msg-delete'] = 'Process HospiDiag supprimé';
$locales['CHDProcess-msg-found'] = 'Process HospiDiag retrouvé';
$locales['CHDProcess-msg-modify'] = 'Process HospiDiag modifié';
$locales['CHDProcess-nb_examens_bio_par_technicien'] = 'Nombre d\'examens de biologie par technicien';
$locales['CHDProcess-nb_examens_bio_par_technicien-court'] = 'Nb examens bio/technicien';
$locales['CHDProcess-nb_examens_bio_par_technicien-desc'] = 'Cet indicateur évalue la productivité du personnel des laboratoires, Î° travers l\'activité de biologie';
$locales['CHDProcess-nb_icr_par_salle'] = 'ICR par salle d\'intervention chirurgicale';
$locales['CHDProcess-nb_icr_par_salle-court'] = 'ICR/salle';
$locales['CHDProcess-nb_icr_par_salle-desc'] = 'Cet indicateur reflÎ¸te l\'utilisation et donc, indirectement l\'organisation au niveau des blocs opératoires : il permet de mettre en évidence une sous ou sur utilisation des salles de blocs disponibles';
$locales['CHDProcess-niveau_prerequis_hopital_numerique'] = 'Niveau d\'atteinte des prérequis HÏ„pital Numérique';
$locales['CHDProcess-niveau_prerequis_hopital_numerique-court'] = 'Niveau prérequis HÏ„pital Numérique';
$locales['CHDProcess-niveau_prerequis_hopital_numerique-desc'] = 'Cet indicateur évalue la solidité des bases du systÎ¸me d\'information de l\'établissement de santé';
$locales['CHDProcess-pct_depenses_admin_logistique_technique'] = 'Poids des dépenses administratives, logistiques et techniques (y compris activité externalisée)';
$locales['CHDProcess-pct_depenses_admin_logistique_technique-court'] = 'Poids dépenses administratives, logistiques et techniques';
$locales['CHDProcess-pct_depenses_admin_logistique_technique-desc'] = 'Ce ratio global mesure le pourcentage des dépenses consacrées aux Â« frais généraux Â» de l\'établissement, compte tenu de ce qui est sous
traité ou produit pour le compte d\'autres structures. 
Il donne une premiÎ¸re mesure du poids des fonctions support de l\'établissement';
$locales['CHDProcess-poids_cout_personnel_med'] = 'CoÏ‹t du personnel médical des services cliniques rapporté aux recettes';
$locales['CHDProcess-poids_cout_personnel_med-court'] = 'CoÏ‹t personnel médical/recettes';
$locales['CHDProcess-poids_cout_personnel_med-desc'] = 'Cet indicateur permet de mesurer le poids des coÏ‹ts de personnel médical dans les recettes et ainsi la performance de l\'organisation
médicale';
$locales['CHDProcess-poids_cout_personnel_non_med'] = 'CoÏ‹t du personnel non médical des services cliniques rapporté aux recettes';
$locales['CHDProcess-poids_cout_personnel_non_med-court'] = 'CoÏ‹t personnel non médical/recettes';
$locales['CHDProcess-poids_cout_personnel_non_med-desc'] = 'Cet indicateur permet de mesurer le poids des coÏ‹ts de personnel soignant dans les recettes et ainsi la performance de l\'organisation
soignante';
$locales['CHDProcess-poids_cout_personnel_services_medico_technique'] = 'CoÏ‹t du personnel relatif aux services médico-techniques rapporté aux recettes';
$locales['CHDProcess-poids_cout_personnel_services_medico_technique-court'] = 'CoÏ‹t personnel services médico-techniques/recettes';
$locales['CHDProcess-poids_cout_personnel_services_medico_technique-desc'] = 'Cet indicateur permet de mesurer le poids des coÏ‹ts de personnel médico-technique dans les recettes et ainsi la performance de l\'organisation médico-technique.
La définition des plateaux médico-techniques retenue ici est celle des remontées Icare : laboratoire, imagerie, radiothérapie, bloc opératoire, explorations fonctionnelles. Ils n\'incluent pas en particulier les plateaux de rééducation';
$locales['CHDProcess-taux_cesarienne'] = 'Taux de césarienne';
$locales['CHDProcess-taux_cesarienne-court'] = 'Taux de césarienne';
$locales['CHDProcess-taux_cesarienne-desc'] = 'Cet indicateur met en évidence les pratiques médicales en obstétrique';
$locales['CHDProcess-taux_chir_ambu'] = 'Taux de chirurgie ambulatoire';
$locales['CHDProcess-taux_chir_ambu-court'] = 'Taux de chirurgie ambu';
$locales['CHDProcess-taux_chir_ambu-desc'] = 'Cet indicateur mesure le dynamisme de l\'évolution des pratiques professionnelles chirurgicales v ers l\'ambulatoire (hors endoscopies) dans l\'établissement';
$locales['CHDProcess-taux_gestes_marqueurs_chir_ambu'] = 'Taux des 18 gestes marqueurs en chirurgie ambulatoire';
$locales['CHDProcess-taux_gestes_marqueurs_chir_ambu-court'] = 'Taux des gestes marqueurs en chirurgie ambu';
$locales['CHDProcess-taux_gestes_marqueurs_chir_ambu-desc'] = 'Cet indicateur mesure le dynamisme de l\'évolution des pratiques professionnelles chirurgicales v ers l\'ambulatoire, au travers de l\'analyse des 18 gestes marqueurs (1Â° niveau de substitution)';
$locales['CHDProcess-taux_peridurale'] = 'Taux de péridurale (accouchements par voie basse)';
$locales['CHDProcess-taux_peridurale-court'] = 'Taux de péridurale';
$locales['CHDProcess-taux_peridurale-desc'] = 'Cet indicateur met en évidence les pratiques médicales en obstétrique';
$locales['CHDProcess-taux_utilisation_places_chir_ambu'] = 'Taux d\'utilisation des places en chirurgie ambulatoire';
$locales['CHDProcess-taux_utilisation_places_chir_ambu-court'] = 'Taux d\'utilisation des places en chirurgie ambu';
$locales['CHDProcess-taux_utilisation_places_chir_ambu-desc'] = 'Cet indicateur reflÎ¸te l\'organisation au niveau de la chirurgie l\'ambulatoire : il permet de mettre en évidence une sur ou sous capacité d\'accueil des patients en chirurgie ambulatoire (hors endoscopies)';
$locales['CHDProcess-title-create'] = 'Créer un process HospiDiag';
$locales['CHDProcess-title-modify'] = 'Modifier un process HospiDiag';
$locales['CHDProcess.all'] = 'Tous les process HospiDiag';
$locales['CHDProcess.none'] = 'Aucun process HospiDiag';
$locales['CHDProcess.one'] = 'Un process HospiDiag';
$locales['CHDQualite'] = 'Qualité HospiDiag';
$locales['CHDQualite-annee'] = 'Année';
$locales['CHDQualite-annee-court'] = 'Année';
$locales['CHDQualite-annee-desc'] = 'Année';
$locales['CHDQualite-conformite_delais_envoie'] = 'Conformité du délai d\'envoi du courrier de fin d\'hospitalisation';
$locales['CHDQualite-conformite_delais_envoie-court'] = 'Conformité délai d\'envoi courrier de fin d\'hospi';
$locales['CHDQualite-conformite_delais_envoie-desc'] = 'Cet indicateur évalue le contenu du courrier de fin d\'hospitalisation et son délai d\'envoi';
$locales['CHDQualite-conformite_dossier_anesth'] = 'Score de conformité du dossier anesthésique';
$locales['CHDQualite-conformite_dossier_anesth-court'] = 'Conformité dossier anesthésie';
$locales['CHDQualite-conformite_dossier_anesth-desc'] = 'Cet indicateur évalue la tenue du dossier anesthésique, sur la base de 13 critÎ¸res';
$locales['CHDQualite-conformite_dossier_patient'] = 'Score de conformité du dossier patient';
$locales['CHDQualite-conformite_dossier_patient-court'] = 'Conformité dossier patient';
$locales['CHDQualite-conformite_dossier_patient-desc'] = 'Cet indicateur évalue la tenue et le contenu du dossier des patients hospitalisés, sur la base de 10 critÎ¸res au maximum';
$locales['CHDQualite-depistage_nutri'] = 'Dépistage des troubles nutritionnels';
$locales['CHDQualite-depistage_nutri-court'] = 'Dépistage troubles nutri';
$locales['CHDQualite-depistage_nutri-desc'] = 'Cet indicateur évalue le dépistage des troubles nutritionnels du patient adulte Î° l\'admission en médecine et en chirurgie';
$locales['CHDQualite-hd_etablissement_id'] = 'Etablissement';
$locales['CHDQualite-hd_etablissement_id-court'] = 'Etablissement';
$locales['CHDQualite-hd_etablissement_id-desc'] = 'Etablissement HospiDiag';
$locales['CHDQualite-hd_qualite_id'] = 'ID';
$locales['CHDQualite-hd_qualite_id-court'] = 'ID';
$locales['CHDQualite-hd_qualite_id-desc'] = 'Identifiant';
$locales['CHDQualite-msg-create'] = 'Qualité HospiDiag créé';
$locales['CHDQualite-msg-delete'] = 'Qualité HospiDiag supprimée';
$locales['CHDQualite-msg-found'] = 'Qualité HospiDiag retrouvé';
$locales['CHDQualite-msg-modify'] = 'Qualité HospiDiag modifié';
$locales['CHDQualite-niveau_certif'] = 'Niveau de certification';
$locales['CHDQualite-niveau_certif-court'] = 'Certification';
$locales['CHDQualite-niveau_certif-desc'] = 'Cet indicateur renseigne sur le niveau de certification de l\'établissement Î° l\'issue de la procédure de certification des établissements de santé mise en Âœuvre par la Haute Autorité de Santé';
$locales['CHDQualite-pep_bloc_op'] = 'Codage PEP Bloc Opératoire';
$locales['CHDQualite-pep_bloc_op-court'] = 'Codage PEP Bloc op';
$locales['CHDQualite-pep_bloc_op-desc'] = 'Le résultat présenté témoigne du niveau de conformité de l\'établissement au critÎ¸re 26a du manuel de certification HAS relatif au bloc opératoire. Il s\'agit d\'un critÎ¸re qualifié de pratique exigible prioritaire, c\'est-Î°-dire porteur d\'un niveau d\'exigence élevé en termes de qualité et de sécurité';
$locales['CHDQualite-pep_med'] = 'Codage PEP Médicaments';
$locales['CHDQualite-pep_med-court'] = 'Codage PEP Médicaments';
$locales['CHDQualite-pep_med-desc'] = 'Le résultat présenté témoigne du niveau de conformité de l\'établissement au critÎ¸re 20a du manuel de certification HAS relatif Î° la prise en charge médicamenteuse. Il s\'agit d\'un critÎ¸re qualifié de pratique exigible prioritaire, c\'est-Î°-dire porteur d\'un niveau d\'exigence élevé en termes de qualité et de sécurité';
$locales['CHDQualite-pep_urg'] = 'Codage PEP Urgences';
$locales['CHDQualite-pep_urg-court'] = 'Codage PEP Urgences';
$locales['CHDQualite-pep_urg-desc'] = 'Le résultat présenté témoigne du niveau de conformité de l\'établissement au critÎ¸re 25a du manuel de certification HAS relatif aux urgences. Il s\'agit d\'un critÎ¸re qualifié de pratique exigible prioritaire, c\'est-Î°-dire porteur d\'un niveau d\'exigence élevé en termes de qualité et de sécurité';
$locales['CHDQualite-rcp_cancer'] = 'Exhaustivité et modalités d\'organisation de la RCP en cancérologie';
$locales['CHDQualite-rcp_cancer-court'] = 'Organisation RCP cancérologie';
$locales['CHDQualite-rcp_cancer-desc'] = 'Cet indicateur évalue l\'exhaustivité et les modalités d\'organisation de la RCP lors de la prise en charge initiale d\'un patient atteint de cancer';
$locales['CHDQualite-score_naso'] = 'Score agrégé de lutte contre les infections nosocomiales';
$locales['CHDQualite-score_naso-court'] = 'Score lutte infections nosocomiales';
$locales['CHDQualite-score_naso-desc'] = 'Cet indicateur mesure les moyens mis en Âœuvre pour lutter contre les infections nosocomiales dans les établissements, Î° travers quatre indicateurs ; 
ICALIN : Indicateur Composite des Activités de Lutte contre les Infections Nosocomiales, ICSHA Indicateur de Consommation de Solutions Hydro-Alcooliques, ICATB Indice Composite de bon usage des AnTiBiotiques 
SURVISO indicateur de réalisation d\'une SURveillance des Infections du Site Opératoire. 
Le poids relatif des 4 indicateurs dans le score agrégé a été défini en prenant en compte l\'importance des acteurs et des secteurs d\'activité concernés, ainsi que l\'influence du domaine évalué sur la prévention des infections nosocomiales : ICALIN compte pour 40% dans le score agrégé, ICSHA pour 30%, ICATB pour 20% et SURVISO pour 10%';
$locales['CHDQualite-title-create'] = 'Créer une qualité HospiDiag';
$locales['CHDQualite-title-modify'] = 'Modifier une qualité HospiDiag';
$locales['CHDQualite-tracabilite_eval_douleur'] = 'TraÎ·abilité de l\'évaluation de la douleur';
$locales['CHDQualite-tracabilite_eval_douleur-court'] = 'TraÎ·abilité éval douleur';
$locales['CHDQualite-tracabilite_eval_douleur-desc'] = 'Cet indicateur mesure l\'évaluation de la douleur av ec une échelle dans le dossier patient';
$locales['CHDQualite.all'] = 'Toutes les qualités HospiDiag';
$locales['CHDQualite.none'] = 'Aucune qualité HospiDiag';
$locales['CHDQualite.one'] = 'Une qualité HospiDiag';
$locales['CHDResshum'] = 'Ressources humaines HospiDiag';
$locales['CHDResshum-annee'] = 'Année';
$locales['CHDResshum-annee-court'] = 'Année';
$locales['CHDResshum-annee-desc'] = 'Année';
$locales['CHDResshum-hd_etablissement_id'] = 'Etablissement';
$locales['CHDResshum-hd_etablissement_id-court'] = 'Etablissement';
$locales['CHDResshum-hd_etablissement_id-desc'] = 'Etablissement HospiDiag';
$locales['CHDResshum-hd_resshum_id'] = 'ID';
$locales['CHDResshum-hd_resshum_id-court'] = 'ID';
$locales['CHDResshum-hd_resshum_id-desc'] = 'Identifiant';
$locales['CHDResshum-interim_med'] = 'Interim Médical';
$locales['CHDResshum-interim_med-court'] = 'Interim médical';
$locales['CHDResshum-interim_med-desc'] = 'Cet indicateur permet de mesurer la difficulté de l\'établissement Î° pourvoir les postes de personnel médical.';
$locales['CHDResshum-msg-create'] = 'Ressource humaine HospiDiag créé';
$locales['CHDResshum-msg-delete'] = 'Ressource humaine HospiDiag supprimée';
$locales['CHDResshum-msg-found'] = 'Resshum HospiDiag retrouvé';
$locales['CHDResshum-msg-modify'] = 'Ressource humaine HospiDiag modifié';
$locales['CHDResshum-nb_accouchement_par_obs_sage_femme'] = 'Nombre d\'accouchements par obstetricien et sage-femme';
$locales['CHDResshum-nb_accouchement_par_obs_sage_femme-court'] = 'Nb accouchements/gynécologue';
$locales['CHDResshum-nb_accouchement_par_obs_sage_femme-desc'] = 'Cet indicateur mesure le nombre d\'accouchements réalisés par les équipes en obstétrique (obstétriciens et sages-femmes).';
$locales['CHDResshum-nb_iade_par_anesth'] = 'Nombre d\'IADE par anesthésiste';
$locales['CHDResshum-nb_iade_par_anesth-court'] = 'Nb IADE/anesthésiste';
$locales['CHDResshum-nb_iade_par_anesth-desc'] = 'Cet indicateur reflÎ¸te la structure des équipes d\'anesthésie dans les établissements publics et ESPIC';
$locales['CHDResshum-nb_icr_anesth_par_anesth'] = 'Nombre d\'ICR d\'anésthésie par anesthésiste et IADE';
$locales['CHDResshum-nb_icr_anesth_par_anesth-court'] = 'Nb ICR/anesth';
$locales['CHDResshum-nb_icr_anesth_par_anesth-desc'] = 'Cet indicateur mesure l\'activité chirurgicale par les équipes d\'anesthésie (anesthésiste et IADE).';
$locales['CHDResshum-nb_icr_par_chir'] = 'Nombre d\'ICR par chirurgien';
$locales['CHDResshum-nb_icr_par_chir-court'] = 'Nb ICR/chirurgien';
$locales['CHDResshum-nb_icr_par_chir-desc'] = 'Cet indicateur mesure l\'activité chirurgicale du personnel médical en chirurgie';
$locales['CHDResshum-nb_ide_as_par_cadre'] = 'Nombre d\'IDE+AS par cadre infirmier';
$locales['CHDResshum-nb_ide_as_par_cadre-court'] = 'Nb IDE+AD/cadre infirmier';
$locales['CHDResshum-nb_ide_as_par_cadre-desc'] = 'Cet indicateur donne une idée de la structure des équipes de cadres.  Il permet de détecter un potentiel sureffectif en cadres.';
$locales['CHDResshum-nb_sage_femme_par_obs'] = 'Nombre de sages-femmes par obstétricien';
$locales['CHDResshum-nb_sage_femme_par_obs-court'] = 'Nb sage-femme/gynécologue';
$locales['CHDResshum-nb_sage_femme_par_obs-desc'] = 'Cet indicateur reflÎ¸te la structure des équipes d\'obstétrique dans les établissements publics et espic';
$locales['CHDResshum-taux_absenteisme_pnm'] = 'Taux d\'absentéisme du PNM';
$locales['CHDResshum-taux_absenteisme_pnm-court'] = 'Nb moyenne heures travaillées (non médical)';
$locales['CHDResshum-taux_absenteisme_pnm-desc'] = 'Cet indicateur mesure la durée moyenne du temps de travail du personnel non médical dans l\'établissement';
$locales['CHDResshum-title-create'] = 'Créer une ressource humaine HospiDiag';
$locales['CHDResshum-title-modify'] = 'Modifier une ressource humaine HospiDiag';
$locales['CHDResshum-turn_over_global'] = 'Turn-over global';
$locales['CHDResshum-turn_over_global-court'] = 'Turn-over PNM';
$locales['CHDResshum-turn_over_global-desc'] = 'Cet indicateur permet de mesurer la rotation du personnel non médical dans l\'établissement.  Le turn-over est également un levier d\'ajustement de la masse salariale, en cas de non remplacement du personnel sortant';
$locales['CHDResshum.all'] = 'Toutes les ressources humaines HospiDiag';
$locales['CHDResshum.none'] = 'Aucune ressource humain HospiDiag';
$locales['CHDResshum.one'] = 'Ressource humaine HospiDiag';
$locales['CImportConflict'] = 'Conflit d\'importation';
$locales['CImportConflict-audit'] = 'Audit';
$locales['CImportConflict-audit-court'] = 'Audit';
$locales['CImportConflict-audit-desc'] = 'Conflit d\'audit';
$locales['CImportConflict-conflict-choice'] = 'Action Î° effectuer';
$locales['CImportConflict-delete-audit|pl'] = 'Conflits d\'audit supprimés';
$locales['CImportConflict-delete-doublons'] = 'Vider les doublons';
$locales['CImportConflict-delete|pl'] = 'Conflits supprimés';
$locales['CImportConflict-display-audit-conflicts'] = 'Afficher les conflits de l\'audit';
$locales['CImportConflict-display-conflicts'] = 'Afficher les conflits de l\'importation';
$locales['CImportConflict-display-doublons'] = 'Afficher les doublons';
$locales['CImportConflict-field'] = 'Champ';
$locales['CImportConflict-field-court'] = 'Champ';
$locales['CImportConflict-field-desc'] = 'Champ du conflit';
$locales['CImportConflict-field-name'] = 'Nom du champs';
$locales['CImportConflict-file_version'] = 'Version du fichier';
$locales['CImportConflict-file_version-court'] = 'Version';
$locales['CImportConflict-file_version-desc'] = 'Version du fichier du conflit';
$locales['CImportConflict-handle'] = 'Gestion des conflits d\'importation';
$locales['CImportConflict-handle-audit'] = 'Gestion des conflits de l\'audit';
$locales['CImportConflict-handle-conflict'] = 'Gestion des conflits d\'importation';
$locales['CImportConflict-handle-conflicts'] = 'Gestion des conflits';
$locales['CImportConflict-handle-duplicates'] = 'Gestion des doublons de correspondants médicaux';
$locales['CImportConflict-import'] = 'Import';
$locales['CImportConflict-import-delete-conflicts'] = 'Supprimer tous les conflits';
$locales['CImportConflict-import-delete-conflicts-audit'] = 'Supprimer tous les conflits d\'audit';
$locales['CImportConflict-import_conflict_id'] = 'ID';
$locales['CImportConflict-import_conflict_id-court'] = 'ID';
$locales['CImportConflict-import_conflict_id-desc'] = 'Identifiant du conflit d\'importation';
$locales['CImportConflict-import_tag'] = 'Tag d\'importation';
$locales['CImportConflict-import_tag-court'] = 'Tag';
$locales['CImportConflict-import_tag-desc'] = 'Tag de l\'importation qui a créé le conflit';
$locales['CImportConflict-medecin conflict exists'] = 'Des conflits existent pour ce correspondant';
$locales['CImportConflict-medecin conflict exists|pl'] = 'Des conflits existent pour un ou plusieurs correspondants';
$locales['CImportConflict-msg-create'] = 'Conflit créé';
$locales['CImportConflict-msg-delete'] = 'Conflit supprimé';
$locales['CImportConflict-msg-modify'] = 'Conflit modifié';
$locales['CImportConflict-object_class'] = 'Contexte';
$locales['CImportConflict-object_class-court'] = 'Contexte';
$locales['CImportConflict-object_class-desc'] = 'Contexte';
$locales['CImportConflict-object_id'] = 'ID contexte';
$locales['CImportConflict-object_id-court'] = 'ID contexte';
$locales['CImportConflict-object_id-desc'] = 'Identifiant de l\'objet de contexte';
$locales['CImportConflict-praticien'] = 'Correspondant médical';
$locales['CImportConflict-praticien|pl'] = 'Correspondants médicaux';
$locales['CImportConflict-title-create'] = 'Création de conflits d\'importation';
$locales['CImportConflict-title-modify'] = 'Modification de conflits d\'importation';
$locales['CImportConflict-total-conflicts'] = 'Nombre de conflits';
$locales['CImportConflict-total-conflicts-audit'] = 'Nombre de conflits d\'audit';
$locales['CImportConflict-total-conflicts-audit medecins'] = 'Nombre de correspondants en conflit d\'audit';
$locales['CImportConflict-total-conflicts medecins'] = 'Nombre de correspondants en conflit';
$locales['CImportConflict-total-duplicates'] = 'Nombre de doublons';
$locales['CImportConflict-value'] = 'Valeur';
$locales['CImportConflict-value-court'] = 'Valeur';
$locales['CImportConflict-value-csv'] = 'Valeur du fichier RPPS';
$locales['CImportConflict-value-desc'] = 'Valeur du conflit';
$locales['CImportConflict-value-mediboard'] = 'Valeur actuelle';
$locales['CImportConflict.all'] = 'Tous les conflits';
$locales['CImportConflict.none'] = 'Aucun conflit';
$locales['CImportConflict.one'] = 'Conflit';
$locales['CImportImportConflict'] = 'Conflit d\'importation d\'un correspondant médical';
$locales['CMedecinImport'] = 'Importation de la base';
$locales['CMedecinImport.type.adeli'] = 'Adeli';
$locales['CMedecinImport.type.all'] = 'Tous';
$locales['CMedecinImport.type.rpps'] = 'Rpps';
$locales['CMedecinImport-audit'] = 'Audit de l\'import';
$locales['CMedecinImport-complete-tel'] = 'Completer le numéro de téléphone';
$locales['CMedecinImport-continue'] = 'Automatique';
$locales['CMedecinImport-count'] = 'Nombre Î° importer';
$locales['CMedecinImport-csv-file-get-last'] = 'Récupérer le dernier fichier';
$locales['CMedecinImport-doublon-key'] = 'Clé';
$locales['CMedecinImport-doublon-medecin|pl'] = 'Correspondants médicaux en doublon';
$locales['CMedecinImport-doublon.none'] = 'Aucun correspondant médical en doublon';
$locales['CMedecinImport-dry_run'] = 'Essai Î° blanc';
$locales['CMedecinImport-end'] = 'Importation terminée';
$locales['CMedecinImport-exists-conflict-nb'] = 'Nombre de correspondants médicaux déjÎ° existants en conflit : %d';
$locales['CMedecinImport-exists-nb'] = 'Nombre de correspondants médicaux déjÎ° existants : %d';
$locales['CMedecinImport-exists-ok-nb'] = 'Nombre de correspondants médicaux déjÎ° existants sans conflits : %d';
$locales['CMedecinImport-exists-unused-nb'] = 'Parmis les correspondants médicaux déjÎ° existants importés %d sont inutilisés';
$locales['CMedecinImport-exists-used-nb'] = 'Parmis les correspondants médicaux déjÎ° existants importés %d sont utilisés';
$locales['CMedecinImport-file-downloaded'] = 'Fichier RPPS téléchargé';
$locales['CMedecinImport-file-failed'] = 'Erreur lors du téléchargement du fichier RPPS';
$locales['CMedecinImport-file-ignore-always'] = 'Le fichier RPPS est toujours ignoré pour ce correspondant médical';
$locales['CMedecinImport-file-last-update'] = 'DerniÎ¸re mise Î° jour du fichier RPPS';
$locales['CMedecinImport-file-last-version'] = 'DerniÎ¸re version disponible';
$locales['CMedecinImport-file-title-diff'] = 'Nom de colonne attendu %s, nom de colonne présent %s';
$locales['CMedecinImport-file-unable-get-file'] = 'Impossible de récupérer les informations du fichier';
$locales['CMedecinImport-file-version'] = 'Version du fichier';
$locales['CMedecinImport-handle-all'] = 'Valider toute la page';
$locales['CMedecinImport-id.none'] = 'Pas d\'identifiant pour le correspondant médical';
$locales['CMedecinImport-ignore-rpps'] = 'Ignorer RPPS';
$locales['CMedecinImport-import-rpps-or-adeli'] = 'Type Î° importer';
$locales['CMedecinImport-last_id'] = 'Avancement de l\'import';
$locales['CMedecinImport-msg-reset offset ?'] = 'Voulez-vous vraiment recommencer au début du fichier ?';
$locales['CMedecinImport-nb_exists'] = 'Nombre de correspondants médicaux déjÎ° existants';
$locales['CMedecinImport-nb_exists_conflict'] = 'Nombre de conflits';
$locales['CMedecinImport-nb_exists_ok'] = 'Parmis les correspondants médicaux existants, nombre d\'importés sans conflits';
$locales['CMedecinImport-nb_exists_unused'] = '- n\'étant pas déclarés sur un patient';
$locales['CMedecinImport-nb_exists_used'] = '- étant déclarés sur un patient';
$locales['CMedecinImport-nb_news'] = 'Nombre de nouveaux correspondants médicaux';
$locales['CMedecinImport-nb_rpps_ignore'] = 'Nombre de correspondants médicaux pour lesquels le fichier a été ignoré';
$locales['CMedecinImport-nb_tel_error'] = 'Nombre de numéros de téléphones erronés';
$locales['CMedecinImport-new-nb'] = 'Nombre de nouveaux correspondants médicaux : %d';
$locales['CMedecinImport-no-csv-file'] = 'Le fichier d\'import RPPS est introuvable';
$locales['CMedecinImport-no-file'] = 'Imposible d\'ouvrir le fichier d\'import RPPS';
$locales['CMedecinImport-no-update'] = 'Importer uniquement les nouveaux correspondants';
$locales['CMedecinImport-ok'] = 'Correspondant médical mis Î° jour';
$locales['CMedecinImport-reset-stats'] = 'Réinitialiser les statistiques';
$locales['CMedecinImport-rpps-ignored-nb'] = 'Nombre de correspondants médicaux pour lesquels le fichier RPPS a été ignoré automatiquement : %d';
$locales['CMedecinImport-stats'] = 'Statistiques d\'importation';
$locales['CMedecinImport-stats-actual'] = 'En cours';
$locales['CMedecinImport-stats-total'] = 'Total';
$locales['CMedecinImport-titles-bad-number'] = 'Le nombre de colonnes du fichier CSV ne correspond pas au nombre attendu';
$locales['CMedecinImport-total_time'] = 'Durée';
$locales['CMedecinImport-update-error'] = 'Erreur lors de la mise Î° jour du correspondant médical.';
$locales['mod-dPopenData-tab-vw_etab_details'] = 'Détails de l\'établissement';
$locales['mod-dPopenData-tab-vw_field_details'] = 'Détails du champ';
$locales['mod-openData-auto'] = 'Automatique';
$locales['mod-openData-hd-data-no-calc'] = 'Donnée non calculée';
$locales['mod-openData-hd-data-no-conc'] = 'Non concerné';
$locales['mod-openData-hd-display-etab'] = 'Détails de l\'établissement';
$locales['mod-openData-hospiDiag-display-infos'] = 'Afficher les détails';
$locales['mod-openData-hospiDiage-ds.none'] = 'La source de données hospi_diag n\'est pas définie.';
$locales['mod-openData-import-all'] = 'Importer tout le fichier';
$locales['mod-openData-import-commune-france'] = 'Communes franÎ·aises';
$locales['mod-openData-import-commune-suisse'] = 'Communes suisses';
$locales['mod-openData-import-communes-2013'] = '2013';
$locales['mod-openData-import-communes-2014'] = '2014 (avec démographie)';
$locales['mod-openData-import-hd'] = 'Import de données Hospi Diag';
$locales['mod-openData-import-hd-annee'] = 'Année Î° importer';
$locales['mod-openData-import-line-%d-error-%s'] = 'Ligne %d : %s';
$locales['mod-openData-import-options'] = 'Options d\'import';
$locales['mod-openData-import-update'] = 'Mettre Î° jour les données';
$locales['mod-openData-start-at'] = 'Commencer Î°';
$locales['mod-openData-step'] = 'Pas';
$locales['mod-openData-tab-ajax_display_conflicts'] = 'Gestion des conflits';
$locales['mod-openData-tab-ajax_display_doublons'] = 'Afficher les doublons créés par l\'importation';
$locales['mod-openData-tab-ajax_get_medecin_file'] = 'Récupération du fichier RPPS';
$locales['mod-openData-tab-ajax_import_communes'] = 'Import des communes';
$locales['mod-openData-tab-ajax_import_medecins'] = 'Import des médecins';
$locales['mod-openData-tab-ajax_reset_doublons_medecin'] = 'Suppression des doublons';
$locales['mod-openData-tab-ajax_reset_stats'] = 'Remise Î° zéro des statistiques';
$locales['mod-openData-tab-ajax_search_hd_etab'] = 'Recherche d\'une établissement HospiDiag';
$locales['mod-openData-tab-ajax_search_medecin_conflict'] = 'Recherche de conflits d\'import de médecins';
$locales['mod-openData-tab-ajax_show_options'] = 'Afficher les options';
$locales['mod-openData-tab-ajax_update_stats'] = 'Mise Î° jour des statistiques';
$locales['mod-openData-tab-ajax_vw_import_medecins'] = 'Import des médecins';
$locales['mod-openData-tab-configure'] = 'Configurer';
$locales['mod-openData-tab-vw_etab_details'] = 'Détails de l\'établissement';
$locales['mod-openData-tab-vw_field_details'] = 'Détails du champ';
$locales['mod-openData-tab-vw_hd'] = 'Données Hospi Diag';
$locales['mod-openData-tab-vw_import_communes'] = 'Importer les communes';
$locales['mod-openData-version'] = 'Version du fichier Î° importer';
$locales['mod-openData-vw-details-identite-activite'] = 'Groupes d\'activité';
$locales['mod-openData-vw-details-identite-finances'] = 'Finances';
$locales['mod-openData-vw-details-identite-informatisation'] = 'Informatisation';
$locales['mod-openData-vw-details-identite-infrastructure'] = 'Infrastructure';
$locales['mod-openData-vw-details-identite-intervention'] = 'Interventions courantes';
$locales['mod-openData-vw-details-identite-rh'] = 'Ressources humaines';
$locales['mod-openData-vw-details-identite-volumetrie'] = 'Volumétries';
$locales['mod-openData-vw-hd-data'] = 'Visualisation des données';
$locales['mod-openData-vw-hd-identite'] = 'Carte d\'identité';
$locales['mod-openData-vw-perfs-activite'] = 'Indicateurs de performance d\'activité';
$locales['mod-openData-vw-perfs-finance'] = 'Indicateurs de performance des finances';
$locales['mod-openData-vw-perfs-orga'] = 'Indicateurs de performance d\'organisation';
$locales['mod-openData-vw-perfs-qualite'] = 'Indicateurs de performance de qualité des soins';
$locales['mod-openData-vw-perfs-rh'] = 'Indicateurs de performance des ressources humaines';
$locales['mod-openData-zip'] = 'Utiliser le zip plutÏ„t que de télécharger le fichier';
$locales['module-openData-court'] = 'Données libres';
$locales['module-openData-long'] = 'Données libres';
$locales['openData-Hd-action prepare database'] = 'Créer les tables Hospi Diag';
$locales['openData-msg-Hd activite create'] = 'Activite d\'établissement HospiDiag créé';
$locales['openData-msg-Hd activite zone create'] = 'Zone d\'activite d\'établissement HospiDiag créé';
$locales['openData-msg-Hd etab create'] = 'Etablissement HospiDiag créé';
$locales['openData-msg-Hd finances create'] = 'Finances d\'établissement HospiDiag créé';
$locales['openData-msg-Hd identite create'] = 'Identité d\'établissement HospiDiag créé';
$locales['openData-msg-Hd prepare database'] = 'Préparation de la base de données Hospi Diag';
$locales['openData-msg-Hd process create'] = 'Process d\'établissement HospiDiag créé';
$locales['openData-msg-Hd qualite create'] = 'Qualité d\'établissement HospiDiag créé';
$locales['openData-msg-Hd resshum create'] = 'Ressources humaines d\'établissement HospiDiag créé';
$locales['mod-openData-import-hd-finess'] = 'Import données de géolocalisation Finess';
$locales['mod-openData-import-geolocalisation'] = "Importer latitude/longitude";
