<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Cda;

use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Core\Module\CAbstractModuleCache;
use Ox\Mediboard\PlanningOp\CSejour;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Class CModuleCacheXDS
 * @package Ox\Interop\Xds
 */
class CModuleCacheXDS extends CAbstractModuleCache
{
    /** @var string */
    public $module = 'cda';

    /**
     * @inheritdoc
     * @throws CMbException|InvalidArgumentException
     */
    public function clearSpecialActions(): void
    {
        $repo_cda = new CCDARepository(null, new CSejour());
        if ($repo_cda->clearCache()) {
            CAppUI::stepAjax('CCDAFactory-msg-success deleted cache');
        }
    }
}
