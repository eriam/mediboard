<?php
$locales['BloodSalvage.quality-protocole']                            = 'Protocole qualité';
$locales['CBloodSalvage']                                             = 'Récupération Sanguine per-opératoire';
$locales['CBloodSalvage-_datetime']                                   = 'Date';
$locales['CBloodSalvage-_datetime-court']                             = 'Date';
$locales['CBloodSalvage-_datetime-desc']                              = 'Date';
$locales['CBloodSalvage-_recuperation_end']                           = 'Fin de récupération';
$locales['CBloodSalvage-_recuperation_end-court']                     = 'Fin de récupération';
$locales['CBloodSalvage-_recuperation_end-desc']                      = 'Fin de récupération';
$locales['CBloodSalvage-_recuperation_start']                         = 'Début de récupération';
$locales['CBloodSalvage-_recuperation_start-court']                   = 'Début de récupération';
$locales['CBloodSalvage-_recuperation_start-desc']                    = 'Début de récupération';
$locales['CBloodSalvage-_transfusion_end']                            = 'Fin retransfusion';
$locales['CBloodSalvage-_transfusion_end-court']                      = 'Fin retransfusion';
$locales['CBloodSalvage-_transfusion_end-desc']                       = 'Fin retransfusion';
$locales['CBloodSalvage-_transfusion_start']                          = 'Début retransfusion';
$locales['CBloodSalvage-_transfusion_start-court']                    = 'Début retransfusion';
$locales['CBloodSalvage-_transfusion_start-desc']                     = 'Début retransfusion';
$locales['CBloodSalvage-anticoagulant_cip']                           = 'Anticoagulant';
$locales['CBloodSalvage-anticoagulant_cip-court']                     = 'Anticoagulant';
$locales['CBloodSalvage-anticoagulant_cip-desc']                      = 'Anticoagulant utilisé pendant l\'intervention';
$locales['CBloodSalvage-blood_salvage_id']                            = 'Blood Salvage';
$locales['CBloodSalvage-blood_salvage_id-court']                      = 'Blood Salvage';
$locales['CBloodSalvage-blood_salvage_id-desc']                       = 'Identifiant de la procédure RSPO';
$locales['CBloodSalvage-cell_saver_id']                               = 'Cell Saver';
$locales['CBloodSalvage-cell_saver_id-court']                         = 'Cell Saver';
$locales['CBloodSalvage-cell_saver_id-desc']                          = 'Cell Saver';
$locales['CBloodSalvage-desc']                                        = 'Récupération Sanguine per-opératoire (RSPO)';
$locales['CBloodSalvage-hgb_patient']                                 = 'Hb du patient post-transfusion';
$locales['CBloodSalvage-hgb_patient-court']                           = 'Hb du patient post-transfusion';
$locales['CBloodSalvage-hgb_patient-desc']                            = 'Hb du patient post-transfusion';
$locales['CBloodSalvage-hgb_pocket']                                  = 'Hb de la poche';
$locales['CBloodSalvage-hgb_pocket-court']                            = 'Hb de la poche';
$locales['CBloodSalvage-hgb_pocket-desc']                             = 'Hb de la poche';
$locales['CBloodSalvage-msg-create']                                  = 'RSPO créé';
$locales['CBloodSalvage-msg-delete']                                  = 'RSPO supprimé';
$locales['CBloodSalvage-msg-modify']                                  = 'RSPO modifié';
$locales['CBloodSalvage-nurse_sspi']                                  = 'Personnel SSPI';
$locales['CBloodSalvage-nurse_sspi.all']                              = 'Liste IDE';
$locales['CBloodSalvage-nurse_sspi.report']                           = 'InfirmiÎ¸re';
$locales['CBloodSalvage-operation_id']                                = 'Intervention';
$locales['CBloodSalvage-operation_id-court']                          = 'Intervention';
$locales['CBloodSalvage-operation_id-desc']                           = 'Intervention';
$locales['CBloodSalvage-receive_kit']                                 = 'Kit de recueil';
$locales['CBloodSalvage-receive_kit_lot']                             = 'Lot';
$locales['CBloodSalvage-receive_kit_lot-court']                       = 'Lot';
$locales['CBloodSalvage-receive_kit_lot-desc']                        = 'Lot du kit de recueil';
$locales['CBloodSalvage-receive_kit_ref']                             = 'Kit de recueil: Réf.';
$locales['CBloodSalvage-receive_kit_ref-court']                       = 'Kit de recueil';
$locales['CBloodSalvage-receive_kit_ref-desc']                        = 'Référence du kit de recueil';
$locales['CBloodSalvage-recuperation_end']                            = 'Fin de récupération';
$locales['CBloodSalvage-recuperation_end-court']                      = 'Fin de récupération';
$locales['CBloodSalvage-recuperation_end-desc']                       = 'Fin de récupération';
$locales['CBloodSalvage-recuperation_start']                          = 'Début de récupération';
$locales['CBloodSalvage-recuperation_start-court']                    = 'Début de récupération';
$locales['CBloodSalvage-recuperation_start-desc']                     = 'Début de récupération';
$locales['CBloodSalvage-sample']                                      = 'Prélévement';
$locales['CBloodSalvage-sample-court']                                = 'Prélévément sanguin';
$locales['CBloodSalvage-sample-desc']                                 = 'Prélévement sanguin pour suivi qualité';
$locales['CBloodSalvage-saved_volume']                                = 'Volume sanguin récupéré';
$locales['CBloodSalvage-saved_volume-court']                          = 'Volume sanguin récupéré';
$locales['CBloodSalvage-saved_volume-desc']                           = 'Volume sanguin récupéré';
$locales['CBloodSalvage-title-create']                                = 'Inscription au protocole RSPO';
$locales['CBloodSalvage-title-modify']                                = 'Modification du protocole RSPO';
$locales['CBloodSalvage-transfused_volume']                           = 'Volume retransfusé';
$locales['CBloodSalvage-transfused_volume-court']                     = 'Volume retransfusé';
$locales['CBloodSalvage-transfused_volume-desc']                      = 'Volume retransfusé';
$locales['CBloodSalvage-transfusion_end']                             = 'Fin de transfusion';
$locales['CBloodSalvage-transfusion_end-court']                       = 'Fin de transfusion';
$locales['CBloodSalvage-transfusion_end-desc']                        = 'Fin de transfusion';
$locales['CBloodSalvage-transfusion_start']                           = 'Début de transfusion';
$locales['CBloodSalvage-transfusion_start-court']                     = 'Début de transfusion';
$locales['CBloodSalvage-transfusion_start-desc']                      = 'Début de transfusion';
$locales['CBloodSalvage-type_ei_id']                                  = 'ID';
$locales['CBloodSalvage-type_ei_id-court']                            = 'Incident transfusionnel';
$locales['CBloodSalvage-type_ei_id-desc']                             = 'Type d\'incident transfusionnel';
$locales['CBloodSalvage-wash_kit']                                    = 'Kit de lavage';
$locales['CBloodSalvage-wash_kit_lot']                                = 'nÂ° de lot du kit utilisé';
$locales['CBloodSalvage-wash_kit_lot-court']                          = 'Lot';
$locales['CBloodSalvage-wash_kit_lot-desc']                           = 'Numero de lot du kit utilisé';
$locales['CBloodSalvage-wash_kit_ref']                                = 'Kit de lavage: RÎ¸f.';
$locales['CBloodSalvage-wash_kit_ref-court']                          = 'Kit de lavage';
$locales['CBloodSalvage-wash_kit_ref-desc']                           = 'Référence du kit utilisé';
$locales['CBloodSalvage-wash_volume']                                 = 'Volume de lavage';
$locales['CBloodSalvage-wash_volume-court']                           = 'Volume de lavage';
$locales['CBloodSalvage-wash_volume-desc']                            = 'Volume de lavage';
$locales['CBloodSalvage.all']                                         = 'Toutes les RSPO';
$locales['CBloodSalvage.anesthesia']                                  = 'Anesthésie pratiquée';
$locales['CBloodSalvage.anesthesist']                                 = 'Anesthésiste';
$locales['CBloodSalvage.none']                                        = 'Pas de récupération sanguine';
$locales['CBloodSalvage.one']                                         = 'Récupération Sanguine';
$locales['CBloodSalvage.operations']                                  = 'Détails de l\'intervention';
$locales['CBloodSalvage.report']                                      = 'Compte-rendu';
$locales['CBloodSalvage.report-long']                                 = 'Compte-rendu auto transfusion per-opératoire';
$locales['CBloodSalvage.sample.non']                                  = 'Non prescrit';
$locales['CBloodSalvage.sample.prel']                                 = 'Prélévé';
$locales['CBloodSalvage.sample.trans']                                = 'Prélévé et transmis';
$locales['CBloodSalvage.select']                                      = 'Une RSPO';
$locales['CBloodSalvage.timers']                                      = 'Chronos de l\'intervention';
$locales['CBloodSalvage.volumes']                                     = 'Volumes';
$locales['CCellSaver']                                                = 'Cell Saver';
$locales['CCellSaver-back-blood_salvages']                            = 'RSPO utilisant ce Cell saver';
$locales['CCellSaver-back-blood_salvages.empty']                      = 'Aucun Cell Saver';
$locales['CCellSaver-cell_saver_id']                                  = 'Cell Saver ID';
$locales['CCellSaver-cell_saver_id-court']                            = 'Cell Saver ID';
$locales['CCellSaver-cell_saver_id-desc']                             = 'Cell Saver ID';
$locales['CCellSaver-marque']                                         = 'Marque';
$locales['CCellSaver-marque-court']                                   = 'Marque';
$locales['CCellSaver-marque-desc']                                    = 'Marque';
$locales['CCellSaver-modele']                                         = 'ModÎ¸le';
$locales['CCellSaver-modele-court']                                   = 'ModÎ¸le';
$locales['CCellSaver-modele-desc']                                    = 'ModÎ¸le';
$locales['CCellSaver-msg-create']                                     = 'Cell saver créé';
$locales['CCellSaver-msg-delete']                                     = 'Cell saver supprimé';
$locales['CCellSaver-msg-modify']                                     = 'Cell saver modifié';
$locales['CCellSaver-title-create']                                   = 'Création d\'un cell saver';
$locales['CCellSaver-title-modify']                                   = 'Modification du cell saver';
$locales['CCellSaver.all']                                            = 'Tous les Cell Saver';
$locales['CCellSaver.create']                                         = 'Ajouter un Cell Saver';
$locales['CCellSaver.marque']                                         = 'Marque';
$locales['CCellSaver.modele']                                         = 'ModÎ¸le';
$locales['CCellSaver.modify']                                         = 'Modifier le Cell Saver';
$locales['CCellSaver.name']                                           = 'Cell Saver';
$locales['CCellSaver.none']                                           = 'Aucun Cell saver';
$locales['CCellSaver.one']                                            = 'Cell Saver';
$locales['CCellSaver.select']                                         = 'Choisissez un Cell Saver';
$locales['COperation-back-blood_salvages']                            = 'Cell Savers de l\'intervention';
$locales['COperation-back-blood_salvages.empty']                      = 'Aucun Cell Saver';
$locales['CTypeEi']                                                   = 'ModÎ¸les de fiche';
$locales['CTypeEi-back-blood_salvages']                               = 'Cell Savers';
$locales['CTypeEi-back-blood_salvages.empty']                         = 'Aucun Cell Saver';
$locales['CTypeEi-concerne']                                          = 'Concerne';
$locales['CTypeEi-concerne-court']                                    = 'Concerne';
$locales['CTypeEi-concerne-desc']                                     = 'Concerne';
$locales['CTypeEi-desc']                                              = 'Description';
$locales['CTypeEi-desc-court']                                        = 'Description';
$locales['CTypeEi-desc-desc']                                         = 'Description';
$locales['CTypeEi-evenements']                                        = 'Catégorie d\'incident';
$locales['CTypeEi-evenements-court']                                  = 'Catégorie d\'incident';
$locales['CTypeEi-evenements-desc']                                   = 'Catégorie d\'incident';
$locales['CTypeEi-msg-create']                                        = 'ModÎ¸le créé';
$locales['CTypeEi-msg-delete']                                        = 'ModÎ¸le supprimé';
$locales['CTypeEi-msg-modify']                                        = 'ModÎ¸le modifié';
$locales['CTypeEi-name']                                              = 'Nom';
$locales['CTypeEi-name-court']                                        = 'Nom';
$locales['CTypeEi-name-desc']                                         = 'Nom';
$locales['CTypeEi-title-create']                                      = 'Création d\'un nouveau modÎ¸le';
$locales['CTypeEi-title-modify']                                      = 'Modification du modÎ¸le';
$locales['CTypeEi-type_ei_id']                                        = 'TypeEI ID';
$locales['CTypeEi-type_ei_id-court']                                  = 'TypeEI ID';
$locales['CTypeEi-type_ei_id-desc']                                   = 'Incident transfusionnel';
$locales['CTypeEi-type_signalement']                                  = 'Type de signalement';
$locales['CTypeEi-type_signalement-court']                            = 'Type de signalement';
$locales['CTypeEi-type_signalement-desc']                             = 'Type de signalement';
$locales['CTypeEi.all']                                               = 'Tous les modÎ¸les de fiche';
$locales['CTypeEi.concerne.mat']                                      = 'Un Matériel';
$locales['CTypeEi.concerne.med']                                      = 'Un Medicament';
$locales['CTypeEi.concerne.pat']                                      = 'Un Patient';
$locales['CTypeEi.concerne.pers']                                     = 'Un Membre du Personnel';
$locales['CTypeEi.concerne.vis']                                      = 'Un Visiteur';
$locales['CTypeEi.create']                                            = 'Créer un modÎ¸le de fiche';
$locales['CTypeEi.modify']                                            = 'Modifier le modÎ¸le de fiche';
$locales['CTypeEi.none']                                              = 'Pas de modÎ¸le';
$locales['CTypeEi.one']                                               = 'ModÎ¸le de fiche';
$locales['CTypeEi.select']                                            = 'Choisissez un modÎ¸le de fiche';
$locales['CTypeEi.type_signalement.inc']                              = 'Un incident';
$locales['CTypeEi.type_signalement.ris']                              = 'Un risque d\'incident';
$locales['config-bloodSalvage']                                       = 'Configuration de la procédure de récupération sanguine';
$locales['config-bloodSalvage-AntiCoagulantList']                     = 'Liste des anticoagulants utilisables';
$locales['config-bloodSalvage-AntiCoagulantList-desc']                = 'Liste des anticoagulants utilisables';
$locales['config-bloodSalvage-inLivretTherapeutique']                 = 'Anticoagulants du livret thérapeutique uniquement';
$locales['config-bloodSalvage-inLivretTherapeutique-desc']            = 'Anticoagulants du livret thérapeutique uniquement';
$locales['mod-bloodSalvage-tab-configure']                            = 'Configuration';
$locales['mod-bloodSalvage-tab-httpreq_json_stats']                   = 'Statistiques';
$locales['mod-bloodSalvage-tab-httpreq_liste_op_prat']                = 'Liste des interventions';
$locales['mod-bloodSalvage-tab-httpreq_liste_patients_bs']            = 'Liste des Cell Saver';
$locales['mod-bloodSalvage-tab-httpreq_liste_plages']                 = 'Liste des plages';
$locales['mod-bloodSalvage-tab-httpreq_total_time']                   = 'Temps total';
$locales['mod-bloodSalvage-tab-httpreq_vw_bloodSalvage']              = 'Cell Saver';
$locales['mod-bloodSalvage-tab-httpreq_vw_bloodSalvage_volumes']      = 'Volumes des Cell Saver';
$locales['mod-bloodSalvage-tab-httpreq_vw_blood_salvage_personnel']   = 'Personnel';
$locales['mod-bloodSalvage-tab-httpreq_vw_bs_sspi_timing']            = 'Timing SSPI';
$locales['mod-bloodSalvage-tab-httpreq_vw_recuperation_start_timing'] = 'Timing début de récupération';
$locales['mod-bloodSalvage-tab-httpreq_vw_sspi_bs']                   = 'Info Cell Saver';
$locales['mod-bloodSalvage-tab-inc_personnel']                        = 'Personnel';
$locales['mod-bloodSalvage-tab-print_rapport']                        = 'Impression du rapport';
$locales['mod-bloodSalvage-tab-vw_bloodSalvage']                      = 'Cell Saver';
$locales['mod-bloodSalvage-tab-vw_bloodSalvage-desc']                 = 'Cell Saver';
$locales['mod-bloodSalvage-tab-vw_bloodSalvage_sspi']                 = 'Salle de reveil';
$locales['mod-bloodSalvage-tab-vw_bloodSalvage_sspi-desc']            = 'Salle de reveil';
$locales['mod-bloodSalvage-tab-vw_cellSaver']                         = 'Gestion des Cell Saver';
$locales['mod-bloodSalvage-tab-vw_stats']                             = 'Statistiques';
$locales['mod-bloodSalvage-tab-vw_typeEi_manager']                    = 'Gestion des incidents';
$locales['module-bloodSalvage-court']                                 = 'Récupération Sanguine';
$locales['module-bloodSalvage-long']                                  = 'Récupération sanguine per-opératoire';
$locales['msg-CBloodSalvage-create']                                  = 'RSPO créé';
$locales['msg-CBloodSalvage-delete']                                  = 'RSPO supprimé';
$locales['msg-CBloodSalvage-modify']                                  = 'RSPO modifié';
$locales['msg-CCellSaver-create']                                     = 'Cell saver créé';
$locales['msg-CCellSaver-delete']                                     = 'Cell saver supprimé';
$locales['msg-CCellSaver-modify']                                     = 'Cell saver modifié';
$locales['msg-CTypeEi-create']                                        = 'Fiche créée';
$locales['msg-CTypeEi-delete']                                        = 'Fiche supprimée';
$locales['msg-CTypeEi-modify']                                        = 'Fiche modifiée';
