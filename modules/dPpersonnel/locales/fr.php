<?php
$locales['CAffectationPersonnel'] = 'Affectation personnel';
$locales['CAffectationPersonnel-_debut'] = 'Début';
$locales['CAffectationPersonnel-_debut-court'] = 'Déb';
$locales['CAffectationPersonnel-_debut-desc'] = 'Début de l\'affectation';
$locales['CAffectationPersonnel-_debut_dt'] = 'Début';
$locales['CAffectationPersonnel-_debut_dt-court'] = 'Déb';
$locales['CAffectationPersonnel-_debut_dt-desc'] = 'Début de l\'affectation';
$locales['CAffectationPersonnel-_fin'] = 'Fin';
$locales['CAffectationPersonnel-_fin-court'] = 'Fin';
$locales['CAffectationPersonnel-_fin-desc'] = 'Fin de l\'affectation';
$locales['CAffectationPersonnel-_fin_dt'] = 'Fin';
$locales['CAffectationPersonnel-_fin_dt-court'] = 'Fin';
$locales['CAffectationPersonnel-_fin_dt-desc'] = 'Fin de l\'affectation';
$locales['CAffectationPersonnel-affect_id'] = 'ID';
$locales['CAffectationPersonnel-affect_id-court'] = 'ID';
$locales['CAffectationPersonnel-affect_id-desc'] = 'Identifiant d\'affectation';
$locales['CAffectationPersonnel-back-affectation_child'] = 'Affectations';
$locales['CAffectationPersonnel-back-affectation_child.empty'] = 'Aucune affectation';
$locales['CAffectationPersonnel-debut'] = 'Début';
$locales['CAffectationPersonnel-debut-court'] = 'Déb';
$locales['CAffectationPersonnel-debut-desc'] = 'Début réel de l\'affectation';
$locales['CAffectationPersonnel-failed-unique'] = 'Le personnel ne peut pas Îºtre affecté plusieurs fois Î° la mÎºme cible';
$locales['CAffectationPersonnel-fin'] = 'Fin';
$locales['CAffectationPersonnel-fin-court'] = 'Fin';
$locales['CAffectationPersonnel-fin-desc'] = 'Fin réel de l\'affectation';
$locales['CAffectationPersonnel-msg-create'] = 'Affectation créée';
$locales['CAffectationPersonnel-msg-delete'] = 'Affectation supprimée';
$locales['CAffectationPersonnel-msg-modify'] = 'Affectation modifiée';
$locales['CAffectationPersonnel-object_class'] = 'Type';
$locales['CAffectationPersonnel-object_class-court'] = 'Type';
$locales['CAffectationPersonnel-object_class-desc'] = 'Type de cible';
$locales['CAffectationPersonnel-object_id'] = 'Cible';
$locales['CAffectationPersonnel-object_id-court'] = 'Cible';
$locales['CAffectationPersonnel-object_id-desc'] = 'Cible de l\'affectation';
$locales['CAffectationPersonnel-parent_affectation_id'] = 'Affectation parente';
$locales['CAffectationPersonnel-parent_affectation_id-court'] = 'Affectation parente';
$locales['CAffectationPersonnel-parent_affectation_id-desc'] = 'Affectation parente';
$locales['CAffectationPersonnel-personnel_id'] = 'Personnel';
$locales['CAffectationPersonnel-personnel_id-court'] = 'Pers.';
$locales['CAffectationPersonnel-personnel_id-desc'] = 'Personnel concerné';
$locales['CAffectationPersonnel-realise'] = 'Réalisée';
$locales['CAffectationPersonnel-realise-court'] = 'Réel';
$locales['CAffectationPersonnel-realise-desc'] = 'Affectation réalisée';
$locales['CAffectationPersonnel-title-create'] = 'Création d\'une affectation de personnel';
$locales['CAffectationPersonnel-title-modify'] = 'Modification de l\'affectation';
$locales['CAffectationPersonnel-user_id'] = 'Personnel';
$locales['CAffectationPersonnel.all'] = 'Toutes les affectations de personnel';
$locales['CAffectationPersonnel.more'] = 'Des affectations de personnel';
$locales['CAffectationPersonnel.none'] = 'Pas d\'affectation de personnel';
$locales['CAffectationPersonnel.object_class.'] = '-';
$locales['CAffectationPersonnel.object_class.CBloodSalvage'] = 'Récupération sanguine';
$locales['CAffectationPersonnel.object_class.COperation'] = 'Intervention';
$locales['CAffectationPersonnel.object_class.CPlageOp'] = 'Plage opératoire';
$locales['CAffectationPersonnel.one'] = 'Une affectation de personnel';
$locales['CAffectationPersonnel.select'] = 'Choisir une affectation';
$locales['CMbObject-back-affectations_personnel'] = 'Affectations de personnel';
$locales['CMbObject-back-affectations_personnel.empty'] = 'Aucune affectation de personnel';
$locales['CMediusers-back-personnels'] = 'RÏ„les de personnel';
$locales['CMediusers-back-personnels.empty'] = 'Aucun rÏ„le';
$locales['CMediusers-back-plages_conge'] = 'Plages de congés ';
$locales['CMediusers-back-plages_conge.empty'] = 'Aucun congés';
$locales['CMediusers-back-remplacements'] = 'Remplacements';
$locales['CMediusers-back-remplacements.empty'] = 'Aucun remplacement';
$locales['CMediusers-back-user_remplacant'] = 'RemplaÎ·ant';
$locales['CMediusers-back-user_remplacant.empty'] = 'Pas de remplaÎ·ant';
$locales['CMediusers-back-user_remplace'] = 'Utilisateur remplacé';
$locales['CMediusers-back-user_remplace.empty'] = 'Utilisateur non remplacé';
$locales['CPersonnel'] = 'Personnel';
$locales['CPersonnel-Added staff'] = 'Personnel ajouté';
$locales['CPersonnel-Planned staff'] = 'Personnel prévu';
$locales['CPersonnel-Staff in the room'] = 'Personnel en salle';
$locales['CPersonnel-Unknown groomer'] = 'Panseur indéfini';
$locales['CPersonnel-_user_first_name'] = 'Prénom';
$locales['CPersonnel-_user_first_name-court'] = 'Prénom';
$locales['CPersonnel-_user_first_name-desc'] = 'Prénom de la personne';
$locales['CPersonnel-_user_last_name'] = 'Nom';
$locales['CPersonnel-_user_last_name-court'] = 'Nom';
$locales['CPersonnel-_user_last_name-desc'] = 'Nom de la personne';
$locales['CPersonnel-actif'] = 'Actif';
$locales['CPersonnel-actif-court'] = 'Actif';
$locales['CPersonnel-actif-desc'] = 'Actif, si le rÏ„le est toujours joué par cette personne';
$locales['CPersonnel-back-affectations'] = 'Affectations';
$locales['CPersonnel-back-affectations.empty'] = 'Aucun personnel concerné';
$locales['CPersonnel-emplacement'] = 'Type';
$locales['CPersonnel-emplacement-court'] = 'Type';
$locales['CPersonnel-emplacement-desc'] = 'Type de rÏ„le';
$locales['CPersonnel-msg-This person does not have the rights to be created in this establishment'] = 'Cette personne n\'a pas les droits pour Îºtre créée dans cet établissement';
$locales['CPersonnel-msg-create'] = 'Personnel créé';
$locales['CPersonnel-msg-create-multiple'] = '%d personnels créés';
$locales['CPersonnel-msg-delete'] = 'Personnel supprimé';
$locales['CPersonnel-msg-errors-multiple'] = '%d personnels en erreur';
$locales['CPersonnel-msg-modify'] = 'Personnel modifié';
$locales['CPersonnel-msg-modify-multiple'] = '%d personnels modifiés';
$locales['CPersonnel-personnel_id'] = 'Identifiant';
$locales['CPersonnel-personnel_id-court'] = 'Id';
$locales['CPersonnel-personnel_id-desc'] = 'Identifiant interne Mediboard';
$locales['CPersonnel-planned staff'] = 'personnel prévu';
$locales['CPersonnel-real staff'] = 'personnel réel';
$locales['CPersonnel-title-create'] = 'Création de personnel';
$locales['CPersonnel-title-create-multiple'] = 'Création multiple';
$locales['CPersonnel-title-modify'] = 'Modification du personnel';
$locales['CPersonnel-user_id'] = 'Personne';
$locales['CPersonnel-user_id-court'] = 'Pers.';
$locales['CPersonnel-user_id-desc'] = 'Personne physique (utilisateur)';
$locales['CPersonnel.all'] = 'Tous le personnel';
$locales['CPersonnel.emplacement.aide_soignant'] = 'Aide soignant';
$locales['CPersonnel.emplacement.aux_puericulture'] = 'Auxiliaire de puériculture';
$locales['CPersonnel.emplacement.brancardier'] = 'Brancardier';
$locales['CPersonnel.emplacement.circulante'] = 'Circulante';
$locales['CPersonnel.emplacement.iade'] = 'IADE';
$locales['CPersonnel.emplacement.instrumentiste'] = 'Instrumentiste';
$locales['CPersonnel.emplacement.manipulateur'] = 'Manipulateur';
$locales['CPersonnel.emplacement.op'] = 'Aide opératoire';
$locales['CPersonnel.emplacement.op_panseuse'] = 'Panseur';
$locales['CPersonnel.emplacement.reveil'] = 'Reveil';
$locales['CPersonnel.emplacement.sagefemme'] = 'Sage femme';
$locales['CPersonnel.emplacement.service'] = 'Service';
$locales['CPersonnel.none'] = 'Pas de personnel';
$locales['CPersonnel.one'] = 'Un membre du personnel';
$locales['CPersonnel.select'] = 'Choisir';
$locales['CPersonnel|pl'] = 'Personnels';
$locales['CPlageConge'] = 'Plages de congés ';
$locales['CPlageConge-_date_max'] = 'Date max.';
$locales['CPlageConge-_date_max-court'] = 'Date max.';
$locales['CPlageConge-_date_max-desc'] = 'Date max.';
$locales['CPlageConge-_date_min'] = 'Date min.';
$locales['CPlageConge-_date_min-court'] = 'Date min.';
$locales['CPlageConge-_date_min-desc'] = 'Date min.';
$locales['CPlageConge-_duree'] = 'Durée du congés';
$locales['CPlageConge-_duree-court'] = 'Durée';
$locales['CPlageConge-_duree-desc'] = 'Durée de la plage de congés';
$locales['CPlageConge-action-Create holiday|pl'] = 'Créer congés';
$locales['CPlageConge-choix-periode'] = 'Affichage par ';
$locales['CPlageConge-conflit'] = 'Conflict with the plage %s';
$locales['CPlageConge-conflit %s'] = 'conflit avec la %s';
$locales['CPlageConge-corresponding'] = 'Plages de congés correspondantes';
$locales['CPlageConge-create'] = 'Création d\'une plage';
$locales['CPlageConge-date_debut'] = 'Date de debut';
$locales['CPlageConge-date_debut-court'] = 'Date début';
$locales['CPlageConge-date_debut-desc'] = 'Date de début de la plage de congés ';
$locales['CPlageConge-date_fin'] = 'Date de fin';
$locales['CPlageConge-date_fin-court'] = 'Date fin';
$locales['CPlageConge-date_fin-desc'] = 'Date de fin de la plage de congés ';
$locales['CPlageConge-from'] = ' du ';
$locales['CPlageConge-libelle'] = 'Libellé';
$locales['CPlageConge-libelle-court'] = 'Libellé';
$locales['CPlageConge-libelle-desc'] = 'de la plage de congés ';
$locales['CPlageConge-list'] = 'Nombre de plages de congés par utilisateur';
$locales['CPlageConge-msg-create'] = 'Plage de congés créée';
$locales['CPlageConge-msg-delete'] = 'Plage de congés supprimée';
$locales['CPlageConge-msg-modify'] = 'Plage de congés modifiée';
$locales['CPlageConge-pct_retrocession'] = 'Pourcentage de rétrocession';
$locales['CPlageConge-pct_retrocession-court'] = 'Pourcentage de rétrocession';
$locales['CPlageConge-pct_retrocession-desc'] = 'Pourcentage de rétrocession accordé au remplaÎ·ant';
$locales['CPlageConge-plage_id'] = 'Id plage congé';
$locales['CPlageConge-plage_id-court'] = 'Id plage conge';
$locales['CPlageConge-plage_id-desc'] = 'Id de la plage de congé';
$locales['CPlageConge-replacer_id'] = 'RemplaÎ·ant';
$locales['CPlageConge-replacer_id-court'] = 'RemplaÎ·ant';
$locales['CPlageConge-replacer_id-desc'] = 'RemplaÎ·ant durant la période';
$locales['CPlageConge-searchbydate'] = 'Recherche par date';
$locales['CPlageConge-title-create'] = 'Création d\'une plage de congés';
$locales['CPlageConge-title-modify'] = 'Modification de la  plage congés';
$locales['CPlageConge-to'] = ' au ';
$locales['CPlageConge-user-search'] = 'Recherche personnel';
$locales['CPlageConge-user_id'] = 'Utilisateur';
$locales['CPlageConge-user_id-court'] = 'Utilisateur';
$locales['CPlageConge-user_id-desc'] = 'Utilisateur concerné par la plage';
$locales['CPlageConge.all'] = 'Toutes les plages de congés ';
$locales['CPlageConge.none'] = 'Pas de plages de congés ';
$locales['CPlageConge.one'] = 'Plage de congés ';
$locales['CPlageConge|pl'] = 'CONGES';
$locales['CRemplacement'] = 'Remplacement';
$locales['CRemplacement-conflit %s'] = 'Conflit avec le remplacement %s';
$locales['CRemplacement-debut'] = 'Début';
$locales['CRemplacement-debut-court'] = 'Début';
$locales['CRemplacement-debut-desc'] = 'Début du remplacement';
$locales['CRemplacement-depassement_duree_max %s'] = 'Durée maximale de remplacement autorisée:  %s heure(s)';
$locales['CRemplacement-description'] = 'Description';
$locales['CRemplacement-description-court'] = 'Description';
$locales['CRemplacement-description-desc'] = 'Description';
$locales['CRemplacement-fin'] = 'Fin';
$locales['CRemplacement-fin-court'] = 'Fin';
$locales['CRemplacement-fin-desc'] = 'Fin du remplacement';
$locales['CRemplacement-libelle'] = 'Libellé';
$locales['CRemplacement-libelle-court'] = 'Libellé';
$locales['CRemplacement-libelle-desc'] = 'Libellé';
$locales['CRemplacement-msg-create'] = 'Remplacement créé';
$locales['CRemplacement-msg-delete'] = 'Remplacement supprimé';
$locales['CRemplacement-msg-modify'] = 'Remplacement modifé';
$locales['CRemplacement-msg-remplacant'] = 'remplaÎ·ant du';
$locales['CRemplacement-remplacant_id'] = 'RemplaÎ·ant';
$locales['CRemplacement-remplacant_id-court'] = 'RemplaÎ·ant';
$locales['CRemplacement-remplacant_id-desc'] = 'RemplaÎ·ant';
$locales['CRemplacement-remplace_id'] = 'Utilisateur remplacé';
$locales['CRemplacement-remplace_id-court'] = 'Remplacé';
$locales['CRemplacement-remplace_id-desc'] = 'Utilisateur remplacé';
$locales['CRemplacement-remplacement_id'] = 'Identifiant';
$locales['CRemplacement-remplacement_id-court'] = 'Identifiant';
$locales['CRemplacement-remplacement_id-desc'] = 'Identifiant';
$locales['CRemplacement-title-create'] = 'Créer un remplacement';
$locales['CRemplacement-title-modify'] = 'Modifier le remplacement';
$locales['CRemplacement.all'] = 'Tous les remplacement';
$locales['CRemplacement.hide_old'] = 'Masquer les terminés';
$locales['CRemplacement.hide_old.desc'] = 'Masquer les remplacements terminés';
$locales['CRemplacement.none'] = 'Pas de remplacement';
$locales['CRemplacement.one'] = 'Un remplacement';
$locales['CRemplacement|pl'] = 'Remplacements';
$locales['CStoredObject-back-affectations_personnel'] = 'Affectation de personnel';
$locales['CStoredObject-back-affectations_personnel.empty'] = 'Pas d\'affectation de personnel';
$locales['For'] = ' pour ';
$locales['From %s to %s'] = 'du %s au %s';
$locales['Next month'] = 'Mois suivant';
$locales['Next week'] = 'Semaine suivante';
$locales['Next year'] = 'Année suivante';
$locales['Plage %s from %s to %s'] = 'Plage %s du %s au %s';
$locales['Previous month'] = 'Mois précédent';
$locales['Previous week'] = 'Semaine précédente';
$locales['Previous year'] = 'Année précédente';
$locales['config-personnel-CPlageConge'] = 'Plage de congé';
$locales['config-personnel-CPlageConge-show_replacer'] = 'RemplaÎ·ant global';
$locales['config-personnel-CPlageConge-show_replacer-desc'] = 'Gérer le remplaÎ·ant globalement au niveau de la plage de congés';
$locales['config-personnel-CRemplacement'] = 'Remplacement';
$locales['config-personnel-CRemplacement-duree_max'] = 'Durée maximale de remplacement autorisée (heures)';
$locales['config-personnel-CRemplacement-duree_max-desc'] = 'Durée maximale de remplacement autorisée (en heures)';
$locales['config-personnel-global'] = 'Global';
$locales['config-personnel-global-see_retrocession'] = 'Afficher la rétrocession dans la plage de congé';
$locales['config-personnel-global-see_retrocession-desc'] = 'Afficher le champ de rétrocession dans la plage de congé';
$locales['mod-dPpersonnel-tab-ajax_affectations_multiple'] = 'Affectations multiples';
$locales['mod-dPpersonnel-tab-ajax_edit_personnel'] = 'Création / Modification de personnel';
$locales['mod-dPpersonnel-tab-ajax_edit_plage_conge'] = 'Formulaire de plage de congés';
$locales['mod-dPpersonnel-tab-ajax_list_personnel'] = 'Liste des personnels';
$locales['mod-dPpersonnel-tab-ajax_plage_conge'] = 'Listes de plages de congés';
$locales['mod-dPpersonnel-tab-ajax_planning'] = 'Planning des congés';
$locales['mod-dPpersonnel-tab-ajax_update_affectations'] = 'Mise Î° jour des affectations';
$locales['mod-dPpersonnel-tab-configure'] = 'Configuration du module Personnel';
$locales['mod-dPpersonnel-tab-httpreq_do_personnels_autocomplete'] = 'Autocomplete de personnel';
$locales['mod-dPpersonnel-tab-vw_affectations_multiples'] = 'Affectations multiples';
$locales['mod-dPpersonnel-tab-vw_affectations_pers'] = 'Gestion des affectations';
$locales['mod-dPpersonnel-tab-vw_edit_personnel'] = 'Gestion du personnel';
$locales['mod-dPpersonnel-tab-vw_edit_remplacement'] = 'Edition de remplacement';
$locales['mod-dPpersonnel-tab-vw_idx_plages_conge'] = 'Gestion plages de congés';
$locales['mod-dPpersonnel-tab-vw_planning_conge'] = 'Planning des congés';
$locales['mod-dPpersonnel-tab-vw_remplacement_user'] = 'Remplacements';
$locales['mod-personnel-tab-vw_edit_remplacement'] = 'Edition de remplacement';
$locales['module-dPpersonnel-court'] = 'Personnel';
$locales['module-dPpersonnel-long'] = 'Affectation du personnel';
$locales['module-dPpersonnel-trigramme'] = 'Pers';
$locales['module-personnel-court'] = 'Personnel';
