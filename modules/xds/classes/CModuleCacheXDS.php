<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Interop\Xds;

use Ox\Core\CAppUI;
use Ox\Core\CMbException;
use Ox\Core\Module\CAbstractModuleCache;
use Ox\Interop\Xds\Factory\CXDSFactory;

/**
 * Class CModuleCacheXDS
 * @package Ox\Interop\Xds
 */
class CModuleCacheXDS extends CAbstractModuleCache
{
    /** @var string */
    public $module = 'xds';

    /**
     * @inheritdoc
     * @throws CMbException
     */
    public function clearSpecialActions(): void
    {
        $repo_xds = new CXDSRepository(CXDSFactory::TYPE_XDS);
        if ($repo_xds->clearCache()) {
            CAppUI::stepAjax('CXDSFactory-msg-success deleted cache');
        }
    }
}
