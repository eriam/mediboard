{{*
 * @package Mediboard\OxCabinet
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
*}}
<table class="tbl me-no-hover" style="display: block;width:98%;  overflow-wrap: break-word;">
    <tbody style="display: block;width:100%;">
    <tr>
        <td>
            <h1 style="text-align: center; font-size: 30px"><strong>Conditions générales d'utilisation de l'espace PS</strong></h1>
            <br>
            <h1 style="text-align: center">Mentions légales</h1>
            <p>L'espace PS est un service édité par la société <strong>OpenXtrem</strong> SAS, dont le siège social est
                situé au 4 rue
                Paul
                Vatine, ZI des, Rue des Quatre Chevaliers, 17180 PERIGNY
                <strong>OpenXtrem</strong> fait partie du groupe SOFTWAY MEDICAL SAS, dont le siège social est situé à
                l'ARTEPARC BAT C,
                Route de
                la Côte d'Azur - 13590 MEYREUIL
                Son représentant légal est M. Patrice TAISSON</p>

            <br>
            <h2>Article 1. Objet</h2>
            <p>L'espace PS, (ci-après dénommé « le service »), permet à un Professionnel de Santé (ci-après dénommé «
                l'utilisateur ») de consulter, de modifier et de compléter des informations médicales et/ou
                administratives de
                patients ayant été pris en charge parmi l'un des établissements de santé ou de centre de centre de santé
                clients
                de <strong>OpenXtrem</strong>. Cet accès étant limité aux patients pour lesquels l'utilisateur déclare
                avoir des liens.
                Le présent document a pour objet de définir les conditions d'utilisation du service ainsi que la
                politique de
                confidentialité afférente pour les utilisateurs.</p>

            <br>
            <h2>Article 2. Accès et fonctionnement du service</h2>
            <p>La demande d'utilisation du service s'effectue auprès de la société <strong>OpenXtrem</strong>
                <strong>OpenXtrem</strong> vérifie l'identité de l'utilisateur à l'aide du fournisseur d'identité PRO
                Santé Connect.
                Lorsque
                l'identité de l'utilisateur est vérifiée et validée, il est redirigé vers son portail.
                Les données sont hébergées par la société SOFTWAY MEDICAL
                2.1 Identification électronique par Pro Santé Connect
                PRO Santé Connect est un téléservice mis en ?uvre par l'Agence du Numérique en Santé (ANS) contribuant à
                simplifier l'identification électronique des professionnels intervenant en santé.
                L'utilisateur peut se connecter grâce à son application mobile e-CPS ou sa carte CPS, avec un lecteur de
                cartes
                et les composants nécessaires.
                Consulter les conditions générales d'utilisation de Pro Santé Connect sur le site
                https://integrateurs-cps.asipsante.fr/pages/prosanteconnect/cgu
                <br>
            <h2>Article 3. Engagements de l'utilisateur</h2>
            <p>L'utilisation du service implique le respect de la législation applicable et des présentes conditions
                d'utilisation.
                L'utilisateur s'engage à ne pas porter atteinte ou tenter de porter atteinte à l'intégrité, au
                fonctionnement ou
                à la sécurité du système d'information à la base du fonctionnement du service.
                En cas de manquement de l'utilisateur aux présentes conditions générales d'utilisation ou à la
                législation
                applicable, <strong>OpenXtrem</strong> se réserve le droit, sans préavis ni mise en demeure, de
                suspendre
                unilatéralement,
                temporairement ou définitivement le compte de l'utilisateur à l'origine du manquement.</p>

            <br>
            <h2>Article 4. Engagements d'OpenXtrem</h2>
            <p><strong>OpenXtrem</strong> met tout en ?uvre pour assurer un fonctionnement régulier et une disponibilité
                permanente du
                service.
                Celui-ci est néanmoins mis à disposition de l'utilisateur « en l'état », sans garantie de l'absence
                d'erreurs,
                de périodes d'indisponibilité, de failles ou de défauts.
                <strong>OpenXtrem</strong> ne pourra en aucun cas être tenue pour responsable :
                - de toute interruption ou restriction d'accès à la plateforme du fait d'opérations de maintenance, de
                mises à
                niveau ou de modification ;
                - des éventuels préjudices indirects qui seraient subis par l'utilisateur utilisant ses services ;
                - de tout préjudice subi par un utilisateur ou un tiers qui résulterait d'une utilisation non conforme
                de la
                plateforme aux présentes conditions générales d'utilisation ;
                - de tout manquement à ses obligations prévues aux présentes conditions générales d'utilisation qui
                serait dû ou
                trouverait son origine dans un cas fortuit ou un cas de force majeure tel que défini par la
                jurisprudence. Sont
                également considérés comme des cas de force majeure les grèves totales ou partielles, internes ou
                externes à
                <strong>OpenXtrem</strong> ou Softway Medical, ainsi que le blocage et/ou les dysfonctionnements des
                réseaux de
                télécommunications, d'électricité ou informatiques dès lors que ces dysfonctionnements n'ont pas pour
                origine
                les moyens techniques mis en ?uvre par <strong>OpenXtrem</strong> ou Softway Medical et ne relèvent pas
                de leurs
                responsabilités.</p>

            <br>
            <h2>Article 5. Protection des données à caractère personnel (RGPD)</h2>
            <p><strong>OpenXtrem</strong> construit avec ses utilisateurs des relations fortes et durables, fondées sur
                la confiance
                réciproque :
                assurer la sécurité et la confidentialité des données personnelles est une priorité.
                <strong>OpenXtrem</strong> respecte l'ensemble des dispositions réglementaires et législatives
                françaises et européennes
                relatives à la protection des données personnelles.
                <strong>OpenXtrem</strong> applique une politique extrêmement stricte pour garantir la protection des
                données
                personnelles de ses
                utilisateurs :
                - Chaque utilisateur reste maître de ses données. Le responsable du traitement n'en dispose pas
                librement.
                - Elles sont traitées de manière transparente, confidentielle et sécurisée.
                - <strong>OpenXtrem</strong> est engagé dans une démarche continue de protection des données de ses
                utilisateurs, en
                conformité
                avec la Loi Informatique et Libertés du 6 janvier 1978 modifiée (ci-après « LIL ») et du Règlement (UE)
                général
                sur la protection des données du 27 avril 2016 (ci-après « RGPD »).
                - <strong>OpenXtrem</strong> via Softway Medical dispose d'un DPO (Data Protection Officier) déclaré
                auprès de la CNIL.
                - Les données personnelles des utilisateurs sont hébergées dans des Data Centers situés en France.
                Dans cet objectif <strong>OpenXtrem</strong> a établi une politique confidentialité disponible en annexe
                de ces présentes
                conditions générales d'utilisation.</p>

            <br>
            <h2>Article 6. Propriété intellectuelle</h2>
            <p>L'utilisateur s'engage à respecter les droits de propriété intellectuelle d'<strong>OpenXtrem</strong> et
                des tiers, et
                notamment
                à ne pas porter atteinte à tout signe distinctif (ex. marque, logo, etc.) reproduit sur le service.
                6.1 Création de liens
                La mise en place de liens vers le service n'est conditionnée à aucun accord préalable sous réserve de ne
                pas
                utiliser la technique du lien profond, c'est-à-dire que les pages du site ne doivent pas être imbriquées
                à
                l'intérieur des pages d'un autre site, mais visibles par l'ouverture d'une fenêtre indépendante.
                Cette autorisation ne s'applique en aucun cas aux sites Internet diffusant des informations à caractère
                raciste,
                pornographique, sexiste, xénophobe, polémique ou pouvant, d'une façon générale, porter atteinte à la
                sensibilité
                du plus grand nombre.
                6.2 Liens externes
                Des liens vers d'autres sites, privés ou officiels, français ou étrangers, peuvent être proposés. Leur
                présence
                ne saurait engager <strong>OpenXtrem</strong> quant à leur contenu et ne vise qu'à permettre au visiteur
                de trouver plus
                facilement d'autres ressources documentaires sur le sujet consulté. Le contenu des pages, diffusé à
                titre
                purement informatif, ne saurait donc engager la responsabilité d'<strong>OpenXtrem</strong>.
                6.3 Loi applicable et tribunaux compétents
                Les présentes conditions générales d'utilisation sont régies par la loi française. Tout litige résultant
                de leur
                application relèvera de la compétence des tribunaux français.
                6.4 Modification des conditions générales d'utilisation
                <strong>OpenXtrem</strong> s'engage à avertir via la page de connexion au service l'utilisateur de
                toutes modifications
                des
                présentes conditions générales d'utilisation.</p>

            <br/>
            <h1 style="text-align: center">Annexe</h1>
            <br/>
            <h2>Politique de protection des données personnelles des utilisateurs du service</h2>
            <p>    Le responsable du traitement (Softway Medical) souhaite vous informer par l'intermédiaire de la présente
                politique de la manière dont nous protégeons vos données à caractère personnel traitées via le présent
                site
                Internet.
                La présente politique décrit la manière dont le responsable du traitement traite les données
                personnelles des
                utilisateurs (ci-après le/les « utilisateur(s) ») lors de leur navigation sur le site Web de l'Espace PS
                du
                responsable du traitement (ci-après le « Site ») et de leur utilisation des services du responsable du
                traitement.
                Cette Politique peut être modifiée, complétée ou mise à jour afin notamment de se conformer à toute
                évolution
                légale, réglementaire, jurisprudentielle et technique. Cependant, les Données Personnelles des
                utilisateurs
                seront toujours traitées conformément à la politique en vigueur au moment de leur collecte, sauf si une
                prescription légale impérative venait à en disposer autrement et serait d'application rétroactive.
                Cette politique fait partie intégrante des Conditions Générales d'Utilisation du Service.</p>

            <br>
            <h2>Identité et coordonnées des responsables de traitement</h2>
            <p>
                Le responsable du traitement des Données Personnelles est :
                SOFWAY MEDICAL
                ARTEPARC BAT C
                Route de la Côte d'Azur
                13590 MEYREUIL
                Ci-après désigné « LE REPONSABLE DU TRAITEMENT » ; Pour les Données Personnelles collectées dans le
                cadre de
                la création de la fiche de l'utilisateur et de sa navigation sur le Site ou son utilisation de
                l'Application
                : le responsable du traitement met à votre disposition le nom de son représentant légal. Représentant
                légal
                : M. Patrice TAISSON Coordonnées du DPO : rgpd@softwaymedical.fr Les sous-traitants sont : L'Agence du
                Numérique en Santé (ci-après désigné « ANS ») qui prend les mesures propres à assurer la protection et
                la
                confidentialité des informations nominatives qu'elle détient ou qu'elle traite dans le respect des
                dispositions de la LIL et du RGPD. Le sous-traitant s'engage à conserver les données dans des
                datacenters
                situés en France. Le sous-traitant s'engage à développer ses solutions par des méthodes garantissant la
                confidentialité par conception et par défaut. L'ANS s'engage à ne pas utiliser les données personnelles
                des
                utilisateurs à des fins de prospection ou publicitaire.</p>
            <br>
            <h2>Collecte & origine des données</h2>
            <p>Toutes les données concernant les utilisateurs sont soit collectées directement auprès de ces derniers
                soit au
                travers de leur authentification via le service ?Pro Santé Connect- proposé par l'ANS.
                Le responsable du traitement s'engage à recueillir le consentement de ses utilisateurs et/ou à leur
                permettre de
                s'opposer à l'utilisation de leurs données pour certaines finalités, dès que cela est nécessaire.
                Dans tous les cas, les utilisateurs sont informés des finalités pour lesquelles leurs données sont
                collectées
                via les différents formulaires de collecte de données en ligne et via la Charte de gestion des
                cookies.</p>

            <br>
            <h2>Finalité des données collectées</h2>
            <br>
            <h2>Nécessité de la collecte</h2>
            <p>
                Lors de l'utilisation de l'espace PS, l'utilisateur communique certaines Données Personnelles. Si
                l'utilisateur
                ne souhaite pas communiquer les informations qui lui sont demandées, il se peut qu'il ne puisse pas
                accéder à
                certaines parties du Site ou de l'Application, et que le responsable du traitement soit dans
                l'impossibilité de
                répondre à sa demande.</p><br/>
            <p>
                Lors de l'authentification un ensemble de données personnelles sont recueillies au travers du service
                d'authentification ?Pro Santé Connect- de l'ANS et éventuellement complétées par l'utilisateur.</p>
            <br>
            <h2>Finalités</h2>
                <p>Le recueil de vos Données Personnelles a pour base légale :
                - l'intérêt légitime du responsable du traitement à assurer la meilleure qualité de ses services, à
                fournir à
                ses utilisateurs la meilleure prise en charge possible et à améliorer le fonctionnement de son Site
                - l'obligation légale d'authentifier le professionnel de santé accédant à des données de santé
                - L'intérêt légitime des établissements de santé d'assurer une traçabilité des accès aux données de
                    santé.</p><br/>
            <p>
                Les données de l'utilisateur sont traitées par le responsable du traitement pour :
                - permettre la navigation sur le Site
                - permettre d'avoir un accès aux données de santé des patients pour lesquelles l'utilisateur participe à
                leur
                prise en charge médicale et/ou paramédicale</p><br/>
            <p>
                De plus, dans le cadre de l'intérêt légitime des établissements de santé d'assurer la traçabilité
                exhaustive des
                accès aux données de santé dont ils sont responsables, le responsable de traitement partage les données
                d'identification de l'utilisateur avec les établissements pour lesquels l'utilisateur accède aux données
                de santé qu'ils exposent. L'utilisateur peut alors contacter le DPO de l'établissement de santé concerné
                pour exercer ses droits.</p><br/>
            <p>
                À titre subsidiaire les données des utilisateurs sont également collectées pour :
                - améliorer la navigation sur le Site et l'utilisation de l'Application
                - effectuer des statistiques sur l'utilisation de l'outil, un reporting pour les équipes de recherche &
                développement du responsable du traitement (sans qu'aucune Donnée personnelle ne soit utilisée).</p><br/>
            <p>
                Le caractère obligatoire ou facultatif des données personnelles demandées et les éventuelles
                conséquences d'un
                défaut de réponse à l'égard des utilisateurs du responsable du traitement sont précisés lors de leur(s)
                collecte(s).</p><br/>
            <p>
                Types de données traitées
                Le responsable du traitement est susceptible de traiter, en tant que Responsable de Traitement, tout ou
                partie
                des données suivantes :</p><br/>
            <p>
                - pour permettre l'authentification de l'utilisateur : nom, prénom, numéro RPPS du professionnel de
                santé
                connecté, catégorie professionnelle.
                - pour permettre aux établissements de santé d'assurer la traçabilité des accès aux données de santé des
                patients consultés : nom, prénom et numéro RPPS de l'utilisateur.
                - pour permettre la navigation sur le Site : données de connexion et d'utilisation du Site ou de
                l'Application
                - pour prévenir et lutter contre la fraude informatique (spamming, hacking?) : matériel informatique
                utilisé
                pour la navigation, l'adresse IP, l'identifiant de l'utilisateur, les cookies présents dans le
                navigateur
                - pour améliorer la navigation sur le Site ou l'utilisation de l'Application : données de connexion et
                d'utilisation, des informations cryptées sur le terminal de l'utilisateur
                Non-communication des données personnelles</p><br/>
            <p>
                Les Données Personnelles de l'utilisateur ne seront pas transmises à des acteurs commerciaux ou
                publicitaires.
                Dans la limite de leurs attributions respectives et pour les finalités rappelées ci-dessus, les
                principales
                personnes susceptibles d'avoir accès aux données des utilisateurs du responsable du traitement sont le
                responsable du traitement, et les établissements de santés qui exposent les données de santé des
                patients
                consultés par l'utilisateur à des fins de traçabilité des accès aux données de santé.
                Durée de conservation des données
                Les données destinées à identifier l'utilisateur sont conservés jusqu'à 24 mois après la dernière
                connexion de
                celui-ci.</p><br/>
            <p>
                Les traces de connexions au site sont conservées 24 mois.
                Conformément aux prescriptions légales, les établissements de santés responsable des données de santé
                consultées
                conservent les traces d'accès aux données médicales 20 ans à partir de la dernière activité médicale.
                Dans le
                cas où le patient est mineur, les données sont conservées jusqu'à ses 28 ans.</p><br/>
            <p>
                Les droits des utilisateurs
                Chaque fois que le responsable du traitement traite des Données Personnelles, il prend toutes les
                mesures
                raisonnables pour s'assurer de l'exactitude et de la pertinence des Données Personnelles au regard des
                finalités
                pour lesquelles il les traite.</p><br/>
            <p>
                Conformément à la réglementation européenne en vigueur, les utilisateurs du responsable du traitement
                disposent
                des droits suivants :</p><br/>
            <p>
                - droit d'accès (article 15 RGPD) et de rectification (article 16 RGPD), de mise à jour, de complétude
                des
                données des usagers
                - droit de verrouillage ou d'effacement des données des usagers à caractère personnel (article 17 du
                RGPD),
                lorsqu'elles sont inexactes, incomplètes, équivoques, périmées, ou dont la collecte, l'utilisation, la
                communication ou la conservation est interdite (en savoir plus)
                - droit de retirer à tout moment un consentement (article 13-2c RGPD)
                - droit à la limitation du traitement des données des usagers (article 18 RGPD)
                - droit d'opposition au traitement des données des usagers (article 21 RGPD) (en savoir plus)
                - droit à la portabilité des données que les usagers auront fournies, lorsque ces données font l'objet
                de
                traitements automatisés fondés sur leur consentement ou sur un contrat (article 20 RGPD)</p><br/>
            <p>
                Si l'utilisateur souhaite savoir comment le responsable du traitement utilise ses Données Personnelles,
                demander
                à les rectifier ou s'oppose à leur traitement, l'utilisateur peut contacter le DPO du responsable du
                traitement
                par écrit ou par mail. Dans ce cas, l'utilisateur doit indiquer les Données Personnelles qu'il
                souhaiterait que
                le responsable du traitement corrige, mette à jour ou supprime. Enfin, les utilisateurs du responsable
                du
                traitement peuvent déposer une réclamation auprès des autorités de contrôle, et notamment de la CNIL
                (https://www.cnil.fr/fr/plaintes).</p><br/>
            <p>
                "Cookies" et balises (tags) Internet
                Un « cookie » est un petit fichier d'information envoyé sur le navigateur de l'utilisateur et enregistré
                au sein
                du terminal de l'utilisateur (ex : ordinateur, smartphone), (ci-après « Cookies »). Ce fichier comprend
                des
                informations telles que le nom de domaine de l'utilisateur, le fournisseur d'accès Internet de
                l'utilisateur, le
                système d'exploitation, ainsi que la date et l'heure d'accès. Les Cookies ne risquent en aucun cas
                d'endommager
                le terminal de l'utilisateur.</p><br/>
            <p>
                Le responsable du traitement est susceptible de traiter les informations de l'utilisateur concernant sa
                visite
                du Site, telles que les pages consultées, les recherches effectuées. Ces informations permettent au
                responsable
                du traitement d'améliorer le contenu du Site, la navigation de l'utilisateur.
                Les Cookies facilitant la navigation et/ou la fourniture des services proposés par le Site,
                l'utilisateur peut
                configurer son navigateur pour qu'il lui permette de décider s'il souhaite ou non les accepter de
                manière que
                des Cookies soient enregistrés dans le terminal ou, au contraire, qu'ils soient rejetés, soit
                systématiquement,
                soit selon leur émetteur. L'utilisateur peut également configurer son logiciel de navigation de manière
                que
                l'acceptation ou le refus des Cookies lui soient proposés ponctuellement, avant qu'un Cookie soit
                susceptible
                d'être enregistré dans son terminal. Le responsable du traitement informe l'utilisateur que, dans ce
                cas, il se
                peut que les fonctionnalités de son logiciel de navigation ne soient pas toutes disponibles.</p><br/>
            <p>
                Si l'utilisateur refuse l'enregistrement de Cookies dans son terminal ou son navigateur, ou si
                l'utilisateur
                supprime ceux qui y sont enregistrés, l'utilisateur est informé que sa navigation et son expérience sur
                le Site
                peuvent être limitées. Cela pourrait également être le cas lorsque le responsable du traitement ou l'un
                de ses
                prestataires ne peut pas reconnaître, à des fins de compatibilité technique, le type de navigateur
                utilisé par
                le terminal, les paramètres de langue et d'affichage ou le pays depuis lequel le terminal semble
                connecté à
                Internet.</p><br/>
            <p>
                Le cas échéant, le responsable du traitement décline toute responsabilité pour les conséquences liées au
                fonctionnement dégradé du Site et des services éventuellement proposés par <strong>OpenXtrem</strong>,
                résultant du refus
                de
                Cookies par l'utilisateur de l'impossibilité pour le responsable du traitement d'enregistrer ou de
                consulter les
                Cookies nécessaires à leur fonctionnement du fait du choix de l'utilisateur. Pour la gestion des Cookies
                et des
                choix de l'utilisateur, la configuration de chaque navigateur est différente. Elle est décrite dans le
                menu
                d'aide du navigateur, qui permettra de savoir de quelle manière l'utilisateur peut modifier ses souhaits
                en
                matière de Cookies.
                À tout moment, l'utilisateur peut faire le choix d'exprimer et de modifier ses souhaits en matière de
                Cookies.</p><br/>
            <p>
                Le responsable du traitement pourra en outre faire appel aux services de prestataires externes pour
                l'aider à
                recueillir et traiter les informations décrites dans cette section.
                Nom du cookie et finalité
            </p>
            <ul>
                <li>
                    mediboard-uainfo : nous permet de garder en mémoire des informations sur le navigateur (origine,
                    taille d'écran...)
                </li>
                <li>timming : ce cookie est créé par le serveur apache (Apache request duration and start time)
                    mediboard-cookie-supported : comme son nom l'indique ce cookie nous permet d'avertir l'utilisateur
                    si il a bloqué l'usage des cookies
                </li>
                <li>tamm : porte l'id de session phpLe responsable du traitement met en ?uvre toutes les mesures
                    techniques et organisationnelles afin d'assurer la sécurité des traitements de données à caractère
                    personnel et la
                    confidentialité de Données Personnelles.
                </li>
                <li>mediboard-oxPyxvital-codecps, 300 secondes, Stocker le code CPS du praticien</li>
                <li>mediboard-pyxvitalClientErrorCount, 3600 secondes, Nombre d'erreurs du client Pyxvital</li>
                <li>mediboard-pyxvitalDriverStatus, 3600 secondes, Statut du Driver Pyxvital</li>
                <li>mediboard-pyxvitalDriverErrors, 3600 secondes, Nombre d'erreurs liées au driver Pyxvital</li>
                <li>mediboard-pyxvitalDriverNationalId, 3600 secondes, Identifiant du driver de Pyxvital</li>
                <li>mediboard-pyxvitalUpdated, session, Driver à jour</li>
                <li>mediboard-pyxvitalUpdated[adeli], session, Driver à jour (en fonction de l'adeli)</li>
                <li>mediboard-ElementHeight, session, Hauteurs sélectionnées des zones de texte</li>
                <li>mediboard-TabState, session, Etat des volets actifs</li>
                <li>mediboard-effects, session, Etat des zones de page affichées</li>
            </ul>
            </p>

            <br>
            <h2>Durée de conservation : durée de vie de la session</h2>
            <p>À ce titre, le responsable du traitement prend toutes les précautions utiles, au regard de la nature des
                données
                et des risques présentés par le traitement, afin de préserver la sécurité des données et, notamment,
                d'empêcher
                qu'elles soient déformées, endommagées, ou que des tiers non autorisés y aient accès (protection
                physique des
                locaux, procédés d'authentification avec accès personnel et sécurisé via des identifiants et mots de
                passe
                confidentiels, journalisation des connexions, chiffrement de certaines données...).</p>
            <p>
                Nous contacter - Coordonnées du DPO
                Si l'utilisateur a des questions ou des réclamations concernant le respect par le responsable du
                traitement de
                la présente Politique, ou si l'utilisateur souhaite faire part au responsable du traitement de
                recommandations
                ou des commentaires visant à améliorer la qualité de la présente politique, l'utilisateur peut contacter
                le
                responsable du traitement par l'intermédiaire de son DPO.</p>
        </td>
    </tr>
    </tbody>
</table>
