<?php
$locales['AFCT'] = 'Affectation';
$locales['ATCD'] = 'Antécédent';
$locales['CFwImport-Error-No user table'] = 'Aucune table ou fichier pour les utilisateurs';
$locales['CImportCampagn-action-Create'] = 'Nouvelle campagne d\'importation';
$locales['CImportCampaign'] = 'Campagne d\'importation';
$locales['CImportCampaign-closing_date'] = 'Date de fin';
$locales['CImportCampaign-closing_date-court'] = 'Fin';
$locales['CImportCampaign-closing_date-desc'] = 'Date de fin';
$locales['CImportCampaign-creation_date'] = 'Date de création';
$locales['CImportCampaign-creation_date-court'] = 'Création';
$locales['CImportCampaign-creation_date-desc'] = 'Date de création';
$locales['CImportCampaign-creator_id'] = 'Créateur de la campagne';
$locales['CImportCampaign-creator_id-court'] = 'Créateur';
$locales['CImportCampaign-creator_id-desc'] = 'Créateur de la campagne';
$locales['CImportCampaign-import_campaign_id'] = 'Identifiant';
$locales['CImportCampaign-import_campaign_id-court'] = 'ID';
$locales['CImportCampaign-import_campaign_id-desc'] = 'Identifiant de la campagne d\'importation';
$locales['CImportCampaign-msg-create'] = 'Campagne d\'importation créée';
$locales['CImportCampaign-msg-delete'] = 'Campagne d\'importation supprimée';
$locales['CImportCampaign-msg-modify'] = 'Campagne d\'importation modifiée';
$locales['CImportCampaign-name'] = 'Nom';
$locales['CImportCampaign-name-court'] = 'Nom';
$locales['CImportCampaign-name-desc'] = 'Nom de la campagne d\'importation (TAG)';
$locales['CImportCampaign-title-create'] = 'Créer une campagne d\'importation';
$locales['CImportCampaign-title-modify'] = 'Modifier une campagne d\'importation';
$locales['CImportCampaign.all'] = 'Toutes les campagnes d\'importation';
$locales['CImportCampaign.none'] = 'Aucune campagne d\'importation';
$locales['CImportCampaign.one'] = 'Une campagne d\'importation';
$locales['CImportEntity'] = 'Objet importé';
$locales['CImportEntity-Ask-Delete all entities for type'] = 'Voulez-vous vraiment supprimer tous les liens des entitées de %s pour cette campagne d\'importation ?';
$locales['CImportEntity-Msg-delete|pl'] = 'Liens vers les objets importés supprimés.';
$locales['CImportEntity-external_class-court'] = 'Ext classe';
$locales['CImportEntity-external_class-desc'] = 'Classe externe';
$locales['CImportEntity-external_id-court'] = 'ID externe';
$locales['CImportEntity-external_id-desc'] = 'Identifiant dans la base externe';
$locales['CImportEntity-internal_id-court'] = 'ID interne';
$locales['CImportEntity-internal_id-desc'] = 'Identifiant interne';
$locales['CImportEntity-label-All'] = 'Tout';
$locales['CImportEntity-label-Only imported|pl'] = 'Seulement importés';
$locales['CImportEntity-label-Only in error|pl'] = 'Seulement en erreur';
$locales['CImportEntity-last_error-court'] = 'DerniÎ¸re erreur';
$locales['CImportEntity-last_error-desc'] = 'DerniÎ¸re erreur d\'importation';
$locales['CImportEntity-last_import_date-court'] = 'Date import';
$locales['CImportEntity-last_import_date-desc'] = 'Date de la derniÎ¸re importation';
$locales['CImportEntity-msg-create'] = 'Entité d\'importation créée';
$locales['CImportFwLegacyController-Error-Mapper does not exist or cannot be instanciated for type'] = 'Le mapper pour %s n\'a pas pu Îºtre instancié ou n\'existe pas';
$locales['CONS'] = 'Constante';
$locales['CORR'] = 'Correspondants médicaux';
$locales['CSLT'] = 'Consultation';
$locales['DMED'] = 'Dossier médical';
$locales['FILE'] = 'Fichier';
$locales['GAIP'] = 'Galaxie Solde';
$locales['MEDC'] = 'Médecins';
$locales['OPRT'] = 'Interventions';
$locales['PATI'] = 'Patient';
$locales['PLGC'] = 'Plage de consultation';
$locales['PersisterException-File must have a context'] = 'Le fichier doit avoir un contexte (aucun trouvé durant l\'importation)';
$locales['SEJR'] = 'Séjour';
$locales['TRT'] = 'Traitement';
$locales['USER'] = 'Utilisateur';
$locales['XmlMapperBuilder-Error-Directory is mandatory '] = 'Aucun répertoire spécifié pour l\'importation';
$locales['mod-import-tab-ajax_search_import_campaign'] = 'Recherche de campagne d\'importation';
$locales['mod-import-tab-configure'] = 'Administrer';
$locales['mod-import-tab-vw_campaign_objects'] = 'Objets importés';
$locales['mod-import-tab-vw_edit_import_campaign'] = 'Modifier une campagne d\'importation';
$locales['mod-import-tab-vw_import_campaigns'] = 'Campagnes d\'importation';
$locales['mod-import-type-acte_ccam'] = 'Acte CCAM';
$locales['mod-import-type-acte_ngap'] = 'Acte NGAP';
$locales['mod-import-type-allergie'] = 'Allergie';
$locales['mod-import-type-antecedent'] = 'Antécédent';
$locales['mod-import-type-consultation'] = 'Consultation';
$locales['mod-import-type-consultation_autre'] = 'Consultation secondaire';
$locales['mod-import-type-correspondant_medical'] = 'Correspondant médical';
$locales['mod-import-type-doc_externe'] = 'Documents externes';
$locales['mod-import-type-document'] = 'Document';
$locales['mod-import-type-evenement'] = 'Evénement patient';
$locales['mod-import-type-fichier'] = 'Fichier';
$locales['mod-import-type-intervention'] = 'Intervention';
$locales['mod-import-type-medecin'] = 'Médecin';
$locales['mod-import-type-ordonnance'] = 'Ordonnance';
$locales['mod-import-type-patient'] = 'Patient';
$locales['mod-import-type-plage_consultation'] = 'Plage de consultation';
$locales['mod-import-type-plage_consultation_autre'] = 'Plage de consultation secondaire';
$locales['mod-import-type-sejour'] = 'Séjour';
$locales['mod-import-type-traitement'] = 'Traitement';
$locales['mod-import-type-utilisateur'] = 'Utilisateurs';
$locales['mod-import-type-vaccin'] = 'Vaccin';
$locales['module-import-court'] = 'import';
$locales['module-import-long'] = 'Framework d\'import';
$locales['module-import-trigramme'] = 'Imp';
