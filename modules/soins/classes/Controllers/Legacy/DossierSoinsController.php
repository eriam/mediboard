<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Mediboard\Soins\Controllers\Legacy;

use Exception;
use Ox\Core\CAppUI;
use Ox\Core\CLegacyController;
use Ox\Core\CStoredObject;
use Ox\Core\CView;
use Ox\Core\Module\CModule;
use Ox\Interop\Imeds\CImeds;
use Ox\Mediboard\Admin\CAccessMedicalData;
use Ox\Mediboard\Etablissement\CGroups;
use Ox\Mediboard\Mediusers\CMediusers;
use Ox\Mediboard\Mpm\CPrescriptionLineMedicament;
use Ox\Mediboard\Mpm\CPrescriptionLineMix;
use Ox\Mediboard\Mpm\CPrescriptionLineMixItem;
use Ox\Mediboard\Patients\CDossierMedical;
use Ox\Mediboard\Patients\CPatient;
use Ox\Mediboard\PlanningOp\COperation;
use Ox\Mediboard\PlanningOp\CSejour;
use Ox\Mediboard\PlanSoins\CAdministration;
use Ox\Mediboard\Soins\DossierSoinsService;
use Ox\Mediboard\System\CExchangeSource;
use Ox\Mediboard\System\CSourceHTTP;

/**
 * Class DossierSoinsController
 * @package Ox\Mediboard\Soins\Controllers\Legacy
 */
class DossierSoinsController extends CLegacyController
{
    /**
     *  Copié du script "ajax_vw_perop_administrations.php" adapté pour une opération particuliÎ¸re
     *
     *  Charge les événements perop pour une intervention
     */
    public function ajaxViewPeropOperation(): void
    {
        $this->checkPermRead();

        $prescription_id      = CView::get("prescription_id", "ref class|CPrescription");
        $sejour_id            = CView::get("sejour_id", "ref class|CSejour");
        $operation_id         = CView::get("operation_id", "ref class|COperation");
        $show_administrations = CView::get("show_administrations", "bool default|0");

        CView::checkin();

        $sejour     = CSejour::find($sejour_id);
        $operation  = COperation::findOrFail($operation_id);
        $operations = $sejour->loadRefsOperations(["annulee" => "= '0'"]);

        CAccessMedicalData::logAccess($sejour);

        // Tri des gestes et administrations perop par ordre chronologique
        $perops = [];

        // Chargement des administrations
        $administrations              = CAdministration::getPerop($prescription_id, true, $operation->_id);
        $count_administrations_gestes = 0;

        foreach ($administrations as $_adm) {
            $_adm->loadRefsFwd();
            $object = $_adm->_ref_object;

            if ($object instanceof CPrescriptionLineMedicament || $object instanceof CPrescriptionLineMixItem) {
                $_produit = $object->_ref_produit;
                $_produit->loadRapportUnitePriseByCIS($object);
                $_produit->updateRatioMassique();

                if ($_produit->_ratio_mg) {
                    $_adm->_quantite_mg = $_adm->quantite / $_produit->_ratio_mg;
                }

                [$unite_lt, $qte_lt] = CPrescriptionLineMedicament::computeQteUnitLTPerop(
                    $object,
                    $_adm->quantite
                );

                $_adm->_ref_object->_unite_livret = $unite_lt;
                $_adm->_ref_object->_qte_livret   = $qte_lt;
            }

            $section              = CAdministration::getSectionPerop($operation->_id, $_adm->dateTime);
            $_adm->_perop_section = $section;

            $perops[$_adm->dateTime][$_adm->_guid] = $_adm;
            $count_administrations_gestes++;
        }

        if ($sejour->_ref_prescription_sejour && $sejour->_ref_prescription_sejour->_id) {
            // Chargements des perfusions pour afficher les poses et les retraits
            $prescription_line_mix                  = new CPrescriptionLineMix();
            $prescription_line_mix->prescription_id = $prescription_id;
            $prescription_line_mix->perop           = 1;
            /** @var CPrescriptionLineMix[] $mixes */
            $mixes = $prescription_line_mix->loadMatchingList();

            CStoredObject::massLoadFwdRef($mixes, "praticien_id");

            foreach ($mixes as $_mix) {
                $_mix->loadRefPraticien();
                $_mix->loadRefsLines();
                if ($_mix->date_pose && $_mix->time_pose) {
                    $section = CAdministration::getSectionPerop($operation->_id, $_mix->_pose);

                    $_mix->_perop_section                         = $section;
                    $perops[$section][$_mix->_pose][$_mix->_guid] = $_mix;
                }
                if ($_mix->date_retrait && $_mix->time_retrait) {
                    $section = CAdministration::getSectionPerop($operation->_id, $_mix->_retrait);

                    $_mix->_perop_section                            = $section;
                    $perops[$section][$_mix->_retrait][$_mix->_guid] = $_mix;
                }
                $count_administrations_gestes++;
            }
            ksort($perops);
        }

        // Load the Perop gestures
        $anesths_perop = $operation->loadRefsAnesthPerops();

        if (!$show_administrations) {
            foreach ($anesths_perop as $_anesth_perop) {
                $_anesth_perop->updateFormFields();
                $_anesth_perop->loadRefUser();

                $section = CAdministration::getSectionPerop($operation->_id, $_anesth_perop->datetime);

                $_anesth_perop->_perop_section = $section;

                $perops[$_anesth_perop->datetime][$_anesth_perop->_guid] = $_anesth_perop;
                $count_administrations_gestes++;
            }
        }
        $this->renderSmarty(
            "inc_vw_perop_administrations",
            [
                "perops"                       => $perops,
                "operation"                    => $operation,
                "operations"                   => $operations,
                "count_administrations_gestes" => $count_administrations_gestes,
                "show_administrations"         => $show_administrations,
                "prescription_id"              => $prescription_id,
            ]
        );
    }

    public function reloadPatientBanner()
    {
        $this->checkPermRead();

        $patient_id = CView::get("patient_id", "ref class|CPatient");

        CView::checkin();

        $patient = CPatient::findOrFail($patient_id);
        $patient->loadRefDossierMedical();
        $patient->loadRefLatestConstantes();

        $this->renderSmarty(
            "inc_infos_patients_soins",
            [
                "patient" => $patient,
            ]
        );
    }

    /**
     * Charge le dossier de séjour - Remplace le script ajax_vw_dossier_sejour
     *
     * @throws Exception
     */
    public function viewDossierSejour(): void
    {
        $this->checkPermRead();

        $sejour_id           = CView::get("sejour_id", "ref class|CSejour");
        $date                = CView::get("date", "date default|now");
        $defined_default_tab =
            "dossier_traitement" . (CAppUI::gconf("soins Other vue_condensee_dossier_soins") ? "_compact" : "");

        if (CAppUI::gconf("soins dossier_soins tab_prescription_med") && CMediusers::get()->isPraticien()) {
            $defined_default_tab = "prescription_sejour";
        }

        $default_tab       = CView::get("default_tab", "str default|$defined_default_tab");
        $popup             = CView::get("popup", "bool default|0");
        $modal             = CView::get("modal", "bool default|0");
        $operation_id      = CView::get("operation_id", "ref class|COperation");
        $mode_pharma       = CView::get("mode_pharma", "bool default|0");
        $mode_protocole    = CView::get("mode_protocole", "bool default|0", true);
        $type_prescription = CView::get("type_prescription", "enum list|pre_admission|sejour|sortie default|sejour");
        $line_guid_open    = CView::get("line_guid_open", "str");

        CView::checkin();

        $service = new DossierSoinsService($sejour_id, $date, $operation_id);

        $service->loadDossierSoins();

        $form_tabs = $service->form_tabs;

        $isImedsInstalled = CModule::getActive("dPImeds") && CImeds::getTagCIDC(CGroups::loadCurrent());

        $late_objectifs = $service->getLateObjectifsSoins();

        $source_labo = CExchangeSource::get(
            "OxLabo" . CGroups::loadCurrent()->_id,
            CSourceHTTP::TYPE,
            false,
            "OxLaboExchange",
            false
        );

        $this->renderSmarty(
            "inc_dossier_sejour",
            [
                "sejour"                  => $service->getSejour(),
                "patient"                 => $service->getPatient(),
                "date"                    => $date,
                "date_plan_soins"         => $service->getDatePlanSoins(),
                "default_tab"             => $default_tab,
                "popup"                   => $popup,
                "modal"                   => $modal,
                "operation_id"            => $operation_id,
                "mode_pharma"             => $mode_pharma,
                "is_praticien"            => CAppUI::$user->isPraticien(),
                "mode_protocole"          => $mode_protocole,
                "operation"               => $service->getOperation(),
                "form_tabs"               => $form_tabs,
                "late_objectifs"          => $late_objectifs,
                "isImedsInstalled"        => $isImedsInstalled,
                "isPrescriptionInstalled" => $service->isPrescriptionInstalled,
                "type_prescription"       => $type_prescription,
                "line_guid_open"          => $line_guid_open,
                "getSourceLabo"           => $source_labo->active ? true : false,
            ]
        );
    }
}
