<?php
$locales['CEchangeFTP-_date_max'] = 'Date maximum';
$locales['CEchangeFTP-purge-search'] = 'Chercher les échanges FTP Î° purger';
$locales['CExchangeFTP'] = 'Echange FTP';
$locales['CExchangeFTP-_date_max'] = 'Date maximum';
$locales['CExchangeFTP-_date_max-court'] = 'Date maximum';
$locales['CExchangeFTP-_date_max-desc'] = 'Date maximum de recherche';
$locales['CExchangeFTP-_date_min'] = 'Date minimum';
$locales['CExchangeFTP-_date_min-court'] = 'Date minimum';
$locales['CExchangeFTP-_date_min-desc'] = 'Date minimum de recherche';
$locales['CExchangeFTP-_self_receiver'] = 'Destinataire';
$locales['CExchangeFTP-_self_receiver-court'] = 'Destinataire';
$locales['CExchangeFTP-_self_receiver-desc'] = 'Destinataire';
$locales['CExchangeFTP-_self_sender'] = 'Emetteur';
$locales['CExchangeFTP-_self_sender-court'] = 'Emetteur';
$locales['CExchangeFTP-_self_sender-desc'] = 'Emetteur';
$locales['CExchangeFTP-date_echange'] = 'Date d\'échange';
$locales['CExchangeFTP-date_echange-court'] = 'Date d\'échange';
$locales['CExchangeFTP-date_echange-desc'] = 'Date d\'échange de l\'envoi / réception';
$locales['CExchangeFTP-destinataire'] = 'Destinataire';
$locales['CExchangeFTP-destinataire-court'] = 'Destinataire';
$locales['CExchangeFTP-destinataire-desc'] = 'Destinataire de l\'échange destinataire FTP';
$locales['CExchangeFTP-echange_ftp_id'] = 'Identifiant';
$locales['CExchangeFTP-echange_ftp_id-court'] = 'Identifiant';
$locales['CExchangeFTP-echange_ftp_id-desc'] = 'Identifiant de l\'échange FTP ';
$locales['CExchangeFTP-emetteur'] = 'Emetteur';
$locales['CExchangeFTP-emetteur-court'] = 'Emetteur';
$locales['CExchangeFTP-emetteur-desc'] = 'Emetteur de l\'échange FTP';
$locales['CExchangeFTP-ftp_fault'] = 'Erreur FTP';
$locales['CExchangeFTP-ftp_fault-court'] = 'Erreur FTP';
$locales['CExchangeFTP-ftp_fault-desc'] = 'Erreur FTP';
$locales['CExchangeFTP-function_name'] = 'Fonction';
$locales['CExchangeFTP-function_name-court'] = 'Fonction';
$locales['CExchangeFTP-function_name-desc'] = 'Fonction FTP appellée';
$locales['CExchangeFTP-input'] = 'ParamÎ¸tres';
$locales['CExchangeFTP-input-court'] = 'ParamÎ¸tres';
$locales['CExchangeFTP-input-desc'] = 'ParamÎ¸tres de la fonction FTP';
$locales['CExchangeFTP-msg-create'] = 'Echange FTP crée';
$locales['CExchangeFTP-msg-delete'] = 'Echange FTP supprimé';
$locales['CExchangeFTP-msg-delete_count'] = '%d échanges FTP Î° supprimer';
$locales['CExchangeFTP-msg-deleted_count'] = '%d échanges supprimés';
$locales['CExchangeFTP-msg-modify'] = 'Echange FTP modifié';
$locales['CExchangeFTP-msg-purge_count'] = '%d échanges FTP Î° purger';
$locales['CExchangeFTP-msg-purged_count'] = '%d échanges purgés';
$locales['CExchangeFTP-output'] = 'Résultat';
$locales['CExchangeFTP-output-court'] = 'Résultat';
$locales['CExchangeFTP-output-desc'] = 'Résultat de la fonction FTP';
$locales['CExchangeFTP-purge'] = 'Purge';
$locales['CExchangeFTP-purge-court'] = 'Purge';
$locales['CExchangeFTP-purge-desc'] = 'Purge';
$locales['CExchangeFTP-purge-search'] = 'Chercher les échanges FTP Î° purger';
$locales['CExchangeFTP-response_time'] = 'Temps de réponse';
$locales['CExchangeFTP-response_time-court'] = 'Tps rép.';
$locales['CExchangeFTP-response_time-desc'] = 'Temps de réponse';
$locales['CExchangeFTP-services'] = 'Services';
$locales['CExchangeFTP-title-create'] = 'Création d\'un échange FTP';
$locales['CExchangeFTP-title-modify'] = 'Modifier l\'échange FTP';
$locales['CExchangeFTP.all'] = 'Tous les échanges FTP';
$locales['CExchangeFTP.none'] = 'Aucun échange FTP';
$locales['CExchangeFTP.one'] = 'echange FTP';
$locales['CFTP-msg-passive_mode'] = 'Activation du mode passif';
$locales['CFTP-success-authentification'] = 'Authentifié au serveur en tant que \'%s\'';
$locales['CFTP-success-connection'] = 'Connecté au serveur \'%s\' sur le port \'%s\'';
$locales['CFTP-success-deletion'] = 'Fichier distant \'%s\' supprimé';
$locales['CFTP-success-transfer_in'] = 'Fichier local \'%s\' récupéré depuis fichier distant \'%s\'");';
$locales['CFTP-success-transfer_out'] = 'Fichier local \'%s\' transféré vers fichier distant \'%s\'';
$locales['CSenderFTP'] = 'Expéditeur FTP';
$locales['CSenderFTP-_reachable'] = 'Accessible';
$locales['CSenderFTP-_reachable-court'] = 'Accessible';
$locales['CSenderFTP-_reachable-desc'] = 'Accessible';
$locales['CSenderFTP-_ref_last_message'] = 'Dernier message';
$locales['CSenderFTP-_ref_last_message-court'] = 'Dernier message';
$locales['CSenderFTP-_ref_last_message-desc'] = 'Dernier message';
$locales['CSenderFTP-actif'] = 'Actif';
$locales['CSenderFTP-actif-court'] = 'Actif';
$locales['CSenderFTP-actif-desc'] = 'Actif';
$locales['CSenderFTP-group_id'] = 'Etablissement';
$locales['CSenderFTP-group_id-court'] = 'Etab.';
$locales['CSenderFTP-group_id-desc'] = 'Etablissement';
$locales['CSenderFTP-libelle'] = 'Libellé';
$locales['CSenderFTP-libelle-court'] = 'Libellé';
$locales['CSenderFTP-libelle-desc'] = 'Libellé';
$locales['CSenderFTP-msg-create'] = 'Expéditeur FTP créé';
$locales['CSenderFTP-msg-delete'] = 'Expéditeur FTP supprimé';
$locales['CSenderFTP-msg-modify'] = 'Expéditeur FTP modifié';
$locales['CSenderFTP-nom'] = 'Nom';
$locales['CSenderFTP-nom-court'] = 'Nom';
$locales['CSenderFTP-nom-desc'] = 'Nom';
$locales['CSenderFTP-save_unsupported_message'] = 'Enregistrer le message non pris en charge';
$locales['CSenderFTP-save_unsupported_message-desc'] = 'Enregistrer le message non pris en charge par l\'acteur';
$locales['CSenderFTP-sender_ftp_id'] = 'Identifiant';
$locales['CSenderFTP-sender_ftp_id-court'] = 'Identifiant';
$locales['CSenderFTP-sender_ftp_id-desc'] = 'Identifiant expéditeur FTP';
$locales['CSenderFTP-title-create'] = 'Nouvel expéditeur FTP';
$locales['CSenderFTP-title-modify'] = 'Modification expéditeur FTP ';
$locales['CSenderFTP-user_id'] = 'Utilisateur associé';
$locales['CSenderFTP-user_id-court'] = 'Utilisateur associé';
$locales['CSenderFTP-user_id-desc'] = 'Utilisateur associé Î° l\'expéditeur';
$locales['CSenderFTP-utilities'] = 'Utilitaires expéditeur FTP';
$locales['CSenderFTP-utilities_dispatch'] = 'Récupérer et traiter les fichiers';
$locales['CSenderFTP-utilities_read-files-senders'] = 'Récupérer fichiers';
$locales['CSenderFTP.all'] = 'Tous les expéditeurs FTP';
$locales['CSenderFTP.none'] = 'Aucun expéditeur FTP';
$locales['CSenderFTP.one'] = 'Expéditeur FTP';
$locales['CSenderFTP_actions'] = 'Actions';
$locales['CSenderFTP_configs-formats'] = 'Configs formats';
$locales['CSenderFTP_formats-available'] = 'Formats disponibles';
$locales['CSenderFTP_routes'] = 'Routes';
$locales['CSenderSFTP'] = 'Expéditeur SFTP';
$locales['CSenderSFTP-_reachable'] = 'Accessible';
$locales['CSenderSFTP-_reachable-court'] = 'Accessible';
$locales['CSenderSFTP-_reachable-desc'] = 'Accessible';
$locales['CSenderSFTP-actif'] = 'Actif';
$locales['CSenderSFTP-actif-court'] = 'Actif';
$locales['CSenderSFTP-actif-desc'] = 'Actif';
$locales['CSenderSFTP-group_id'] = 'Etablissement';
$locales['CSenderSFTP-group_id-court'] = 'Etab.';
$locales['CSenderSFTP-group_id-desc'] = 'Etablissement';
$locales['CSenderSFTP-libelle'] = 'Libellé';
$locales['CSenderSFTP-libelle-court'] = 'Libellé';
$locales['CSenderSFTP-libelle-desc'] = 'Libellé';
$locales['CSenderSFTP-msg-create'] = 'Expéditeur SFTP créé';
$locales['CSenderSFTP-msg-delete'] = 'Expéditeur SFTP supprimé';
$locales['CSenderSFTP-msg-modify'] = 'Expéditeur SFTP modifié';
$locales['CSenderSFTP-nom'] = 'Nom';
$locales['CSenderSFTP-nom-court'] = 'Nom';
$locales['CSenderSFTP-nom-desc'] = 'Nom';
$locales['CSenderSFTP-save_unsupported_message'] = 'Enregistrer le message non pris en charge';
$locales['CSenderSFTP-save_unsupported_message-desc'] = 'Enregistrer le message non pris en charge par l\'acteur';
$locales['CSenderSFTP-sender_sftp_id'] = 'Identifiant';
$locales['CSenderSFTP-sender_sftp_id-court'] = 'Identifiant';
$locales['CSenderSFTP-sender_sftp_id-desc'] = 'Identifiant expéditeur SFTP';
$locales['CSenderSFTP-title-create'] = 'Création d\'un expéditeur SFTP';
$locales['CSenderSFTP-title-modify'] = 'Modification d\'un expéditeur SFTP';
$locales['CSenderSFTP-user_id'] = 'Utilisateur associé';
$locales['CSenderSFTP-user_id-court'] = 'Utilisateur associé';
$locales['CSenderSFTP-user_id-desc'] = 'Utilisateur associé';
$locales['CSenderSFTP-utilities'] = 'Utilitaires expéditeur SFTP';
$locales['CSenderSFTP.all'] = 'Tous les expéditeur SFTP';
$locales['CSenderSFTP.none'] = 'Aucun expéditeur SFTP';
$locales['CSenderSFTP.one'] = 'Un expéditeur SFTP';
$locales['CSenderSFTP_actions'] = 'Actions';
$locales['CSenderSFTP_configs-formats'] = 'Configs formats';
$locales['CSenderSFTP_formats-available'] = 'Formats disponibles';
$locales['CSenderSFTP_routes'] = 'Routes';
$locales['CSourceFTP'] = 'Source FTP';
$locales['CSourceFTP-_incompatible'] = 'RÏ„le de la source incompatible avec le rÏ„le de l\'instance';
$locales['CSourceFTP-_incompatible-court'] = 'RÏ„le de la source incompatible avec le rÏ„le de l\'instance';
$locales['CSourceFTP-_incompatible-desc'] = 'RÏ„le de la source incompatible avec le rÏ„le de l\'instance';
$locales['CSourceFTP-_reachable'] = 'Accessible';
$locales['CSourceFTP-_reachable-court'] = 'Accessible';
$locales['CSourceFTP-_reachable-desc'] = 'La source est-elle accessible';
$locales['CSourceFTP-_response_time'] = 'Temps de réponse';
$locales['CSourceFTP-_response_time-court'] = 'Temps de réponse';
$locales['CSourceFTP-_response_time-desc'] = 'Temps de réponse';
$locales['CSourceFTP-active'] = 'Active';
$locales['CSourceFTP-active-court'] = 'Active';
$locales['CSourceFTP-active-desc'] = 'Active';
$locales['CSourceFTP-call-undefined-method'] = 'Call to undefined method Ftp::%s()';
$locales['CSourceFTP-change-directory-failed'] = 'Impossible de changer de répertoire';
$locales['CSourceFTP-close-connexion-failed'] = 'Déconnexion impossible en tant que \'%s\'';
$locales['CSourceFTP-connexion-failed'] = 'Impossible de se connecter au serveur \'%s\'';
$locales['CSourceFTP-counter'] = 'Compteur fichier';
$locales['CSourceFTP-counter-court'] = 'Compteur fichier';
$locales['CSourceFTP-counter-desc'] = 'Compteur fichier';
$locales['CSourceFTP-delete-file-failed'] = 'Impossible de supprimer le fichier \'%s\'';
$locales['CSourceFTP-download-file-failed'] = 'Impossible de récupérer le fichier source \'%s\' en fichier cible \'%s\'';
$locales['CSourceFTP-fileextension'] = 'Extension du fichier';
$locales['CSourceFTP-fileextension-court'] = 'Extension du fichier';
$locales['CSourceFTP-fileextension-desc'] = 'Extension du fichier source FTP';
$locales['CSourceFTP-fileextension_write_end'] = 'Préfix transfert OK';
$locales['CSourceFTP-fileextension_write_end-court'] = 'Préfix transfert OK';
$locales['CSourceFTP-fileextension_write_end-desc'] = 'Préfix du fichier de transfert ok';
$locales['CSourceFTP-filenbroll'] = 'Permutation circulaire';
$locales['CSourceFTP-filenbroll-court'] = 'Permutation circulaire';
$locales['CSourceFTP-filenbroll-desc'] = 'Permutation circulaire source FTP';
$locales['CSourceFTP-fileprefix'] = 'Préfix du fichier';
$locales['CSourceFTP-fileprefix-court'] = 'Préfix du fichier';
$locales['CSourceFTP-fileprefix-desc'] = 'Préfix du fichier source FTP';
$locales['CSourceFTP-function-not-available'] = 'Fonction \'ftp_connect\' FTP non disponibles';
$locales['CSourceFTP-getlistfiles-failed'] = 'Echec de récupération de la liste de fichiers';
$locales['CSourceFTP-host'] = 'Serveur';
$locales['CSourceFTP-host-court'] = 'Serveur';
$locales['CSourceFTP-host-desc'] = 'Serveur de la source FTP';
$locales['CSourceFTP-identification-failed'] = 'Combinaison login/mot de passe incorrect pour l\'utilisateur \'%s\'';
$locales['CSourceFTP-libelle'] = 'Libellé';
$locales['CSourceFTP-libelle-desc'] = 'Libellé de la source FTP';
$locales['CSourceFTP-loggable'] = 'Loggable';
$locales['CSourceFTP-loggable-court'] = 'Loggable';
$locales['CSourceFTP-loggable-desc'] = 'Aucun log Î° produire';
$locales['CSourceFTP-manage_files'] = 'Options sur les fichiers';
$locales['CSourceFTP-mode'] = 'Mode transport';
$locales['CSourceFTP-mode-court'] = 'Mode transport';
$locales['CSourceFTP-mode-desc'] = 'Mode transport source FTP';
$locales['CSourceFTP-msg-create'] = 'Source FTP créée';
$locales['CSourceFTP-msg-delete'] = 'Source FTP supprimée';
$locales['CSourceFTP-msg-modify'] = 'Source FTP modifiée';
$locales['CSourceFTP-name'] = 'Nom';
$locales['CSourceFTP-name-court'] = 'Nom';
$locales['CSourceFTP-name-desc'] = 'Nom source FTP';
$locales['CSourceFTP-no-source'] = 'Aucune source d\'échange disponible pour ce nom : \'%s\'';
$locales['CSourceFTP-passive-mode-on-failed'] = 'Impossible d\'activer le mode passif';
$locales['CSourceFTP-password'] = 'Mot de passe';
$locales['CSourceFTP-password-court'] = 'Mot de passe';
$locales['CSourceFTP-password-desc'] = 'Mot de passe source FTP';
$locales['CSourceFTP-pasv'] = 'Mode passif';
$locales['CSourceFTP-pasv-court'] = 'Mode passif';
$locales['CSourceFTP-pasv-desc'] = 'Mode passif source FTP';
$locales['CSourceFTP-port'] = 'Port';
$locales['CSourceFTP-port-court'] = 'Port';
$locales['CSourceFTP-port-desc'] = 'Port source FTP';
$locales['CSourceFTP-reachable-source'] = 'Connexion effectuée Î° la source de donnée : \'%s\'';
$locales['CSourceFTP-rename-file-failed'] = 'ProblÎ¸me lors du renommage de \'%s\' en \'%s\'';
$locales['CSourceFTP-role'] = 'RÏ„le';
$locales['CSourceFTP-role-court'] = 'RÏ„le';
$locales['CSourceFTP-role-desc'] = 'RÏ„le de la source FTP';
$locales['CSourceFTP-socket-connection-failed'] = 'Connexion au serveur \'%s\' sur le port \'%s\' impossible : (\'%s\') \'%s\'';
$locales['CSourceFTP-source_ftp_id'] = 'Identifiant';
$locales['CSourceFTP-source_ftp_id-court'] = 'Identifiant';
$locales['CSourceFTP-source_ftp_id-desc'] = 'Identifiant source FTP';
$locales['CSourceFTP-ssl'] = 'SSL';
$locales['CSourceFTP-ssl-desc'] = 'Le serveur supporte-t-il le mode FTPS ?';
$locales['CSourceFTP-timeout'] = 'Timeout';
$locales['CSourceFTP-timeout-court'] = 'Timeout';
$locales['CSourceFTP-timeout-desc'] = 'Timeout source FTP';
$locales['CSourceFTP-title-create'] = 'Nouvelle source FTP';
$locales['CSourceFTP-title-modify'] = 'Modifier la source FTP';
$locales['CSourceFTP-type_echange'] = 'Type échange';
$locales['CSourceFTP-type_echange-court'] = 'Type échange';
$locales['CSourceFTP-type_echange-desc'] = 'Type échange (hprimxml, hprim21, ...)';
$locales['CSourceFTP-upload-file-failed'] = 'Impossible de copier le fichier source \'%s\' en fichier cible \'%s\'';
$locales['CSourceFTP-user'] = 'Utilisateur';
$locales['CSourceFTP-user-court'] = 'Utilisateur';
$locales['CSourceFTP-user-desc'] = 'Utilisateur source FTP';
$locales['CSourceFTP._reachable.'] = 'Accessible';
$locales['CSourceFTP._reachable.0'] = 'Non';
$locales['CSourceFTP._reachable.1'] = 'Partiellement';
$locales['CSourceFTP._reachable.2'] = 'Oui';
$locales['CSourceFTP.all'] = 'Toutes les sources FTP';
$locales['CSourceFTP.filenbroll.'] = 'Permutation circulaire';
$locales['CSourceFTP.filenbroll.1'] = '1';
$locales['CSourceFTP.filenbroll.2'] = '2';
$locales['CSourceFTP.filenbroll.3'] = '3';
$locales['CSourceFTP.filenbroll.4'] = '4';
$locales['CSourceFTP.mode.'] = 'Mode transport';
$locales['CSourceFTP.mode.FTP_ASCII'] = 'FTP_ASCII';
$locales['CSourceFTP.mode.FTP_BINARY'] = 'FTP_BINARY';
$locales['CSourceFTP.none'] = 'Pas de source FTP';
$locales['CSourceFTP.one'] = 'Une source FTP';
$locales['CSourceFTP.role.prod'] = 'Production';
$locales['CSourceFTP.role.qualif'] = 'Qualification';
$locales['CSourceSFTP'] = 'Source SFTP';
$locales['CSourceSFTP-_incompatible'] = 'RÏ„le de la source incompatible avec le rÏ„le de l\'instance';
$locales['CSourceSFTP-_incompatible-court'] = 'RÏ„le de la source incompatible avec le rÏ„le de l\'instance';
$locales['CSourceSFTP-_incompatible-desc'] = 'RÏ„le de la source incompatible avec le rÏ„le de l\'instance';
$locales['CSourceSFTP-_reachable'] = 'Accessible';
$locales['CSourceSFTP-_reachable-court'] = 'Accessible';
$locales['CSourceSFTP-_reachable-desc'] = 'La source est-elle accessible';
$locales['CSourceSFTP-_response_time'] = 'Temps de réponse';
$locales['CSourceSFTP-_response_time-court'] = 'Temps de réponse';
$locales['CSourceSFTP-_response_time-desc'] = 'Temps de réponse';
$locales['CSourceSFTP-active'] = 'Active';
$locales['CSourceSFTP-active-court'] = 'Active';
$locales['CSourceSFTP-active-desc'] = 'Active';
$locales['CSourceSFTP-fileextension'] = 'Extension';
$locales['CSourceSFTP-fileextension-desc'] = 'Extension des fichiers';
$locales['CSourceSFTP-fileextension_write_end'] = 'Extension de fin';
$locales['CSourceSFTP-fileextension_write_end-desc'] = 'Extension de fin';
$locales['CSourceSFTP-fileprefix'] = 'Répertoire';
$locales['CSourceSFTP-fileprefix-desc'] = 'Répertoire des fichiers';
$locales['CSourceSFTP-host'] = 'Serveur';
$locales['CSourceSFTP-host-court'] = 'Serveur';
$locales['CSourceSFTP-host-desc'] = 'Serveur';
$locales['CSourceSFTP-libelle'] = 'Libellé';
$locales['CSourceSFTP-libelle-desc'] = 'Libellé de la source SFTP';
$locales['CSourceSFTP-loggable'] = 'Loggable';
$locales['CSourceSFTP-loggable-desc'] = 'Aucun log Î° produire';
$locales['CSourceSFTP-msg-create'] = 'Source SFTP créée';
$locales['CSourceSFTP-msg-delete'] = 'Source SFTP supprimée';
$locales['CSourceSFTP-msg-modify'] = 'Source SFTP modifiée';
$locales['CSourceSFTP-name'] = 'Nom';
$locales['CSourceSFTP-name-court'] = 'Nom';
$locales['CSourceSFTP-name-desc'] = 'Nom';
$locales['CSourceSFTP-password'] = 'Mot de passe';
$locales['CSourceSFTP-password-court'] = 'Mot de passe';
$locales['CSourceSFTP-password-desc'] = 'Mot de passe';
$locales['CSourceSFTP-port'] = 'Port';
$locales['CSourceSFTP-port-court'] = 'Port';
$locales['CSourceSFTP-port-desc'] = 'Port';
$locales['CSourceSFTP-role'] = 'RÏ„le';
$locales['CSourceSFTP-role-court'] = 'RÏ„le';
$locales['CSourceSFTP-role-desc'] = 'RÏ„le';
$locales['CSourceSFTP-source_sftp_id'] = 'Identifiant';
$locales['CSourceSFTP-source_sftp_id-court'] = 'Identifiant';
$locales['CSourceSFTP-source_sftp_id-desc'] = 'Identifiant';
$locales['CSourceSFTP-timeout'] = 'Timeout';
$locales['CSourceSFTP-timeout-court'] = 'Timeout';
$locales['CSourceSFTP-timeout-desc'] = 'Timeout';
$locales['CSourceSFTP-title-create'] = 'Nouvelle source SFTP';
$locales['CSourceSFTP-title-modify'] = 'Modification d\'une source SFTP';
$locales['CSourceSFTP-type_echange'] = 'Type échange';
$locales['CSourceSFTP-type_echange-court'] = 'Type échange';
$locales['CSourceSFTP-type_echange-desc'] = 'Type échange';
$locales['CSourceSFTP-user'] = 'Utilisateur';
$locales['CSourceSFTP-user-court'] = 'Utilisateur';
$locales['CSourceSFTP-user-desc'] = 'Utilisateur';
$locales['CSourceSFTP._reachable.'] = 'Accessible';
$locales['CSourceSFTP._reachable.0'] = 'Non';
$locales['CSourceSFTP._reachable.1'] = 'Partiellement';
$locales['CSourceSFTP._reachable.2'] = 'Oui';
$locales['CSourceSFTP.all'] = 'Toutes les sources SFTP';
$locales['CSourceSFTP.none'] = 'Pas de source SFTP';
$locales['CSourceSFTP.one'] = 'Une source SFTP';
$locales['CSourceSFTP.role.prod'] = 'Production';
$locales['CSourceSFTP.role.qualif'] = 'Qualification';
$locales['Error-file'] = 'Impossible de récupérer les fichiers';
$locales['List-of-file'] = 'Liste des fichiers';
$locales['No-file'] = 'Aucun fichier';
$locales['config-ftp'] = 'Couche de transport FTP';
$locales['config-ftp-ftphost'] = 'HÏ„te';
$locales['config-ftp-ftppass'] = 'Mot de passe';
$locales['config-ftp-ftpuser'] = 'Utilisateur';
$locales['config-ftp-mode'] = 'Mode de transport';
$locales['config-ftp-mode-FTP_ASCII'] = 'FTP_ASCII';
$locales['config-ftp-mode-FTP_BINARY'] = 'FTP_BINARY';
$locales['config-ftp-pasv'] = 'Mode passif';
$locales['config-ftp-pasv-0'] = 'Actif';
$locales['config-ftp-pasv-1'] = 'Passif';
$locales['config-ftp-port'] = 'Port';
$locales['config-ftp-purge-echange'] = 'Purge échanges';
$locales['config-ftp-timeout'] = 'Timeout';
$locales['config-read-files-senders'] = 'Lire les fichiers des expéditeurs';
$locales['config-source-http'] = 'Configuration source HTTP';
$locales['mod-eai-tab-ajax_show_file_FTP'] = 'Affichage du contenu';
$locales['mod-ftp-tab-ajax_connexion_ftp'] = 'Connexion FTP';
$locales['mod-ftp-tab-ajax_dispatch_files'] = 'Envoi des fichiers';
$locales['mod-ftp-tab-ajax_getFiles_ftp'] = 'Récupération des fichiers ';
$locales['mod-ftp-tab-ajax_purge_echange'] = 'Purge échanges FTP';
$locales['mod-ftp-tab-ajax_read_ftp_files'] = 'Lecture fichiers FTP';
$locales['mod-ftp-tab-configure'] = 'Configurer';
$locales['mod-ftp-tab-vw_idx_exchange_ftp'] = 'Echanges FTP';
$locales['utilities-source-ftp-manageFiles'] = 'Gestion des fichiers';
